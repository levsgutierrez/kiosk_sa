<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Sys_ModuleActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('sys_modules_actions')->insert([
    	[
            'moduleCode'    =>  '110',
            'actionName'    =>  'AccessUser',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'110',
            'actionName'    => 	'CreateUser',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
		[
            'moduleCode'    => 	'110',
            'actionName'    => 	'EditUser',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
		[
            'moduleCode'    =>  '110',
            'actionName'    =>  'DeleteUser',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'120',
            'actionName'    => 	'AccessOutletKiosk',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '120',
            'actionName'    =>  'AddOutletKiosk',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '120',
            'actionName'    =>  'EditOutletKiosk',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '120',
            'actionName'    =>  'DeleteOutletKiosk',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '130',
            'actionName'    =>  'AccessDevice',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '140',
            'actionName'    =>  'AccessKioskManagement',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '140',
            'actionName'    =>  'AddKiosk',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '140',
            'actionName'    =>  'EditKiosk',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '140',
            'actionName'    =>  'DeleteKiosk',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '150',
            'actionName'    =>  'AccessActivityLogs',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '210',
            'actionName'    =>  'AccessMenu',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
		[
            'moduleCode'    => 	'210',
            'actionName'    => 	'AddItem',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'210',
            'actionName'    => 	'EditItem',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'210',
            'actionName'    => 	'DuplicateItem',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'210',
            'actionName'    => 	'DeleteItem',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '220',
            'actionName'    =>  'AccessMenuCategories',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'220',
            'actionName'    => 	'AddCategories',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'220',
            'actionName'    => 	'EditCategories',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'220',
            'actionName'    => 	'DeleteCategories',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'220',
            'actionName'    => 	'AddSubCategories',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'220',
            'actionName'    => 	'EditSubCategories',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'220',
            'actionName'    => 	'DeleteSubCategories',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '230',
            'actionName'    =>  'AccessItems',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'230',
            'actionName'    => 	'AddItems',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'230',
            'actionName'    => 	'DeleteItems',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
		[
            'moduleCode'    =>  '410',
            'actionName'    =>  'AccessMembers',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'410',
            'actionName'    => 	'AddMembers',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'410',
            'actionName'    => 	'EditMembers',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'410',
            'actionName'    => 	'DeleteMembers',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'410',
            'actionName'    => 	'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '420',
            'actionName'    =>  'AccessMembership',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'420',
            'actionName'    => 	'AddMembership',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'420',
            'actionName'    => 	'EditMembership',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'420',
            'actionName'    => 	'DeleteMembership',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '430',
            'actionName'    =>  'AccessTopup',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'430',
            'actionName'    => 	'TopupMembers',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'430',
            'actionName'    => 	'ReprintReceipt',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'430',
            'actionName'    => 	'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '440',
            'actionName'    =>  'AccessDepositStatistics',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'440',
            'actionName'    => 	'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '450',
            'actionName'    =>  'AccessExpenditure',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'450',
            'actionName'    => 	'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'460',
            'actionName'    => 	'AccessMembersStatistics',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '460',
            'actionName'    =>  'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    => 	'510',
            'actionName'    => 	'AccessInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '510',
            'actionName'    =>  'ChangeInventoryStatus',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '520',
            'actionName'    =>  'AccessIncomingInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '520',
            'actionName'    =>  'AddIncomingInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '520',
            'actionName'    =>  'DeleteIncomingInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '520',
            'actionName'    =>  'ReprintReceipt',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '520',
            'actionName'    =>  'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '530',
            'actionName'    =>  'AccessOutgoingInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '530',
            'actionName'    =>  'AddOutgoingInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '530',
            'actionName'    =>  'ClearInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '530',
            'actionName'    =>  'ReprintInventory',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '530',
            'actionName'    =>  'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '540',
            'actionName'    =>  'AccessInventoryInquiry',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '540',
            'actionName'    =>  'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '550',
            'actionName'    =>  'AccessInventoryLedger',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        [
            'moduleCode'    =>  '550',
            'actionName'    =>  'AllowExport',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
        ]);
    }
}
