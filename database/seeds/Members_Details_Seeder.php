<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Members_Details_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('members_details')->insert([
      [
        'accountNumber'     => '1111-1111-1111-1111',
        'accountCardNumber' => '2222-2222-2222-2222',
        'membershipTypes'   => '1',
        'membersName'       => 'Heimun',
        'nric'              => 'S9876543E',
        'contactNumber'     => '9895-2898',
        'registeredDate'    => Carbon::now(),
        'membersPoints'     => '0',
        'accountBalance'    => '0.00',
        'status'            => '1',
        'deposit'           => '50',
        'deleted'           => '0',
        'created_at'        =>  Carbon::now(),
        'updated_at'        =>  Carbon::now(),
      ],[
        'accountNumber'     => '3333-3333-3333-3333',
        'accountCardNumber' => '4444-4444-4444-4444',
        'membershipTypes'   => '2',
        'membersName'       => 'Lev',
        'nric'              => 'S9632587P',
        'contactNumber'     => '9596-7422',
        'registeredDate'    => Carbon::now(),
        'membersPoints'     => '0',
        'accountBalance'    => '0.00',
        'status'            => '1',
        'deposit'           => '50',
        'deleted'           => '0',
        'created_at'        =>  Carbon::now(),
        'updated_at'        =>  Carbon::now(),
        ]
      ]);

      DB::table('deposits')->insert([
      [
        'depositType'       => 'deposit',
        'accountNumber'     => '1111-1111-1111-1111',
        'contactNumber'     => '9895-2898',
        'amount'            => '50',
        'deleted'           => '0',
        'created_at'        =>  Carbon::now(),
        'updated_at'        =>  Carbon::now(),
      ],[
        'depositType'       => 'deposit',
        'accountNumber'     => '3333-3333-3333-3333',
        'contactNumber'     => '9596-7422',
        'amount'            => '50',
        'deleted'           => '0',
        'created_at'        =>  Carbon::now(),
        'updated_at'        =>  Carbon::now(),
        ]
      ]);
    }
}
