<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class General_Settings_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('general_settings')->insert([
        [
          'settingsName'  => 'defaultDepositAmount',
          'valueDouble'   =>  '50',
          'created_at'    =>  Carbon::now(),
          'updated_at'    =>  Carbon::now(),
        ]
      ]);
    }
}
