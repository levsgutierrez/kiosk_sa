<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class LevAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_user_rights')->insert([
        [
           'userID'      => 'Lev',
           'moduleCode'  => '110',
           'actionName'  => 'CreateUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ],
        [
           'userID'      => 'Lev',
           'moduleCode'  => '110',
           'actionName'  => 'EditUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ],
        [
           'userID'      => 'Lev',
           'moduleCode'  => '110',
           'actionName'  => 'DeleteUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ],
        [
           'userID'      => 'Lev',
           'moduleCode'  => '110',
           'actionName'  => 'AccessUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ]
        ]);

        DB::table('sys_user_rights')->insert([
        [
           'userID'      => 'Administrator',
           'moduleCode'  => '110',
           'actionName'  => 'CreateUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ],
       	[
           'userID'      => 'Administrator',
           'moduleCode'  => '110',
           'actionName'  => 'EditUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ],
       	[
           'userID'      => 'Administrator',
           'moduleCode'  => '110',
           'actionName'  => 'DeleteUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ],
       	[
           'userID'      => 'Administrator',
           'moduleCode'  => '110',
           'actionName'  => 'AccessUser',
           'active'      => '1',
           'created_at'  =>  Carbon::now(),
           'updated_at'  =>  Carbon::now(),
        ]
       	]);
    }
}
