<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Sys_FileNo_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys__file_nos')->insert([
    	[
            'FileType'      => 'MenuID',
            'FileNo'        => 'M003',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
    			'FileType'    	=> 'SideID',
    			'FileNo'  		=> 'S000',
    			'created_at'    =>  Carbon::now(),
    			'updated_at'    =>  Carbon::now(),
      	],[
          'FileType'    => 'TopupId',
          'FileNo'      => 'T0000',
          'created_at'  =>  Carbon::now(),
          'updated_at'  =>  Carbon::now(),
        ],[
    			'FileType'    => 'CategoryID',
    			'FileNo'  		=> 'C007',
    			'created_at'  =>  Carbon::now(),
    			'updated_at'  =>  Carbon::now(),
        ],[
    			'FileType'    => 'DepositId',
    			'FileNo'  		=> 'D0000',
    			'created_at'  =>  Carbon::now(),
    			'updated_at'  =>  Carbon::now(),
        ],[
    			'FileType'    => 'InventoryId',
    			'FileNo'  		=> 'IO0000',
    			'created_at'  =>  Carbon::now(),
    			'updated_at'  =>  Carbon::now(),
        ]

    ]);
    }
}
