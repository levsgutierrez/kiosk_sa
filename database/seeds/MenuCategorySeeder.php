<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MenuCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_categories')->insert([
    	[
			
			'CategoryID'  => 'C001',
			'CategoryName'  => 'Main Dish',
			'SeqNo'  	=> '1',
			'Flag'   	=> '0',
			'Status'   	=> '0',
			'CreatedBy'   	=> 'Lev',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],
    	[
			'CategoryID'  => 'C002',
			'CategoryName'  => 'Pasta',
			'SeqNo'  	=> '2',
			'Flag'   	=> '0',
			'Status'   	=> '0',
			'CreatedBy'   	=> 'Lev',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],
    	[
    		'CategoryID'  => 'C003',
			'CategoryName'  => 'Drinks and Beverages',
			'SeqNo'  	=> '3',
			'Flag'   	=> '0',
			'Status'   	=> '0',
			'CreatedBy'   	=> 'Lev',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],
    	]);

    	DB::table('menu_categories')->insert([
    	[
    		'CategoryID'  => 'C004',
			'CategoryName'  => 'Pork',
			'SeqNo'  	=> '1',
			'Flag'   	=> '6',
			'Status'   	=> '0',
			'CreatedBy'   	=> 'Lev',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],
    	[
    		'CategoryID'  => 'C005',
			'CategoryName'  => 'Chicken',
			'SeqNo'  	=> '2',
			'Flag'   	=> '6',
			'Status'   	=> '0',
			'CreatedBy'   	=> 'Lev',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],
    	[
    		'CategoryID'  => 'C006',
			'CategoryName'  => 'Spaghetti',
			'SeqNo'  	=> '3',
			'Flag'   	=> '6',
			'Status'   	=> '0',
			'CreatedBy'   	=> 'Lev',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],
    	]);
    }
}
