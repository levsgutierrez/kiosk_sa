<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Topup_Account_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('top_up_accounts')->insert([
      [
        'topupId'           => 'T001',
        'accountCardNumber' => '5555-5555-5555-5555',
        'initialBalance'    => '0',
        'topupAmount'       => '50',
        'giftAmount'        => '0',
        'totalAmount'       => '50',
        'memberPoints'     => '0',
        'remarks'           => null,
        'operator'          => null,
        'topupDate'         => '2018-02-09 02:55:41.000',
        'shopName'          => null,
        'status'            => '1',
        'invoiceAmount'     => '0',
        'paymentType'       => 'Cash',
        'outletId'          => null,
        'deleted'           => '0',
        'created_at'        =>  Carbon::now(),
        'updated_at'        =>  Carbon::now(),
      ]
    ]);
  }
}
