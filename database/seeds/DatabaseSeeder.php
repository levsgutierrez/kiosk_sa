<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Sys_FileNo_seeder::class);
        $this->call(Membership_Types::class);
        $this->call(Members_Details_Seeder::class);
        $this->call(General_Settings_Seeder::class);
        $this->call(Topup_Accounts_Seeder::class);
        //$this->call(mintCompaniesSeeder::class);
        //$this->call(MenuCategorySeeder::class);
        $this->call(MenuItemSeeder::class);
        $this->call(Sys_ModuleSeeder::class);
        $this->call(Sys_ModuleActionSeeder::class);
    }
}
