<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Sys_ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('sys_modules')->insert([
    	[
            'moduleCode'    => 	'100',
            'moduleName'    => 	'Settings Management',
            'parentCode'	=>	'0',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
            'moduleCode'    => 	'110',
            'moduleName'    => 	'User Management',
            'parentCode'	=>	'100',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
            'moduleCode'    => 	'120',
            'moduleName'    => 	'Kiosks Management',
            'parentCode'	=>	'100',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'130',
            'moduleName'    => 	'Device Settings',
            'parentCode'	=>	'100',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'140',
            'moduleName'    => 	'Kiosk Management',
            'parentCode'	=>	'100',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    =>   '150',
            'moduleName'    =>  'System Update',
            'parentCode'    =>  '100',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
            'moduleCode'    => 	'160',
            'moduleName'    => 	'Activity Log',
            'parentCode'	=>	'100',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'200',
            'moduleName'    => 	'Menu Management',
            'parentCode'	=>	'0',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'210',
            'moduleName'    => 	'Menu Items',
            'parentCode'	=>	'200',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'220',
            'moduleName'    => 	'Menu Categories',
            'parentCode'	=>	'200',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'230',
            'moduleName'    => 	'Menu Item Display',
            'parentCode'	=>	'200',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'300',
            'moduleName'    => 	'Sales Management',
            'parentCode'	=>	'0',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'310',
            'moduleName'    => 	'Receipt History',
            'parentCode'	=>	'300',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'320',
            'moduleName'    => 	'Discount Settings',
            'parentCode'	=>	'300',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'330',
            'moduleName'    => 	'Cash Out',
            'parentCode'	=>	'300',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'340',
            'moduleName'    => 	'Kiosk History',
            'parentCode'	=>	'300',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'350',
            'moduleName'    => 	'Order History',
            'parentCode'	=>	'300',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'400',
            'moduleName'    => 	'Membership Accounts',
            'parentCode'	=>	'0',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'410',
            'moduleName'    => 	'Membership Details',
            'parentCode'	=>	'400',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'420',
            'moduleName'    => 	'Membership Types',
            'parentCode'	=>	'400',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'430',
            'moduleName'    => 	'Topup Accounts',
            'parentCode'	=>	'400',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'440',
            'moduleName'    => 	'Deposit Statistics',
            'parentCode'	=>	'400',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'450',
            'moduleName'    => 	'Expenditure List',
            'parentCode'	=>	'400',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'460',
            'moduleName'    => 	'Members Statistics',
            'parentCode'	=>	'400',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'500',
            'moduleName'    => 	'Inventory Management',
            'parentCode'	=>	'0',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'510',
            'moduleName'    => 	'Inventory',
            'parentCode'	=>	'500',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'520',
            'moduleName'    => 	'Incoming Inventory',
            'parentCode'	=>	'500',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'530',
            'moduleName'    => 	'Outgoing Inventory',
            'parentCode'	=>	'500',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'540',
            'moduleName'    => 	'Inventory Inquiry',
            'parentCode'	=>	'500',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'550',
            'moduleName'    => 	'Inventory Ledger',
            'parentCode'	=>	'500',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'600',
            'moduleName'    => 	'Sales Report',
            'parentCode'	=>	'0',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'610',
            'moduleName'    => 	'Daily Sales Statistics',
            'parentCode'	=>	'600',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'620',
            'moduleName'    => 	'Item Sales Statistics',
            'parentCode'	=>	'600',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
        	'moduleCode'    => 	'630',
            'moduleName'    => 	'Invoice Statistics',
            'parentCode'	=>	'600',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
			'moduleCode'    => 	'640',
            'moduleName'    => 	'OrderInvoice Statistics',
            'parentCode'	=>	'600',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],[
			'moduleCode'    => 	'650',
            'moduleName'    => 	'Scheduling Statistics',
            'parentCode'	=>	'600',
            'created_at'    =>  Carbon::now(),
            'updated_at'    =>  Carbon::now(),
        ],
    ]);
    }
}
