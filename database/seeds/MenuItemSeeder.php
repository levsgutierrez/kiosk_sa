<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MenuItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
    	[
			'SKU'      	=> 'M001',
			'ItemName'  => 'Ayataka Green Tea',
			'Desc'  	=> 'So Refreshing',
			'Unit'   	=> 'each',
			'Price'   	=> '12',
			'ImgNo'   	=> ' ',
			'CategoryID'   	=> 'no_data',
			'SubCategoryID'   	=> 'no_data',
			'PrinterName'   => 'no_data',
			'CollectPlace'  => null,
			'KitchenDesc'   => null,
			'SafetyStock'   => null,
			'Status'   		=> null,
			'OutletID'   	=> null,
			'CanTakeOut'   	=> 'on',
			'PackPrice'   	=> '0.5',
			'Discount'   	=> null,
			'Department'   	=> null,
			'deleted'   	=> 0,
			'BriefCode'   	=> null,
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],[
        	'SKU'      	=> 'M002',
			'ItemName'  => 'Breakfast',
			'Desc'  	=> 'So Refreshing',
			'Unit'   	=> 'each',
			'Price'   	=> '13',
			'ImgNo'   	=> '',
			'CategoryID'   	=> 'no_data',
			'SubCategoryID'   	=> 'no_data',
			'PrinterName'   => 'no_data',
			'CollectPlace'  => null,
			'KitchenDesc'   => null,
			'SafetyStock'   => null,
			'Status'   		=> null,
			'OutletID'   	=> null,
			'CanTakeOut'   	=> 'on',
			'PackPrice'   	=> '0.2',
			'Discount'   	=> null,
			'Department'   	=> null,
			'deleted'   	=> 0,
			'BriefCode'   	=> null,
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],[
			'SKU'      	=> 'M003',
			'ItemName'  => 'Burgers',
			'Desc'  	=> 'So Refreshing',
			'Unit'   	=> 'each',
			'Price'   	=> '12',
			'ImgNo'   	=> '',
			'CategoryID'   	=> 'no_data',
			'SubCategoryID'   	=> 'no_data',
			'PrinterName'   => 'no_data',
			'CollectPlace'  => null,
			'KitchenDesc'   => null,
			'SafetyStock'   => null,
			'Status'   		=> null,
			'OutletID'   	=> null,
			'CanTakeOut'   	=> 'on',
			'PackPrice'   	=> '0.2',
			'Discount'   	=> null,
			'Department'   	=> null,
			'deleted'   	=> 0,
			'BriefCode'   	=> null,
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	]

    	]);
    }
}
