<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\mintCompany;

class mintCompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         mintCompany::on('mint_companies')->insert([
    	[
			'name'      => 'Lev',
			'email'   	=> "lev@m-int.com.sg",
			'password'  => Hash::make('password'),
			'company'   => 'mint_Mcdo',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],[
        	'name'      => 'Heimun',
			'email'   	=> "heimun@m-int.com.sg",
			'password'  => Hash::make('password'),
			'company'   => 'sqlsrv_cloud',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],[
        	'name'      => 'Administrator',
			'email'   	=> "admin@m-int.com.sg",
			'password'  => Hash::make('password'),
			'company'   => 'sqlsrv_cloud',
			'created_at'=>  Carbon::now(),
			'updated_at'=>  Carbon::now(),
    	],

    	]);
    }
}
