<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Topup_Accounts_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       DB::table('top_up_accounts')->insert([
         [
           'topupId'            => 'T000',
           'accountCardNumber'  => '2222-2222-2222-2222',
           'initialBalance'     => '0.00',
           'topupAmount'        => '50.00',
           'giftAmount'         => '0.00',
           'totalAmount'        => '50.00',
           'topupDate'          => Carbon::now(),
           'status'             => '0',
           'paymentType'        => 'Cash',
           'invoiceAmount'      => '0',
           'created_at'         =>  Carbon::now(),
           'updated_at'         =>  Carbon::now(),
         ]
       ]);
     }
}
