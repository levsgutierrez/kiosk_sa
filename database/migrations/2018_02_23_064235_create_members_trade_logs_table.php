<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTradeLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members_trade_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('accountCardNumber');
            $table->string('actionType')->nullable();
            $table->string('actionTypeId')->nullable();
            $table->decimal('amount',15,2)->default(0.00);
            $table->string('deleted')->default(0);
            $table->string('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members_trade_logs');
    }
}
