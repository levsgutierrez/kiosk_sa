<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_ts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orderNo');
            $table->integer('transactionId');
            $table->string('SKU');
            $table->string('quantity');
            $table->decimal('price',15,2)->nullable();
            $table->decimal('oldPrice',15,2)->nullable();
            $table->decimal('addedPrice',15,2)->nullable();
            $table->decimal('discount',15,2)->nullable();
            $table->string('remarks')->nullable();
            $table->string('status')->nullable();
            $table->string('shopName')->nullable();
            $table->dateTime('transactionDate')->nullable();
            // $table->dateTime('weight')->nullable();
            $table->decimal('packingPrice',15,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_ts');
    }
}
