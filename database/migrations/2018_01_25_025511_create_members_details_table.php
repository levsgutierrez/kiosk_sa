<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('accountNumber');
            $table->string('accountCardNumber');
            $table->integer('membershipTypes');
            $table->string('membersName');
            $table->string('nric', 9);
            $table->string('contactNumber');
            $table->dateTime('registeredDate');
            $table->integer('membersPoints')->default(0);
            $table->decimal('accountBalance')->default(0.00);
            $table->boolean('status')->default(1);
            $table->integer('outletId')->nullable();
            $table->decimal('deposit')->default(0.00);
            $table->dateTime('cancellationDate')->nullable();
            $table->decimal('defaultDepositAmount')->default(0);
            $table->string('deleted')->default(0);
            $table->string('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members_details');
    }
}
