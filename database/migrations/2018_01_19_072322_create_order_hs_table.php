<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_hs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orderNo');
            $table->dateTime('orderDate');
            $table->string('tableNo')->nullable();
            $table->string('tablePax')->nullable();
            $table->string('remarks')->nullable();
            $table->string('accountNumber');
            $table->decimal('totalAmount',15,2)->nullable();
            $table->decimal('discount',15,2)->nullable();
            $table->decimal('amountReceived',15,2)->nullable();
            $table->string('paymentType');
            $table->string('otherPayments')->nullable();
            $table->decimal('amountChange',15,2)->nullable();
            $table->string('status')->default(0);
            $table->string('deleted')->default(0);
            $table->string('orderType')->nullable();
            $table->string('shopName')->nullable();
            $table->string('outletId')->nullable();
            $table->string('oldOrderNo')->nullable();
            $table->string('fileType')->nullable();
            $table->string('kioskNumber')->nullable();
            $table->string('paymentNumber')->nullable();
            $table->string('createdBy')->nullable();
            $table->decimal('invoiceAmount',15,2);
            $table->timestamps();

            // $table->string('Addition1');
            // $table->string('Addition2');
            // $table->decimal('CouponValue',15,2);
            // $table->decimal('Mantissa',15,2);
            // $table->string('OtherPayType',15,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_hs');
    }
}
