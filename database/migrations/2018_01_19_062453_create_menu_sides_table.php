<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuSidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_sides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SideID');
            $table->string('SideName');
            $table->string('SideMinQty');
            $table->string('SideMaxQty');
            $table->string('SerialCode');
            $table->string('SKU');
            $table->decimal('SidePrice',15,2);
            $table->string('ItemName');
            $table->integer('MaxQty');
            $table->string('SeqID');
            $table->string('Flag')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_sides');
    }
}
