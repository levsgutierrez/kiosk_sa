<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuAddOnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_add_ons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SKU');
            $table->string('AddOns');
            $table->string('ItemName');
            $table->decimal('Price',15,2);
            $table->string('Flag')->default('0');
            $table->string('CreatedBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_add_ons');
    }
}
