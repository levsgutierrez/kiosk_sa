<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitchensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kitchens', function (Blueprint $table) {
            $table->integer('id')->increments();
            $table->string('orderNo');
            $table->string('transactionType');
            $table->string('tableNumber');
            $table->string('SKU',10);
            $table->string('itemName');
            $table->string('remarks',50);
            $table->integer('quantity');
            $table->string('actionType')->nullable();
            $table->dateTime('foodCollectionTime')->nullable();
            $table->dateTime('mealReadyTime')->nullable();
            $table->string('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kitchens');
    }
}
