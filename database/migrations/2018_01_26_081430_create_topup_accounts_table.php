<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopupAccountsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('top_up_accounts', function (Blueprint $table) {
          $table->increments('id');
          $table->string('topupId');
          $table->string('accountCardNumber');
          $table->decimal('initialBalance', 15,2);
          $table->decimal('topupAmount', 15,2)->default(0);
          $table->decimal('giftAmount', 15,2)->default(0);
          $table->decimal('totalAmount', 15,2);
          $table->integer('memberPoints')->default(0);
          $table->string('remarks')->nullable();
          $table->dateTime('topupDate');
          $table->string('shopName')->nullable();
          $table->boolean('status')->default(0);
          $table->string('paymentType');
          $table->string('createdBy')->nullable();
          $table->decimal('invoiceAmount',15,2)->default(0);
          $table->integer('outletId')->nullable();
          $table->string('deleted')->default(0);
          $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('top_up_accounts');
  }
}
