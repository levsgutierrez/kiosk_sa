<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('itemCode');
            $table->string('itemName');
            $table->boolean('isMenu')->default(0);
            $table->decimal('price', 15,2);
            $table->string('quantity');
            $table->string('limitQuantity');
            $table->string('changeTime')->nullable();
            $table->string('remarks')->nullable();
            $table->string('shopName')->nullable();
            $table->integer('outletId')->nullable();
            $table->boolean('status')->default(0);
            $table->string('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_stocks');
    }
}
