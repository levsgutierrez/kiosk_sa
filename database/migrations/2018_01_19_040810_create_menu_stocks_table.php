<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ShopName',50);
            $table->string('GCode',50);
            $table->string('GName',50);
            $table->string('Unit',15);
            $table->boolean('IsMenu',15);
            $table->decimal('Price',15,2);
            $table->decimal('Qty',15,2);
            $table->decimal('LimitQty',15,2);
            $table->string('Remarks');
            $table->boolean('Status');

            $table->string('CreatedBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_stocks');
    }
}
