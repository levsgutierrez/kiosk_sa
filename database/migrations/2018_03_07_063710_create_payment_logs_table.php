<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paymentLogId');
            $table->string('paymentLogType');
            $table->string('orderNo')->nullable();
            $table->string('denominations');
            $table->string('quantity');
            $table->integer('totalAmount');
            $table->string('kioskNo')->nullable();
            $table->string('machineNo')->nullable();
            $table->string('deleted')->default(0);
            $table->string('operator')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_logs');
    }
}
