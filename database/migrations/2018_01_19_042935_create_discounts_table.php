<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('discountName',50);
            $table->string('discountType',50);
            $table->string('SKU')->nullable();
            $table->string('categoryId')->nullable();
            $table->decimal('minimumAmount',15,2)->nullable();

            // $table->string('discountMethod',10)->nullable();
            $table->decimal('discountAmount',15,2)->nullable();
            $table->decimal('discountPercentage',15,2)->nullable();

            // $table->string('discountEffectivePeriods',10)->nullable();
            $table->date('beginDate')->nullable();
            $table->date('endDate')->nullable();
            $table->string('week')->nullable();
            $table->string('beginTime')->nullable();
            $table->string('endTime')->nullable();

            $table->integer('priority')->nullable();
            $table->integer('status')->default(1);
            $table->string('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
