<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SKU');
            $table->string('ItemName',50);
            $table->string('Desc')->nullable();
            $table->string('Unit');
            $table->decimal('Price',15,2);
            $table->string('ImgNo')->nullable();
            $table->string('CategoryID')->nullable();
            $table->string('SubCategoryID')->nullable();
            $table->string('PrinterName')->nullable();
            $table->string('CollectPlace')->nullable();
            $table->string('KitchenDesc')->nullable();
            $table->string('SafetyStock')->nullable();
            $table->string('Status')->nullable();
            $table->string('OutletID')->nullable();
            $table->string('CanTakeOut')->nullable();
            $table->decimal('PackPrice',15,2)->nullable();
            $table->string('Discount')->nullable();
            $table->string('Department')->nullable();
            $table->string('deleted')->default(0);
            $table->string('BriefCode')->nullable();
            $table->string('CreatedBy')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
