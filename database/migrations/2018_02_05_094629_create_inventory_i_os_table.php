<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryIOsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_i_os', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inventoryId');
            $table->integer('inventoryType');
            $table->string('actionName')->nullable();
            $table->string('itemCode');
            $table->string('itemName');
            $table->decimal('price', 15,2)->nullable();
            $table->boolean('isMenu')->default(0);
            $table->integer('initialQuantity')->nullable();
            $table->integer('totalQuantity')->nullable();
            $table->string('operator')->nullable();
            $table->string('remarks')->nullable();
            $table->string('shopName')->nullable();
            $table->integer('outletId')->nullable();
            $table->string('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_i_os');
    }
}
