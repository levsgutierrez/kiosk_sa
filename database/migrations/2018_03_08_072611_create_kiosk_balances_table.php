<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKioskBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosk_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kioskBalanceId');
            $table->string('orderCount');
            $table->string('amount');
            $table->string('remarks');
            $table->string('deleted');
            $table->string('notes');
            $table->string('coins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosk_balances');
    }
}
