<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTransactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_transacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('OrderNo',10);
            $table->string('TxnID',10);
            $table->string('SKU');
            $table->string('Qty');
            $table->decimal('Price',15,2);
            $table->decimal('OldPrice',15,2);
            $table->decimal('AddedPrice',15,2);
            $table->decimal('Discount',15,2);
            $table->integer('State');
            $table->string('ShopName');
            $table->date('TxnDate');
            $table->decimal('Weight',15,2);
            $table->decimal('PackingPrice',15,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_transacts');
    }
}
