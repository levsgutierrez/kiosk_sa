<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_ds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orderNo');
            $table->string('SKU');
            $table->string('transactionId');
            $table->string('sidesId')->nullable();
            $table->string('sidesName')->nullable();
            $table->string('groupName')->nullable();
            $table->string('Quantity');
            $table->decimal('price',15,2);
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_ds');
    }
}
