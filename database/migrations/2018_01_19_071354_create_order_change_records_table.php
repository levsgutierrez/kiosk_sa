<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderChangeRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_change_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('OrderNo',10);
            $table->string('TxnID',10);
            $table->string('SKU',10);
            $table->integer('Qty');
            $table->decimal('Price',15,2);
            $table->decimal('OldPrice',15,2);
            $table->decimal('AddedPrice',15,2);
            $table->decimal('Discount',15,2);
            $table->boolean('State',15,2);
            $table->string('ShopName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_change_records');
    }
}
