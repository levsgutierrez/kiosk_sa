<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multi_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SKU');
            $table->integer('Flag');
            $table->string('Lang1');
            $table->string('Lang2');
            $table->string('Lang3');
            $table->string('Lang4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multi_languages');
    }
}
