<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.Logout');

Route::prefix('admin')->group(function(){
	Route::get('/login', 	'Auth\MintController@showLoginForm')->name('login');
	Route::post('/login', 	'Auth\MintController@login')		->name('login.submit');
	Route::get('/register', 'Auth\MintController@register'); //one time SU only
	Route::get('/logout', 	'Auth\MintController@logout')		->name('logout');
});

// Forbidden

Route::get('/','MainController@dashboard')->name('admin.dashboard');


// Settings Management
Route::prefix('SettingsManagement')->group(function()
{
	Route::get('/SystemModules'	,'SettingsController@SystemModules');

	Route::get('/UserManagement'						,'SettingsController@UserManagement');
	Route::post('/UserManagement/AddUser'				,'SettingsController@AddUser');
	Route::post('/UserManagement/DeleteUser'			,'SettingsController@DeleteUser');

	Route::any('/UserRights/SetRights'					,'SettingsController@SetRights');
	Route::any('/UserRights/GetRights'					,'SettingsController@GetRights');

	Route::get('/KioskManagement'						,'SettingsController@KioskManagement');
	Route::post('/KioskManagement/AddKiosk'				,'SettingsController@AddKiosk');
	Route::post('/KioskManagement/EditKiosk'			,'SettingsController@EditKiosk');
	Route::any('/KioskManagement/DeleteKiosk/{name}'	,'SettingsController@DeleteKiosk');
	Route::post('/KioskManagement/GetKioskCategories'	,'SettingsController@GetKioskCategories');
	Route::post('/KioskManagement/UpdateKioskStatus'	,'SettingsController@UpdateKioskStatus');

	Route::get('/OutletManagement'						,'SettingsController@OutletManagement');
	Route::post('/OutletManagement/AddOutlet'			,'SettingsController@AddOutlet');
	Route::post('/OutletManagement/DeleteOutlet'		,'SettingsController@DeleteOutlet');
	Route::post('/OutletManagement/SaveOutletGroup'		,'SettingsController@SaveOutletGroup');
	Route::post('/OutletManagement/GetOutletGroup'		,'SettingsController@GetOutletGroup');

	Route::get('/DeviceSettings'						,'SettingsController@DeviceSettings');
	Route::post('/DeviceSettings/SaveMoneyAcceptor'			,'SettingsController@SaveMoneyAcceptor');
	Route::post('/DeviceSettings/SaveReceipt'			,'SettingsController@SaveReceipt');
	Route::post('/DeviceSettings/saveReceiptType'		,'SettingsController@saveReceiptType');
});


Route::get('/SettingsManagement/SystemUpdates','SettingsController@SystemUpdates');
Route::get('/SettingsManagement/POS','SettingsController@POS');
Route::get('/SettingsManagement/ActivityLog','SettingsController@ActivityLog');

// Menu Management
Route::prefix('/MenuManagement')->group(function()
{
	Route::get('/MenuItems'			,'MenuController@MenuItems');
	Route::any('/AddMenuItem'			,'MenuController@AddMenuItem');
	Route::any('/DuplicateMenuItem'			,'MenuController@DuplicateMenuItem');
	Route::any('/DeleteMenuItem/{id}'	,'MenuController@DeleteMenuItem');


	Route::get('/SideDish/{id}'	,'MenuController@SideDish');
	Route::post('/AddSideDish'	,'MenuController@AddSideDish');
	Route::post('/DeleteSideDish'	,'MenuController@DeleteSideDish');
	Route::post('/DetailsSideDish'	,'MenuController@DetailsSideDish');
	Route::post('/DetailsSideDish_byName'	,'MenuController@DetailsSideDish_byName');


	Route::get('/AddOns/{id}'	,'MenuController@AddOns');
	Route::post('/AddOns/NewAddOns'	,'MenuController@NewAddOns');
	Route::post('/AddOns/AddOnsItemList'	,'MenuController@AddOnsItemList');
	Route::post('/AddOns/DeleteAddOns'	,'MenuController@DeleteAddOns');

	Route::get('/MenuCategories'			,'MenuController@MenuCategories');
	Route::post('/MenuCategories/AddCategory','MenuController@AddCategory');
	Route::post('/MenuCategories/DeleteCategory','MenuController@DeleteCategory');

	Route::post('/MenuCategories/AddSubCategory','MenuController@AddSubCategory');
	Route::post('/MenuCategories/DeleteSubCategory','MenuController@DeleteSubCategory');

	Route::post('/MenuCategories/SaveGroup','MenuController@SaveGroup');
	Route::post('/MenuCategories/GetGroup','MenuController@GetGroup');

	Route::get('/MenuItemDisplay','MenuController@MenuItemDisplay');
	Route::post('/MenuItemDisplay/SaveDisplay','MenuController@SaveDisplay');
	Route::post('/MenuItemDisplay/GetDisplayGroup','MenuController@GetDisplayGroup');
});

Route::get('/MenuManagement/MenuItemDetails','MenuController@MenuItemDetails');
Route::get('/MenuManagement/MenuItemDishes','MenuController@MenuItemDishes');
Route::get('/MenuManagement/ImportedDishes','MenuController@ImportedDishes');

// Sales Management
Route::prefix('/SalesManagement')->group(function()
{
	Route::get('/ReceiptHistory','SalesController@ReceiptHistory');

	Route::get('/DiscountSettings','SalesController@DiscountSettings');
	Route::post('/DiscountSettings/RetrieveMenuItemName ','SalesController@RetrieveMenuItemName');
	Route::post('/DiscountSettings/AddDiscount ','SalesController@AddDiscount');

	Route::get('/ReplenishCash','SalesController@ReplenishCash');
	Route::post('/ReplenishCash/AddReplenishCash','SalesController@AddReplenishCash');

	Route::get('/CashOut','SalesController@CashOut');
	Route::post('/CashOut/AddCashOut','SalesController@AddCashOut');

	Route::get('/KioskHistory','SalesController@KioskHistory');

	Route::get('/OrderHistory','SalesController@OrderHistory');
	Route::get('/OrderHistory/GetOrderHistoryByDate','SalesController@GetOrderHistoryByDate');
});

// Membership Accounts
Route::prefix('/MembershipAccounts')->group(function()
{
	Route::get('/MembersDetails','MembershipController@MembersDetails');
	Route::get('/MembersDetails/GetTotalBalance','MembershipController@GetTotalBalance');
	Route::post('/MembersDetails/AddMembers','MembershipController@AddMembers');
	Route::post('/MembersDetails/DeleteMembers','MembershipController@DeleteMembers');
	Route::post('/MembersDetails/AddDefaultDepositAmount','MembershipController@AddDefaultDepositAmount');

	Route::get('/MembershipTypes','MembershipController@MembershipTypes');
	Route::post('/MembershipTypes/AddMembershipType','MembershipController@AddMembershipType');
	Route::post('/MembershipTypes/DeleteMembershipType','MembershipController@DeleteMembershipType');

	Route::get('/TopupAccounts','MembershipController@TopupAccounts');
	Route::get('/TopupAccounts/GetTotalTopupAmount','MembershipController@GetTotalTopupAmount');
	Route::get('/TopupAccounts/GetTotalGiftAmount','MembershipController@GetTotalGiftAmount');
	Route::post('/TopupAccounts/GetTopupAccountsByDate','MembershipController@GetTopupAccountsByDate');
	Route::post('/TopupAccounts/RetrieveTopupAccountsDetails','MembershipController@RetrieveTopupAccountsDetails');

	Route::post('/TopupAccounts/TopUpMembersAccount','MembershipController@TopUpMembersAccount');
	Route::post('/TopupAccounts/GetTopupAccountsByCardNumber','MembershipController@GetTopupAccountsByCardNumber');
	Route::post('/TopupAccounts/AddInvoiceAmount','MembershipController@AddInvoiceAmount');
	// Route::get('/TopupInquiry','MembershipController@TopupInquiry');
	// Route::post('/TopupInquiry/GetTopupTransactionsByDate','MembershipController@GetTopupTransactionsByDate');

	Route::get('/DepositStatistics','MembershipController@DepositStatistics');
	Route::post('/DepositStatistics/GetDepositStatsByDate','MembershipController@GetDepositStatsByDate');
	Route::get('/DepositStatistics/GetTotalDepositAmount','MembershipController@GetTotalDepositAmount');

	Route::get('/ExpenditureList','MembershipController@ExpenditureList');
	Route::post('/ExpenditureList/GetExpenditureByDate','MembershipController@GetExpenditureByDate');

	Route::get('/MembersStatistics','MembershipController@MembersStatistics');
	Route::post('/MembersStatistics/GetMemberStatsByDate','MembershipController@GetMemberStatsByDate');
});

// Inventory Management
Route::prefix('/InventoryManagement')->group(function()
{
  Route::get('/Inventory','InventoryController@Inventory');
	Route::post('/UpdateMenuItemStatus','InventoryController@UpdateMenuItemStatus');

	Route::get('/IncomingInventory','InventoryController@IncomingInventory');
	Route::post('/IncomingInventory/RetrieveMenuItemName','InventoryController@RetrieveMenuItemName');
	Route::post('/IncomingInventory/AddIncomingInventory','InventoryController@AddIncomingInventory');
	Route::post('/IncomingInventory/GetInventoryByDate','InventoryController@GetInventoryByDate');
	Route::get('/IncomingInventory/DeleteItem/{id}','InventoryController@DeleteItem');

	Route::get('/OutgoingInventory','InventoryController@OutgoingInventory');
	Route::post('/OutgoingInventory/AddOutgoingInventory','InventoryController@AddOutgoingInventory');
	Route::post('/OutgoingInventory/GetMenuItemName','InventoryController@GetMenuItemName');
	Route::post('/OutgoingInventory/GetOutgoingInventoryByDate','InventoryController@GetOutgoingInventoryByDate');
	Route::get('/OutgoingInventory/DeleteOutgoingItem/{id}','InventoryController@DeleteOutgoingItem');

	Route::get('/InventoryInquiry','InventoryController@InventoryInquiry');

	Route::get('/InventoryLedger','InventoryController@InventoryLedger');
	Route::post('/InventoryLedger/GetLedgerByDate','InventoryController@GetLedgerByDate');

});

// Sales Reports
Route::prefix('/SalesReports')->group(function()
{
	Route::get('/DailySalesReport','SalesReportController@DailySalesReport');
	Route::post('/DailySalesReport/GetDailySalesByDate','InventoryController@GetDailySalesByDate');

	Route::get('/ItemSalesStatistics','SalesReportController@ItemSalesStatistics');
	Route::post('/ItemSalesStatistics/GetItemSalesByDate','InventoryController@GetItemSalesByDate');

	Route::get('/DailySalesStatistics','SalesReportController@DailySalesStatistics');

	Route::get('/InvoiceStatistics','SalesReportController@InvoiceStatistics');
	Route::post('/InvoiceStatistics/GetInvoiceStatsByDate','SalesReportController@GetInvoiceStatsByDate');

	Route::get('/OrderInvoiceStatistics','SalesReportController@OrderInvoiceStatistics');
	Route::get('/SchedulingStatistics','SalesReportController@SchedulingStatistics');
});

//Route::get('/', 'MainController@dashboard');
