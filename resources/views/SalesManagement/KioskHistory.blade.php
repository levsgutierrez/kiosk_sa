@extends('includes.template')
@section('content')
<head>
</head>
<!-- Page Header -->
<div id="content">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h1 class="page-title txt-color-blueDark"><i class="fa fa-file-text fa-fw "></i>  Kiosk History Transactions</h1>
  </div>

  @if(session()->has('message'))
  <div class="alert alert-block alert-success">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
      <p>
          {{ session()->get('message') }}
      </p>
  </div>
  @endif

  @if(session()->has('deleted'))
  <div class="alert alert-block alert-warning">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
      <p>
          {{ session()->get('deleted') }}
      </p>
  </div>
  @endif

  @if($errors->any())
  <div class="alert alert-block alert-danger">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
      <p>
          {{$errors->first()}}
      </p>
  </div>
  @endif


@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<script>
window.onload = function(){
  $('#SalesManagement').addClass("active open");
  $("#SalesManagementBlock").css('display', 'block');
  $("#KioskHistory").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Sales Management</li><li><a href="{{URL::asset(`/SalesManagement/KioskHistory`)}}">Kiosk History</a></li>');
}
</script>
@endsection
