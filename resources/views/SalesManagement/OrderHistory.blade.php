@extends('includes.template')
@section('content')
<head>
</head>

<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Order History<span>
    </div>

    <div class="row"></div>
    <div class="row" style="margin-left:0.2vw;">
    <!-- Buttons / Search Buttons -->
    <fieldset>
      <div class="col-md-2">
        <label> Select a Date Range </label>
      </div>
      <br/>
      <br/>
      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="fromDate" type="text" placeholder="From">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>

      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="toDate" type="text" placeholder="Select a date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>

      <!-- Checkbox selections -->
      <div class="col-sm-4 smart-form" style="margin-top: 0.8vh;">
      <section>
        <div class="inline-group" id="orderHistoryCb">
          <label class="checkbox" style="margin-top: -0.1vh;">
            <input type="checkbox" value="Meal Prepartion" name="checkbox">
            <i></i>Meal Preparation</label>
          <label class="checkbox">
            <input type="checkbox" value="Meal Completed" name="checkbox">
            <i></i>Meal Completed</label>
          <label class="checkbox">
            <input type="checkbox" value="Food Collected" name="checkbox">
            <i></i>Food Collected</label>
        </div>
      </section>
    </div>

      <div class="col-md-2" style="margin-left:-5.5vw;">
        <button type="button" id="clearDatesBtn" class="btn btn-default">Clear</button>
        <button type="button" id="searchByDate" class="btn btn-default">Search</button>
      </div>

        <div class="col-md-2"></div>
        <div class="MenuButtons text-right" style="padding: 1vh; margin-right:0.6vw;">
          <div id="exportBtn" style="margin-top: 1vh;"></div>
        </div>
    </fieldset>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Petty Cash Replenishment Transactions</h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox"></div><!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                              <table id="datatable_fixed_column" class="table table-striped table-bordered display" width="100%">
                                  <thead>
                                      <tr>
                                        <th>Order Number</th>
                                        <th>Table Number</th>
                                        <th>Menu Item SKU</th>
                                        <th>Menu Item Name</th>
                                        <th>Order DateTime</th>
                                        <th>Order Complete DateTime</th>
                                        <th>Delivered DateTime</th>
                                        <th>Remarks</th>
                                        <th>Actions</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($data['kitchens'] as $kitchens)
                                    <meta name="csrf_token" content="{{ csrf_token() }}" />
                                      <tr>
                                        <td>{{ $kitchens->orderNumber }}</td>
                                        <td>{{ $kitchens->tableNumber }}</td>
                                        <td>{{ $kitchens->SKU }}</td>
                                        <td>{{ $kitchens->ItemName }}</td>
                                        <td>{{ $kitchens->created_at }}</td>
                                        <td>{{ $kitchens->created_at }}</td>
                                        <td>{{ $kitchens->created_at }}</td>
                                        <td>{{ $kitchens->remarks}}</td>
                                        <td>
                                          <button type="button" class="btn btn-sm btn-default" id="editPaymentLog"
                                          data-id="{{$kitchens->id}}"
                                          onclick="edit(this)">Delete</button>
                                        </td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                                  <tfoot>
                                </tfoot>
                              </table>
                        </div><!-- end widget content -->
                </div><!-- end widget div -->
            </div><!-- end widget -->
      </article>
  </section>
</div> <!-- /content -->


@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>
<script src="{{URL::asset('js/clockpicker.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script>
window.onload = function(){
  $('#SalesManagement').addClass("active open");
  $("#SalesManagementBlock").css('display', 'block');
  $("#OrderHistory").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Sales Management</li><li><a href="{{URL::asset('/SalesManagement/OrderHistory')}}">Order History</a></li>');
}

$(document).ready(function() {
  search_by_date();
  clearDates();

   var errorClass = 'invalid';
   var errorElement = 'em';

   var $checkoutForm = $('#discounts-form').validate({
       errorClass      : errorClass,
       errorElement    : errorElement,
       highlight: function(element) {
           $(element).parent().removeClass('state-success').addClass("state-error");
           $(element).removeClass('valid');
       },
       unhighlight: function(element) {
           $(element).parent().removeClass("state-error").addClass('state-success');
           $(element).addClass('valid');
       },

   // Rules for form validation
       rules : {
            discountName : {
               required : true
           },
            discountType: {
              required : true
         },
       },

       // Messages for form validation
       messages : {
           discountName : {
               required : 'Enter a Discount Name!'
           },
           discountType : {
              required : 'Choose a Discount Type!'
           },
         },

       // Do not change code below
       errorPlacement : function(error, element) {
           error.insertAfter(element.parent());
       }
   });

 var responsiveHelper_datatable_fixed_column = undefined;
 var breakpointDefinition = {
   tablet : 1024,
   phone : 480
 };

 /* COLUMN FILTER  */
   var otable = $('#datatable_fixed_column').DataTable({
   "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
       "t"+
       "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
   "autoWidth" : true,
   "preDrawCallback" : function() {
     // Initialize the responsive datatables helper once.
     if (!responsiveHelper_datatable_fixed_column) {
       responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
     }
   },
   "rowCallback" : function(nRow) {
     responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
   },
   "drawCallback" : function(oSettings) {
     responsiveHelper_datatable_fixed_column.respond();
   }
 });

 var buttons = new $.fn.dataTable.Buttons(otable, {
     buttons: [
         'excelHtml5',
         'csvHtml5',
         'pdfHtml5'
         ]
       }).container().appendTo($('#exportBtn'));

   // Apply the filter
   $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
       otable
           .column( $(this).parent().index()+':visible' )
           .search( this.value )
           .draw();
   });
   /* END COLUMN FILTER */

   // Date Range Picker
    $("#fromDate").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#toDate").datepicker("option", "minDate", selectedDate);
        },
        dateFormat: 'yy-mm-dd'
    });
    $("#toDate").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#fromDate").datepicker("option", "maxDate", selectedDate);
        },
        dateFormat: 'yy-mm-dd'
    });

    function search_by_date(){
        $("#searchByDate").click(function() {
          var fromDate = $("#fromDate").val();
          var toDate = $("#toDate").val();

          $.ajax({
              type: "POST",
              url: "{{url('MembershipAccounts/DepositStatistics/GetDepositStatsByDate')}}",
              data: {
                    'fromDate': fromDate,
                    'toDate': toDate
              },
              dataType: "json",
              success: function(data)
              {
                console.log(data);
                  $("#depositStatisticsBody").empty();

                  for(var i = 0; i < data.length; i++)
                  {
                      var myvar = '<tr>'+
                      '<td>'+ data[i].created_at +'</td>'+
                      '<td>'+ data[i].accountNumber +'</td>'+
                      '<td>'+ data[i].accountCardNumber +'</td>'+
                      '<td>'+ data[i].membersName +'</td>'+
                      '<td>'+ data[i].amount +'</td>'+
                      '<td>'+ data[i].refund +'</td>'+
                      '<td>'+ data[i].operator +'</td>'+
                      '</tr>';
                      $("#depositStatisticsBody").append(myvar);
                  }
               },
           });
      });
    }



/* Hide Delete Button */
$('#delete').hide();

function clearDates(){
  $("#clearDatesBtn").click(function() {
    $('#fromDate').val('').datepicker("refresh");
    $('#toDate').val('').datepicker("refresh");
    $('#fromDate').datepicker('setDate', null);
    $('#toDate').datepicker('setDate', null);

    $('#orderHistoryCb input[type=checkbox]').prop('checked', false);
  });
}

}); //document.ready


</script>
@endsection
