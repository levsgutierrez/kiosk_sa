@extends('includes.template')
@section('content')

<head>
  <style>
    .input-disabled{background-color:#EBEBE4 !important;border:1px solid #ABADB3;padding:2px 1px;}
    #discountMenuItem{display:none;}
    #discountCategoryName{display:none;}
  </style>

  <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/clockpicker.css')}}">
</head>

<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Cash Out<span>
    </div>

    <!-- Menu Item Buttons -->
    <div class="MenuButtons text-right" style="padding: 1vh; margin-right:0.6vw;">
        <button type="button" class="btn btn-primary" onclick="showAddModal();"> Cash Out</button>
        <div id="exportBtn" style="margin-top: 1.5vh;"></div>
    </div>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cash Out Transactions</h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox"></div><!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                              <table id="datatable_fixed_column" class="table table-striped table-bordered display" width="100%">
                                  <thead>
                                      <tr>
                                        <th>Payment Log Id</th>
                                        <th>Payment Log Type</th>
                                        <th>Denominations</th>
                                        <th>Total Amount</th>
                                        <th>Operator</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($data['payment_logs'] as $payment_logs)
                                    <meta name="csrf_token" content="{{ csrf_token() }}" />
                                      <tr>
                                        <td>{{ $payment_logs->paymentLogId }}</td>
                                        <td>{{ $payment_logs->paymentLogType }}</td>
                                        <td>{{ $payment_logs->denominations }}</td>
                                        <td>{{ $payment_logs->totalAmount }}</td>
                                        <td>{{ $payment_logs->operator }}</td>
                                        <td>{{ $payment_logs->created_at }}</td>
                                        <td>
                                          <button type="button" class="btn btn-sm btn-default" id="editPaymentLog"
                                          data-id="{{$payment_logs->id}}"
                                          onclick="edit(this)">Edit</button>
                                        </td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                                  <tfoot>
                                </tfoot>
                              </table>
                        </div><!-- end widget content -->
                </div><!-- end widget div -->
            </div><!-- end widget -->
      </article>
  </section>
</div> <!-- /content -->

<!-- ================================================================================ -->
<!--                              CASH OUT MODAL                                      -->
<!-- ================================================================================ -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Cash Out Details</h4>
            </div>
            <div class="modal-body no-padding">
                <form id='cashout-form' class="smart-form" novalidate="novalidate" method="POST" action="{{url('SalesManagement/CashOut/AddCashOut')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" id="id" name="id">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Cash Out Date</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-calendar"></i>
                              <input type="text" id="cashOutDate" name="cashOutDate" disabled>
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Kiosk Number</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-tags"></i>
                              <input type="text" id="kioskNumber" name="kioskNumber" aria-required="true" aria-invalid="false" class="valid">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Denominations</label>
                          <section class="col col-8">
                              <div class="col-sm-12 smart-form" style="margin-left:-1.5vw;">
        														<div class="col col-4">
        															<label class="radio">
        																<input type="radio" value="$0.10" name="radioDenominations">
        																<i></i>$.010</label>
        															<label class="radio">
        																<input type="radio" value="$0.20" name="radioDenominations">
        																<i></i>$0.20</label>
        															<label class="radio">
        																<input type="radio" value="$0.50" name="radioDenominations">
        																<i></i>$0.50</label>
        														</div>
        														<div class="col col-4">
        															<label class="radio">
        																<input type="radio" value="$1.00" name="radioDenominations">
        																<i></i>$1.00</label>
        															<label class="radio">
        																<input type="radio" value="$2.00" name="radioDenominations">
        																<i></i>$2.00</label>
        															<label class="radio">
        																<input type="radio" value="$5.00" name="radioDenominations">
        																<i></i>$5.00</label>
        														</div>
        														<div class="col col-4">
        															<label class="radio">
        																<input type="radio" value="$10.00" name="radioDenominations">
        																<i></i>$10.00</label>
        															<label class="radio">
        																<input type="radio" value="$50.00" name="radioDenominations">
        																<i></i>$50.00</label>
        														</div>
                                    <input type="text" id="denominations" name="denominations" hidden>
                                </div>
                            </section>
                         </div>
                       </section>

                       <section>
                         <div class="row">
                           <label class="label col col-4">Total Amount</label>
                           <section class="col col-8">
                             <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                               <input type="text" id="totalAmount" name="totalAmount" aria-required="true" aria-invalid="false" class="valid">
                             </label>
                           </section>
                         </div>
                       </section>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="newOrUpdate">Add</button>
                      <a type="button" class="btn btn-danger" data-dismiss="modal" id="delete" onclick="deleteMember()"> Delete </a>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



@include('includes.scripts')

<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>
<script src="{{URL::asset('js/clockpicker.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


<script>
window.onload = function(){
  $('#MembershipAccounts').addClass("active open");
  $("#SalesManagementBlock").css('display', 'block');
  $("#CashOut").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Sales Management</li><li><a href="{{URL::asset('SalesManagement/CashOut')}}">Cash Out</a></li>');
}

 $(document).ready(function() {

   getDate();

  	var errorClass = 'invalid';
    var errorElement = 'em';

    var $checkoutForm = $('#discounts-form').validate({
        errorClass      : errorClass,
        errorElement    : errorElement,
        highlight: function(element) {
            $(element).parent().removeClass('state-success').addClass("state-error");
            $(element).removeClass('valid');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass("state-error").addClass('state-success');
            $(element).addClass('valid');
        },

    // Rules for form validation
        rules : {
             discountName : {
                required : true
            },
             discountType: {
						   required : true
					},
        },

        // Messages for form validation
        messages : {
            discountName : {
                required : 'Enter a Discount Name!'
            },
            discountType : {
						   required : 'Choose a Discount Type!'
  					},
          },

        // Do not change code below
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

	var responsiveHelper_datatable_fixed_column = undefined;
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};

	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}
  });

  var buttons = new $.fn.dataTable.Buttons(otable, {
      buttons: [
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
          ]
        }).container().appendTo($('#exportBtn'));

    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    });
    /* END COLUMN FILTER */

/* Hide Delete Button */
 $('#delete').hide();
 $("#cashOutDate").addClass('input-disabled');

 function getDate(){
  /* Getting todays date */
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var output = d.getFullYear() + '/' +
        ((''+month).length<2 ? '0' : '') + month + '/' +
        ((''+day).length<2 ? '0' : '') + day;

    $("#cashOutDate").val(output);
 }

 // Show and Hide Radio Buttons On Selection
 $('input[name="radioDenominations"]').on('change', function(e) {
   // console.log($('input[name=radio]:checked').val());
   var y = $('input[name=radioDenominations]:checked').val();
   $("#denominations").val(y);
   // console.log(y);
});

}); //document.ready

function showAddModal()
{
	$('#kioskNumber').val('');
	$('#totalAmount').val('');
  $('input[name=radioDenominations]').attr('checked',false);

  $('#myModal').modal({backdrop: 'static', keyboard: false});
  $('#myModal').modal('show');
}

function deleteCashOut(){
   $('#cashout-form').attr('action',"{{url('SalesManagement/CashOut/DeleteCashOut')}}");
   $('#cashout-form').removeAttr('onsubmit');
   $('#cashout-form').submit();
}
</script>
@endsection
