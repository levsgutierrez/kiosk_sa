@extends('includes.template')
@section('content')

<head>
  <style>
    .input-disabled{background-color:#EBEBE4 !important;border:1px solid #ABADB3;padding:2px 1px;}
    #discountMenuItem{display:none;}
    #discountCategoryName{display:none;}
  </style>

  <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/clockpicker.css')}}">
</head>

<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Discount Settings<span>
    </div>

    <!-- Menu Item Buttons -->
    <div class="MenuButtons text-right" style="padding: 1vh; margin-right:0.6vw;">
        <button type="button" class="btn btn-primary" onclick="showAddModal();">Add New Discount</button>
        <div id="exportBtn" style="margin-top: 1.5vh;"></div>
    </div>


    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Users</h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox"></div><!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                              <table id="datatable_fixed_column" class="table table-striped table-bordered display" width="100%">
                                  <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Discount Name</th>
                                        <th>Discount Type</th>
                                        <th>Discount Amount</th>
                                        <th>Discount Percentage (%)</th>
                                        <th>Item Name</th>
                                        <th>Item Category</th>
                                        <th>Minimum Amount</th>
                                        <th>Dates</th>
                                        <th>Dates</th>
                                        <th>Weeks</th>
                                        <th>Timing</th>
                                        <th>Timing</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <meta name="csrf_token" content="{{ csrf_token() }}" />
                                    @foreach($data['discounts'] as $discounts)
                                      <tr>
                                          <td>{{$discounts->id}}</td>
                                          <td>{{$discounts->discountName}}</td>
                                          <td>@if($discounts->discountType == 1)
                                              Specific Item Discount
                                          @elseif ($discounts->discountType == 2)
                                              Item Category Discount
                                          @elseif ($discounts->discountType == 3)
                                              All Menu Items Discount
                                          @else
                                              Discount off Total Bill Amount
                                          @endif</td>
                                          <td>{{$discounts->discountAmount}}</td>
                                          <td>{{$discounts->discountPercentage}}</td>
                                          <td>{{$discounts->MenuItemName}}</td>
                                          <td>{{$discounts->ItemCategoryName}}</td>
                                          <td>{{$discounts->minimumAmount}}</td>
                                          <td>{{$discounts->beginDate}}</td>
                                          <td>{{$discounts->endDate}}</td>
                                          <td>{{$discounts->week}}</td>
                                          <td>{{$discounts->beginTime}}</td>
                                          <td>{{$discounts->endTime}}</td>
                                          <td>@if($discounts->status == 1)
                                            <span class="label label-success">active</span>
                                          @else
                                            <span class="label label-danger">inactive</span>
                                          @endif</td>
                                          <td>
                                            <button type="button" class="btn btn-sm btn-default" id="editDiscount"
                                              data-id="{{$discounts->id}}"
                                              onclick="edit(this)">Edit</button>
                                          </td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot>
                                </tfoot>
                              </table>
                        </div><!-- end widget content -->
                </div><!-- end widget div -->
            </div><!-- end widget -->
      </article>
  </section>
</div> <!-- /content -->

<!-- ================================================================================ -->
<!--                            ADD NEW DISCOUNT MODAL                                -->
<!-- ================================================================================ -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Add New Discount Type</h4>
            </div>
            <div class="modal-body no-padding">
                <form id='discounts-form' class="smart-form" novalidate="novalidate" method="POST" action="{{url('SalesManagement/DiscountSettings/AddDiscount')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" id="id" name="id">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Discount Name</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-tags"></i>
                              <input type="text" id="discountName" name="discountName" aria-required="true" aria-invalid="false" class="valid">
                            </label>
                          </section>
                        </div>
                      </section>

                      <hr />
                      <br />
                      <section>
                        <div class="row">
                          <label class="label col col-4">Discount Type</label>
                          <section class="col col-8">
                            <label class="select">
                              <select class="form-control" id="discountType" name="discountType" onChange="checkValues()">
                                <option value="0" style="display:none;" selected>Select One</option>
                                <option value="1">Specific Item Discount</option>
                                <option value="2">Item Category Discount</option>
                                <option value="3">All Menu Items Discount</option>
                                <option value="4">Discount off Total Bill Amount</option>
                              </select> <i></i>
                            </label>
                          </section>
                        </div>
                      </section>

                      <!-- To popup only if 1 is selected -->
                      <section>
                        <div id="discountMenuItem" class="row">
                          <label class="label col col-4">Menu Item</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-list-ul"></i>
                              <input type="text" id="itemName" name="itemName" data-toggle="modal" data-target="#myMenuItemModal">
                            </label>
                          </section>
                        </div>
                      </section>

                      <!-- To popup only if 2 is selected -->
                      <section>
                        <div id="discountCategoryName"  class="row">
                          <label class="label col col-4">Menu Category</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-list-ul"></i>
                              <input type="text" id="menuCategory" name="menuCategory" data-toggle="modal" data-target="#myCategoryModal">
                            </label>
                          </section>
                        </div>
                      </section>

                      <!-- To popup only if 4 is selected -->
                      <section>
                        <div id="discountTotalBill"  class="row">
                          <label class="label col col-4">Minimum Amount for Discount</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                              <input type="number" id="minimumAmount" name="minimumAmount">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Discount Method</label>
                          <section class="col col-8">
                            <div class="col-sm-12 smart-form" style="margin-top: 0.8vh;">
                              <label class="radio" style="margin-top: -0.1vh;">
                                <input type="radio" id="fixedAmount" value="Fixed" name="radioDiscount">
                                <i></i>Fixed Discount Amount ($)
                              </label>
                                <input type="number" id="fixedDiscountAmount" name="fixedDiscountAmount" aria-required="true" aria-invalid="false" class="valid">
                              <label class="radio">
                                <input type="radio" id="discountPercent" value="Percentage" name="radioDiscount">
                                  <i></i>Discount Percentage (%)
                              </label>
                              <input type="number" id="discountPercentage" name="discountPercentage" aria-required="true" aria-invalid="false" class="valid">
                            </div>
                          </section>
                        </div>
                      </section>

                      <hr />
                      <br />
                      <label> Discount Effective Periods</label>
                      <br />

                      <section>
                        <div class="row">
                          <label class="label col col-4">Choose preferred Periods</label>
                          <section class="col col-8">
                              <div class="col-sm-12 smart-form" style="margin-top: 0.8vh;">
                                  <label class="radio" style="margin-top: -0.1vh;">
                                    <input type="radio" id="dateRange" value="Range of Dates" name="radioDate">
                                      <i></i>Specific Range of Dates</label>

                                      <section>
                                        <div class="row">
                                          <section class="col col-11" style="margin-left:-0.65vw;">
                                            <div class="input col col-6" id="datepicker1" style="float:left;">
                                              <label class="input"> <i class="icon-prepend fa fa-calendar-o"></i>
                                                <input class="form-control" id="beginDate" type="text" placeholder="From">
                                             </label>
                                            </div>
                                            <div class="input col col-6" id="datepicker2" style="margin-left:-0.95vw; float:right;">
                                              <label class="input"> <i class="icon-prepend fa fa-calendar-o"></i>
                                                 <input class="form-control" id="endDate" type="text" placeholder="Select a date">
                                             </label>
                                            </div>
                                          </section>
                                        </div>
                                      </section>

                                    <label class="radio">
                                      <input type="radio" id="daysWeek" value="Specific Days in a Week" name="radioDate">
                                        <i></i>Specific Days in a Week</label>
                                      <section>
              													<div class="row" id="specificDays" style="border-style: dotted; width: 18vw; margin-left: 0.3vw;">
              														<div class="col col-4" style="margin-bottom: 1vh;">
              															<label class="checkbox">
              																<input type="checkbox" value="Monday" name="checkbox">
              																<i></i>Monday</label>
              															<label class="checkbox">
              																<input type="checkbox" value="Tuesday" name="checkbox">
              																<i></i>Tuesday</label>
              															<label class="checkbox">
              																<input type="checkbox" value="Wednesday" name="checkbox">
              																<i></i>Wednesday</label>
              														</div>
              														<div class="col col-4">
              															<label class="checkbox">
              																<input type="checkbox" value="Thursday" name="checkbox">
              																<i></i>Thursday</label>
              															<label class="checkbox">
              																<input type="checkbox" value="Friday" name="checkbox">
              																<i></i>Friday</label>
              															<label class="checkbox">
              																<input type="checkbox" value="Saturday" name="checkbox">
              																<i></i>Saturday</label>
              														</div>
              														<div class="col col-4">
              															<label class="checkbox">
              																<input type="checkbox" value="Sunday" name="checkbox">
              																<i></i>Sunday</label>
              														</div>
                                          <input type="text" id="week">
              													</div>
              												</section>
                                  </div>
                              </section>
                         </div>
                       </section>
                      <hr />
                      <br />
                      <section>
                        <div class="row">
                          <label class="label col col-4">Start & End Time</label>
                          <section class="col col-8">
                            <div class="input clockpicker col col-6" id="clockpicker" style="float:left;">
                              <label class="input"> <i class="icon-prepend fa fa-clock-o"></i>
                               <input type="text" id="beginTime" name="beginTime" value="08:00" />
                             </label>
                            </div>
                            <div class="input clockpicker col col-6" id="clockpicker2" style="margin-bottom:2vh;float:right;">
                              <label class="input"> <i class="icon-prepend fa fa-clock-o"></i>
                               <input type="text" id="endTime" name="endTime" value="20:00" />
                             </label>
                            </div>
                          </section>
                        </div>
                      </section>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="newOrUpdate">Add</button>
                      <a type="button" class="btn btn-danger" data-dismiss="modal" id="delete" onclick="deleteMember()"> Delete </a>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- ================================================================================ -->
<!--                             MENU ITEMS SKU MODAL                                 -->
<!-- ================================================================================ -->
<div class="modal fade" id="myMenuItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Menu Item</h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
    			<header>
    				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
    				<h2>Menu Item</h2>
    			</header>
          <!-- widget div-->
          <div>
            <!-- widget content -->
            <div class="widget-body no-padding">
              <table id="datatable2" class="table table-striped table-bordered" width="100%">
                <thead>
                  <tr>
                    <th></th>
                    <th>SKU</th>
                    <th>Menu Item Name</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['menus'] as $menus)
                  <meta name="csrf_token" content="{{ csrf_token() }}" />
                    <tr>
                      <td><input type="radio" id="menuItemCheckbox" class="radio" value="{{$menus->ItemName}}" name="radioButtonGroup"/></td>
                      <td>{{ $menus->SKU }}</td>
                      <td>{{ $menus->ItemName }}</td>
                  </tr>
                  @endforeach
               </tbody>
             </table>
           </div>
        </div><!-- end widget content -->
      </div><!-- end widget div -->
    </div><!-- end widget -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="getCheckboxValueMenuItem">Select</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ================================================================================ -->
<!--                            MENU CATEGORIES MODAL                                 -->
<!-- ================================================================================ -->
<div class="modal fade" id="myCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Menu Category</h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
    			<header>
    				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
    				<h2>Menu Category</h2>
    			</header>
        <!-- widget div-->
        <div>
            <!-- widget content -->
            <div class="widget-body no-padding">
              <table id="datatable2" class="table table-striped table-bordered" width="100%">
                <thead>
                  <tr>
                    <th></th>
                    <th>Category ID</th>
                    <th>Category Name</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['menu_categories'] as $menu_categories)
                  <meta name="csrf_token" content="{{ csrf_token() }}" />
                    <tr>
                      <td><input type="radio" id="categoryCheckbox" class="radio" value="{{$menu_categories->CategoryID}}" name="checkboxGroup"/></td>
                      <td>{{ $menu_categories->CategoryID }}</td>
                      <td>{{ $menu_categories->CategoryName }}</td>
                  </tr>
                  @endforeach
               </tbody>
          </table>
        </div>
        </div><!-- end widget content -->
      </div><!-- end widget div -->
    </div><!-- end widget -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="getCheckboxValueCategoryName">Select</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@include('includes.scripts')

<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>
<script src="{{URL::asset('js/clockpicker.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


<script>
window.onload = function(){
  $('#MembershipAccounts').addClass("active open");
  $("#SalesManagementBlock").css('display', 'block');
  $("#DiscountSettings").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Sales Management</li><li><a href="{{URL::asset('SalesManagement/DiscountSettings')}}">Discount Settings</a></li>');
  $('#fixedDiscountAmount').hide();
  $('#discountTotalBill').hide();
  $('#discountPercentage').hide();
  $('#specificDays').hide();
  $('#endDate').hide();
  $('#beginDate').hide();
  $('#datepicker1').hide();
  $('#datepicker2').hide();
}
 $(document).ready(function() {

   getCheckedValueMenuItem();
   getCheckedValueCategoryName();
   checkValues();

  	var errorClass = 'invalid';
    var errorElement = 'em';

    var $checkoutForm = $('#discounts-form').validate({
        errorClass      : errorClass,
        errorElement    : errorElement,
        highlight: function(element) {
            $(element).parent().removeClass('state-success').addClass("state-error");
            $(element).removeClass('valid');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass("state-error").addClass('state-success');
            $(element).addClass('valid');
        },

    // Rules for form validation
        rules : {
             discountName : {
                required : true
            },
             discountType: {
						   required : true
					},
        },

        // Messages for form validation
        messages : {
            discountName : {
                required : 'Enter a Discount Name!'
            },
            discountType : {
						   required : 'Choose a Discount Type!'
  					},
          },

        // Do not change code below
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

	var responsiveHelper_datatable_fixed_column = undefined;
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};

// Date Range Picker
 $("#beginDate").datepicker({
     defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 2,
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',
     onClose: function (selectedDate) {
         $("#endDate").datepicker("option", "minDate", selectedDate);
     },
     dateFormat: 'yy-mm-dd'
 });
 $("#endDate").datepicker({
     defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 2,
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',
     onClose: function (selectedDate) {
         $("#beginDate").datepicker("option", "maxDate", selectedDate);
     },
     dateFormat: 'yy-mm-dd'
 });

// Days in a Week
$('input[type=checkbox]').on('change', function() {
  $('#week').val("");

  var checkedValues = "";
  $('input[type=checkbox]').each(function () {
      if (this.checked)
      {
        checkedValues = checkedValues + $(this).val() + ",";
      }
  });
    $('#week').val(checkedValues);
});

  //Clockpicker
  var input = $('#beginTime');
   input.clockpicker({
   autoclose: true
   });

   //Clockpicker
   var input = $('#endTime');
    input.clockpicker({
    autoclose: true
    });

  // Show and Hide Radio Buttons On Selection
  $('input[name="radioDiscount"]').on('change', function(e) {
    // console.log($('input[name=radio]:checked').val());
    var y = $('input[name=radioDiscount]:checked').val();
    // console.log(y);
    if (y == "Fixed")
    {
      $('#fixedDiscountAmount').show();
      $('#discountPercentage').hide();
      $('#discountPercentage').val('');
    }
    else{
      $('#discountPercentage').show();
      $('#fixedDiscountAmount').hide();
      $('#fixedDiscountAmount').val('');
    }
});

$('input[name="radioDate"]').on('change', function(e) {
  // console.log($('input[name=radio]:checked').val());
  var w = $('input[name=radioDate]:checked').val();
  console.log(w);
  if (w == "Range of Dates")
  {
    $('#endDate').show();
    $('#beginDate').show();
    $('#datepicker1').show();
    $('#datepicker2').show();
    $('#specificDays').hide();
  }
  else{
    $('#specificDays').show();
    $('#endDate').hide();
    $('#beginDate').hide();
    $('#datepicker1').hide();
    $('#datepicker2').hide();
    $('#endDate').val("");
    $('#beginDate').val("");
  }
});

  // Retrieve the Menu Item Name
  function getCheckedValueMenuItem()
  {
    $('#getCheckboxValueMenuItem').click(function(){
        var val = [];
        $(':radio:checked').each(function(i){
          val[i] = $(this).val();
          console.log(val)
          $('#menuCategory').val('');
          $("#myMenuItemModal").modal("hide");
        });
        $('input[id=itemName]').val(val);
        $('#menuCategory').val(" ");
      });
    }

    // Retrieve the Category Name
    function getCheckedValueCategoryName()
    {
      $('#getCheckboxValueCategoryName').click(function(){
          var value = [];
          $(':radio:checked').each(function(i){
            value[i] = $(this).val();
            // val.push($(this).attr("id"));
            $("#myCategoryModal").modal("hide");
          });
          $('input[id=menuCategory]').val(value);
          $('#itemName').val('');
        });
      }

// Dropdown list
  if (document.addEventListener) {
      document.getElementById('discountType').addEventListener('change', function (e) {
          if (this.value === "1"){
            // $('#menuCategory').val('');
            // $('#minimumAmount').val('');
            document.getElementById('discountMenuItem').style.display = "block";
            document.getElementById('discountCategoryName').style.display = "none";
            document.getElementById('discountTotalBill').style.display = "none";
            $('input[name="checkboxGroup"]').prop('checked', false);

          }
          else if (this.value === "2") {
            // $('#itemName').val('');
            // $('#minimumAmount').val('');
            document.getElementById('discountCategoryName').style.display = "block";
            document.getElementById('discountMenuItem').style.display = "none";
            document.getElementById('discountTotalBill').style.display = "none";
            $('input[name="radioButtonGroup"]').prop('checked', false);
          }
          else if (this.value === "4") {
            $('#itemName').val('');
            $('#minimumAmount').val('');
            document.getElementById('discountTotalBill').style.display = "block";
            document.getElementById('discountMenuItem').style.display = "none";
            document.getElementById('discountCategoryName').style.display = "none";
          }
          else {
            $('#itemName').val('');
            $('#menuCategory').val('');
            $('#minimumAmount').val('');
            document.getElementById('discountMenuItem').style.display = "none";
            document.getElementById('discountCategoryName').style.display = "none";
            document.getElementById('discountTotalBill').style.display = "none";

          }
        }, false);
  } else {
      document.getElementById('discountType').attachEvent('onchange', function (e) {
          if (this.value === "1") {
            $('#menuCategory').val('');
            $('#minimumAmount').val('');
            document.getElementById('discountMenuItem').style.display = "block";
            document.getElementById('discountCategoryName').style.display = "none";
            document.getElementById('discountTotalBill').style.display = "none";
          }
          else if (this.value === "2"){
            $('#itemName').val('');
            $('#minimumAmount').val('');
            document.getElementById('discountCategoryName').style.display = "block";
            document.getElementById('discountMenuItem').style.display = "none";
            document.getElementById('discountTotalBill').style.display = "none";
          }
          else if (this.value === "4"){
            $('#itemName').val('');
            $('#minimumAmount').val('');
            document.getElementById('discountCategoryName').style.display = "none";
            document.getElementById('discountMenuItem').style.display = "none";
            document.getElementById('discountTotalBill').style.display = "block";
          }
          else {
            $('#itemName').val('');
            $('#menuCategory').val('');
            $('#minimumAmount').val('');
              document.getElementById('discountMenuItem').style.display = "none";
              document.getElementById('discountCategoryName').style.display = "none";
              document.getElementById('discountTotalBill').style.display = "none";
          }
      });
  }

	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}
  });

  var buttons = new $.fn.dataTable.Buttons(otable, {
      buttons: [
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
          ]
        }).container().appendTo($('#exportBtn'));

    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    });
    /* END COLUMN FILTER */

/* Hide Delete Button */
 $('#delete').hide();

}); //document.ready

//Clear textbox values on dropdown change
function checkValues() {
  var x = document.getElementById("discountType").value;

  if (x == "1")
  {
    $('#menuCategory').val('');
    $('#minimumAmount').val('');
  }
  else if(x == "2")
  {
    $('#itemName').val('');
    $('#minimumAmount').val('');
  }
  else if(x == "4")
  {
    $('#itemName').val('');
    $('#menuCategory').val('');
  }
  else
  {
    $('#menuCategory').val('');
    $('#itemName').val('');
    $('#minimumAmount').val('');
  }
}

function showAddModal()
{
	$('#accountNumber').val('');
	$('#accountCardNumber').val('');
  $('#membershipTypes').val('');
  $('#membersName').val('');
  $('#accountBalance').val('');
  $('#nric').val('');
  $('#contactNumber').val('');
  $('#myModal').modal({backdrop: 'static', keyboard: false});
  $('#myModal').modal('show');
}

function deleteDiscounts(){
   $('#discounts-form').attr('action',"{{url('SalesManagement/DiscountSettings/DeleteDiscounts')}}");
   $('#discounts-form').removeAttr('onsubmit');
   $('#discounts-form').submit();
}
</script>
@endsection
