<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<title> MediaOnline International </title>
		<meta name="description" content="">
		<meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- #CSS Links -->
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/bootstrap.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('css/font-awesome.min.css')}}">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-production-plugins.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-production.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-skins.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-rtl.min.css')}}">

		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/demo.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/app.css')}}">


		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- #FAVICONS -->
		<link rel="shortcut icon" href="{{URL::asset('images/favicon/favicon.ico')}}" type="image/x-icon">
		<link rel="icon" href="{{URL::asset('images/favicon/favicon.ico')}}" type="image/x-icon">

		<!-- #GOOGLE FONT -->
		<link rel="stylesheet" href="{{URL::asset('https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700')}}">

		<!-- #APP SCREEN / ICONS -->
		<!-- Specifying a Webpage Icon for Web Clip
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="{{URL::asset('images/splash/sptouch-icon-iphone.png')}}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('images/splash/touch-icon-ipad.png')}}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('images/splash/touch-icon-iphone-retina.png')}}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('images/splash/touch-icon-ipad-retina.png')}}">

		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="{{URL::asset('images/splash/ipad-landscape.png')}}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="{{URL::asset('images/splash/ipad-portrait.png')}}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="{{URL::asset('images/splash/iphone.png')}}" media="screen and (max-device-width: 320px)">

  </head>

  <body>
    <!-- #HEADER -->
    <header id="header">
      <div id="logo-group">
        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"> <img src="{{URL::asset('images/logo.png')}}" alt="SmartAdmin"> </span>
        <!-- END LOGO PLACEHOLDER -->

        <!-- Note: The activity badge color changes when clicked and resets the number to 0
           Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
        <span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>

        <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
        <div class="ajax-dropdown">
          <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
          <div class="btn-group btn-group-justified" data-toggle="buttons">
            <label class="btn btn-default">
              <input type="radio" name="activity" id="ajax/notify/mail.html">
              Msgs (14) </label>
            <label class="btn btn-default">
              <input type="radio" name="activity" id="ajax/notify/notifications.html">
              notify (3) </label>
            <label class="btn btn-default">
              <input type="radio" name="activity" id="ajax/notify/tasks.html">
              Tasks (4) </label>
          </div>

          <!-- notification content -->
          <div class="ajax-notifications custom-scroll">
            <div class="alert alert-transparent">
              <h4>Click a button to show messages here</h4>
              This blank page message helps protect your privacy, or you can show the first message here automatically.
            </div>
            <i class="fa fa-lock fa-4x fa-border"></i>
          </div>
          <!-- end notification content -->

          <!-- footer: refresh area -->
          <span> Last updated on: 12/12/2013 9:43AM
            <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
              <i class="fa fa-refresh"></i>
            </button>
          </span>
          <!-- end footer -->
        </div>
        <!-- END AJAX-DROPDOWN -->
      </div>

      <!-- #PROJECTS: projects dropdown -->
      <div class="project-context hidden-xs">
        <span class="label">Projects:</span>
        <span class="project-selector dropdown-toggle" data-toggle="dropdown">Recent projects <i class="fa fa-angle-down"></i></span>

        <!-- Suggestion: populate this list with fetch and push technique -->
        <ul class="dropdown-menu">
          <li>
            <a href="javascript:void(0);">Online e-merchant management system - attaching integration with the iOS</a>
          </li>
          <li>
            <a href="javascript:void(0);">Notes on pipeline upgradee</a>
          </li>
          <li>
            <a href="javascript:void(0);">Assesment Report for merchant account</a>
          </li>
          <li class="divider"></li>
          <li>
            <a href="javascript:void(0);"><i class="fa fa-power-off"></i> Clear</a>
          </li>
        </ul>
        <!-- end dropdown-menu-->
      </div>
      <!-- end projects dropdown -->

      <!-- #TOGGLE LAYOUT BUTTONS -->
      <!-- pulled right: nav area -->
      <div class="pull-right">
          <!-- collapse menu button -->
          <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
          </div>
          <!-- end collapse menu -->

          <!-- #MOBILE -->
          <!-- Top menu profile link : this shows only when top menu is active -->
          <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
            <li class="">
              <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                <img src="{{URL::asset('images/avatars/sunny.png')}}" alt="John Doe" class="online" />
              </a>
              <ul class="dropdown-menu pull-right">
                <li>
                  <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="#ajax/profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="{{route('logout')}}" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                </li>
              </ul>
            </li>
          </ul>

          <!-- logout button -->
          <div id="logout" class="btn-header transparent pull-right">
            <span><a href="{{route('logout')}}" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
          </div>
          <!-- end logout button -->

          <!-- search mobile button (this is hidden till mobile view port) -->
          <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
          </div>
          <!-- end search mobile button -->

          <!-- #SEARCH -->
          <!-- input: search field -->
          <form action="#ajax/search.html" class="header-search pull-right">
            <input id="search-fld" type="text" name="param" placeholder="Find reports and more">
            <button type="submit">
              <i class="fa fa-search"></i>
            </button>
            <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
          </form>
          <!-- end input: search field -->

          <!-- fullscreen button -->
          <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
          </div>
          <!-- end fullscreen button -->

          <!-- multiple lang dropdown : find all flags in the flags page -->
          <ul class="header-dropdown-list hidden-xs">
            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{URL::asset('images/blank.gif')}}" class="flag flag-us" alt="United States"> <span> US</span> <i class="fa fa-angle-down"></i> </a>
              <ul class="dropdown-menu pull-right">
                <li class="active">
                  <a href="javascript:void(0);"><img src="{{URL::asset('images/flags/us.png')}}" class="flag flag-us" alt="United States"> English (US)</a>
                </li>
                <li>
                  <a href="javascript:void(0);"><img src="{{URL::asset('images/blank.gif')}}" class="flag flag-cn" alt="China"> 中文</a>
                </li>
              </ul>
            </li>
          </ul>
          <!-- end multiple lang -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->

    <!-- #NAVIGATION -->
    <!-- Left panel : Navigation area -->
    <!-- Note: This width of the aside area can be adjusted through LESS/SASS variables -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is -->
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <img src="{{URL::asset('images/avatars/sunny.png')}}" alt="me" class="online" />
            <span> </span>
            <i class="fa fa-angle-down"></i>
          </a>
        </span>
      </div>
      <!-- end user info -->

      <!-- NAVIGATION : This navigation is also responsive
      To make this navigation dynamic please make sure to link the node
      (the reference to the nav > ul) after page load. Or the navigation
      will not initialize.
      -->
      <nav>
        <!--
        NOTE: Notice the gaps after each icon usage <i></i>..
        Please note that these links work a bit different than
        traditional href="" links. See documentation for details.
        -->
        <ul>
          <li class="active" id="dashboard">
            <a href="{{url('/')}}" title="Dashboard">
							<i class="fa fa-lg fa-fw fa-dashboard"></i>
							<span class="menu-item-parent">Dashboard</span>
						</a>
          </li>

					<li id="SettingsManagement">
            <a href="javascript:;" title="Settings Management">
							<i class="fa fa-lg fa-fw fa-gear"></i>
							<span class="menu-item-parent">Settings Management</span>
						</a>
						<ul id="SettingsManagementBlock">
							<!--<li id="SystemModules">
								<a href="{{url('/SettingsManagement/SystemModules')}}" title="System Modules">
									<i class="fa fa-lg fa-fw fa-gear"></i> System Modules
								</a>
							</li>-->
							<li id="UserManagement">
                <a href="{{url('/SettingsManagement/UserManagement')}}"  title="User Management">
                  <i class="fa fa-lg fa-fw fa-user"></i> User Management
                </a>
              </li>

              <li id="OutletManagement">
								<a href="{{url('/SettingsManagement/OutletManagement')}}"  title="Outlet Management">
									<i class="fa fa-lg fa-fw fa-building"></i> Outlet - Kiosk
								</a>
							</li>

							<li id="DeviceSettings">
								<a href="{{url('/SettingsManagement/DeviceSettings')}}" title="Device Settings">
									<i class="fa fa-cube"></i> Device Settings
								</a>
							</li>
							<li id="KioskManagement">
								<a href="{{url('/SettingsManagement/KioskManagement')}}" title="Menu Scheduling">
									<i class="fa fa-fw fa-laptop"></i> Kiosk Management
								</a>
							</li>
							<li id="SystemUpdates" style="display:none">
								<a href="{{url('/SettingsManagement/SystemUpdates')}}" title="System Updates">
									<i class="fa fa-cube"></i> System Updates
								</a>
							</li>
							<!--<li id="POS">
								<a href="{{url('/SettingsManagement/POS')}}" title="POS">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent"> POS</span>
								</a>
							</li>-->
							<li id="ActivityLog">
								<a href="{{url('/SettingsManagement/ActivityLog')}}" title="Activity Log">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent"> Activity Log</span>
								</a>
							</li>
						</ul>
          </li>
					<!-- Menu Management -->
          <li id="MenuManagement">
            <a href="javascript:;" title="Menu Management">
							<i class="fa fa-lg fa-fw fa-cutlery"></i>
							<span class="menu-item-parent">Menu Management</span>
						</a>
						<ul id="MenuManagementBlock">
							<li id="MenuItems">
								<a href="{{url('/MenuManagement/MenuItems')}}" title="Menu Items">
									<i class="fa fa-lg fa-fw fa-spoon"></i>Menu Items
								</a>
							</li>
							<li id="MenuCategories">
								<a href="{{url('/MenuManagement/MenuCategories')}}" title="Menu Categories">
									<i class="fa fa-lg fa-fw fa-pencil-square-o"></i>Menu Categories
								</a>
							</li>
							<li id="MenuItemDisplay">
								<a href="{{url('/MenuManagement/MenuItemDisplay')}}" title="Menu Item Display">
									<i class="fa fa-barcode"></i>
									<span class="menu-item-parent">Menu Item Display</span>
								</a>
							</li>
							<!--
              <li id="MenuItemDetails">
								<a href="{{url('/MenuManagement/MenuItemDetails')}}" title="Menu Item Details">
									<i class="fa fa-list-ol"></i>
									<span class="menu-item-parent">Menu Items Details</span>
								</a>
							</li>
							<li id="ImportedDishes">
								<a href="{{url('/MenuManagement/ImportedDishes')}}" title="Imported Dishes">
									<i class="fa fa-truck"></i>
									<span class="menu-item-parent">Imported Dishes</span>
								</a>
							</li>
              -->
						</ul>
          </li>
					<!-- Sales Management -->
          <li id="SalesManagement">
            <a href="javascript:;" title="Sales Management">
							<i class="fa fa-lg fa-fw fa-line-chart"></i>
							<span class="menu-item-parent"> Sales Management</span>
						</a>
						<ul id="SalesManagementBlock">
							<li id="ReceiptHistory">
								<a href="{{url('/SalesManagement/ReceiptHistory')}}" title="Receipt History">
									<i class="fa fa-lg fa-fw fa-gear"></i> Receipt History
								</a>
							</li>
							<li id="DiscountSettings">
								<a href="{{url('/SalesManagement/DiscountSettings')}}" title="Discount Settings">
									<i class="fa fa-lg fa-fw fa-picture-o"></i> Discount Settings
								</a>
							</li>
							<li id="ReplenishCash">
								<a href="{{url('/SalesManagement/ReplenishCash')}}" title="Replenish Cash">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent"> Replenish Cash</span>
								</a>
							</li>
							<li id="CashOut">
								<a href="{{url('/SalesManagement/CashOut')}}" title="Cash Out">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent"> Cash Out</span>
								</a>
							</li>
							<li id="KioskHistory">
								<a href="{{url('/SalesManagement/KioskHistory')}}" title="Kiosk History">
									<i class="fa fa-cube"></i> Kiosk History
								</a>
							</li>
							<li id="OrderHistory">
								<a href="{{url('/SalesManagement/OrderHistory')}}" title="Order History">
									<i class="fa fa-cube"></i>Order History
								</a>
							</li>
						</ul>
          </li>

					<!-- Membership Accounts Management -->
          <li id="MembershipAccounts">
            <a href="javascript:;" title="Membership Accounts">
							<i class="fa fa-lg fa-fw  fa-users"></i>
							<span class="menu-item-parent">Membership Accounts</span>
						</a>
						<ul id="MembershipAccountsBlock">
							<li id="MembersDetails">
								<a href="{{url('/MembershipAccounts/MembersDetails')}}" title="Members Details">
									<i class="fa fa-lg fa-fw fa-gear"></i>Members Details
								</a>
							</li>
							<li id="MembershipTypes">
								<a href="{{url('/MembershipAccounts/MembershipTypes')}}" title="Membership Types">
									<i class="fa fa-lg fa-fw fa-picture-o"></i>Membership Types
								</a>
							</li>
							<li id="TopupAccounts">
								<a href="{{url('/MembershipAccounts/TopupAccounts')}}" title="Topup Accounts">
									<i class="fa fa-cube"></i>Topup Accounts
								</a>
							</li>
							<!-- <li id="TopupInquiry">
								<a href="{{url('/MembershipAccounts/TopupInquiry')}}" title="Topup Inquiry">
									<i class="fa fa-cube"></i>Topup Inquiry
								</a>
							</li> -->
							<li id="DepositStatistics">
								<a href="{{url('/MembershipAccounts/DepositStatistics')}}" title="Deposit Statistics">
									<i class="fa fa-cube"></i>Deposit Statistics
								</a>
							</li>
							<li id="ExpenditureList">
								<a href="{{url('/MembershipAccounts/ExpenditureList')}}" title="Expenditure List">
									<i class="fa fa-cube"></i>Expenditure List
								</a>
							</li>
							<li id="MembersStatistics">
								<a href="{{url('/MembershipAccounts/MembersStatistics')}}" title="Members Statistics">
									<i class="fa fa-cube"></i>Members Statistics
								</a>
							</li>
						</ul>
          </li>

					<!-- Inventory -->
					<li id="InventoryManagement">
						<a href="javascript:;" title="Invenetory Management">
							<i class="fa fa-lg fa-fw  fa-users"></i>
							<span class="menu-item-parent">Inventory Management</span>
						</a>
						<ul id="InventoryManagementBlock">
							<li id="Inventory">
								<a href="{{url('/InventoryManagement/Inventory')}}" title="Inventory">
									<i class="fa fa-lg fa-fw fa-gear"></i>Inventory
								</a>
							</li>
							<li id="IncomingInventory">
								<a href="{{url('/InventoryManagement/IncomingInventory')}}" title="Incoming Inventory">
									<i class="fa fa-lg fa-fw fa-picture-o"></i>Incoming Inventory
								</a>
							</li>
							<li id="OutgoingInventory">
								<a href="{{url('/InventoryManagement/OutgoingInventory')}}" title="Outgoing Inventory">
									<i class="fa fa-cube"></i>Outgoing Inventory
								</a>
							</li>
							<li id="InventoryInquiry">
								<a href="{{url('/InventoryManagement/InventoryInquiry')}}" title="Inventory Inquiry">
									<i class="fa fa-cube"></i>Inventory Inquiry
								</a>
							</li>
							<li id="InventoryLedger">
								<a href="{{url('/InventoryManagement/InventoryLedger')}}" title="Inventory Ledger">
									<i class="fa fa-cube"></i>Inventory Ledger
								</a>
							</li>
						</ul>
					</li>

					<!-- Sales Report -->
					<li id="SalesReports">
						<a href="javascript:;" title="Sales Report">
							<i class="fa fa-lg fa-fw  fa-users"></i>
							<span class="menu-item-parent">Sales Reports</span>
						</a>
						<ul id="SalesReportsBlock">
							<li id="DailySalesReport">
								<a href="{{url('/SalesReports/DailySalesReport')}}" title="Daily Sales Report">
									<i class="fa fa-lg fa-fw fa-gear"></i>
									<span class="menu-item-parent">Daily Sales Report</span>
								</a>
							</li>
							<li id="ItemSalesStatistics">
								<a href="{{url('/SalesReports/ItemSalesStatistics')}}" title="Item Sales Statistics">
									<i class="fa fa-lg fa-fw fa-picture-o"></i>
									<span class="menu-item-parent">Item Sales Statistics</span>
								</a>
							</li>
							<li id="DailySalesStatistics">
								<a href="{{url('/SalesReports/DailySalesStatistics')}}" title="Daily Sales Statistics">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent">Daily Sales Statistics</span>
								</a>
							</li>
							<li id="InvoiceStatistics">
								<a href="{{url('/SalesReports/InvoiceStatistics')}}" title="Topup Invoice Statistics">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent">Invoice Statistics</span>
								</a>
							</li>
							<li id="OrderInvoiceStatistics">
								<a href="{{url('/SalesReports/OrderInvoiceStatistics')}}" title="Order Invoice Statistics">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent">Order Statistics</span>
								</a>
							</li>
							<li id="SchedulingStatistics">
								<a href="{{url('/SalesReports/SchedulingStatistics')}}" title="Scheduling Statistics">
									<i class="fa fa-cube"></i>
									<span class="menu-item-parent">Scheduling Statistics</span>
								</a>
							</li>
						</ul>
					</li>
        </ul>
      </nav>

    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>
    <!-- END NAVIGATION -->

	<!-- #MAIN PANEL -->
	<div id="main" role="main">

    <!-- RIBBON -->
  		<!-- RIBBON -->
            <div id="ribbon">

                <span class="ribbon-button-alignment">
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                        <i class="fa fa-refresh"></i>
                    </span>
                </span>

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                </ol>
                <!-- end breadcrumb -->

            </div>
            <!-- END RIBBON -->
		<!-- END RIBBON -->

		<!-- #MAIN CONTENT -->

      <div id="content">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
          <section id="widget-grid" class="">
              <div class="row">
                  @yield('content')
              </div>
          </section>
      	<!-- END #MAIN PANEL -->
      </div><!-- Main Content -->
    </div>


    <!-- #PAGE FOOTER -->
	<div class="page-footer">
		<div class="row">
      <p class="col-md-6 small pull-left text-turquoise" >
         @if(Auth::user()->company) <i class="fa fa-circle fa-fw"></i> Database : {{Auth::user()->company}}  </p> @endif

      <div class="col-xs-6 col-sm-6 ">
				<span class="txt-color-white pull-right">Kiosk Admin <span class="hidden-xs"> - MediaOnline Internatinal</span> © 2017-2019</span>
			</div>
		</div>
		<!-- end row -->
	</div>
		<!-- END FOOTER -->

		<!-- #SHORTCUT AREA : With large tiles (activated via clicking user name tag)
			 Note: These tiles are completely responsive, you can add as many as you like -->
	<div id="shortcut">
		<ul>
			<li>
				<a href="index.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
      </li>
    <li>
      <a href="index.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
  	</li>
  	<li>
			<a href="index.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
		</li>
	</ul>
</div>
	<!-- END SHORTCUT AREA -->

	<script>
		var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();

	</script>


</body>
</html>
