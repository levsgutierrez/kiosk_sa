<!--============================================================== -->
<!--====================== MEMBERS DETAILS JS ===================== -->
<!--============================================================== -->
<script>

function showAddModal(defaultDeposit)
{
	$('#accountNumber').val('');
	$('#accountCardNumber').val('');
  $('#membershipTypes').val('');
  $('#membersName').val('');
  $('#accountBalance').val('');
  $('#nric').val('');
  $('#contactNumber').val('');
	if(defaultDeposit == 0)
  	$('#deposit').val('');
	else {
		$('#deposit').val(defaultDeposit);
		$('#deposit').attr('readonly', true);
	}

	$("#registeredDate").addClass('input-disabled');

	$("#accountBalance").prop("readonly", true);
  $("#accountBalance").addClass('input-disabled');

	$("#accountNumber").removeAttr("disabled");
	$("#accountNumber").removeClass('input-disabled');

	$("#accountCardNumber").removeAttr("disabled");
	$("#accountCardNumber").removeClass('input-disabled');

  $('#myModal').modal({backdrop: 'static', keyboard: false});
  $('#myModal').modal('show');
}

function showDepositModal(id)
{
	$("#generalSettingsId").val(id);
  $('#depositModal').modal({backdrop: 'static', keyboard: false});
  $('#depositModal').modal('show');
}

function clearData()
{
    $('#accountNumber').val("");
    $('#accountCardNumber').val("");
    $('#ItemName').val("");
    $('#membershipTypes').val("");
    $('#membersName').val("");
    $('#balance').val("");
    $('#nric').val("");
    $('#contactNumber').val("");
    $('#CategoryID').val("");
    $('#deposit').val("");
}


function edit(obj)
{
 $('.modal-title').text('Edit Members Details');
 $('#newOrUpdate').text('Update').addClass('btn-warning');
 $('#delete').show();
 $('#id').val(obj.getAttribute('data-id'));
 $('#accountNumber').val(obj.getAttribute('data-accountNumber'));
 $('#accountCardNumber').val(obj.getAttribute('data-accountCardNumber'));
 $('#membershipTypes').val(obj.getAttribute('data-membershipTypes'));
 $('#membersName').val(obj.getAttribute('data-membersName'));
 $('#nric').val(obj.getAttribute('data-nric'));
 $('#contactNumber').val(obj.getAttribute('data-contactNumber'));
 $('#registeredDate').val(obj.getAttribute('data-registeredDate'));
 $('#accountBalance').val(obj.getAttribute('data-accountBalance'));
 $('#deposit').val(obj.getAttribute('data-deposit'));

 if(obj.getAttribute('data-status') == 1)
 		$("#status").prop( "checked", true );
// $("#accountNumber").disabled = true;
 $("#accountNumber").prop("readonly", true);
 $("#accountNumber").addClass('input-disabled');

 $("#regsiteredDate").prop("readonly", true);
 $("#regsiteredDate").addClass('input-disabled');

 // $('#status').val(obj.getAttribute('data-status'));
 $('#myModal').modal('show');
}


</script>
