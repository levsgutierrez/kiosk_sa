@extends('includes.template')
@section('content')

<head>
  <style>
 .vr {
 width: 0.08vw;
 background-color:#d3d3d3;
 position: absolute;
 height: 48vh;
 margin-left: 22.6vw;
 margin-top: 0.1vh;
}
 .vr2 {
 width: 0.05vw;
 background-color:#d3d3d3;
 position: absolute;
 height: 28vh;
 margin-left: 14vw;
 margin-top: -1.49vh;
}

.leftCol {
 float: left;
}

.rightCol {
 float: right;
}

.modal-bodyAddOns {
    max-height: calc(110vh - 200px);
    overflow-y: auto;
}
 </style>
</head>


<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <span><h1 class="page-title txt-color-blueDark"><i class="fa fa-cutlery fa-fw "></i> Menu Items </span>
    </div>

    <!-- Menu Item Buttons -->
    <div class="MenuButtons text-right" style="padding: 1vh;">
        @if(!empty($data['add']))
        <button type="button" class="btn btn-primary" onclick="showAddModal();">Add New Item</button>
        @endif
        <br />
        <!-- <a onclick="alert('To be continue')" class="btn btn-default btn-sm" style="margin-top: 1vh;">Export</a> -->
    </div>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
        {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
          {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
          {{$errors->first()}}
        </p>
    </div>
    @endif
    <!-- END Sessions -->

    <!-- Main Content-->
    <section id="widget-grid" class="">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
            <!-- widget options:
    				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
            data-widget-colorbutton="false"
    			  data-widget-editbutton="false"
    				data-widget-togglebutton="false"
    				data-widget-deletebutton="false"
    				data-widget-fullscreenbutton="false"
    				data-widget-custombutton="false"
    				data-widget-collapsed="true"
    				data-widget-sortable="false"
            -->
            <header>
                <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                <h2>Menu Items</h2>
            </header>

           <!-- widget div-->
               <div>
                 <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                       <!-- This area used as dropdown edit box -->
                    </div><!-- /end widget edit box -->
                    <!-- widget content -->

                    <div class="widget-body no-padding">
                       <table id="dt_basic" class="table table-striped table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
            						<th>SKU</th>
            						<th><i class="text-muted"></i>Item Name</th>
            						<th><i class="fa fa-usd text-muted"></i> Price</th>
            						<!-- <th>Company</th> -->
            						<th><i class="txt-color-blue"></i> Unit</th>
            						<th>Collection Point</th>
            						<th>Printer</th>
            						<th>Description</th>
                        <th><i class="fa fa-fw fa-calendar txt-color-blue"></i> Date</th>
            						<th>Action</th>
              					</tr>
              				</thead>
              				<tbody>
                        <meta name="csrf_token" content="{{ csrf_token() }}" />
                        @foreach($data['Menu'] as $a)
            					<tr class="item{{$a->id}}" >
                        <td style="position: relative;text-align: center;" style=""><img src="{{asset('images\uploads\\'.$a->ImgNo)}}" style="width:60px;height:60px;position: auto;" alt="{{$a->ItemName}}"></td>
            						<td style="vertical-align: middle;">{{$a->SKU}}</td>
            						<td style="vertical-align: middle;">{{$a->ItemName}}</td>
            						<td style="vertical-align: middle;">{{$a->Price}}</td>
            						<td style="vertical-align: middle;">{{$a->Unit}}</td>
            						<td style="vertical-align: middle;">{{$a->CollectPlace}}</td>
            						<td style="vertical-align: middle;">{{$a->PrinterName}}</td>
                        <td style="vertical-align: middle;">{{$a->Desc}}</td>
                        <td style="vertical-align: middle;">{{$a->created_at}}</td>
            						<td style="vertical-align: middle;">
                            @if(!empty($data['edit']))
                            <button class="btn btn-default btn-info btn-sm"
                            data-id="{{$a->id}}"
                            data-SKU="{{$a->SKU}}"
                            data-ItemName="{{$a->ItemName}}"
                            data-Desc="{{$a->Desc}}"
                            data-Price="{{$a->Price}}"
                            data-Unit="{{$a->Unit}}"
                            data-Image="{{$a->ImgNo}}"
                            data-CategoryID="{{$a->CategoryID}}"
                            data-SubCategoryID="{{$a->SubCategoryID}}"
                            data-PrinterName="{{$a->PrinterName}}"
                            data-CollectPlace="{{$a->CollectPlace}}"
                            data-KitchenDesc="{{$a->KitchenDesc}}"
                            data-SafetyStock="{{$a->SafetyStock}}"
                            data-SafetyStock="{{$a->SafetyStock}}"
                            data-Status="{{$a->Status}}"
                            data-OutletID="{{$a->OutletID}}"
                            data-CanTakeOut="{{$a->CanTakeOut}}"
                            data-PackPrice="{{$a->PackPrice}}"
                            data-Discount="{{$a->Discount}}"
                            data-Department="{{$a->Department}}"
                            data-BriefCode="{{$a->BriefCode}}"
                            onclick="edit_data(this);"
                            rel="tooltip" data-placement="top" title="Edit">
                            <i class='fa fa-fw fa-edit'></i></i>
                            </button>

                            <a type="button" class="btn btn-default btn-sm btn-success" href="{{url('/MenuManagement/SideDish/'.$a->SKU)}}" rel="tooltip" data-placement="top" title="Side Dish"><i class='fa fa-fw fa-spoon'></i></a>

                            <a type="button" class="btn btn-default btn-sm btn-warning" href="{{url('/MenuManagement/AddOns/'.$a->SKU)}}" rel="tooltip" data-placement="top" title="Add Ons"><i class='fa fa-fw fa-plus-square'></i></a>
                            @endif
                            @if(!empty($data['duplicate']))
                            <button class="btn btn-default btn-basic btn-sm"
                            data-id="{{$a->id}}"
                            data-SKU="{{$a->SKU}}"
                            data-ItemName="{{$a->ItemName}}"
                            data-Desc="{{$a->Desc}}"
                            data-Price="{{$a->Price}}"
                            data-Unit="{{$a->Unit}}"
                            data-Image="{{$a->ImgNo}}"
                            data-CategoryID="{{$a->CategoryID}}"
                            data-SubCategoryID="{{$a->SubCategoryID}}"
                            data-PrinterName="{{$a->PrinterName}}"
                            data-CollectPlace="{{$a->CollectPlace}}"
                            data-KitchenDesc="{{$a->KitchenDesc}}"
                            data-SafetyStock="{{$a->SafetyStock}}"
                            data-SafetyStock="{{$a->SafetyStock}}"
                            data-Status="{{$a->Status}}"
                            data-OutletID="{{$a->OutletID}}"
                            data-CanTakeOut="{{$a->CanTakeOut}}"
                            data-PackPrice="{{$a->PackPrice}}"
                            data-Discount="{{$a->Discount}}"
                            data-Department="{{$a->Department}}"
                            data-BriefCode="{{$a->BriefCode}}"
                            onclick="duplicate(this);"
                            rel="tooltip" data-placement="top" title="Duplicate">
                            <i class='fa fa-fw fa-copy'></i></i>
                            </button>
                            @endif
                            @if(!empty($data['delete']))
                            <a class="delete-modal btn btn-sm btn-danger"
                            href="{{url('/MenuManagement/DeleteMenuItem/'.$a->SKU)}}" rel="tooltip" data-placement="top" title="Delete">
                            <i class='fa fa-fw fa-trash-o'></i>
                            </a>
                            @endif
                        </td>
            					</tr>
            					@endforeach
              				</tbody>
              			</table>
              		</div><!-- /end widget content -->

      		    </div><!-- /end widget -->

            </div> <!-- /content -->
        </article>
    </section>




    <!-- ================================================================================ -->
    <!--                             ADD NEW MENU ITEM MODAL                              -->
    <!-- ================================================================================ -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	<div class="modal-dialog modal-lg">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearData()">&times;</button>
    				<h4 class="modal-title" id="myModalLabel">Add New Menu Item</h4>
    			</div>

                <div class="modal-body">
                    <div class="row">
                        <form id="wizard-1" novalidate="novalidate" method="POST" action="{{url('/MenuManagement/AddMenuItem')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div id="bootstrap-wizard-1" class="col-sm-12">

                            <div class="form-bootstrapWizard" style="margin-left:8.5vw;">
                                <ul class="bootstrapWizard form-wizard">
                                    <li class="active" data-target="#step1" >
                                        <a href="#tab1" data-toggle="tab" class="disabled"> <span class="step">1</span> <span class="title">Item Properties</span> </a>
                                    </li>
                                    <li data-target="#step2">
                                        <a href="#tab2" data-toggle="tab" class="disabled"> <span class="step">2</span> <span class="title">Item Information</span> </a>
                                    </li>
                                    <li data-target="#step3">
                                        <a href="#tab3" data-toggle="tab" class="disabled"> <span class="step">3</span> <span class="title">Save Changes</span> </a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div> <!-- /form-bootstrapWizard -->

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <br>
                                    <h3><strong>Step 1 </strong> - Menu Item Properties</h3>
                                    <div class="leftCol col-md-6">

                                        <!-- Item Name -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-cutlery fa-lg fa-fw"></i></span>
                                                        <input class="form-control input-lg" placeholder="Menu Item Name" type="text" name="ItemName" id="ItemName">
                                                        <input class="" placeholder="" type="hidden" name="SKU" id="SKU">
                                                        <input class="" placeholder="" type="hidden" name="referenceSKU" id="referenceSKU">
                                                    </div> <!-- /input-group -->
                                                </div> <!-- /form-group -->
                                            </div> <!-- /col-md-12 -->
                                        </div> <!-- /row -->

                                        <!-- Description -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                               <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-pencil-square-o fa-lg fa-fw"></i></span>
                                                        <textarea class="form-control input-lg" placeholder="Menu Item Description" rows="3" type="text" name="Desc" id="Desc" required></textarea>
                                                    </div> <!-- /input-group -->
                                               </div> <!-- /form-group -->
                                            </div> <!-- /col-md-12 -->
                                        </div> <!-- /row -->

                                        <!-- Price & Unit -->
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-usd fa-lg fa-fw"></i></span>
                                                        <input class="form-control input-lg" placeholder="Price" type="number" name="Price" id="Price" min="0">
                                                    </div> <!-- /input-group -->
                                                </div> <!-- /form-group -->
                                            </div> <!-- /col-md-6 -->
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-balance-scale fa-lg fa-fw"></i></span>
                                                        <input class="form-control input-lg" placeholder="Unit" type="text" name="Unit" id="Unit">
                                                    </div> <!-- /input-group -->
                                                </div> <!-- /form-group -->
                                            </div> <!-- /col-sm-6 -->
                                        </div> <!-- /row -->

                                        <!-- Collection Point -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-map-marker fa-lg fa-fw"></i></span>
                                                        <input class="form-control input-lg" placeholder="Collection Point" type="text" name="CollectPlace" id="CollectPlace">
                                                    </div> <!-- /input-group -->
                                                </div> <!-- /form-group -->
                                            </div> <!-- /col-md-12 -->
                                        </div> <!-- /row -->



                                    </div> <!-- /leftcol -->

                                    <!-- <div class="vr"> </div> -->

                                    <!-- Right Col -->
                                    <div class="rightCol col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12">
                                              <label>Item Image</label>
                                              <br/>
                                                <img id="image" class="menuImage" src="{{url('images/defaultSizeImage.png')}}" style="height: 205px; width: 400px"/>
                                                <input type="file" id="fileName" name="image" value="{{old('image')}}" onchange="showImage(this)"  style="display: none;"/>
                                                <input type="hidden" name="old_image" id="old_image" />

                                                <button class="btn btn-success" id="btnAdd" style="margin-top:1vh; margin-left:4.5vw;">Add</button>
                                                &nbsp;
                                                <button class="btn btn-danger" id="btnDelete" style="margin-top:1vh;">Delete</button>
                                                &nbsp;
                                                <!-- <button class="btn btn-default" id="btnExport" style="margin-top:1vh;">Export</button> -->
                                                <!-- <div class="input-group"></div> -->
                                            </div> <!-- /col-md-12 -->
                                        </div> <!-- /row -->
                                        <br />
                                    </div> <!-- /rightcol -->

                                    <div class="row"></div> <!-- /row -->
                                </div> <!-- Tab 1 -->

                                <div class="tab-pane" id="tab2">
                                    <br>
                                    <h3><strong>Step 2</strong> - Menu Item Information</h3>

                                    <div class="leftCol smart-form col-md-6">

                                        <div class="row">
                                            <div class="form-group">

                                                <div class="input-group input-lg">
                                                    <label>Category</label>
                                                    <select class="form-control" name="CategoryID" id="CategoryID">
                                                        <option value="">Select One</option>
                                                         @foreach($data['Category'] as $a)
                                                        <option value="{{$a->CategoryID}}">{{$a->CategoryName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div> <!-- /input-group -->

                                                <div class="input-group input-lg">
                                                    <label>Sub Category</label>
                                                    <select class="form-control" name="SubCategoryID" id="SubCategoryID">
                                                    </select>
                                                </div> <!-- /input-group -->

                                                <!-- Outlet ID -->
                                                <div class="input-group input-lg">
                                                    <label>Outlet</label>
                                                    <select class="form-control" id="OultetID" name="OultetID" value=" ">
                                                        <option value="">Please Select One</option>

                                                        <option value=" " > </option>

                                                    </select>
                                                </div> <!-- /input-group -->

                                                <!-- <div class="input-group input-lg" >
                                                    <label>Item Add Ons</label> -->
                                                    <!-- <input type="text" class="form-control" id="addOns" name="addOns"> -->
                                                    <!-- <div id="addOnsDiv">
                                                    </div> -->
                                                <!-- </div> /input-group -->

                                            </div> <!-- /form-group -->
                                        </div> <!-- /row -->
                                    </div><!-- legt col -->

                                    <div class="rightCol smart-form col-md-6">

                                        <!-- Description -->
                                        <div class="row">
                                            <div class="form-group">

                                                 <!-- Kitchen Printer -->
                                                <div class="input-group input-lg">
                                                  <label>Kitchen Printer</label>
                                                  <input type="text" class="form-control" id="PrinterName" name="PrinterName">
                                                </div> <!-- /input-group -->


                                                <div class="input-group input-lg">
                                                    <label>Multi Language</label>
                                                    <input type="text" class="form-control" id="multiLanguageName" name="multiLanguageName">
                                                </div> <!-- /input-group -->

                                                <div class="input-group input-lg">
                                                    <label class="checkbox">
                                                        <input type="checkbox" id="CanTakeOut" name="CanTakeOut">
                                                        <i></i>Can Take Out?
                                                    </label>
                                                    <div class="input-group ">
                                                        <input type="number" class="form-control" placeholder="TakeAway Charges" id="PackPrice" name="PackPrice" />
                                                    </div>
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="Discount" id="Discount">
                                                        <i></i>Enable Discount on Item
                                                    </label>
                                                </div> <!-- /input group -->

                                            </div> <!-- form group -->
                                        </div><!-- row -->

                                    </div> <!-- /rightCol -->
                                        <div class="row"></div>
                                </div> <!-- /tab2 -->

                                <div class="tab-pane" id="tab3">
                                    <br>
                                    <h3><strong></strong></h3>
                                    <h1 class="text-center text-success"><strong><i class="fa fa-check fa-lg"></i> Complete</strong></h1>
                                    <h4 class="text-center">Click <b id="textDish">Add Dish </b> to apply changes</h4>
                                    <br>
                                    <br>
                                </div>

                            </div> <!-- tab content -->

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="pager wizard no-margin">
                                            <!--<li class="previous first disabled">
                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                            </li>-->
                                            <li class="previous disabled">
                                                <a href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a>
                                            </li>
                                            <!--<li class="next last">
                                            <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                            </li>-->
                                            <li class="next">
                                                <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a>
                                            </li>

                                            <li class="finish">
                                                <a id="addDish" class="btn btn-lg txt-color-darken" onclick="submitForm()" >Add Dish</a>
                                                <a id="duplicate" class="btn btn-lg txt-color-darken" style="display:none" onclick="submitDuplicate()" >Duplicate Dish</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<style>
/*a.disabled {
   pointer-events: none;
   cursor: default;
}*/


</style>

@include('includes.scripts')

<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<!-- ============================= JAVSCRIPT ================================ -->
<script>
window.onload = function(){
    $('#MenuManagement').addClass("active open");
    $("#MenuManagementBlock").css('display', 'block');
    $("#MenuItems").addClass("active");
    $("#dashboard").removeClass("active");
    $(".breadcrumb").append('<li>Menu Management</li><li><a href="{{URL::asset("/MenuManagement/MenuItems")}}">Menu Items</a></li>');
    $('#PackPrice').hide();

}

function clearData()
{
    $('#id').val("");
    $('#SKU').val("");
    $('#ItemName').val("");
    $('#Desc').val("");
    $('#Price').val("");
    $('#Unit').val("");
    $('#CollectPlace').val("");
    $('#PrinterName').val("");
    $('#CategoryID').val("");
    $('#Department').val("");
    $('#multiLanguageName').val("");
    $('#PackPrice').val("");
    $('#old_image').val("");

    $('#referenceSKU').val("");
    resetHeaderGraphic();
}



$('#CanTakeOut').change(function(){
    var $this = $(this);
         if ($this.is(':checked')) {
             $('#PackPrice').show();
         } else {
             $('#PackPrice').hide();
         }
})

function showAddModal()
{
    $('#itemName').val('');
    $('#itemDescription').val('');
    $('#itemPrice').val('');
    $('#Unit').val('');
    $('#collectionPoint').val('');
    $('#kitchenPrinter').val('');
    $('#category').val('');
    //$('#outlets').val(''); //think about how to display this!!
    $('#Department').val('');
    $('#multiLanguageName').val('');
    $('#takeawayCharges').val('');

    $('#addModal').modal({backdrop: 'static', keyboard: false});
    $('#addModal').modal('show');
}

function duplicate(obj)
{
    $('.modal-title').html('<b>Duplicate<b> '+obj.getAttribute('data-ItemName'));
    // $('#newOrUpdate').text('Update').addClass('btn-warning');
    // $('#delete').show();
    // $('#userID').attr('readonly', true).css('background-color' , '#DEDEDE');
    // $('#userid-section').hide();
    $('#duplicate').show()
    $('#addDish').hide()

    var image = "{{url('images/defaultSizeImage.png')}}";
    // if(obj.getAttribute('data-Image') != "")
    // image = "{{url('images/uploads/dataimage')}}".replace('dataimage',obj.getAttribute('data-Image'))||null;
    $('#image').attr("src", image);
    $('#old_image').val('');
    $('#id').val(obj.getAttribute('data-id'));
    $('#SKU').val('');
    $('#referenceSKU').val(obj.getAttribute('data-sku'));
    $('#ItemName').val(obj.getAttribute('data-ItemName')+'_Copy');
    $('#Desc').val(obj.getAttribute('data-Desc'));
    $('#Price').val(obj.getAttribute('data-Price'));
    $('#Unit').val(obj.getAttribute('data-Unit'));
    $('#CollectPlace').val(obj.getAttribute('data-CollectPlace'));
    $('#PrinterName').val(obj.getAttribute('data-PrinterName'));
    $('#CategoryID').val(obj.getAttribute('data-CategoryID'));
    $('#SubCategoryID').val(obj.getAttribute('data-SubCategoryID'));
    $('#Department').val(obj.getAttribute('data-Department'));
    $('#multiLanguageName').val('To Be Indicated');
    $('#PackPrice').val(obj.getAttribute('data-PackPrice'));

    $('#addModal').modal('show');

}
//Image Data

function submitDuplicate()
{
    if(confirm('Are you sure you want to Duplicate the selected item?'))
    {
        $('#wizard-1').attr('action',"{{url('MenuManagement/DuplicateMenuItem')}}");
        $('#wizard-1').removeAttr('onsubmit');
        $('#wizard-1').submit();
    }
}

function showImage(input)
{
    const IMAGE_WIDTH = 100;
    const IMAGE_HEIGHT = 50;
    const validImageDimension = "False";

    var isValidImage = false;
    var imgId = $(input).attr('name');

    if (input.files && input.files[0]) {
        if (checkIfImageExtension(input.files[0])) {
            isValidImage = true;
        }
    }

    if (isValidImage) {
      var reader = new FileReader();
      reader.onload = function (e) {
          var isSuccess = false;
          var image = new Image();

          image.onload = function () {
            if (validImageDimension != "False")
                {
                  if (!(this.width == IMAGE_WIDTH && this.height == IMAGE_HEIGHT))
                  {
                    // TODO Error invalid dimension of image
                    $("#btnAdd").html("Edit");
                    resetHeaderGraphic();
                  }
                  else
                  {
                    $('#image').attr('src', e.target.result);
                    $("#btnAdd").html("Add");
                  }
              }
              else
              {
                $('#image').attr('src', e.target.result);
                $("#btnAdd").html("Add");
              }
          }
          image.src = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }

    else
    {
        //TODO: Error not valid image
        resetHeaderGraphic();
    }
}

// Check file extension
function checkIfImageExtension(file) {
    var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
        return false;
    }
    return true;
}

// Check file extension
function resetHeaderGraphic() {
    $("#fileName").val("");
    $('#image').attr('src', "{{url('images/defaultSizeImage.png')}}");
}

$('#btnAdd').click(function (event) {
    event.preventDefault();
    $("#fileName").trigger('click');
});

$('#btnDelete').click(function (event) {
    event.preventDefault();
    $("#btnAdd").html("Add");
    $("#fileName").val('');
    $('#image').attr('src', "{{url('images/defaultSizeImage.png')}}");
});

function submitForm()
{
    $('#wizard-1').submit();
}



function edit_data(obj)
{
    $('#duplicate').hide()
    $('#addDish').show()
    $('.modal-title').text('Edit '+obj.getAttribute('data-ItemName'));
    // $('#newOrUpdate').text('Update').addClass('btn-warning');
    // $('#delete').show();
    // $('#userID').attr('readonly', true).css('background-color' , '#DEDEDE');
    // $('#userid-section').hide();

    var image = "{{url('images/defaultSizeImage.png')}}";
    if(obj.getAttribute('data-Image') != "")
    image = "{{url('images/uploads/dataimage')}}".replace('dataimage',obj.getAttribute('data-Image'));
    $('#image').attr("src", image);
    $('#old_image').val(obj.getAttribute('data-image'));
    $('#id').val(obj.getAttribute('data-id'));
    $('#SKU').val(obj.getAttribute('data-sku'));
    $('#ItemName').val(obj.getAttribute('data-ItemName'));
    $('#Desc').val(obj.getAttribute('data-Desc'));
    $('#Price').val(obj.getAttribute('data-Price'));
    $('#Unit').val(obj.getAttribute('data-Unit'));
    $('#CollectPlace').val(obj.getAttribute('data-CollectPlace'));
    $('#PrinterName').val(obj.getAttribute('data-PrinterName'));
    $('#CategoryID').val(obj.getAttribute('data-CategoryID'));
    $('#SubCategoryID').val(obj.getAttribute('data-SubCategoryID'));
    $('#Department').val(obj.getAttribute('data-Department'));
    $('#multiLanguageName').val('To Be Indicated');
    $('#PackPrice').val(obj.getAttribute('data-PackPrice'));

    $('#addDish').text('Update Dish');
    $('#textDish').text('Update Dish');

    $('#addModal').modal('show');
}

$("#CategoryID").change(function() {

    $('#SubCategoryID').empty();
    $('#SubCategoryID').append('<option value="">Select Option Header</option>');


    var postData = {
        'CategoryID' : this.value,
    };

    $.ajax({
        type: "POST",
        url: '{{url("/MenuManagement/MenuCategories/GetGroup")}}',
        data: postData,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        success: function(data)
        {
          //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
          $.each(data, function(i) {
                $('#SubCategoryID').append($('<option>', {
                  value: data[i].CategoryID,
                  text : data[i].CategoryName
                }));
           });

        }
      });
})

</script>


 <!-- PAGE RELATED PLUGIN(S)  -->
<script src="{{url('/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="{{url('/js/plugin/fuelux/wizard/wizard.min.js')}}"></script>

<script type="text/javascript">

	// DO NOT REMOVE : GLOBAL FUNCTIONS!

	$(document).ready(function() {


    	var $validator = $("#wizard-1").validate({
		    rules: {
		      ItemName: {
		        required: true
		      },
		      itemPrice: {
		        required: true
		      },
		      Unit: {
		        required: true
		      },
		    },

		    messages: {
		      itemPrice: "Please specify Menu Item Price to be reflected on Menu.",
		      itemUnit: "Please specify Unit of Menu Item Portion for easy viewing.",
		      ItemName: {
		        required: "We need an Item Name to display Dish on the Menu.",
		        ItemName: "Your Menu Item should have a Name."
		      }
		    },

		    highlight: function (element) {
		      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    },
		    unhighlight: function (element) {
		      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function (error, element) {
		      if (element.parent('.input-group').length) {
		        error.insertAfter(element.parent());
		      } else {
		        error.insertAfter(element);
		      }
		    }
		});

		$('#bootstrap-wizard-1').bootstrapWizard({
		    'tabClass': 'form-wizard',
		    'onNext': function (tab, navigation, index) {
		      var $valid = $("#wizard-1").valid();
		      if (!$valid) {
		        $validator.focusInvalid();
		        return false;
		      } else {
		        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
		          'complete');
		        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
		        .html('<i class="fa fa-check"></i>');
		      }
		    }
		});

		// fuelux wizard
		  var wizard = $('.wizard').wizard();

		wizard.on('finished', function (e, data) {
		    //$("#fuelux-wizard").submit();
		    //console.log("submitted!");
		    $.smallBox({
		      title: "Congratulations! Your form was submitted",
		      content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
		      color: "#5F895F",
		      iconSmall: "fa fa-check bounce animated",
		      timeout: 4000
		    });
		});


    var responsiveHelper_dt_basic = undefined;
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };

    $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth" : true,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_dt_basic.respond();
            }

        });
})




</script>
@endsection
