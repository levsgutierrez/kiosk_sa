@extends('includes.template')
@section('content')
<script>
window.onload = function(){
    $('#MenuManagement').addClass("active open");
    $("#MenuManagementBlock").css('display', 'block');
    $("#MenuItems").addClass("active");
    $("#dashboard").removeClass("active");
    $(".breadcrumb").append('<li>Menu Management</li><li><a href="{{URL::asset("/MenuManagement/MenuItems")}}">Menu Items</a></li><li>{{$data['ItemName']}}</a></li>');

    $('#result').empty(); //this is just to clear the \no data available sht\ on the chosen_dt table
}
</script>
<style>
.vertical-center {
    vertical-align: middle !important;
}
</style>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i> 
				<font color="red"><b>{{$data['ItemName']}}</b></font>'s Side Dish Configuration

		</h1>
		
        <div class="btn-group pull-right">
            <a type="button" class="btn btn-warning" id="" data-toggle="modal" href="{{url('/MenuManagement/MenuItems')}}" style="margin-right:2vh">Back</a>
            <button type="button" class="btn btn-primary pull-right" id="createNew" data-toggle="modal" href="#myModal" style="margin-right:2vh">Create Side Dish</button>
        </div>
	</div>


</div>

    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

<div id="content">

	<!-- widget grid -->
	<section id="widget-grid" class="">

		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-5 col-sm-5 col-md-5 col-lg-5">				
				<table class="table table-bordered table-hover" id="sidelist">
					<thead>
						<tr>
							<td>Title</td>
							<td>Min Qty</td>
							<td>Max Qty</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
					@foreach($data['Sides'] as $s)			
					<tronclick="details('{{$s->SideID}}','{{$s->SideName}}')">
						<td align="center"><button class="btn btn-success" style="display:inline-block;width: 100%; height: 100%;" onclick="details('{{$s->SideID}}','{{$s->SideName}}')">{{$s->SideName}}</button></td>
						<td align="center">{{$s->SideMinQty}}</td>
						<td align="center">{{$s->SideMaxQty}}</td>
						<td align="center">
							<button type="button" class="btn btn-sm btn-default" id="editSides" 
                                data-SideID="{{$s->SideID}}" 
                                data-SideName="{{$s->SideName}}" 
                                data-SideMinQty="{{$s->SideMinQty}}" 
                                data-SideMaxQty="{{$s->SideMaxQty}}" 
                                data-SerialCode="{{$s->SerialCode}}" 
                                onclick="edit(this)">Edit
                           	</button>
                           	<button type="button" class="btn btn-sm btn-danger" id="deleteSides" 
                           		data-SideID="{{$s->SideID}}" 
                                data-SideName="{{$s->SideName}}"
                                onclick="delete_me(this)">Delete
                           	</button>
                        </td>
					</tr>
					@endforeach		
					</tbody>
				</table>
			</article>
			<!-- WIDGET END -->

			<!-- NEW WIDGET START -->
			<article class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
				<table class="table table-bordered table-hover" id="viewDetails">
					<thead>
						<tr>
							<td>SKU</td>
							<td>Item Name</td>
							<td>Set Price</td>
							<td>Quantity</td>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</article>
			<!-- WIDGET END -->
		</div>
		<!-- end row -->
	</section>
	<!-- end widget grid -->
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearData()">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Create Side dish for <b>{{$data['ItemName']}}</b></h4>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <form id="wizard-1" novalidate="novalidate" method="POST" action="{{url('/MenuManagement/AddSideDish')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div id="bootstrap-wizard-1" class="col-sm-12">
                        
                        <div class="form-bootstrapWizard">
                            <ul class="bootstrapWizard form-wizard ">
                                <li class="active" data-target="#step1" >
                                    <a href="#tab1" data-toggle="tab" class="disabled" id="hehe"> <span class="step">1</span> <span class="title">Create SideDish</span> </a>
                                </li>
                                <li data-target="#step2">
                                    <a href="#tab2" data-toggle="tab" class="disabled"> <span class="step">2</span> <span class="title">Add Items</span> </a>
                                </li>
                                <li data-target="#step3">
                                    <a href="#tab3" data-toggle="tab" class="disabled"> <span class="step">3</span> <span class="title">Specify Price!</span> </a>
                                </li>
                                <li data-target="#step4">
                                    <a href="#tab4" data-toggle="tab" class="disabled"> <span class="step">4</span> <span class="title">Save!</span> </a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div> <!-- /form-bootstrapWizard -->

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <br>
                                <h3><strong>Step 1 </strong> - Menu Item Basic Information</h3>
                                <div class="row">
                                	<div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-cutlery fa-lg fa-fw"></i></span>
                                                <input class="form-control input-lg" placeholder="SideDish Name" type="text" name="SideName" id="SideName">
                                                <input class="" placeholder="" type="hidden" name="id" id="id">
                                                <input class="" placeholder="" type="hidden" name="SideID" id="SideID" value="{{$data['SKU']}}">
                                            </div> <!-- /input-group -->
                                        </div> <!-- /form-group -->
                                    </div> <!-- /col-md-12 -->
                                </div>
                                
                                <div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-cutlery fa-lg fa-fw"></i></span>
                                                <input class="form-control input-lg" placeholder="Minimum Qty" type="text" name="MinQty" id="MinQty">
                                            </div> <!-- /input-group -->
                                        </div> <!-- /form-group -->
                                    </div> <!-- /col-md-12 -->
                                	
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-cutlery fa-lg fa-fw"></i></span>
                                                <input class="form-control input-lg" placeholder="Maximum Qty" type="text" name="MaxQty" id="MaxQty">
                                            </div> <!-- /input-group -->
                                        </div> <!-- /form-group -->
                                    </div> <!-- /col-md-12 -->
                                </div>

                                <div class="row">
                                	<div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-cutlery fa-lg fa-fw"></i></span>
                                                <input class="form-control input-lg" placeholder="Serial Number" type="text" name="SerialCode" id="SerialCode">
                                                <input class="" placeholder="" type="hidden" name="id" id="id">
                                            </div> <!-- /input-group -->
                                        </div> <!-- /form-group -->
                                    </div> <!-- /col-md-12 -->
                                </div>

                                <div class="row"></div> <!-- /row -->
                            </div> <!-- Tab 1 -->
                            
                            <div class="tab-pane" id="tab2">
                                <br>
                                <h3><strong>Step 2</strong> - Additional Information</h3>
                                <!-- Description -->
                                <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
	                                    <table id="dt_basic" class="table table-striped table-hover" width="100%">
				                        <thead>
				                            <tr>
				                                <th></th>
				        						<th>SKU</th>
				        						<th><i class="text-muted"></i>ITEM NAME</th>
				        						<th><i class="fa fa-usd text-muted"></i>PRICE</th>
				          					</tr>
				          				</thead>
				          				<tbody>
				                            <meta name="csrf_token" content="{{ csrf_token() }}" />
				                            @foreach($data['Menu'] as $a)
				        					<tr class=" " >
												<td style="text-align: center;"><label class="checkbox-inline" ><input type="checkbox" class="checkbox style-0" id="sku_{{$a->SKU}}" onclick="checkBoxAddFunction(this);" 
				        							value="{{$a->SKU}}" 
				        							data-id="{{$a->id}}"
				        							data-ItemName="{{$a->ItemName}}"
				        							data-sku="{{$a->SKU}}"

				        							><span></span></label></td>
				        						<td style="vertical-align: middle;">{{$a->SKU}}</td>
				        						<td style="vertical-align: middle;">{{$a->ItemName}}</td>
				        						<td style="vertical-align: middle;">{{$a->Price}}</td>
				        					</tr>
				        					@endforeach
				          				</tbody>
	          							</table>
	          						</div> <!-- /form-group -->
                                </div> <!-- /col-md-12 -->
                                </div> <!-- /col-md-12 -->
                              
                                <div class="row"></div>
                            </div> <!-- /tab2 -->

                            <div class="tab-pane" id="tab3">
                                <br>
                                <h3><strong>Step 3</strong> - Specify Price</h3>
                                <table id="chosen_dt" class="table table-bordered" width="100%">
	                                <thead>
			                            <tr>
			        						<th>SKU</th>
			        						<th><i class="text-muted"></i>ITEM NAME</th>
			        						<th>SET PRICE</th>
			        						<th>SET QTY</th>
			        						<th></th>
			          					</tr>
			          				</thead>
			          				<tbody id="result" ></tbody>
		          				</table>
		          				<input type="hidden" id="rowCount" name="rowCount" value="0">
                                <div class="row"></div>
                            </div>
                            <div class="tab-pane" id="tab4">
                                <br>
                                <h3><strong></strong></h3>
                                <h1 class="text-center text-success"><strong><i class="fa fa-check fa-lg"></i> Complete</strong></h1>
                                <h4 class="text-center">Click <b id="textDish">Save Dish </b> to apply changes</h4>
                                <br>
                                <br>
                            </div>

                        </div> <!-- tab content -->

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="pager wizard no-margin">
                                        <!--<li class="previous first disabled">
                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                        </li>-->
                                        <li class="previous disabled">
                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a>
                                        </li>
                                        <!--<li class="next last">
                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                        </li>-->
                                        <li class="next">
                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a>
                                        </li>

                                        <li class="finish">
                                            <a id="addDish" class="btn btn-lg txt-color-darken" onclick="submitForm()" >Save Dish</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.scripts')

<!-- PAGE RELATED PLUGIN(S)  -->
<script src="{{url('/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="{{url('/js/plugin/fuelux/wizard/wizard.min.js')}}"></script>

<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script>
$(document).ready(function() {
    
	var $validator = $("#wizard-1").validate({
		    rules: {
		      SideName: {
		        required: true
		      },
		      MinQty: {
		        required: true,
		        minlength : 1,
		        maxlength : 3,
		      },
		      MaxQty: {
		        required: true,
		        minlength : 1,
		        maxlength : 3,
		      },
		      SerialCode: {
		        required: true
		      },
		    },

		    messages: {
		       SideName: {
		        required: "Your Side Dish should have a Name.",
		      },
		       MinQty: {
		        required: "You need to specify Minimum Quantity",
		        maxlength : 'Numbers only are accepted.'
		      },
		       MaxQty: {
		        required: "You need to specify Maximum Quantity.",
		        maxlength: "Numbers only are accepted."
		      },
		       SerialCode: {
		        required: "We need Serial Code.",
		        SerialCode: "Your Menu Item should have a Name."
		      }
		    },

		    highlight: function (element) {
		      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    },
		    unhighlight: function (element) {
		      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function (error, element) {
		      if (element.parent('.input-group').length) {
		        error.insertAfter(element.parent());
		      } else {
		        error.insertAfter(element);
		      }
		    }
		});

		$('#bootstrap-wizard-1').bootstrapWizard({
		    'tabClass': 'form-wizard',
		    'onNext': function (tab, navigation, index) {
		      var $valid = $("#wizard-1").valid();
		      if (!$valid) {
		        $validator.focusInvalid();
		        return false;
		      } else {
		        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
		        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
		        .html('<i class="fa fa-check"></i>');
		      }
		    }
		});

		// fuelux wizard
		  var wizard = $('.wizard').wizard();

		wizard.on('finished', function (e, data) {
		    //$("#fuelux-wizard").submit();
		    //console.log("submitted!");
		    $.smallBox({
		      title: "Congratulations! Your form was submitted",
		      content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
		      color: "#5F895F",
		      iconSmall: "fa fa-check bounce animated",
		      timeout: 4000
		    });
		});

	/* Simple Jquery Table */
	var responsiveHelper_dt_basic = undefined;
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };


    $('#sidelist').dataTable({
    	  	"bPaginate": false,
		    "bLengthChange": false,
		    "bFilter": false,
		    "bInfo": false,
		    "bAutoWidth": false
    });
    $('#chosen_dt').dataTable({
    	  	"bPaginate": false,
		    "bLengthChange": false,
		    "bFilter": true,
		    "bInfo": false,
		    "bAutoWidth": false
    });
    $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth" : true,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_dt_basic.respond();
            }

        });
});

$('#createNew').click(function(){
    $('#SideName').val("").prop('readonly', false);
    $('#MinQty').val("");
    $('#MaxQty').val("");
    $('#SerialCode').val("");
    $('#SerialCode').val("");
    $('input:checkbox').removeAttr('checked');
    $('#hehe').click();
})

function checkBoxAddFunction(obj) 
{   
    var items = '';
    if(obj.checked)
    {  
		items += "<tr class='item_"+obj.getAttribute('data-sku')+"'>";
		items += "<td>"+obj.getAttribute('data-sku')+"<input type='hidden' name='iSKU_[]' value='"+obj.getAttribute('data-sku')+"'></td>";
		items += "<td>"+obj.getAttribute('data-ItemName')+"<input type='hidden' name='itemName_[]' value='"+obj.getAttribute('data-ItemName')+"'></td>";
		items += "<td><input type='text' id='setPrice_[]' name='setPrice_[]'></td>";
		items += "<td><input type='text' id='qty_[]' name='qty_[]'></td>";
		items += "<td><i class='fa fa-trash'></i></td>";
		items += "</tr>";
		$('#result').append(items);
		
		//rowCount++;
		rowCount = $('input[type="checkbox"]:checked').length;
		$('#rowCount').val(rowCount);
	}
	else
    {
		rowCount = $('input[type="checkbox"]:checked').length;
		$('.item_'+obj.getAttribute('data-sku')).remove();
		$('#rowCount').val(rowCount);
	}
}

function submitForm()
{
    $('#wizard-1').submit();
}

function details(id,name)
{
	var postData = {
		'SideID' : id,
		'SideName' : name,
	};
  //console.log(postData);
  $.ajax({
      type: "POST",
      url: '{{url("MenuManagement/DetailsSideDish")}}',
      data: postData,
      beforeSend: function (xhr) {
      var token = $('meta[name="csrf_token"]').attr('content');
      if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      success: function(data)
      {
      	var items='';
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
        $.each(data, function(i) {
           	items += "<tr class='item_"+data[i].SKU+"'>";
			items += "<td>"+data[i].SKU+"</td>";
			items += "<td>"+data[i].ItemName+"</td>";
			items += "<td>"+data[i].SidePrice+"</td>";
			items += "<td>"+data[i].MaxQty+"</td>";
			items += "</tr>";
         })

        $('#viewDetails tbody').empty().append(items).show();
        }
        
    });
}

function edit(obj)
{
    $('#result').empty(); //clear tables just incase he click the edit again
	
    $('.modal-title').text('Edit Side Dish '+obj.getAttribute('data-SideName'));
 //    $('#newOrUpdate').text('Update').addClass('btn-warning');
 //    $('#delete').show();
 //    $('#userID').attr('readonly', true).css('background-color' , '#DEDEDE');
    
    
    $('#SideName').val(obj.getAttribute('data-SideName')).prop('readonly', true);
    $('#MinQty').val(obj.getAttribute('data-SideMinQty'));
    $('#MaxQty').val(obj.getAttribute('data-SideMaxQty'));
    $('#SerialCode').val(obj.getAttribute('data-SerialCode'));

    var postData = {
		'SideName' : obj.getAttribute('data-SideName'),
	};

    $.ajax({
	type: "POST",
	url: '{{url("MenuManagement/DetailsSideDish_byName")}}',
	data: postData,
	beforeSend: function (xhr) 
	{
		var token = $('meta[name="csrf_token"]').attr('content');
		if (token) {
		    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
		}
	},
	success: function(data)
	{	var rowCount = $('input[type="checkbox"]:checked').length;
		var items = "";
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
		$.each(data, function(i) {
        	$('#sku_'+data[i].SKU).attr('checked', true);
        	items += "<tr class='item_"+data[i].SKU+"'>";
			items += "<td>"+data[i].SKU+"<input type='hidden' name='iSKU_[]' value='"+data[i].SKU+"'></td>";
			items += "<td>"+data[i].ItemName+"<input type='hidden' name='itemName_[]' value='"+data[i].ItemName+"'></td>";
			items += "<td><input type='text' id='setPrice_[]' name='setPrice_[]' value='"+data[i].SidePrice+"'></td>";
			items += "<td><input type='text' id='qty_[]' name='qty_[]' value='"+data[i].MaxQty+"'></td>";
			items += "<td><i class='fa fa-trash'></i></td>";
			items += "</tr>";
			rowCount++;
		});
		$('#result').append(items);
		$('#rowCount').val(rowCount);
    }
	});
    
    $('#myModal').modal('show');
}

function delete_me(obj)
{
	console.log(obj);
	if(confirm('Delete detected, cannot be undone'))
	{
		var postData = {
			'SideID' : obj.getAttribute('data-SideID'),
			'SideName' : obj.getAttribute('data-SideName')
		}
		
		$.ajax({
		type: "POST",
		url: '{{url("MenuManagement/DeleteSideDish")}}',
		data: postData,
		beforeSend: function (xhr) 
		{
		var token = $('meta[name="csrf_token"]').attr('content');
		if (token) {
		    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
		}
		},
		success: function(data)
		{	
			//alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
			location.reload();
		}
		});

		

	}
}

</script>


@endsection

