@extends('includes.template')
@section('content')
<script>
window.onload = function(){
    $('#MenuManagement').addClass("active open");
    $("#MenuManagementBlock").css('display', 'block');
    $("#MenuItems").addClass("active");
    $("#dashboard").removeClass("active");
    $(".breadcrumb").append('<li>Menu Management</li><li><a href="{{URL::asset("/MenuManagement/MenuItems")}}">Menu Items</a></li><li>Add-Ons</a></li>');

    $('#result').empty(); //this is just to clear the \no data available sht\ on the chosen_dt table
}
</script>
<style>
.vertical-center {
    vertical-align: middle !important;
}
</style>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i> 
				<font color="red"><b>{{$data['ItemName']}}</b></font>'s AddOns Configuration
		</h1>
		<div class="btn-group pull-right">
			<a type="button" class="btn btn-warning" id="" data-toggle="modal" href="{{url('/MenuManagement/MenuItems')}}" style="margin-right:2vh">Back</a>
			<button type="button" class="btn btn-primary" id="addUser" data-toggle="modal" href="#myModal" style="margin-right:2vh">Create AddOns</button>
		</div>
	</div>


</div>

    @if(session()->has('message'))`
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

<div id="content">
	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-4 col-sm-4 col-md-4 col-lg-4">				
			<table class="table table-bordered table-hover" id="sidelist">
				<thead>
					<tr>
						<td>Add-Ons Name</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					@foreach($data['AddOnsList'] as $a)
					<tr >
						<td onclick="details('{{$a->AddOns}}')"><button class="btn btn-warning" style="display:inline-block;width: 100%; height: 100%;">{{$a->AddOns}}</button></td>
						<td>
							<button type="button" class="btn btn-sm btn-default" id="editSides" 
                            data-AddOns="{{$a->AddOns}}" 
                            data-CategoryID="{{$a->CategoryID}}" 
                            data-ItemName="{{$a->ItemName}}" 
                            data-Price="{{$a->Price}}" 
                            onclick="edit(this)">Edit
                       	</button>
                       	<button type="button" class="btn btn-sm btn-danger" id="deleteSides" 
						 	data-AddOns="{{$a->AddOns}}" 
                            data-CategoryID="{{$a->CategoryID}}" 
                            onclick="delete_me(this)">Delete
                       	</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</article>
		<!-- WIDGET END -->

		<!-- NEW WIDGET START -->
		<article class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
			<table class="table table-bordered table-hover" id="viewDetails">
				<thead>
					<tr>
						<td>Item Name</td>
						<td>Set Price</td>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</article>
		<!-- WIDGET END -->
	</div>
	<!-- end row -->
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearData()">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Create Remarks<b></b></h4>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <form id="wizard-2" novalidate="novalidate" method="POST" action="{{url('MenuManagement/AddOns/NewAddOns')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="SKU" value="{{$data['SKU']}}">
                    <div id="bootstrap-wizard-2" class="col-sm-12">
                        
                        <div class="form-bootstrapWizard" style="margin-left:19vw;">
                            <ul class="bootstrapWizard form-wizard ">
                                <li class="active" data-target="#step1" >
                                    <a href="#tab1" data-toggle="tab" class="disabled"> <span class="step">1</span> <span class="title">Create Add-Ons</span> </a>
                                </li>
    
                                <li data-target="#step2">
                                    <a href="#tab2" data-toggle="tab" class="disabled"> <span class="step">2</span> <span class="title">Save!</span> </a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div> <!-- /form-bootstrapWizard -->

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <br>
                                <h3><strong>Step 1 </strong> - Create Add-Ons for Dishes</h3>
   
                                <div class="col-sm-4">
                                	<div class="row">
                                		<div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-cutlery fa-lg fa-fw"></i></span>
                                                <input class="form-control input-lg" placeholder="Title" type="text" name="AddOns" id="AddOns">
                                                <input type="hidden" name="rowno" id="rowno">

                                            </div> <!-- /input-group -->
                                        </div> <!-- /form-group -->
                                    </div> <!-- /form-group -->
                                </div>

                                <div class="col-sm-8">
                                	<div class="form-group">
	                                    <table id="simpleTable" class="table table-hover table-striped">
				                        <thead>
				                            <tr>
				        						<th><i class="text-muted"></i>ITEM NAME</th>
				        						<th><i class="fa fa-usd text-muted"></i>PRICE</th>
				        						<th><button type="button" class="btn pull-right btn-primary" onclick="addRow()">Add</button></th>
				          					</tr>
				          				</thead>
				          				<tbody id="items">
				          					<tr align="center">
												<td><input type="text" name="itemName[]"></td>
												<td><input type="text" name="price[]" value="0.00"></td>
											</tr>
				          				</tbody>
	          							</table>

                                    </div> <!-- /form-group -->
                                </div>

                                <div class="row"></div> <!-- /row -->
				                <meta name="csrf_token" content="{{ csrf_token() }}" />
                            </div> <!-- Tab 1 -->
                            
                            <div class="tab-pane" id="tab2">
                                <br>
                                <h3><strong></strong></h3>
                                <h1 class="text-center text-success"><strong><i class="fa fa-check fa-lg"></i> Complete</strong></h1>
                                <h4 class="text-center">Click <b id="textDish">Save Remarks </b> to apply changes</h4>
                                <br>
                                <br>
                            </div>

                        </div> <!-- tab content -->

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="pager wizard no-margin">
                                        <!--<li class="previous first disabled">
                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                        </li>-->
                                        <li class="previous disabled">
                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a>
                                        </li>
                                        <!--<li class="next last">
                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                        </li>-->
                                        <li class="next">
                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a>
                                        </li>

                                        <li class="finish">
                                            <a id="addDish" class="btn btn-lg txt-color-darken" onclick="submitForm()" >Save Remarks</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.scripts')

<!-- PAGE RELATED PLUGIN(S)  -->
<script src="{{url('/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="{{url('/js/plugin/fuelux/wizard/wizard.min.js')}}"></script>

<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script>
$(document).ready(function() {
    
	var $validator = $("#wizard-2").validate({
		    rules: {
		      SideName: {
		        required: true
		      },
		      MinQty: {
		        required: true,
		        minlength : 1,
		        maxlength : 3,
		      },
		      MaxQty: {
		        required: true,
		        minlength : 1,
		        maxlength : 3,
		      },
		      SerialCode: {
		        required: true
		      },
		    },

		    messages: {
		       SideName: {
		        required: "Your Side Dish should have a Name.",
		      },
		       MinQty: {
		        required: "You need to specify Minimum Quantity",
		        maxlength : 'Numbers only are accepted.'
		      },
		       MaxQty: {
		        required: "You need to specify Maximum Quantity.",
		        maxlength: "Numbers only are accepted."
		      },
		       SerialCode: {
		        required: "We need Serial Code.",
		        SerialCode: "Your Menu Item should have a Name."
		      }
		    },

		    highlight: function (element) {
		      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    },
		    unhighlight: function (element) {
		      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function (error, element) {
		      if (element.parent('.input-group').length) {
		        error.insertAfter(element.parent());
		      } else {
		        error.insertAfter(element);
		      }
		    }
		});

		$('#bootstrap-wizard-2').bootstrapWizard({
		    'tabClass': 'form-wizard',
		    'onNext': function (tab, navigation, index) {
		      var $valid = $("#wizard-2").valid();
		      if (!$valid) {
		        $validator.focusInvalid();
		        return false;
		      } else {
		        $('#bootstrap-wizard-2').find('.form-wizard').children('li').eq(index - 1).addClass(
		          'complete');
		        $('#bootstrap-wizard-2').find('.form-wizard').children('li').eq(index - 1).find('.step')
		        .html('<i class="fa fa-check"></i>');
		      }
		    }
		});

		// fuelux wizard
		  var wizard = $('.wizard').wizard();

		wizard.on('finished', function (e, data) {
		    //$("#fuelux-wizard").submit();
		    //console.log("submitted!");
		    $.smallBox({
		      title: "Congratulations! Your form was submitted",
		      content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
		      color: "#5F895F",
		      iconSmall: "fa fa-check bounce animated",
		      timeout: 4000
		    });
		});

	$("#simpleTable").on('click', '.btnDelete', function () {
    	$(this).closest('tr').remove();
    	var rowCount = document.getElementById('simpleTable').rows.length-1;
		document.getElementById("rowno").value = rowCount;
	});
	
});

function addRow(){
	var rowCount = document.getElementById('simpleTable').rows.length; 

	var initialElementCount = 0;

	var tbody = document.getElementById('simpleTable').getElementsByTagName("tbody")[0];
	var row = document.createElement("TR");
	var i=0;

	var cell0 = document.createElement("TD");
	  cell0.setAttribute("align", "center");
	  cell0.innerHTML = '<td><input type="text" name="itemName[]"></td>';

	  var cell1 = document.createElement("TD");
	  cell1.setAttribute("align", "center");
	  cell1.innerHTML = '<td><input type="text" name="price[]" value="0.00"></td>';

	  var cell2 = document.createElement("TD");
	  cell2.setAttribute("align", "center");
	  cell2.innerHTML = "<td><button type='button' class='btn btn-sm btn-danger btnDelete'>X</button></td>";

	
	//alert(rowCount);
	document.getElementById("rowno").value = rowCount;
	row.appendChild(cell0);
	row.appendChild(cell1);
	row.appendChild(cell2);
	tbody.appendChild(row);
}

function submitForm()
{
    $('#wizard-2').submit();
}

function details(AddOns)
{
	var postData = {
		'AddOns' : AddOns,
		'SKU' : "{{$data['SKU']}}",
	};
  console.log(postData);
  $.ajax({
      type: "POST",
      url: '{{url("MenuManagement/AddOns/AddOnsItemList")}}',
      data: postData,
      beforeSend: function (xhr) {
      var token = $('meta[name="csrf_token"]').attr('content');
      if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      success: function(data)
      {
      	var items='';
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
        $.each(data, function(i) {
           	items += "<tr>";
			items += "<td>"+data[i].ItemName+"</td>";
			items += "<td>"+data[i].Price+"</td>";
			items += "</tr>";
         })

        $('#viewDetails tbody').empty().append(items).show();
        }
        
    });
}



function edit(obj)
{
	console.log(obj);
	$('.modal-title').text('Edit Add Ons '+obj.getAttribute('data-AddOns'));
    
    $('#AddOns').val(obj.getAttribute('data-AddOns')).prop('readonly', true);
    $('#CategoryID').val(obj.getAttribute('data-CategoryID'));

    var postData = {
		'AddOns' : obj.getAttribute('data-AddOns'),
		'SKU' : "{{$data['SKU']}}",
	};

    $.ajax({
	type: "POST",
	 url: '{{url("MenuManagement/AddOns/AddOnsItemList")}}',
	data: postData,
	beforeSend: function (xhr) 
	{
		var token = $('meta[name="csrf_token"]').attr('content');
		if (token) {
		    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
		}
	},
	success: function(data)
	{	
		var rowCount = 0;
		var items = "";
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!

        $.each(data, function(i) {
           	items += "<tr align='center'>";
			items += "<td><input type='text' name='itemName[]' value='"+data[i].ItemName+"'></td>";
			items += "<td class='inline'><input type='text' name='price[]' value='"+data[i].Price+"'></td>";
			items += "<td><button type='button' class='btn btn-sm btn-danger btnDelete'>X</button></td>";
			items += "</tr>";
			rowCount++;
         })

        $('#simpleTable tbody').empty().append(items).show();
	    $('#rowno').val(rowCount);

    }
	});
    
    $('#myModal').modal('show');
}

function delete_me(obj)
{
	console.log(obj);
	if(confirm('Delete detected, cannot be undone'))
	{
		var postData = {
			'AddOns' : obj.getAttribute('data-AddOns'),
			'SKU' : "{{$data['SKU']}}",
		}

		$.ajax({
		type: "POST",
		url: '{{url("/MenuManagement/AddOns/DeleteAddOns")}}',
		data: postData,
		beforeSend: function (xhr) 
		{
		var token = $('meta[name="csrf_token"]').attr('content');
		if (token) {
		    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
		}
		},
		success: function(data)
		{	
			//alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
			location.reload();
		}
		});

		

	}
}

</script>


@endsection

