@extends('includes.template')
@section('content')

<div id="content">
	<div class="row">
	    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
	        <h1 class="page-title txt-color-blueDark"><i class="fa fa-desktop fa-fw "></i> Menu Item Display <span></span></h1>
	    </div>
    </div>

     <!-- Sessions -->
        @if(session()->has('message'))
        <div class="alert alert-block alert-success">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
            <p>
              {{ session()->get('message') }}
            </p>
        </div>
        @endif
     

      @if(session()->has('deleted'))
      <div class="alert alert-block alert-warning">
          <a class="close" data-dismiss="alert" href="#">×</a>
          <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
          <p>
              {{ session()->get('deleted') }}
          </p>
      </div>
      @endif

      @if($errors->any())
      <div class="alert alert-block alert-danger">
          <a class="close" data-dismiss="alert" href="#">×</a>
          <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
          <p>
              {{$errors->first()}}
          </p>
      </div>
      @endif
    <!-- END Sessions -->

    <section id="widget-grid" class="">

		<article class="col-sm-3 col-md-3 col-lg-3">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"
				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
					<h2>Categories</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<div class="tree smart-form">
							<ul>
								@foreach($data['category'] as $c)
								<li id="ul_{{$c->CategoryID}}">
									<span><i class="fa fa-lg fa-minus-circle"></i> {{$c->CategoryName}}</span>
									<ul >
										
									</ul>
								</li>
								@endforeach
							</ul>
						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>	
			<!-- end widget -->
		</article>
		
		<article class="col-sm-5 col-md-5 col-lg-5">
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" 
                data-widget-deletebutton="false"
                data-widget-editbutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
					<h2><i id="title"></i></h2>
					@if(!empty($data['add']) || !empty($data['delete']))
					<button type="button" class="btn btn-primary btn-sm pull-right" onclick="SaveGroup();" style="margin-left: 6px" >Save</button>
                	@endif
                	<button type="button" class="btn btn-default btn-sm pull-right" onclick="$('#details_table tbody').empty();" style="margin-left: 6px">Clear</button>
				</header>
				<div>
					<div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
					<form id="groupDisplay-form" class="smart-form" novalidate="novalidate" method="POST" action=" ">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<div class="widget-body">
							<input type="hidden" id="groupID" name="groupID">
	                            <table id="details_table" class="table table-striped table-bordered table-hover" width="100%">
	                                <thead>
	                                    <tr>
	                                    	<th width="5%">Code</th>
	                                        <th width="60%" data-class="expand"><i class="text-muted hidden-md hidden-sm hidden-xs"></i>Menu Item</th>
	                                        <th width="35%">Action</th>
	                                    </tr>
	                                </thead>
	                                <tbody></tbody>
	                            </table>
	                    </div><!-- /end widget content -->
	                </form>
				</div>
			</div>
		</article>

		<article class="col-sm-4 col-md-4 col-lg-4">
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-3" 
                data-widget-deletebutton="false"
                data-widget-editbutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
					<h2><i id="title">Menu</i></h2>

				</header>
				<div>
					<div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
					<div class="widget-body">
						<input type="hidden" id="groupID" name="groupID">
                            <table id="menu_table" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                    	 @if(!empty($data['add']))
                                    	<th  width="20%">Action</th>
                                    	@endif
                                    	<th  width="80%">Order</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($data['Menu'] as $menu)
                                	<tr>
                                		@if(!empty($data['add']))
                                		<td>
                                        <button type="button" class="btn btn-sm btn-default"
                                            data-SKU="{{$menu->SKU}}"
                                            data-ItemName="{{$menu->ItemName}}"
                                            onclick="tolist(this)"><i class="fa fa-mail-reply"></i></button>
                                        </td>
                                        @endif
                                        <td>{{$menu->ItemName}}</td>
                                	</tr>
                                	@endforeach
                                </tbody>
                            </table>
                        </div><!-- /end widget content -->
					</div>
				</div>
		</article>
	
	</section>

</div>




@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<script>
window.onload = function(){
    $('#MenuManagement').addClass("active open");
    $("#MenuManagementBlock").css('display', 'block');
    $("#MenuItemDisplay").addClass("active");
    $("#dashboard").removeClass("active");
    $(".breadcrumb").append('<li>Menu Management</li><li><a href="{{URL::asset("/MenuManagement/MenuItems")}}">Menu Item Display</a></li>');
}
</script>
<script type="text/javascript">
	
	$(document).ready(function() {

		@foreach($data['subCategory'] as $c)
			var id = '#ul_'+"{{$c->ParentCode}}";	
			$(id +' > ul').append('<li onclick="menuList(`{{$c->ChildCode}}`,`{{$c->CategoryName}}`)"><span><a>{{$c->CategoryName}}</a></span></li>');
		@endforeach

		$('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');

		$('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
			var children = $(this).parent('li.parent_li').find(' > ul > li');
			if (children.is(':visible')) {
				children.hide('fast');
				$(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
			} else {
				children.show('fast');
				$(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
			}
			e.stopPropagation();
		});
	})

	function menuList($id,$name)
	{
		$('#details_table tbody').empty();
	    $('#title').text($name);
	    $('#groupID').val($id);

	    var postData = {
        'CategoryID' : $id,
    	};

	    //console.log(postData);
	    $.ajax({
	      type: "POST",
	      url: '{{url("/MenuManagement/MenuItemDisplay/GetDisplayGroup")}}',
	      data: postData,
	      headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      },
	      beforeSend: function (xhr) {
	      var token = $('meta[name="csrf_token"]').attr('content');
	      if (token) {
	            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	        }
	      },
	      success: function(data)
	      {
	        var items='';
	        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
	        $.each(data, function(i) {
	            items += "<tr id='item_"+data[i].SKU+"'>";
	            items += "<td><input type='hidden' name='SKU[]' value='"+data[i].SKU+"'>"+data[i].SKU+"</td>";
	            items += "<td><input type='hidden' name='ItemName[]' value='"+data[i].ItemName+"'>"+data[i].ItemName+"</td>";
	            items +="<td>";
	            @if(!empty($data['add']))
	            items += "<button type='button' class='btn btn-sm btn-default fa fa-arrow-up' onclick='rowUp(this)'></button><button type='button' class='btn btn-sm btn-default fa fa-arrow-down' onclick='rowDown(this)'></button>";
	            @endif
	            @if(!empty($data['delete']))
	            items +="<button type='button' class='btn btn-sm btn-default fa fa-times' onclick='(this).closest(`tr`).remove();'></button>";
	            @endif
	            items += "</td></tr>";
	         })

	        $('#details_table tbody').empty().append(items).show();
	        }
	        
	    });
	}

	function tolist(obj)
	{
	    console.log(obj);
	    if($('#item_'+obj.getAttribute('data-SKU')).length == 0)
	    {
	        var items;
	        items += "<tr id='item_"+obj.getAttribute('data-SKU')+"'>";
	        items += "<td><input type='hidden' name='SKU[]' value='"+obj.getAttribute('data-SKU')+"'>"+obj.getAttribute('data-SKU')+"</td>";
	        items += "<td><input type='hidden' name='ItemName[]' value='"+obj.getAttribute('data-ItemName')+"'>"+obj.getAttribute('data-ItemName')+"</td>";
	        items += "<td><button type='button' class='btn btn-sm btn-default fa fa-arrow-up' onclick='rowUp(this)'></button><button type='button' class='btn btn-sm btn-default fa fa-arrow-down' onclick='rowDown(this)'></button><button type='button' class='btn btn-sm btn-default fa fa-times' onclick='(this).closest(`tr`).remove();'></button>";
	        items += "</tr>";
	        $('#details_table').append(items);
	    }
	    else
	        //alert('Hey! You put it there already! Stupid Lah!');
	        $('#item_'+obj.getAttribute('data-SKU')).fadeOut();
	        $('#item_'+obj.getAttribute('data-SKU')).fadeIn();
	        $('#item_'+obj.getAttribute('data-SKU')).fadeOut();
	        $('#item_'+obj.getAttribute('data-SKU')).fadeIn();
	}

	function rowUp(obj)
	{
    
	    var row = $(obj).closest("tr");

	    // Get the previous element in the DOM
	    var previous = row.prev();
	 
	    //Check to see if it is a row
	    if (previous.is("tr")) {
	        // Move row above previous
	        row.detach();
	        previous.before(row);
	 
	        // draw the user's attention to it
	        row.fadeOut();
	        row.fadeIn();
	    }
    // else - already at the top
	}

	function rowDown(obj) {
	    
	    var row = $(obj).closest("tr");

	    // Get the next element in the DOM
	    var next = row.next();
	 
	    //Check to see if it is a row
	    if (next.is("tr")) {
	        // Move row above next
	        row.detach();
	        next.after(row);
	 
	        // draw the user's attention to it
	        row.fadeOut();
	        row.fadeIn();
	    }
	    // else - already at the top
	}

	function SaveGroup()
	{
	    if(confirm('Are you sure you want to save the selected item/s?'))
	    {
	        $('#groupDisplay-form').attr('action',"{{url('MenuManagement/MenuItemDisplay/SaveDisplay')}}");
	        $('#groupDisplay-form').removeAttr('onsubmit');
	        $('#groupDisplay-form').submit();
	    }
	  
	}
</script>
@endsection
