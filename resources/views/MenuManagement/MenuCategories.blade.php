@extends('includes.template')
@section('content')

<!-- Page Header -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><i class="fa fa-edit fa-fw "></i> Menu Categories </h1>
        </div>
    </div>



    <!-- Sessions -->
        @if(session()->has('message'))
        <div class="alert alert-block alert-success">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
            <p>
              {{ session()->get('message') }}
            </p>
        </div>
        @endif
     

      @if(session()->has('deleted'))
      <div class="alert alert-block alert-warning">
          <a class="close" data-dismiss="alert" href="#">×</a>
          <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
          <p>
              {{ session()->get('deleted') }}
          </p>
      </div>
      @endif

      @if($errors->any())
      <div class="alert alert-block alert-danger">
          <a class="close" data-dismiss="alert" href="#">×</a>
          <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
          <p>
              {{$errors->first()}}
          </p>
      </div>
      @endif
    <!-- END Sessions -->

    <div class="row"></div>
    <!-- Main Content-->
    
    <section id="widget-grid" >
        
        <article class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" 
                data-widget-deletebutton="false"
                data-widget-editbutton="true"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false">
                <header>
                    <h2>Categories</h2>
                    @if(!empty($data['add']))
                    <button type="button" class="btn btn-primary btn-sm pull-right" onclick="add();" >Add</button>
                    @endif
                </header>

                <div>                  
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <table id=" " class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                               
                                <th data-class=" "><i class="text-muted hidden-md hidden-sm hidden-xs"></i>Category Name</th>
                                <th data-class=" ">Order Number</th>
                                 @if(!empty($data['edit']))<th data-class=" ">Action</th>@endif
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($data['category'] as $c)
                               <tr onclick='category_details(this)' data-CategoryID="{{$c->CategoryID}}" data-CategoryName="{{$c->CategoryName}}">
                                  
                                   <td>{{$c->CategoryName}}</td>
                                   <td>{{$c->SeqNo}}</td>
                                   @if(!empty($data['edit']))
                                   <td><button type="button" class="btn btn-sm btn-default" id="editUser"
                                            data-id="{{$c->id}}"
                                            data-CategoryID="{{$c->CategoryID}}"
                                            data-CategoryName="{{$c->CategoryName}}"
                                            data-SeqNo="{{$c->SeqNo}}"
                                            onclick="edit(this)">Edit</button>
                                    </td>
                                    @endif
                               </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div><!-- /end widget content -->
    
                </div><!-- /end widget div -->
            </div><!-- /end widget -->
        </article>

        <article class="col-xs-3 col-sm-3 col-md-3 col-lg-3">          
           <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" 
                data-widget-deletebutton="false"
                data-widget-editbutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false">
                <header>
                    <h2>Sub-Categories</h2>
                    @if(!empty($data['addsub']))
                    <button type="button" class="btn btn-primary btn-sm pull-right" onclick="subadd();" >Add</button>
                    @endif
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                    </div><!-- /end widget edit box -->
                    
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-hide="phone">Category Name</th>
                                    @if(!empty($data['editsub']))
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($data['subCategory'] as $sub)
                               <tr>
                                   <td>{{$sub->CategoryName}}</td>
                                   @if(!empty($data['editsub']))
                                   <td><button type="button" class="btn btn-sm btn-default"
                                            data-subCategoryID="{{$sub->CategoryID}}"
                                            data-subCategoryName="{{$sub->CategoryName}}"
                                            onclick="editSub(this)">Edit</button>
                                    @endif
                                    @if(!empty($data['edit']))
                                        <button type="button" class="btn btn-sm btn-default"
                                            data-subCategoryID="{{$sub->CategoryID}}"
                                            data-subCategoryName="{{$sub->CategoryName}}"
                                            onclick="tolist(this)"><i class="fa fa-mail-forward"></i></button>
                                    </td>
                                    @endif
                               </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div><!-- /end widget content -->
                </div><!-- /end widget div -->
            </div><!-- /end widget -->
        </article> 

        <article class="col-xs-4 col-sm-4 col-md-3 col-lg-4">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
                <header>
                <span class="widget-icon"></span>
                <h2><b><i id="title"></i></b></h2>
                <button type="button" class="btn btn-primary btn-sm pull-right" onclick="saveGroup();" >Save</button>
                <button type="button" class="btn btn-default btn-sm pull-right" onclick="$('#details_table tbody').empty();" >Clear</button>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                    </div><!-- /end widget edit box -->
                    <!-- widget content -->
                    
                    <form id="groupCategory-form" class="smart-form" novalidate="novalidate" method="POST" action=" ">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="widget-body no-padding"> 
                            <input type="hidden" id="groupID" name="groupID">
                            <table id="details_table" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand"><i class="text-muted hidden-md hidden-sm hidden-xs"></i>SubCategory</th>
                                        @if(!empty($data['edit']))
                                        <th>Order</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div><!-- /end widget content -->
                    </form>
                </div><!-- /end widget div -->
            </div>
        </article> 
    </section>
</div>

<!--======================= Modal ====================-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">
                    <!-- <img src="img/logo.png" width="150" alt="SmartAdmin"> -->
                    Category
                </h4>
            </div>
            <div class="modal-body no-padding">

                <form id="category-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('/MenuManagement/MenuCategories/AddCategory')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <fieldset>
                        <section>
                            <div class="row">
                                <label class="label col col-4">Category Name</label>
                                <section class="col col-8">
                                    <label class="input">
                                        <input type="hidden" id="CategoryID" name="CategoryID">
                                        <input type="text" id="categoryName" name="categoryName" placeholder="Category Name" autocomplete="off">
                                        <input type="hidden" id="id" name="id" value="">
                                    </label>
                                </section>
                            </div>
                        </section>

                        <section>
                            <div class="row">
                                <label class="label col col-4">Sequence Number</label>
                                <section class="col col-8">
                                    <label class="input">
                                        <input type="text" id="SeqNo" name="SeqNo">
                                    </label>
                                </section>
                            </div>
                        </section>

                    </fieldset>

                    <footer>
                        <button type="submit" class="btn btn-primary" id="newOrUpdate"> Create </button>
                        @if(!empty($data['delete']))
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete" onclick="deleteCategory()"> Delete </button>
                        @endif

                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--======================= Modal Sub Category ====================-->
<div class="modal fade" id="myModalSubCat" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">
                    <!-- <img src="img/logo.png" width="150" alt="SmartAdmin"> -->
                    Sub Category
                </h4>
            </div>
            <div class="modal-body no-padding">

                <form id="subcat-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('/MenuManagement/MenuCategories/AddSubCategory')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <fieldset>
                        <section>
                            <div class="row">
                                <label class="label col col-4">Sub Category
                                </label>
                                <section class="col col-8">
                                    <label class="input">
                                        <input type="text" id="subcategoryName" name="subcategoryName" placeholder="Sub Category" autocomplete="off">
                                        <input type="hidden" id="subCategoryID" name="subCategoryID">
                                    </label>
                                </section>
                            </div>
                        </section>
                    </fieldset>

                    <footer>
                        <button type="submit" class="btn btn-primary" id="subNewOrUpdate"> Create </button>
                        @if(!empty($data['deletesub']))
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="subdelete" onclick="deleteSubCategory()"> Delete </button>
                        @endif
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@include('includes.scripts')
<!-- ================================= JAVASCRIP ======================================= -->
<script>
window.onload = function(){
  $('#MenuManagement').addClass("active open");
  $("#MenuManagementBlock").css('display', 'block');
  $("#MenuCategories").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Menu Management</li><li><a href="{{URL::asset("/MenuManagement/MenuCategories")}}">Menu Categories</a></li>');

  $('#delete').hide();
  $('#subdelete').hide();

}

function add()
{
    $('#id').val('');
    $('#categoryName').val('');
    $('#SeqNo').val('');
    $('#myModal').modal('show');

}

function subadd()
{
    $('#subCategoryID').val('');
    $('#subcategoryName').val('');
    $('#myModalSubCat').modal('show');
}

function edit(obj)
{
    $('.modal-title').text('Edit '+obj.getAttribute('data-CategoryName'));
    $('#newOrUpdate').text('Update').addClass('btn-warning');
    $('#delete').show();
    $('#userID').attr('readonly', true).css('background-color' , '#DEDEDE');

    $('#CategoryID').val(obj.getAttribute('data-CategoryID'));
    $('#categoryName').val(obj.getAttribute('data-CategoryName'));
    $('#SeqNo').val(obj.getAttribute('data-SeqNo'));
    $('#myModal').modal('show');
}

function deleteCategory(){
    if(confirm('Are you sure you want to delete the selected item?'))
    {
        $('#category-form').attr('action',"{{url('MenuManagement/MenuCategories/DeleteCategory')}}");
        $('#category-form').removeAttr('onsubmit');
        $('#category-form').submit();
    }
}

function deleteSubCategory(){
    if(confirm('Are you sure you want to delete the selected item?'))
    {
        $('#subcat-form').attr('action',"{{url('MenuManagement/MenuCategories/DeleteSubCategory')}}");
        $('#subcat-form').removeAttr('onsubmit');
        $('#subcat-form').submit();
    }
}

function editSub(obj)
{
    $('.modal-title').text('Edit '+obj.getAttribute('data-subCategoryName'));
    $('#subNewOrUpdate').text('Update').addClass('btn-warning');
    $('#subdelete').show();

    $('#subCategoryID').val(obj.getAttribute('data-subCategoryID'));
    $('#subcategoryName').val(obj.getAttribute('data-subCategoryName'));
    $('#myModalSubCat').modal('show');
}

function category_details(obj)
{
    console.log(obj);
    $('#details_table tbody').empty();
    $('#title').text(obj.getAttribute('data-categoryName'));
    $('#groupID').val(obj.getAttribute('data-CategoryID'));

    var postData = {
        'CategoryID' : obj.getAttribute('data-CategoryID'),
    };

    //console.log(postData);
    $.ajax({
      type: "POST",
      url: '{{url("/MenuManagement/MenuCategories/GetGroup")}}',
      data: postData,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function (xhr) {
      var token = $('meta[name="csrf_token"]').attr('content');
      if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      success: function(data)
      {
        var items='';
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
        $.each(data, function(i) {
            items += "<tr id='item_"+data[i].CategoryID+"'>";
            items += "<td><input type='hidden' name='sub_item[]' value='"+data[i].CategoryID+"'>"+data[i].CategoryName+"</td>";
            @if(!empty($data['edit']))
            items += "<td><button type='button' class='btn btn-sm btn-default fa fa-arrow-up' onclick='rowUp(this)'></button><button type='button' class='btn btn-sm btn-default fa fa-arrow-down' onclick='rowDown(this)'></button><button type='button' class='btn btn-sm btn-default fa fa-times' onclick='(this).closest(`tr`).remove();'></button></td>";
            @endif
            items += "</tr>";
         })

        $('#details_table tbody').empty().append(items).show();
        }
        
    });

}

function tolist(obj)
{
    //console.log(obj);
    if($('#item_'+obj.getAttribute('data-subCategoryID')).length == 0)
    {
        var items;
        items += "<tr id='item_"+obj.getAttribute('data-subCategoryID')+"'>";
        //items +="<td>"+document.getElementById('details_table').rows.length+"</td>";
        items += "<td><input type='hidden' name='sub_item[]' value='"+obj.getAttribute('data-subCategoryID')+"'>";
        items += obj.getAttribute('data-subCategoryName')+"</td>";
        items += "<td><button type='button' class='btn btn-sm btn-default fa fa-arrow-up' onclick='rowUp(this)'></button><button type='button' class='btn btn-sm btn-default fa fa-arrow-down' onclick='rowDown(this)'></button><button type='button' class='btn btn-sm btn-default fa fa-times' onclick='(this).closest(`tr`).remove();'></button></td>";
        items += "</tr>";
        $('#details_table').append(items);
    }
    else
        //alert('Hey! You put it there already! Stupid Lah!');
        //$('#item_'+obj.getAttribute('data-subid')).closest('tr').remove();
        $('#item_'+obj.getAttribute('data-subCategoryID')).fadeOut();
        $('#item_'+obj.getAttribute('data-subCategoryID')).fadeIn();
        $('#item_'+obj.getAttribute('data-subCategoryID')).fadeOut();
        $('#item_'+obj.getAttribute('data-subCategoryID')).fadeIn();
}

function rowUp(obj) {
    
    var row = $(obj).closest("tr");

    // Get the previous element in the DOM
    var previous = row.prev();
 
    //Check to see if it is a row
    if (previous.is("tr")) {
        // Move row above previous
        row.detach();
        previous.before(row);
 
        // draw the user's attention to it
        row.fadeOut();
        row.fadeIn();
    }
    // else - already at the top
}

function rowDown(obj) {
    
    var row = $(obj).closest("tr");

    // Get the next element in the DOM
    var next = row.next();
 
    //Check to see if it is a row
    if (next.is("tr")) {
        // Move row above next
        row.detach();
        next.after(row);
 
        // draw the user's attention to it
        row.fadeOut();
        row.fadeIn();
    }
    // else - already at the top
}

function saveGroup()
{
    if(confirm('Are you sure you want to save the selected item/s?'))
    {
        $('#groupCategory-form').attr('action',"{{url('MenuManagement/MenuCategories/SaveGroup')}}");
        $('#groupCategory-form').removeAttr('onsubmit');
        $('#groupCategory-form').submit();
    }
  
}
</script>
@endsection
