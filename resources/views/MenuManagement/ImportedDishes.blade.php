@extends('includes.template')
@section('content')
<head>
</head>
@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<script>
window.onload = function(){
  $('#MenuManagement').addClass("active open");
  $("#MenuManagementBlock").css('display', 'block');
  $("#ImportedDishes").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Settings Management</li><li><a href="{{URL::asset('/SettingsManagement/SystemModules')}}">System Modules</a></li>');
}
</script>

@endsection
