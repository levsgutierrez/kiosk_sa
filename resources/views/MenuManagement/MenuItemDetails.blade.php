@extends('includes.template')
@section('content')
<head>
</head>


@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<script>
window.onload = function(){
  $('#MenuManagement').addClass("active open");
  $("#MenuManagementBlock").css('display', 'block');
  $("#MenuItemDetails").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Menu Management</li><li><a href="{{URL::asset('/MenuManagement/MenuItemDetails')}}">Menu Item Details</a></li>');
}
</script>
@endsection
