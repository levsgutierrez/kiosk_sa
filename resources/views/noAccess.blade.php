<!DOCTYPE html>
<html lang="en-us" id="lock-page">
	<head>
		<meta charset="utf-8">
		<title> SmartAdmin</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<!-- #CSS Links -->
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/bootstrap.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('css/font-awesome.min.css')}}">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-production-plugins.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-production.min.css')}}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-skins.min.css')}}">

		<!-- SmartAdmin RTL Support -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/smartadmin-rtl.min.css')}}">

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/demo.min.css')}}">

		<!-- page related CSS -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/lockscreen.min.css')}}">

		<!-- #FAVICONS -->
		<link rel="shortcut icon" href="{{URL::asset('images/favicon/favicon.ico')}}" type="image/x-icon">
		<link rel="icon" href="{{URL::asset('images/favicon/favicon.ico')}}" type="image/x-icon">

		<!-- #GOOGLE FONT -->
		<link rel="stylesheet" href="{{URL::asset('https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700')}}">


	</head>
	
	<body>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->

			<form class="lockscreen animated flipInY" action="index.html">
				<div class="logo">
					<h1 class="semi-bold"><i class="fa fa-warning text-warning"></i> Permission Required</h1>
				</div>
				<div>
					<img src="img/avatars/sunny-big.png" alt="" width="120" height="120" />
					<div>
						<h1><i class="fa fa-user fa-3x text-muted air air-top-right hidden-mobile"></i>Hello {{Auth::user()->name}} <small><i class="fa fa-lock text-muted"></i> &nbsp;It seems you dont have permission to access this page</small></h1>
						<p class="text-muted">
							Please contact your administrator
						</p>

						
						<p class="no-margin margin-top-5">
							 Want to get back? <a href="{{ redirect()->back()->getTargetUrl() }}"> Click here <i class="fa fa-mail-reply"></i> </a> 
						</p>
						<p class="no-margin margin-top-5">
							Logged as someone else? <a href="{{url('admin/logout')}}"> Click here</a>
						</p>
					</div>

				</div>
				<p class="font-xs margin-top-5">
					Copyright MediaOnline International 2014-2020.

				</p>
			</form>

		</div>

		<!--================================================== -->	

		@include('includes.scripts')

	</body>
</html>