@extends('includes.template')
@section('content')

<head>
  <style>
    .input-disabled{background-color:#EBEBE4 !important;border:1px solid #ABADB3;padding:2px 1px;}
  </style>
</head>

<!-- Page Header -->
<div id="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-left: 0.8vw;">
      <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i>  Topup Account Transactions</h1>
    </div>
</div>

  <!-- Sessions -->
      @if(session()->has('message'))
      <div class="alert alert-block alert-success">
          <a class="close" data-dismiss="alert" href="#">×</a>
          <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
          <p>
            {{ session()->get('message') }}
          </p>
      </div>
      @endif


    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif
  <!-- END Sessions -->

  <div class="row"></div>
  <div class="row" style="margin-left:0.5vw;">
    <!-- Buttons / Search Buttons -->
    <fieldset>
      <div class="col-md-2">
        <label> Select a Date Range </label>
      </div>
      <br/>
      <br/>
      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="fromDate" type="text" placeholder="From">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>

      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="toDate" type="text" placeholder="Select a date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <button type="button" id="clearDatesBtn" class="btn btn-default">Clear</button>
        <button type="button" id="searchByDate" class="btn btn-default">Search</button>
      </div>

        <div class="col-md-3"></div>
        <div class="MenuButtons text-right" style="padding: 1vh; margin-right:1.5vw;">
          <button type="button" class="btn btn-primary" onclick="showAddModal();" style="margin-left:5vw; margin-right:0.2vw;" >TopUp Account Card</button>
          <button type="button" class="btn btn-default" style="margin-right:0.2vw;">Reprint Receipt</button>
          <!-- <button type="button" id="exportBtn" class="btn btn-success" style="margin-right:0.2vw;">Export</button> -->
          <div id="exportBtn" style="margin-top: 1.5vh;"></div>
        </div>
    </fieldset>
  </div>

  <!-- Main Content -->
  <section id="widget-grid" class="">
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  		<!-- Widget ID (each widget will need unique ID)-->
  		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
  			<header>
  				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
  				<h2>Transactions Details</h2>

  			</header>

  			<!-- widget div-->
  			<div>
  				<!-- widget edit box -->
  				<div class="jarviswidget-editbox"></div><!-- end widget edit box -->

  					<!-- widget content -->
  					<div class="widget-body no-padding">
  						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                  <!-- <div id="exportBtn" class="pull-right" style="margin-top: 1.5vh;"></div> -->
  							<thead>
                  <meta name="csrf_token" content="{{ csrf_token() }}" />
  								<tr>
  									<th>Id</th>
  									<th>Account Card Number</th>
  									<th>Members Name</th>
  									<th>Initial Balance</th>
  									<th>Topup Amount</th>
  									<th>Gift Amount</th>
  									<th>Total Amount</th>
  									<th>Invoice Amount</th>
  									<th>Payment Types</th>
  									<th>Created At</th>
  									<th>Created By</th>
  									<th>Invoice</th>
  									<th>Invoice Date</th>
  									<th>Operator</th>
  								</tr>
                </thead>
                <tbody id="topUpAccountsBody">
                  @foreach($data['members_details'] as $topup_accounts)
                    <tr>
                      <td>{{$topup_accounts->topupId}}</td>
                      <td>{{$topup_accounts->accountCardNumber}}</td>
                      <td>{{$topup_accounts->membersDetailsName}}</td>
                      <td>
                        @if($topup_accounts->initialBalance == .00)
                           0.00
                        @else
                          {{$topup_accounts->initialBalance}}
                        @endif
                      </td>
                      <td>{{$topup_accounts->topupAmount}}</td>
                      <td>
                        @if($topup_accounts->giftAmount == .00)
                           0.00
                        @else
                          {{$topup_accounts->giftAmount}}
                      @endif
                      </td>
                      <td>{{$topup_accounts->totalAmount}}</td>
                      <td>
                        @if($topup_accounts->invoiceAmount == .00)
                           0.00
                        @else
                          {{$topup_accounts->invoiceAmount}}
                      @endif
                      </td>
                      <td>{{$topup_accounts->paymentType}}</td>
                      <td>{{$topup_accounts->created_at}}</td>
                      <td>{{$topup_accounts->createdBy}}</td>
                      <td>
                        @if($topup_accounts->invoiceAmountModal == "" || $topup_accounts->invoiceAmountModal == null)
                          <button class="btn btn-success"
                          data-topupId="{{$topup_accounts->topupId}}"
                          data-accountCardNumber="{{$topup_accounts->accountCardNumber}}"
                          data-membersDetailsName="{{$topup_accounts->membersDetailsName}}"
                          data-initialBalance="{{$topup_accounts->initialBalance}}"
                          data-topupAmount="{{$topup_accounts->topupAmount}}"
                          data-giftAmount="{{$topup_accounts->giftAmount}}"
                          data-totalAmount="{{$topup_accounts->totalAmount}}"
                          data-invoiceAmount="{{$topup_accounts->invoiceAmount}}"
                          data-paymentType="{{$topup_accounts->paymentType}}"
                          data-createdBy="{{$topup_accounts->createdBy}}"
                          onclick="invoice_btn(this);"
                          rel="tooltip" data-placement="top" title="Invoice"> Invoice
                          </button>
                        @else
                          {{$topup_accounts->invoiceAmountModal}}
                        @endif
                  </td>
                    <td>{{$topup_accounts->invoiceCreatedAt}}</td>
                    <td>{{$topup_accounts->invoiceOperator}}</td>
                  </tr>
                @endforeach
    					   </tbody>
    					</table>
    			</div><!-- end widget content -->
    		</div><!-- end widget div -->
  		</div><!-- end widget -->
  	</article>
  </section>
  <!-- Total Deposit -->
  <div class="col-md-8">
    <!-- <label>Total Members Count: <p id="membersCount"></p></label> -->
    <div class="col-md-4 pull-left"><label>Total Topup Amount: $<span id="totalTopupAmount"></span></label></div>
    <div class="col-md-4 pull-left"><label>Total Gift Amount: $<span id="totalGiftAmount"></span></label></div>
  </div>
</div> <!-- /content -->

<!-- ================================================================================ -->
<!--                             TOP UP ACCOUNT MODAL                                 -->
<!-- ================================================================================ -->
<div class="modal fade" id="topupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Topup Membership Account</h4>
            </div>
            <div class="modal-body no-padding">
                <form id="topup-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('MembershipAccounts/TopupAccounts/TopUpMembersAccount')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" id="id" name="id">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Account Card Number</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-credit-card "></i>
                              <input type="text" id="accountCardNumber" name="accountCardNumber" data-mask="9999-9999-9999-9999" aria-required="true" aria-invalid="false" class="valid">
                            </label>
                          </section>
                        </div>
                      </section>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Members' Name</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                              <input type="text" id="membersName" name="membersName" readonly>
                            </label>
                          </section>
                        </div>
                      </section>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Initial Balance</label>
                          <section class="col col-8">
                            <label class="input">
                              <input type="number" id="initialBalance" name="initialBalance" readonly>
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Amount to TopUp</label>
                          <section class="col col-8">
                            <label class="input">
                              <input type="number" id="topupAmount" name="topupAmount">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Gift Amount</label>
                          <section class="col col-8">
                            <label class="input">
                              <input type="number" id="giftAmount" name="giftAmount">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Total Balance</label>
                          <section class="col col-8">
                            <label class="input">
                              <input type="number" id="totalAmount" name="totalAmount">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Invoice Amount</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-calendar"></i>
                              <input type="text" id="invoiceAmount" name="invoiceAmount" >
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Payment Methods</label>
                          <section class="col col-8">
                            <label class="select">
                              <select class="form-control" id="paymentType" name="paymentType">
                                <option value="0" selected disabled>Select One</option>
                                <option value="Cash">Cash</option>
                                <option value="NETS">NETS</option>
                                <option value="VISA/MASTERS">VISA/MASTERS</option>
                              </select> <i></i>
                            </label>
                          </section>
                        </div>
                      </section>

                    </fieldset>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="newOrUpdate">Add</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- ================================================================================ -->
<!--                             TOP UP ACCOUNT MODAL                                 -->
<!-- ================================================================================ -->
<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Invoice Amount</h4>
            </div>
            <div class="modal-body no-padding">
                <form id="topup-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('MembershipAccounts/TopupAccounts/AddInvoiceAmount')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" id="id" name="id">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-6">Please Enter Invoice Amount:</label>
                          <section class="col col-6">
                            <label class="input"> <i class="icon-prepend fa fa-credit-card "></i>
                              <input type="number" id="invoiceAmountModal" name="invoiceAmountModal">
                            </label>
                          </section>
                        </div>
                      </section>
                    </fieldset>

                    <input type="hidden" id="topUpIdModal" name="topUpIdModal"/>
                    <input type="hidden" id="accountCardNumberModal" name="accountCardNumberModal"/>
                    <input type="hidden" id="operatorModal" name="operatorModal"/>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="invoiceAmountBtn">Enter</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@include('includes.scripts')

<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


<script>
window.onload = function(){
	$(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset("/MembershipAccounts/TopupAccounts")}}">Topup Accounts</a></li>');
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#TopupAccounts").addClass("active");
	$("#dashboard").removeClass("active");
}
$(document).ready(function() {
  retrieve_data();
  getTotalAmount();
  search_by_date();
  retrieveTopupAccountBalance();
  getTotalTopupAmount();
  getTotalGiftAmount();
  clearDates();

	var errorClass = 'invalid';
  var errorElement = 'em';

  var $checkoutForm = $('#kiosk-form').validate({
      errorClass      : errorClass,
      errorElement    : errorElement,
      highlight: function(element) {
          $(element).parent().removeClass('state-success').addClass("state-error");
          $(element).removeClass('valid');
      },
      unhighlight: function(element) {
          $(element).parent().removeClass("state-error").addClass('state-success');
          $(element).addClass('valid');
      },

      // Rules for form validation
      rules : {
          kioskID : {
              required : true
          },
      },

      // Messages for form validation
      messages : {
          kioskID : {
              required : 'Please enter your preffered Kiosk ID'
          },
      },

      // Do not change code below
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });

  /* Spinners */
  $("#topupAmount").spinner({
    step : 0.01,
    numberFormat : "n"
  });
  $("#giftAmount").spinner({
    step : 0.01,
    numberFormat : "n"
  });
  $("#invoiceAmountModal").spinner({
    step : 0.01,
    numberFormat : "n"
  });

  var responsiveHelper_datatable_fixed_column = undefined;
  var breakpointDefinition = {
  	tablet : 1024,
  	phone : 480
  };

	/* COLUMN FILTER  */
  var otable = $('#datatable_fixed_column').DataTable({
	"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
	"autoWidth" : true,
	"preDrawCallback" : function() {
		// Initialize the responsive datatables helper once.
		if (!responsiveHelper_datatable_fixed_column) {
			responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
		}
	},
	"rowCallback" : function(nRow) {
		responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
	},
	"drawCallback" : function(oSettings) {
		responsiveHelper_datatable_fixed_column.respond();
	}
});
var buttons = new $.fn.dataTable.Buttons(otable, {
  buttons: ['csv', 'excel', 'pdf', 'print']
  }).container().appendTo($('#exportBtn'));

  // Apply the filter
  $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
      otable
          .column( $(this).parent().index()+':visible' )
          .search( this.value )
          .draw();
  });/* END COLUMN FILTER */

  // Date Range Picker
   $("#fromDate").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 2,
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#toDate").datepicker("option", "minDate", selectedDate);
       },
       dateFormat: 'yy-mm-dd'
   });
   $("#toDate").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 2,
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#fromDate").datepicker("option", "maxDate", selectedDate);
       },
       dateFormat: 'yy-mm-dd'
   });

   function getTotalTopupAmount(){
      $.ajax({
          type: "GET",
          url: "{{url('MembershipAccounts/TopupAccounts/GetTotalTopupAmount')}}",
          dataType: "json",
          success: function(data)
          {
           $("#totalTopupAmount").text(data);
           },
       });
    }

    function getTotalGiftAmount(){
       $.ajax({
           type: "GET",
           url: "{{url('MembershipAccounts/TopupAccounts/GetTotalGiftAmount')}}",
           dataType: "json",
           success: function(data)
           {
            $("#totalGiftAmount").text(data);
            },
        });
     }

   function getTotalAmount()
   {
     $("#topupAmount").change(function () {
       var topupAmount = $('#topupAmount').val();
       if(topupAmount == null || topupAmount == '')
         topupAmount = 0;

       var giftAmount = $('#giftAmount').val();
       if(giftAmount == null || giftAmount == '')
         giftAmount = 0;

       var initialBalance = $('#initialBalance').val();
       if(initialBalance == null || initialBalance == '')
         initialBalance = 0;

        var totalAmount = (parseFloat(topupAmount) + parseFloat(giftAmount) + parseFloat(initialBalance));
        $('#totalAmount').val(totalAmount);
      });

      $("#giftAmount").change(function () {
        var topupAmount = $('#topupAmount').val();
        if(topupAmount == null || topupAmount == '')
          topupAmount = 0;

        var giftAmount = $('#giftAmount').val();
        if(giftAmount == null || giftAmount == '')
          giftAmount = 0;

        var initialBalance = $('#initialBalance').val();
        if(initialBalance == null || initialBalance == '')
          initialBalance = 0;

        var totalAmount = (parseFloat(topupAmount) + parseFloat(giftAmount) + parseFloat(initialBalance));
        $('#totalAmount').val(totalAmount);
      });

      $(" #initialBalance").change(function () {
        var topupAmount = $('#topupAmount').val();
        var giftAmount = $('#giftAmount').val();
        var initialBalance = $('#initialBalance').val();

        var totalAmount = (parseFloat(topupAmount) + parseFloat(giftAmount) + parseFloat(initialBalance));
        $('#totalAmount').val(totalAmount);
       });
     }

     function retrieve_data(){
       $.ajaxSetup({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
       });
       $("#accountCardNumber").change(function (){
       $.ajax({
           type: "POST",
           url: "{{url('MembershipAccounts/TopupAccounts/RetrieveTopupAccountsDetails')}}",
           data: {
                 'accountCardNumber': $('input[name=accountCardNumber]').val(),
           },
           dataType: "json",
           beforeSend: function (xhr)
           {
             var token = $('meta[name="csrf_token"]').attr('content');
             if (token)
             {
               return xhr.setRequestHeader('X-CSRF-TOKEN', token);
             }
           },
           success: function(data)
           {
             var accountBalance = data[0].accountBalance;
             if(accountBalance == .00)
               accountBalance = 0.00;
             // alert(membersName);
             $("#membersName").val(data[0].membersName);
             $("#initialBalance").val(accountBalance);
             // var initialBalance = $(this).find('initialBalance').text();
            },
        });
      });
    }

   function search_by_date(){
       $("#searchByDate").click(function() {
         var fromDate = $("#fromDate").val();
         var toDate = $("#toDate").val();
         $.ajax({
             type: "POST",
             url: "{{url('MembershipAccounts/TopupAccounts/GetTopupAccountsByDate')}}",
             data: {
                   'fromDate': fromDate,
                   'toDate': toDate
             },
             dataType: "json",
             success: function(data)
             {
               console.log(data);
                 $("#topUpAccountsBody").empty();

                 for(var i = 0; i < data.length; i++)
                 {
                     var myvar = '<tr>'+
                     '<td>'+ data[i].topupId +'</td>'+
                     '<td>'+ data[i].accountCardNumber +'</td>'+
                     '<td>'+ data[i].membersDetailsName +'</td>'+
                     '<td>'+ data[i].initialBalance +'</td>'+
                     '<td>'+ data[i].topupAmount +'</td>'+
                     '<td>'+ data[i].giftAmount +'</td>'+
                     '<td>'+ data[i].topupId +'</td>'+
                     '<td>'+ data[i].invoiceAmount +'</td>'+
                     '<td>'+ data[i].paymentType +'</td>'+
                     '<td>'+ data[i].created_at +'</td>'+
                     '</tr>';
                     $("#topUpAccountsBody").append(myvar);
                 }
              },
          });
     });
   }

   function clearDates(){
     $("#clearDatesBtn").click(function() {
       $('#fromDate').val('').datepicker("refresh");
       $('#toDate').val('').datepicker("refresh");
       $('#fromDate').datepicker('setDate', null);
       $('#toDate').datepicker('setDate', null);
       var table = $('#datatable_fixed_column').DataTable({
       retrieve: true
     });
     table.draw();
     });
   }

   // To retrieve topped up account balance if any
   function retrieveTopupAccountBalance(){
     $("#accountCardNumber").change(function() {
       var accountCardNumber = $("#accountCardNumber").val();
       console.log(accountCardNumber);
       $.ajax({
         type: "POST",
         url: "{{url('MembershipAccounts/TopupAccounts/GetTopupAccountsByCardNumber')}}",
         data: {
                 'accountCardNumber': accountCardNumber
               },
             dataType: "json",
             success: function(data)
             {
               $("#initialBalance").val(data.accountBalance);
             },
          });
      })
   }
 }); //documnet.ready

 function invoice_btn(obj)
 {
   $("#topUpIdModal").val(obj.getAttribute('data-topupId'));
   $("#accountCardNumberModal").val(obj.getAttribute('data-accountCardNumber'));
   $("#operatorModal").val(null);

   $('#invoiceModal').modal('show');
 }

 /* Add topup transcation */
 function showAddModal()
 {
   $('#paymentTypes').val('');
   $("#registeredDate").addClass('input-disabled');
   $("#membersName").prop("readonly", true);
   $("#membersName").addClass('input-disabled');

   $("#initialBalance").prop("readonly", true);
   $("#initialBalance").addClass('input-disabled');

   $("#totalAmount").prop("readonly", true);
   $("#totalAmount").addClass('input-disabled');

   $('#topupModal').modal({backdrop: 'static', keyboard: false});
   $('#topupModal').modal('show');
 }

</script>
@endsection
