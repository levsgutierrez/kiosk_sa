@extends('includes.template')
@section('content')

<head>
  <style>
    .input-disabled{background-color:#EBEBE4 !important;border:1px solid #ABADB3;padding:2px 1px;}
  </style>
</head>

<!-- Page Header -->
<div id="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-left: 0.8vw;">
      <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i>  Expenditure List</h1>
    </div>
  </div>

  @if(session()->has('message'))
  <div class="alert alert-block alert-success">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
      <p>
          {{ session()->get('message') }}
      </p>
  </div>
  @endif

  @if(session()->has('deleted'))
  <div class="alert alert-block alert-warning">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
      <p>
          {{ session()->get('deleted') }}
      </p>
  </div>
  @endif

  @if($errors->any())
  <div class="alert alert-block alert-danger">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
      <p>
          {{$errors->first()}}
      </p>
  </div>
  @endif

  <div class="row"></div>
  <div class="row" style="margin-left:0.5vw;">
    <!-- Buttons / Search Buttons -->
    <fieldset>
      <div class="col-md-2">
        <label> Select a Date Range </label>
      </div>
      <br/>
      <br/>
      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="fromDate" type="text" placeholder="From">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>

      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="toDate" type="text" placeholder="Select a date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>

      <!-- Checkbox selections -->
      <div class="col-sm-3 smart-form" style="margin-top: 0.8vh;">
      <section>
        <div class="inline-group" id="menuItemCheckbox">
          <label class="checkbox" style="margin-top: -0.1vh;">
            <input type="checkbox" value="Topup" name="checkbox-inline">
            <i></i>Topup</label>
          <label class="checkbox">
            <input type="checkbox" value="Expenses" name="checkbox-inline">
            <i></i>Expenses</label>
          <label class="checkbox">
            <input type="checkbox" value="Deposit" name="checkbox-inline">
            <i></i>Deposit</label>
        </div>
      </section>
    </div>

      <div class="col-md-2" style="margin-left: -5vw;">
        <button type="button" id="clearDatesBtn" class="btn btn-default">Clear</button>
        <button type="button" id="searchByDate" class="btn btn-default">Search</button>
      </div>

        <div class="col-md-2"></div>
        <div class="MenuButtons text-right" style="padding: 1vh; margin-right:1.5vw;">
        @if(!empty($data['export']))
          <div id="exportBtn" style="margin-top: 1.5vh;"></div>
        @endif
        </div>
    </fieldset>
  </div>

  <!-- Main Content -->
  <section id="widget-grid" class="">
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  		<!-- Widget ID (each widget will need unique ID)-->
  		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
  			<header>
  				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
  				<h2>Transactions Details</h2>
  			</header>

  			<!-- widget div-->
  			<div>
  				<!-- widget edit box -->
  				<div class="jarviswidget-editbox"></div><!-- end widget edit box -->

  					<!-- widget content -->
  					<div class="widget-body no-padding">
  						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                  <!-- <div id="exportBtn" class="pull-right" style="margin-top: 1.5vh;"></div> -->
  							<thead>
                  <meta name="csrf_token" content="{{ csrf_token() }}" />
  								<tr>
  									<th>id</th>
  									<th>Account Number</th>
  									<th>Account Card Number</th>
  									<th>Members Name</th>
  									<th>Action Date</th>
  									<th>Action Type</th>
  									<th>Amount</th>
  									<th>Balance</th>
  									<th>Action Id</th>
  									<th>Operator</th>
  								</tr>
                </thead>
                <tbody id="expenditureListBody">
                  @foreach($data['members_trade_logs'] as $members_trade_logs)
                    <tr>
                      <td>{{$members_trade_logs->id}}</td>
                      <td>{{$members_trade_logs->AccountNumber}}</td>
                      <td>{{$members_trade_logs->accountCardNumber}}</td>
                      <td>{{$members_trade_logs->MembersName}}</td>
                      <td>{{$members_trade_logs->created_at}}</td>
                      <td>{{$members_trade_logs->actionType}}</td>
                      <td>{{$members_trade_logs->amount}}</td>
                      <td>
                        @if($members_trade_logs->AccountBalance == null)
                           0.00
                        @elseif($members_trade_logs->AccountBalance == .00)
                          0.00
                        @else
                          {{$members_trade_logs->AccountBalance}}
                      @endif
                      </td>
                      <td>{{$members_trade_logs->actionTypeId}}</td>
                      <td>{{$members_trade_logs->createdBy}}</td>
                  </tr>
                  @endforeach
    					   </tbody>
    					</table>
    			</div><!-- end widget content -->
    		</div><!-- end widget div -->
  		</div><!-- end widget -->
  	</article>
  </section>
  <!-- Total Deposit -->
  <div class="col-md-8">
    <!-- <label>Total Members Count: <p id="membersCount"></p></label> -->
    <div class="col-md-4 pull-left"><label>Total Topup Amount: $<span id="totalTopupAmount"></span></label></div>
    <div class="col-md-4 pull-left"><label>Total Gift Amount: $<span id="totalGiftAmount"></span></label></div>
  </div>
</div> <!-- /content -->


@include('includes.scripts')

<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


<script>
window.onload = function(){
	$(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset('/MembershipAccounts/TopupAccounts')}}">Topup Accounts</a></li>');
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#ExpenditureList").addClass("active");
	$("#dashboard").removeClass("active");
}
$(document).ready(function() {
  search_by_date();
  clearDates();

  /* Spinners */
  $("#topupAmount").spinner({
    step : 0.01,
    numberFormat : "n"
  });
  $("#giftAmount").spinner({
    step : 0.01,
    numberFormat : "n"
  });

  var responsiveHelper_datatable_fixed_column = undefined;
  var breakpointDefinition = {
  	tablet : 1024,
  	phone : 480
  };

	/* COLUMN FILTER  */
  var otable = $('#datatable_fixed_column').DataTable({
	"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
	"autoWidth" : true,
	"preDrawCallback" : function() {
		// Initialize the responsive datatables helper once.
		if (!responsiveHelper_datatable_fixed_column) {
			responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
		}
	},
	"rowCallback" : function(nRow) {
		responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
	},
	"drawCallback" : function(oSettings) {
		responsiveHelper_datatable_fixed_column.respond();
	}
});
var buttons = new $.fn.dataTable.Buttons(otable, {
  buttons: ['csv', 'excel', 'pdf', 'print']
  }).container().appendTo($('#exportBtn'));

  // Apply the filter
  $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
      otable
          .column( $(this).parent().index()+':visible' )
          .search( this.value )
          .draw();
  });/* END COLUMN FILTER */

  // Date Range Picker
   $("#fromDate").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 2,
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#toDate").datepicker("option", "minDate", selectedDate);
       },
       dateFormat: 'yy-mm-dd'
   });
   $("#toDate").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 2,
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#fromDate").datepicker("option", "maxDate", selectedDate);
       },
       dateFormat: 'yy-mm-dd'
   });

   function search_by_date(){
       $("#searchByDate").click(function() {
         //Checkbox Values
         var checkValues = [];
          $(':checkbox:checked').each(function(i){
            checkValues[i] = $(this).val();
         });

         var fromDate = $("#fromDate").val();
         var toDate = $("#toDate").val();
         var checkedVal = checkValues;

         $.ajax({
             type: "POST",
             url: "{{url('MembershipAccounts/ExpenditureList/GetExpenditureByDate')}}",
             data: {
                   'fromDate': fromDate,
                   'toDate': toDate,
                   'checkedValues': checkedVal
             },
             dataType: "json",
             success: function(data)
             {
               console.log(data);
                 $("#expenditureListBody").empty();

                 for(var i = 0; i < data.length; i++)
                 {
                     var myvar = '<tr>'+
                     '<td>'+ data[i].id +'</td>'+
                     '<td>'+ data[i].AccountNumber +'</td>'+
                     '<td>'+ data[i].accountCardNumber +'</td>'+
                     '<td>'+ data[i].MembersName +'</td>'+
                     '<td>'+ data[i].created_at +'</td>'+
                     '<td>'+ data[i].actionType+'</td>'+
                     '<td>'+ data[i].amount +'</td>'+
                     '<td>'+ data[i].AccountBalance +'</td>'+
                     '<td>'+ data[i].actionTypeId +'</td>'+
                     '<td>'+ data[i].createdBy +'</td>'+
                     '</tr>';

                     $("#expenditureListBody").append(myvar);
                 }
              },
          });
     });
   }
   function clearDates(){
     $("#clearDatesBtn").click(function() {
       $('#fromDate').val('').datepicker("refresh");
       $('#toDate').val('').datepicker("refresh");
       $('#fromDate').datepicker('setDate', null);
       $('#toDate').datepicker('setDate', null);

       $('#menuItemCheckbox input[type=checkbox]').prop('checked', false);
     });
   }
 }); //documnet.ready



</script>
@endsection
