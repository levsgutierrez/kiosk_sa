@extends('includes.template')
@section('content')

<head>
</head>

<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i>Membership Types<span>
    </div>

    <!-- Menu Item Buttons -->
    <div class="MenuButtons text-right" style="padding: 1vh; margin-right:0.6vw;">
        @if(!empty($data['add']))
        <button type="button" class="btn btn-primary" onclick="showAddModal();">Add New Type</button>
        @endif
    </div>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Membership Types</h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox"></div><!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                              <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                  <thead>
                                      <tr>
                                        <!-- <th>Id</th> -->
                                        <th>Membership Type</th>
                                        <th>Discount Percentage (%)</th>
                                        <th>Status</th>
                                        @if(!empty($data['edit']))
                                        <th>Action</th>
                                        @endif
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <meta name="csrf_token" content="{{ csrf_token() }}" />
                                    @foreach($data['membership_types'] as $membershipTypes)
                                      <tr>
                                        <td>{{$membershipTypes->membershipTypes}}</td>
                                        <td>{{$membershipTypes->discountPercentage}}</td>
                                        <td>
                                          @if($membershipTypes->status == 1)
                                            <span class="label label-success">active</span>
                                          @else
                                            <span class="label label-danger">inactive</span>
                                          @endif
                                        </td>
                                         @if(!empty($data['edit']))
                                        <td>
                                          <button type="button" class="btn btn-sm btn-default" id="editType"
                                             data-id="{{$membershipTypes->id}}"
                                             data-membershipTypes="{{$membershipTypes->membershipTypes}}"
                                             data-discountPercentage="{{$membershipTypes->discountPercentage}}"
                                             data-deleted="{{$membershipTypes->deleted}}"
                                             data-status="{{$membershipTypes->status}}"
                                             onclick="edit(this)">Edit</button>
                                       </td>
                                        @endif
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div><!-- end widget content -->
                </div><!-- end widget div -->
            </div><!-- end widget -->
      </article>
  </section>
</div> <!-- /content -->

<!-- ================================================================================ -->
<!--                             ADD NEW MENU ITEM MODAL                              -->
<!-- ================================================================================ -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Membership Type Details</h4>
            </div>
            <div class="modal-body no-padding">
                <form id="membership-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('MembershipAccounts/MembershipTypes/AddMembershipType')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" id="id" name="id">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Membership Type Name</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-user-plus"></i>
                              <input type="text" id="membershipTypes" name="membershipTypes" placeholder="Membership Type Name" autocomplete="off" aria-required="true" aria-invalid="false" class="valid">
                            </label>
                          </section>
                        </div>
                        <div class="row">
                          <label class="label col col-4">Discount Percentage (%)</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-credit-card"></i>
                              <input type="number" id="discountPercentage" name="discountPercentage" value="0" aria-required="true" aria-invalid="false" class="valid">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-3">Status</label>
                          <section class="col col-2" style="margin-left:1.8vw;">
                            <label class="toggle state-success">
                              <input type="checkbox" id="status" name="status" value="1">
                              <i data-swchon-text="Active" data-swchoff-text="Inactive"></i></label>
                          </section>
                          <div class="col-md-7"></div>
                        </div>
                      </section>
                    </fieldset>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="newOrUpdate">Add</button>
                      @if(!empty($data['delete']))
                      <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete" onclick="deleteType()"> Delete </button>
                      @endif
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@include('includes.scripts')

<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>



<script>
window.onload = function(){
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#MembershipTypes").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset('/Membership Accounts/MembershipTypes')}}">Membership Types</a></li>');
}
 $(document).ready(function() {

  var errorClass = 'invalid';
  var errorElement = 'em';

   var $checkoutForm = $('#membership-form').validate({
       errorClass      : errorClass,
       errorElement    : errorElement,
       highlight: function(element) {
           $(element).parent().removeClass('state-success').addClass("state-error");
           $(element).removeClass('valid');
       },
       unhighlight: function(element) {
           $(element).parent().removeClass("state-error").addClass('state-success');
           $(element).addClass('valid');
       },

   // Rules for form validation
   rules : {
       membershipTypes : {
           required : true
       },
       discountPercentage : {
           required : true
       },
   },
   // Messages for form validation
   messages : {
       membershipTypes : {
           required : 'Please enter New Membership Type'
       },
       discountPercentage : {
           required : 'Please enter Membership Type Discount'
       },
   },
   // Do not change code below
   errorPlacement : function(error, element) {
       error.insertAfter(element.parent());
   }
 });
})

	var responsiveHelper_datatable_fixed_column = undefined;
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};


	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}

    });

    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {

        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();

    });
    /* END COLUMN FILTER */

    /* Hide Delete Button */
     $('#delete').hide();

    function showAddModal()
    {
      $('#membershipTypeName').val('');
      $('#membershipTypeDiscount').val('');

      $('#myModal').modal({backdrop: 'static', keyboard: false});
      $('#myModal').modal('show');
    }

    function edit(obj)
    {
     $('.modal-title').text('Edit Membership Type');
     $('#newOrUpdate').text('Update').addClass('btn-warning');
     $('#delete').show();
     $('#id').val(obj.getAttribute('data-id'));
     $('#membershipTypes').val(obj.getAttribute('data-membershipTypes'));
     $('#discountPercentage').val(obj.getAttribute('data-discountPercentage'));
     if(obj.getAttribute('data-status') == 1)
     		$("#status").prop( "checked", true );

     $('#status').val(obj.getAttribute('data-status'));
     $('#myModal').modal('show');
    }

    /* Spinners */
    $("#discountPercentage").spinner({
      step : 0.01,
      numberFormat : "n"
    });

    function deleteType(){
        $('#membership-form').attr('action',"{{url('MembershipAccounts/MembershipTypes/DeleteMembershipType')}}");
        $('#membership-form').removeAttr('onsubmit');
        $('#membership-form').submit();
    };



</script>
@include('includes.MembershipTypesJS')
@endsection
