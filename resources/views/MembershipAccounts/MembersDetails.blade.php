@extends('includes.template')
@section('content')

<head>
  <style>
    .input-disabled{background-color:#EBEBE4 !important;border:1px solid #ABADB3;padding:2px 1px;}
  </style>
</head>

<!-- Page Header -->
<div id="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-left: 0.8vw;">
      <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i>  Members Details</h1>
    </div>
  </div>

    <!-- Menu Item Buttons -->
    <div class="MenuButtons text-right" style="padding: 1vh; margin-right:0.8vw;">
        @foreach($data['membership_defaultDepositAmount'] as $membership_defaultDepositAmount)
          @if(!empty($data['add']))
          <button type="button" class="btn btn-primary" onclick="showAddModal({{ $membership_defaultDepositAmount->valueDouble }});">Add New Member</button>
          @endif
          @if(!empty($data['edit']))
          <button type="button" class="btn btn-danger" onclick="showDepositModal({{ $membership_defaultDepositAmount->id }});">Set Default Deposit Amount</button>
          @endif
        @endforeach
        @if(!empty($data['export']))
        <div id="exportBtn" style="margin-top: 1.5vh;"></div>
        @endif
    </div>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Users</h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox"></div><!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                              <table id="datatable_fixed_column" class="table table-striped table-bordered display" width="100%">
                                  <thead>
                                      <tr>
                                        <!-- <th>Id</th> -->
                                        <th>Account Number</th>
                                        <th>Account Card Number</th>
                                        <th>Membership Type</th>
                                        <th>Name</th>
                                        <th><i class="fa fa-fw fa-calendar txt-color-blue"></i>Registered Date</th>
                                        <th>NRIC</th>
                                        <th>Contact Number</th>
                                        <th><i class="fa fa-usd text-muted"></i>Account Balance</th>
                                        <th><i class="fa fa-usd text-muted"></i>Deposit</th>
                                        <th>Status</th>
                                        @if(!empty($data['edit']))
                                        <th>Action</th>
                                        @endif
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <meta name="csrf_token" content="{{ csrf_token() }}" />
                                    @foreach($data['members_details'] as $members_details)
                                      <tr>
                                          <!-- <td>{{$members_details->id}}</td> -->
                                          <td>{{$members_details->accountNumber}}</td>
                                          <td>{{$members_details->accountCardNumber}}</td>
                                          <td>{{$members_details->membershipTypesName}}</td>
                                          <td>{{$members_details->membersName}}</td>
                                          <td>{{$members_details->registeredDate}}</td>
                                          <td>{{$members_details->nric}}</td>
                                          <td>{{$members_details->contactNumber}}</td>
                                          <td>
                                            @if($members_details->accountBalance == null)
                                               0.00
                                            @elseif($members_details->accountBalance == .00)
                                              0.00
                                            @else
                                              {{$members_details->accountBalance}}
                                          @endif
                                        </td>
                                          <td>
                                            @if($members_details->deposit == .00)
                                               0.00
                                            @else
                                              {{$members_details->deposit}}
                                            @endif
                                          </td>
                                          <td>
                                            @if($members_details->status == 1)
                                              <span class="label label-success">active</span>
                                            @else
                                              <span class="label label-danger">inactive</span>
                                            @endif
                                          </td>
                                          @if(!empty($data['edit']))
                                          <td>
                                            <button type="button" class="btn btn-sm btn-default" id="editMember"
                                              data-id="{{$members_details->id}}"
                                              data-accountNumber="{{$members_details->accountNumber}}"
                                              data-accountCardNumber="{{$members_details->accountCardNumber}}"
                                              data-membershipTypes="{{$members_details->membershipTypesId}}"
                                              data-membersName="{{$members_details->membersName}}"
                                              data-registeredDate="{{$members_details->registeredDate}}"
                                              data-nric="{{$members_details->nric}}"
                                              data-contactNumber="{{$members_details->contactNumber}}"
                                              data-accountBalance="{{$members_details->initialBalance}}"
                                              data-deposit="{{$members_details->deposit}}"
                                              data-status="{{$members_details->status}}"
                                              onclick="edit(this)">Edit</button>
                                          </td>
                                          @endif
                                      </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot>
                                </tfoot>
                              </table>
                        </div><!-- end widget content -->
                </div><!-- end widget div -->
            </div><!-- end widget -->
      </article>
  </section>
  <!-- Total Deposit -->
  <div class="col-md-7">
    <div class="col-md-3 pull-left"><label>Total Members Count: <span id="membersCount"></span></label></div>
    <div class="col-md-4"><label>Total Balance Amount: $<span id="totalBalanceAmount"></span></label></div>
  </div>
</div> <!-- /content -->

<!-- ================================================================================ -->
<!--                              ADD NEW MEMBER MODAL                                -->
<!-- ================================================================================ -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Member Details</h4>
            </div>
            <div class="modal-body no-padding">
                <form id="members-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('MembershipAccounts/MembersDetails/AddMembers')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" id="id" name="id">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Account Number</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-credit-card "></i>
                              <input type="text" id="accountNumber" name="accountNumber" data-mask="9999-9999-9999-9999" aria-required="true" aria-invalid="false" class="valid">
                            </label>
                          </section>
                        </div>
                        <div class="row">
                          <label class="label col col-4">Account Card Number</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-credit-card "></i>
                              <input type="text" id="accountCardNumber" name="accountCardNumber" data-mask="9999-9999-9999-9999" aria-required="true" aria-invalid="false" class="valid">
                            </label>
                          </section>
                        </div>
                      </section>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Membership Type</label>
                          <section class="col col-8">
                            <label class="select">
                              <select class="form-control" id="membershipTypes" name="membershipTypes">
                                <option value="0" selected readonly>Select One</option>
                                @foreach($data['membership_types'] as $membership_types)
                                <option value="{{ $membership_types->id }}">{{ $membership_types->membershipTypes }}</option>
                                @endforeach
                              </select> <i></i>
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Members' Name</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-user "></i>
                              <input type="text" id="membersName" name="membersName">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Members' NRIC</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-id-card"></i>
                              <input type="text" id="nric" name="nric">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Contact Number</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                              <input type="text" id="contactNumber" name="contactNumber" data-mask="9999-9999">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Registered Date</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-calendar "></i>
                              <input type="text" id="registeredDate" name="registeredDate" disabled>
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Account Balance</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                              <input type="text" id="accountBalance" name="accountBalance">
                            </label>
                          </section>
                        </div>
                      </section>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Deposit</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-usd"></i>
                              <input type="text" id="deposit" name="deposit">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-3">Status</label>
                          <section class="col col-2" style="margin-left:1.8vw;">
                            <label class="toggle state-success">
                              <input type="checkbox" id="status" name="status" value="1">
                              <i data-swchon-text="Active" data-swchoff-text="Inactive"></i></label>
                          </section>
                          <div class="col-md-7"></div>
                        </div>
                      </section>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="newOrUpdate">Add</button>
                      @if(!empty($data['delete']))
                      <a type="button" class="btn btn-danger" data-dismiss="modal" id="delete" onclick="deleteMember()"> Delete </a>
                      @endif
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- ================================================================================ -->
<!--                     SET DEFAULT DEPOSIT AMOUNT MODAL                             -->
<!-- ================================================================================ -->
<div class="modal fade" id="depositModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Set Default Deposit Amount</h4>
            </div>
            <div class="modal-body no-padding">
                <form id="members-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('MembershipAccounts/MembersDetails/AddDefaultDepositAmount')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Deposit Amount</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-usd "></i>
                              @foreach($data['membership_defaultDepositAmount'] as $membership_defaultDepositAmount)
                              <input type="text" value="{{ $membership_defaultDepositAmount->valueDouble }}" id="defaultDepositAmount" name="defaultDepositAmount" aria-required="true" aria-invalid="false" class="valid">
                              @endforeach
                            </label>
                          </section>
                        </div>
                      </section>
                    </fieldset>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="insertOrUpdate">Add</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <input type="hidden" id="generalSettingsId" />

                  </form>
                </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('includes.scripts')

<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


<script>
window.onload = function(){
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#MembersDetails").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset('/MembershipAccounts/MembersDetails')}}">Members Details</a></li>');
}
 $(document).ready(function() {

    getDate();
    getMembersCount();
    getTotalBalance();

  	var errorClass = 'invalid';
    var errorElement = 'em';

    var $checkoutForm = $('#members-form').validate({
        errorClass      : errorClass,
        errorElement    : errorElement,
        highlight: function(element) {
            $(element).parent().removeClass('state-success').addClass("state-error");
            $(element).removeClass('valid');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass("state-error").addClass('state-success');
            $(element).addClass('valid');
        },

    // Rules for form validation
        rules : {
             accountNumber : {
                required : true
            },
             accountCardNumber: {
						   required : true
					},
            membershipType : {
						   required : true
					},
            membersName : {
						   required : true
					},
            nric : {
						   required : true
					},
            contactNumber : {
						   required : true
					},
        },

        // Messages for form validation
        messages : {
            accountNumber : {
                required : 'Enter Account Number of New Member!'
            },
            accountCardNumber : {
						   required : 'Enter a Valid Account Card Number'
  					},
            membersName : {
               required : 'Enter Members Name'
             },
            contactNumber : {
               required : 'Enter Members Contact Number'
             },
          },

        // Do not change code below
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

	var responsiveHelper_datatable_fixed_column = undefined;
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};


	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}
  });

  var buttons = new $.fn.dataTable.Buttons(otable, {
      buttons: [
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
          ]
        }).container().appendTo($('#exportBtn'));

    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    });
    /* END COLUMN FILTER */

/* Hide Delete Button */
 $('#delete').hide();

 /* Spinners */
 $("#defaultDepositAmount").spinner({
   step : 1,
   numberFormat : "n"
 });

function getDate(){
 /* Getting todays date */
   var d = new Date();
   var month = d.getMonth()+1;
   var day = d.getDate();
   var output = d.getFullYear() + '/' +
       ((''+month).length<2 ? '0' : '') + month + '/' +
       ((''+day).length<2 ? '0' : '') + day;

   $("#registeredDate").val(output);
}

function getMembersCount()
{
  var rowCount = $('table#datatable_fixed_column tr:last').index() + 1;
  document.getElementById("membersCount").innerHTML = rowCount.toString();
}

function getTotalBalance(){
  // var totalBalanceAmount = document.getElementById("totalBalanceAmount");
     $.ajax({
         type: "GET",
         url: "{{url('MembershipAccounts/MembersDetails/GetTotalBalance')}}",
         dataType: "json",
         success: function(data)
         {
          $("#totalBalanceAmount").text(data);
          },
      });
    }
}); //document.ready

function deleteMember(){
   $('#members-form').attr('action',"{{url('MembershipAccounts/MembersDetails/DeleteMembers')}}");
   $('#members-form').removeAttr('onsubmit');
   $('#members-form').submit();
}
</script>
@include('includes.MembersDetailsJS')
@endsection
