@extends('includes.template')
@section('content')
<head>
</head>
<!-- Page Header -->
<div id="content">
  <div class="row">
  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark"><i class="fa fa-users fa-fw "></i>  Topup Statistics</h1>
  </div>
  @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
          {{$errors->first()}}
        </p>
    </div>
    @endif
</div>

<div class="row">
    <fieldset>
      <div class="row">
        <div class="col-md-2" style="margin-left:2.5vw;">
          <label> Select a Date Range </label>
        </div>
      </div>

    		<div class="col-sm-2" style="margin-left:1vw;">
    			<div class="form-group">
    				<div class="input-group">
    					<input class="form-control" id="to" type="text" placeholder="From">
    					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
    				</div>
    			</div>
    		</div>

    		<div class="col-sm-2">
    			<div class="form-group">
    				<div class="input-group">
    					<input class="form-control" id="from" type="text" placeholder="Select a date">
    					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
    				</div>
    			</div>
    		</div>

        <div class="col-md-4"></div>
    </fieldset>
  </div>

  <!-- Main Content -->
  <section id="widget-grid" class="">
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  		<!-- Widget ID (each widget will need unique ID)-->
  		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
  			<header>
  				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
  				<h2>Transactions Details</h2>
  			</header>

  			<!-- widget div-->
  			<div>
  				<!-- widget edit box -->
  				<div class="jarviswidget-editbox"></div><!-- end widget edit box -->
  					<!-- widget content -->
  					<div class="widget-body no-padding">
  						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
  							<thead>
  								<tr>
  									<th></th>
  									<th>Category Name</th>
  									<th>Category ID</th>
  									<th>Category Name</th>
  									<th>Start date</th>
  									<th>Salary</th>
  								</tr>
                </thead>
                <tbody>
  			            <tr>
  			                <td>Tiger Nixon</td>
  			                <td>System Architect</td>
  			                <td>Edinburgh</td>
  			                <td>61</td>
  			                <td>2014/12/12</td>
  			                <td>$320,800</td>
  			            </tr>
    					   </tbody>
    					</table>
    			</div><!-- end widget content -->
    		</div><!-- end widget div -->
  		</div><!-- end widget -->
  	</article>
  </section>

</div>








@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<script>
window.onload = function(){
<<<<<<< Updated upstream
<<<<<<< Updated upstream
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#TopupStatistics").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset('/MembershipAccounts/TopUpStatistics')}}">Topup Statistics</a></li>');
=======
	$(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset("/MembershipAccounts/TopupStatistics")}}">Topup Statistics</a></li>');
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#TopupStatistics").addClass("active");
=======
	$(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset("/MembershipAccounts/TopupStatistics")}}">Topup Statistics</a></li>');
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#TopupStatistics").addClass("active");
>>>>>>> Stashed changes
	$("#dashboard").removeClass("active");
>>>>>>> Stashed changes
}
</script>
@endsection
