@extends('includes.template')
@section('content')

<head>
  <style>
    .input-disabled{background-color:#EBEBE4 !important;border:1px solid #ABADB3;padding:2px 1px;}
  </style>
</head>

<!-- Page Header -->
<div id="content">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Topup Inquiry</h1>
  </div>

  @if(session()->has('message'))
  <div class="alert alert-block alert-success">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
      <p>
          {{ session()->get('message') }}
      </p>
  </div>
  @endif

  @if(session()->has('deleted'))
  <div class="alert alert-block alert-warning">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
      <p>
          {{ session()->get('deleted') }}
      </p>
  </div>
  @endif

  @if($errors->any())
  <div class="alert alert-block alert-danger">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
      <p>
          {{$errors->first()}}
      </p>
  </div>
  @endif

  <div class="row"></div>
  <div class="row" style="margin-left:0.5vw;">
    <!-- Buttons / Search Buttons -->
    <fieldset>
      <div class="col-md-2">
        <label> Select a Date Range </label>
      </div>
      <br/>
      <br/>
      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="fromDate" type="text" placeholder="From">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>

      <div class="col-sm-2">
        <div class="form-group">
          <div class="input-group">
            <input class="form-control" id="toDate" type="text" placeholder="Select a date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <button type="button" id="clearDatesBtn" class="btn btn-default">Clear</button>
        <button type="button" id="searchByDate" class="btn btn-default">Search</button>
      </div>

        <div class="col-md-3"></div>
        <div class="MenuButtons text-right" style="padding: 1vh; margin-right:1.5vw;">
          <div id="exportBtn" style="margin-top: 1vh;"></div>
        </div>
    </fieldset>
  </div>

  <!-- Main Content -->
  <section id="widget-grid" class="">
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  		<!-- Widget ID (each widget will need unique ID)-->
  		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
  			<header>
  				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
  				<h2>Topup Details</h2>

  			</header>

  			<!-- widget div-->
  			<div>
  				<!-- widget edit box -->
  				<div class="jarviswidget-editbox"></div><!-- end widget edit box -->

  					<!-- widget content -->
  					<div class="widget-body no-padding">
  						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                  <!-- <div id="exportBtn" class="pull-right" style="margin-top: 1.5vh;"></div> -->
  							<thead>
                  <meta name="csrf_token" content="{{ csrf_token() }}" />
  								<tr>
  									<th>Account Number</th>
  									<th>Members Name</th>
  									<th>Contact Number</th>
  									<th>Registered Date</th>
  									<th>Current Balance</th>
  									<th>Date</th>
  									<th>Topup Id</th>
  									<th>Operator</th>
  									<th>Payment Type</th>
  									<th>Amount</th>
  								</tr>
                </thead>
                <tbody id="topUpAccountsBody">
                  @foreach($data['members_details'] as $members_details)
                    <tr>
                      <td>{{$members_details->accountNumber}}</td>
                      <td>{{$members_details->membersName}}</td>
                      <td>{{$members_details->contactNumber}}</td>
                      <td>{{$members_details->registeredDate}}</td>
                      <td>
                        @if($members_details->accountBalance == .00)
                           0.00
                        @else
                          {{$members_details->accountBalance}}
                      @endif
                      </td>
                      <td>{{$members_details->topupDate}}</td>
                      <td>{{$members_details->topupId}}</td>
                      <td>{{$members_details->operator}}</td>
                      <td>{{$members_details->paymentType}}</td>
                      <td>{{$members_details->topupAmount}}</td>
                  </tr>
                  @endforeach
    					   </tbody>
    					</table>
    			</div><!-- end widget content -->
    		</div><!-- end widget div -->
  		</div><!-- end widget -->
  	</article>
  </section>
  <!-- Total Deposit -->
  <div class="col-md-6">
    <!-- <label>Total Members Count: <p id="membersCount"></p></label> -->
    <div class="col-md-3 pull-left"><label>Total Topup Amount: $<span id="totalTopupAmount"></span></label></div>
    <div class="col-md-3"><label>Total Gift Amount: <span id="membersCount"></span></label></div>
  </div>
</div> <!-- /content -->



@include('includes.scripts')

<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


<script>
window.onload = function(){
	$(".breadcrumb").append('<li>Membership Accounts</li><li><a href="{{URL::asset("/MembershipAccounts/TopupInquiry")}}">Topup Inquiry</a></li>');
  $('#MembershipAccounts').addClass("active open");
  $("#MembershipAccountsBlock").css('display', 'block');
  $("#TopupInquiry").addClass("active");
	$("#dashboard").removeClass("active");
}
$(document).ready(function() {
  retrieve_data();
  search_by_date();
  retrieveTopupAccountBalance();
  // getTotalTopupAmount();

	var errorClass = 'invalid';
  var errorElement = 'em';

  var $checkoutForm = $('#kiosk-form').validate({
      errorClass      : errorClass,
      errorElement    : errorElement,
      highlight: function(element) {
          $(element).parent().removeClass('state-success').addClass("state-error");
          $(element).removeClass('valid');
      },
      unhighlight: function(element) {
          $(element).parent().removeClass("state-error").addClass('state-success');
          $(element).addClass('valid');
      },

  // Rules for form validation
      rules : {
          kioskID : {
              required : true
          },
      },

      // Messages for form validation
      messages : {
          kioskID : {
              required : 'Please enter your preffered Kiosk ID'
          },
      },

      // Do not change code below
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });

  /* Spinners */
  $("#topupAmount").spinner({
    step : 0.01,
    numberFormat : "n"
  });
  $("#giftAmount").spinner({
    step : 0.01,
    numberFormat : "n"
  });

var responsiveHelper_datatable_fixed_column = undefined;
var breakpointDefinition = {
	tablet : 1024,
	phone : 480
};

	/* COLUMN FILTER  */
  var otable = $('#datatable_fixed_column').DataTable({
	"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
	"autoWidth" : true,
	"preDrawCallback" : function() {
		// Initialize the responsive datatables helper once.
		if (!responsiveHelper_datatable_fixed_column) {
			responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
		}
	},
	"rowCallback" : function(nRow) {
		responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
	},
	"drawCallback" : function(oSettings) {
		responsiveHelper_datatable_fixed_column.respond();
	}
});
var buttons = new $.fn.dataTable.Buttons(otable, {
  buttons: [
         'csv', 'excel', 'pdf', 'print'
      ]
  }).container().appendTo($('#exportBtn'));

  // Apply the filter
  $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {

      otable
          .column( $(this).parent().index()+':visible' )
          .search( this.value )
          .draw();
  });

  /* END COLUMN FILTER */
  // Date Range Picker
   $("#fromDate").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 2,
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#toDate").datepicker("option", "minDate", selectedDate);
       },
       dateFormat: 'yy-mm-dd'
   });
   $("#toDate").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 2,
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#fromDate").datepicker("option", "maxDate", selectedDate);
       },
       dateFormat: 'yy-mm-dd'
   });

   function search_by_date(){
       $("#searchByDate").click(function() {
         var fromDate = $("#fromDate").val();
         var toDate = $("#toDate").val();

         $.ajax({
             type: "POST",
             url: "{{url('MembershipAccounts/TopupInquiry/GetTopupTransactionsByDate')}}",
             data: {
                   'fromDate': fromDate,
                   'toDate': toDate
             },
             dataType: "json",
             success: function(data)
             {
               console.log(data);
                 $("#topUpAccountsBody").empty();

                 for(var i = 0; i < data.length; i++)
                 {
                     var myvar = '<tr>'+
                     '<td>'+ data[i].accountNumber +'</td>'+
                     '<td>'+ data[i].membersName +'</td>'+
                     '<td>'+ data[i].contactNumber +'</td>'+
                     '<td>'+ data[i].registeredDate +'</td>'+
                     '<td>'+ data[i].accountBalance +'</td>'+
                     '<td>'+ data[i].topupDate +'</td>'+
                     '<td>'+ data[i].topupId +'</td>'+
                     '<td>'+ data[i].operator +'</td>'+
                     '<td>'+ data[i].paymentType +'</td>'+
                     '<td>'+ data[i].topupAmount +'</td>'+
                     '</tr>';
                     $("#topUpAccountsBody").append(myvar);
                 }
              },
          });
     });
   }
   function clearDates(){
     $("#clearDatesBtn").click(function() {
       $('#fromDate').datepicker('setDate', null);
       $('#toDate').datepicker('setDate', null);
     });
   }

   function getTotalTopupAmount(){
        $.ajax({
            type: "GET",
            url: "{{url('MembershipAccounts/TopupAccounts/GetTotalTopupAmount')}}",
            dataType: "json",
            success: function(data)
            {
             $("#totalTopupAmount").text(data);
             },
         });
       }

     function retrieve_data(){
       $.ajaxSetup({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
       });

       $("#accountCardNumber").change(function () {
       $.ajax({
           type: "POST",
           url: "{{url('MembershipAccounts/TopupAccounts/RetrieveTopupAccountsDetails')}}",
           data: {
                 'accountCardNumber': $('input[name=accountCardNumber]').val(),
           },
           dataType: "json",
           beforeSend: function (xhr)
           {
             var token = $('meta[name="csrf_token"]').attr('content');
             if (token)
             {
               return xhr.setRequestHeader('X-CSRF-TOKEN', token);
             }
           },
           success: function(data)
           {
             var accountBalance = data[0].accountBalance;
             if(accountBalance == .00)
               accountBalance = 0;
             // alert(membersName);
             $("#membersName").val(data[0].membersName);
             $("#initialBalance").val(accountBalance);
             // var initialBalance = $(this).find('initialBalance').text();
            },
        });
      });
    }


   function retrieveTopupAccountBalance(){
     $("#accountCardNumber").change(function() {
        var accountCardNumber = $("#accountCardNumber").val();
        console.log(accountCardNumber);
        $.ajax({
            type: "POST",
            url: "{{url('MembershipAccounts/TopupAccounts/GetTopupAccountsByCardNumber')}}",
            data: {
                  'accountCardNumber': accountCardNumber
            },
            dataType: "json",
            success: function(data)
            {
              $("#initialBalance").val(data[0].initialBalance);

             },
         });
       })
     }
 }); //document.ready


</script>
@endsection
