@extends('includes.template')
@section('content')
<head>
</head>

<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Inventory <span>
    </div>

    <!-- Menu Item Buttons -->
    <div class="MenuButtons text-right" style="padding: 1vh; margin-right:0.8vw;">
         <!-- <button type="button" class="btn btn-default" onclick="save();">Save Changes</button> -->
    </div>


    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <div id="messageBox">
    </div>

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
									<h2>Menu Item Inventory </h2>
								</header>

								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox"></div><!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body">
										<select multiple="multiple" size="10" name="duallistbox_demo2" id="initializeDuallistbox">
                      @foreach($data['menus'] as $menus)
                        @if($menus->Status == 1)
                          <option value="{{ $menus->ItemName }}" selected>{{ $menus->ItemName }}</option>
                        @else
                          <option value="{{ $menus->ItemName }}">{{ $menus->ItemName }}</option>
                        @endif
                      @endforeach
										</select>
									</div><!-- end widget content -->
                  <div class="row">
                    <div class="col-md-12">
                    <div class="col-md-4"></div>
                      <div class="col-md-4">
                        <button type="button" class="btn btn-primary col-md-12" id="saveChanges" style="padding:1vh; margin-bottom:2vh; margin-top:2vh;">Save Changes</button>

                      </div>
                      <div class="col-md-4"></div>
                    </div>
                  </div>
              </div><!-- end widget div -->
					</div><!-- end widget -->
			</article><!-- END COL -->
  </section>
</div><!-- end row -->


@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<script src="{{url('js/plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>

<script>
window.onload = function(){
  $('#InventoryManagement').addClass("active open");
  $("#InventoryManagementBlock").css('display', 'block');
  $("#Inventory").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Inventory Management</li><li><a href="{{URL::asset('/InventoryManagement/Inventory')}}">Inventory</a></li>');
}

 $(document).ready(function() {
/*BOOTSTRAP DUALLIST BOX*/
		var initializeDuallistbox = $('#initializeDuallistbox').bootstrapDualListbox({
          nonSelectedListLabel: 'AVAILABLE MENU ITEMS',
          selectedListLabel: 'SOLD OUT MENU ITEMS',
          preserveSelectionOnMove: 'moved',
          moveOnSelect: false,
        });

        // Try to get the selected values

        $('#saveChanges').click(function(){
          // console.log("hello");
          @if(empty($data['edit']))
          alert('it seems you dont have permission to do this. Contact administrator')
          return false;
          @endif
          var myvar = '<div class="row"></div><div class="alert alert-block alert-success">'+
                      '<a class="close" data-dismiss="alert" href="#">×</a>'+
                      '<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>'+
                      '<p>'+
                      'data saved!'+
                      '</p>';

                      $("#messageBox").append(myvar);

        //If the items do not get the "selected" attribute, you could also use this:
              $("select#bootstrap-duallistbox-selected-list_duallistbox_demo2 option").each(function() {
                  var itemName = $(this).html();

                  $.ajax({
                      type: "POST",
                      url: "{{url('InventoryManagement/UpdateMenuItemStatus')}}",
                      data: {
                            'itemName': itemName,
                            'status': 1
                      },
                      dataType: "json",
                      success: function(data)
                      {
                          location.reload();
                      },
                   });
                });

                $("select#bootstrap-duallistbox-nonselected-list_duallistbox_demo2 option").each(function() {
                    var itemName = $(this).html();

                    $.ajax({
                        type: "POST",
                        url: "{{url('InventoryManagement/UpdateMenuItemStatus')}}",
                        data: {
                              'itemName': itemName,
                              'status': 0
                        },
                        dataType: "json",
                        success: function(data)
                        {
                        },
                     });
                  });
              });

          }); // document ready

</script>
@endsection
