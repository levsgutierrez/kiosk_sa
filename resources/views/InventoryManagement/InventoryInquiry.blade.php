@extends('includes.template')
@section('content')
<head>
</head>

<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Inventory Inquiry <span>
    </div>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <div class="row"></div>
    
    <div class="row" style="margin-left:0.5vw;">

      <!-- Buttons / Search Buttons -->
      <fieldset>
          <div class="MenuButtons text-left" style="padding: 1vh;">
            @if(!empty($data['export']))
            <div id="exportBtn"></div>
                @endif
          </div>
      </fieldset>
    </div>

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    		<!-- Widget ID (each widget will need unique ID)-->
    		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
    			<header>
    				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
    				<h2>Inventory Details</h2>
    			</header>

    			<!-- widget div-->
    			<div>
    				<!-- widget edit box -->
    				<div class="jarviswidget-editbox"></div><!-- end widget edit box -->
    					<!-- widget content -->
    					<div class="widget-body no-padding">
    						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
    							<thead>
    								<tr>
    									<th>Item Code</th>
    									<th>Item Name</th>
    									<th>Inventory Quantity</th>
    									<th>Lowest Quantity</th>
    									<th>Status</th>
    								</tr>
                  </thead>
                  <tbody id="inventoryInquiryBody">
                      @foreach($data['inventory_stocks'] as $inventory_stocks)
                    <meta name="csrf_token" content="{{ csrf_token() }}" />
                      <tr>
                        <td>{{$inventory_stocks->itemCode}}</td>
                        <td>{{$inventory_stocks->itemName}}</td>
                        <td>{{$inventory_stocks->quantity}}</td>
                        <td>{{$inventory_stocks->quantity}}</td>
                        <td>  @if($inventory_stocks->status == 1 && $inventory_stocks->quantity > 1)
                            <span class="label label-success">active</span>
                          @else
                            <span class="label label-danger">inactive</span>
                          @endif</td>
                    </tr>
                    @endforeach
    					   </tbody>
    				</table>
    			</div><!-- end widget content -->
    		</div><!-- end widget div -->
  		</div><!-- end widget -->
  	</article>
  </section>
</div><!-- /content -->

@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script>

window.onload = function(){
  $('#InventoryManagement').addClass("active open");
  $("#InventoryManagementBlock").css('display', 'block');
  $("#InventoryInquiry").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Inventory Management</li><li><a href="{{URL::asset(`/InventoryManagement/InventoryInquiry`)}}">Inventory Inquiry</a></li>');
}

$(document).ready(function() {
  getCheckedValue();

  var errorClass = 'invalid';
  var errorElement = 'em';

  var $checkoutForm = $('#kiosk-form').validate({
      errorClass      : errorClass,
      errorElement    : errorElement,
      highlight: function(element) {
          $(element).parent().removeClass('state-success').addClass("state-error");
          $(element).removeClass('valid');
      },
      unhighlight: function(element) {
          $(element).parent().removeClass("state-error").addClass('state-success');
          $(element).addClass('valid');
      },

  // Rules for form validation
      rules : {
          kioskID : {
              required : true
          },
      },

      // Messages for form validation
      messages : {
          kioskID : {
              required : 'Please enter your preffered Kiosk ID'
          },
      },

      // Do not change code below
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });

  var responsiveHelper_datatable_fixed_column = undefined;
  var breakpointDefinition = {
  tablet : 1024,
  phone : 480
  };

  /* COLUMN FILTER  */
  var otable = $('#datatable_fixed_column').DataTable({
  "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
      "t"+
      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
  "autoWidth" : true,
  "preDrawCallback" : function() {
    // Initialize the responsive datatables helper once.
    if (!responsiveHelper_datatable_fixed_column) {
      responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
    }
  },
  "rowCallback" : function(nRow) {
    responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
  },
  "drawCallback" : function(oSettings) {
    responsiveHelper_datatable_fixed_column.respond();
  }
});
var buttons = new $.fn.dataTable.Buttons(otable, {
  buttons: [
         'csv', 'excel', 'pdf', 'print'
      ]
  }).container().appendTo($('#exportBtn'));

  // Apply the filter
  $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {

      otable
          .column( $(this).parent().index()+':visible' )
          .search( this.value )
          .draw();

  });
/* END COLUMN FILTER */


 /* Spinners */
 $("#quantity").spinner({
   step : 1,
   numberFormat : "n"
 });

//Checkboxes
$('input[type="checkbox"]').on('change', function() {
   $('input[type="checkbox"]').not(this).prop('checked', false);
});

function getCheckedValue()
{
  $('#getCheckboxValue').click(function(){
      var val = [];
      $(':checkbox:checked').each(function(i){
        val[i] = $(this).val();
        // val.push($(this).attr("id"));
        $("#myModal").modal("hide");
      });
      $('input[id=itemCode]').val(val);

      $.ajax({
          type: "POST",
          url: "{{url('InventoryManagement/IncomingInventory/RetrieveMenuItemName')}}",
          data: {
                'itemCode': $('input[name=itemCode]').val(),
          },
          dataType: "json",
          success: function(data)
          {
            $("#itemName").val(data[0].itemName)
            console.log(data);
           },
       });

    });
  }
});

</script>
@endsection
