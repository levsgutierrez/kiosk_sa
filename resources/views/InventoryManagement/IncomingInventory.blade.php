@extends('includes.template')
@section('content')
<head>
</head>

<!-- Page Header -->
<div id="content">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Incoming Inventory <span>
    </div>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <div class="row"></div>
    <div class="row" style="margin-left:0.5vw;">
      <!-- Buttons / Search Buttons -->
      <fieldset>
        <div class="col-md-2">
          <label> Select a Date </label>
        </div>
        <br/>
        <br/>
        <div class="col-sm-2">
          <div class="form-group">
            <div class="input-group">
              <input class="form-control" id="fromDate" type="text" placeholder="Select a Date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
          </div>
        </div>

        <div class="col-md-2">
          <button type="button" id="clearDatesBtn" class="btn btn-default">Clear</button>
          <button type="button" id="searchByDate" class="btn btn-default">Search</button>
        </div>

          <div class="col-md-3"></div>
          <div class="MenuButtons text-right" style="padding: 1vh; margin-right:1.5vw;">
            @if(!empty($data['add']))
            <button type="button" class="btn btn-primary" onclick="showAddModal();" style="margin-left:5vw; margin-right:0.2vw;" >Add Inventory</button>
            @endif
            @if(!empty($data['reprint']))
            <button type="button" class="btn btn-default" style="margin-right:0.2vw;">Reprint Receipt</button>
            @endif
            @if(!empty($data['export']))
            <div id="exportBtn" style="margin-top: 1.5vh;"></div>
            @endif
          </div>
      </fieldset>
    </div>

    <!-- Main Content -->
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    		<!-- Widget ID (each widget will need unique ID)-->
    		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
    			<header>
    				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
    				<h2>Incoming Inventory Details</h2>
    			</header>

    			<!-- widget div-->
    			<div>
    				<!-- widget edit box -->
    				<div class="jarviswidget-editbox"></div><!-- end widget edit box -->
    					<!-- widget content -->
    					<div class="widget-body no-padding">
    						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
    							<thead>
    								<tr>
    									<th>ID</th>
    									<th>Item Code</th>
    									<th>Item Name</th>
    									<th>Quantity</th>
    									<th>Remarks</th>
    									<th>Date</th>
    									<th>Operator</th>
    									@if(!empty($data['delete']))
                      <th>Action</th>
                      @endif
    								</tr>
                  </thead>
                  <tbody id="incomingInventoryBody">
                    @foreach($data['inventory_i_os'] as $inventory_i_os)
                    <meta name="csrf_token" content="{{ csrf_token() }}" />
                      <tr>
                        <td>{{$inventory_i_os->inventoryId}}</td>
                        <td>{{$inventory_i_os->itemCode}}</td>
                        <td>{{$inventory_i_os->itemName}}</td>
                        <td>{{$inventory_i_os->totalQuantity}}</td>
                        <td>{{$inventory_i_os->remarks}}</td>
                        <td>{{$inventory_i_os->created_at}}</td>
                        <td>{{$inventory_i_os->operator}}</td>
                        @if(!empty($data['delete']))
                        <td>
                          <a class="delete-modal btn btn-sm btn-danger" href="{{url('/InventoryManagement/IncomingInventory/DeleteItem/'.$inventory_i_os->inventoryId)}}">
                          <i class='fa fa-fw fa-trash-o'></i>
                          </a>
                          <!-- <button type="button" class="btn btn-sm btn-default" id="deleteItem" onclick="delete(this)">Delete</button> -->
                        </td>
                        @endif
                    </tr>
                    @endforeach
    					   </tbody>
    				</table>
    			</div><!-- end widget content -->
    		</div><!-- end widget div -->
  		</div><!-- end widget -->
  	</article>
  </section>
</div><!-- /content -->

<!-- =============================================================================== -->
<!--                         ADD INCOMING INVENTORY  MODAL                           -->
<!-- =============================================================================== -->
<div class="modal fade" id="inventoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="modalTitle">Incoming Inventory</h4>
            </div>
            <div class="modal-body no-padding">
                <form id="incomingInventory-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('InventoryManagement/IncomingInventory/AddIncomingInventory')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" id="id" name="id">
                    <fieldset>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Menu Item SKU</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-list-ul"></i>
                              <input type="text" id="itemCode" name="itemCode" data-toggle="modal" data-target="#myModal">
                            </label>
                          </section>
                        </div>
                      </section>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Menu Item Name</label>
                          <section class="col col-8">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                              <input type="text" id="itemName" name="itemName">
                            </label>
                          </section>
                        </div>
                      </section>
                      <section>
                        <div class="row">
                          <label class="label col col-4">Quantity</label>
                          <section class="col col-8">
                            <label class="input">
                              <input type="number" id="totalQuantity" name="totalQuantity" value="0">
                            </label>
                          </section>
                        </div>
                      </section>

                      <section>
                        <div class="row">
                          <label class="label col col-4">Remarks</label>
                          <section class="col col-8">
                            <label class="input">
                              <textarea type="text" id="remarks" class="col-sm-11" name="remarks"></textarea>
                            </label>
                          </section>
                        </div>
                      </section>
                    </div>
                  </fieldset>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="newOrUpdate">Add</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- ================================================================================ -->
<!--                             MENU ITEMS SKU MODAL                                 -->
<!-- ================================================================================ -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Menu Item</h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
    			<header>
    				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
    				<h2>Incoming Inventory Details</h2>
    			</header>
        <!-- widget div-->
        <div>
            <!-- widget content -->
            <div class="widget-body no-padding">
              <table id="datatable2" class="table table-striped table-bordered" width="100%">
                <thead>
                  <tr>
                    <th></th>
                    <th>SKU</th>
                    <th>Menu Item Name</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['menus'] as $menus)
                  <meta name="csrf_token" content="{{ csrf_token() }}" />
                    <tr>
                      <td><input type="checkbox" id="menuItemCheckbox" class="radio" value="{{$menus->SKU}}" name="checkboxGroup"/></td>
                      <td>{{ $menus->SKU }}</td>
                      <td>{{ $menus->ItemName }}</td>
                  </tr>
                  @endforeach
               </tbody>
          </table>
        </div>
        </div><!-- end widget content -->
      </div><!-- end widget div -->
    </div><!-- end widget -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="getCheckboxValue">Select</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script>

window.onload = function(){
  $('#InventoryManagement').addClass("active open");
  $("#InventoryManagementBlock").css('display', 'block');
  $("#IncomingInventory").addClass("active");
  $("#dashboard").removeClass("active");
  $(".breadcrumb").append('<li>Inventory Management</li><li><a href="{{URL::asset(`/InventoryManagement/IncomingInventory`)}}">Incoming Inventory</a></li>');
}

$(document).ready(function() {
  getCheckedValue();
  search_by_date();
  clearDates();

  var errorClass = 'invalid';
  var errorElement = 'em';

  var $checkoutForm = $('#kiosk-form').validate({
      errorClass      : errorClass,
      errorElement    : errorElement,
      highlight: function(element) {
          $(element).parent().removeClass('state-success').addClass("state-error");
          $(element).removeClass('valid');
      },
      unhighlight: function(element) {
          $(element).parent().removeClass("state-error").addClass('state-success');
          $(element).addClass('valid');
      },

  // Rules for form validation
      rules : {
          kioskID : {
              required : true
          },
      },

      // Messages for form validation
      messages : {
          kioskID : {
              required : 'Please enter your preffered Kiosk ID'
          },
      },

      // Do not change code below
      errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
      }
  });

  var responsiveHelper_datatable_fixed_column = undefined;
  var breakpointDefinition = {
  tablet : 1024,
  phone : 480
  };

  /* COLUMN FILTER  */
  var otable = $('#datatable_fixed_column').DataTable({
  "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
      "t"+
      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
  "autoWidth" : true,
  "preDrawCallback" : function() {
    // Initialize the responsive datatables helper once.
    if (!responsiveHelper_datatable_fixed_column) {
      responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
    }
  },
  "rowCallback" : function(nRow) {
    responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
  },
  "drawCallback" : function(oSettings) {
    responsiveHelper_datatable_fixed_column.respond();
  }
});
var buttons = new $.fn.dataTable.Buttons(otable, {
  buttons: [
         'csv', 'excel', 'pdf', 'print'
      ]
  }).container().appendTo($('#exportBtn'));

  // Apply the filter
  $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {

      otable
          .column( $(this).parent().index()+':visible' )
          .search( this.value )
          .draw();

  });
/* END COLUMN FILTER */

 /* Spinners */
 $("#quantity").spinner({
   step : 1,
   numberFormat : "n"
 });

//Checkboxes
$('input[type="checkbox"]').on('change', function() {
   $('input[type="checkbox"]').not(this).prop('checked', false);
});

function getCheckedValue()
{
  $('#getCheckboxValue').click(function(){
      var val = [];
      $(':checkbox:checked').each(function(i){
        val[i] = $(this).val();
        // val.push($(this).attr("id"));
        $("#myModal").modal("hide");
      });
      $('input[id=itemCode]').val(val);

      $.ajax({
          type: "POST",
          url: "{{url('InventoryManagement/IncomingInventory/RetrieveMenuItemName')}}",
          data: {
                'itemCode': $('input[name=itemCode]').val(),
          },
          dataType: "json",
          success: function(data)
          {
            $("#itemName").val(data[0].itemName)
           },
       });

    });
  }
  // Date Range Picker
   $("#fromDate").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 2,
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       dateFormat: 'yy-mm-dd'
   });

  function search_by_date(){
      $("#searchByDate").click(function() {
        var fromDate = $("#fromDate").val();
        $.ajax({
            type: "POST",
            url: "{{url('InventoryManagement/IncomingInventory/GetInventoryByDate')}}",
            data: {
                  'fromDate': fromDate,
            },
            dataType: "json",
            success: function(data)
            {
              console.log(data);
                $("#incomingInventoryBody").empty();

                for(var i = 0; i < data.length; i++)
                {
                  var url = "{{url('/InventoryManagement/IncomingInventory/DeleteItem')}}" + "/" +data[i].inventoryId;
                    var myvar = '<tr>'+
                    '<td>'+ data[i].inventoryId +'</td>'+
                    '<td>'+ data[i].itemCode +'</td>'+
                    '<td>'+ data[i].itemName +'</td>'+
                    '<td>'+ data[i].totalQuantity +'</td>'+
                    '<td>'+ data[i].remarks +'</td>'+
                    '<td>'+ data[i].created_at +'</td>'+
                    '<td>'+ data[i].operator +'</td>'+
                    '<td><a class="delete-modal btn btn-sm btn-danger" href=' + url + '><i class="fa fa-fw fa-trash-o"></i></a></td>'+
                    '</tr>';
                    $("#incomingInventoryBody").append(myvar);
                }
             },
         });
    });
  }
  function clearDates(){
    $("#clearDatesBtn").click(function() {
      $('#fromDate').val('').datepicker("refresh");
      $('#fromDate').datepicker('setDate', null);
        var table = $('#datatable_fixed_column').DataTable({
        retrieve: true
      });
      table.draw();
    });
  }
});

/* Add inventory */
function showAddModal()
{
  $('#itemCode').val('');
  $('#remarks').val('');
  $("#time").addClass('input-disabled');
  $("#time").prop("readonly", true);

  $('#inventoryModal').modal({backdrop: 'static', keyboard: false});
  $('#inventoryModal').modal('show');
}

</script>
@endsection
