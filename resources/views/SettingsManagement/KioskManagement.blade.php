@extends('includes.template')
@section('content')

	<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-laptop fa-fw "></i> 
				Kiosk Management
		</h1>
	</div>
</div>

    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif


    <section id="widget-grid" class="">

        <!-- NEW WIDGET START -->
	    <article class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

	        <!-- Widget ID (each widget will need unique ID)-->
	        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" 
	            data-widget-editbutton="true" 
	            data-widget-deletebutton="false"
	            data-widget-fullscreenbutton="false"
	            data-widget-collapsed="false"
	            >
	            
	            <!-- widget options:
	            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

	            data-widget-colorbutton="false"
	            data-widget-editbutton="false"
	            data-widget-togglebutton="false"
	            data-widget-deletebutton="false"
	            data-widget-fullscreenbutton="false"
	            data-widget-custombutton="false"
	            data-widget-collapsed="true"
	            data-widget-sortable="false"

	            -->
	            <header>
	                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
	                <h2>Kiosk Names</h2>
	            	@if(!empty($data['add']))
                    <button type="button" class="btn btn-primary btn-sm pull-right" id="addKiosk" data-toggle="modal" href="#myModal" style="margin-left: 6px" >Add</button>
                    @endif
	            </header>

	            <!-- widget div-->
	            <div>

	                <!-- widget edit box -->
	                <div class="jarviswidget-editbox">
	                    <!-- This area used as dropdown edit box -->


	                </div>
	                <!-- end widget edit box -->

	                <!-- widget content -->
	                <div class="widget-body no-padding">
	                    <div class="table-responsive">
	                            
	                        <table class="table table-hover" id="tableUsers">
	                            <thead >
	                                <tr class="text-center">
	                                    <th>KIOSKS</th>
	                                    <th></th>
	                                </tr>
	                            </thead>
	                            
	                            <tbody>
	                            <meta name="csrf_token" content="{{ csrf_token() }}" />
	                            @foreach($data['kiosk'] as $kiosk)
	                                <tr onclick="GetKioskCategories('{{$kiosk->KioskID}}')">
	                                    <td>{{$kiosk->KioskID}}</td>
	                                    @if(!empty($data['delete']))
	                                    <td class="text-right"><a type="button" class="btn btn-danger btn-sm" href="{{url('/SettingsManagement/KioskManagement/DeleteKiosk/'.$kiosk->KioskID)}}"> Delete </a>
	                                    </td>
	                                    @endif
	                                </tr>
	                            @endforeach
	                            </tbody>
	                        </table>
	                        
	                    </div>
	                </div>
	                <!-- end widget content -->

	            </div>
	            <!-- end widget div -->

	        </div>
	        <!-- end widget -->

	    </article>

	    <article class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
	    	<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Category List <p id="title"></p></h2>
				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">

					        <thead>
					            <tr>
				                    <th>Category ID</th>
				                    <th>Category Name</th>
				                    <th>Sub Category ID</th>
				                    <th>Sub Category Name</th>
				                    <th>Breakfast</th>
				                    <th>Lunch</th>
				                    <th>Dinner</th>
					            </tr>
					        </thead>

					        <tbody></tbody>

						</table>

					</div>
					<!-- end widget content -->
				<input type="hidden" id="chosenKiosk">
				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->
		</article>


	</section>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modalTitle">
                        <!-- <img src="img/logo.png" width="150" alt="SmartAdmin"> -->
                        Create Kiosk
                    </h4>
                </div>
                <div class="modal-body no-padding">

                    <form id="kiosk-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('SettingsManagement/KioskManagement/AddKiosk')}}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <fieldset>
                            <section id="userid-section">
                                <div class="row">
                                    <label class="label col col-4">Kiosk ID</label>
                                    <section class="col col-8">
                                        <label class="input"> <i class="icon-prepend fa fa-laptop"></i>
                                            <input type="text" id="kioskID" name="kioskID" placeholder="Kiosk ID" autocomplete="off">
                                        </label>
                                    </section>
                                </div>
                            </section>

                        </fieldset>
                                
                        <footer>
                            <button type="submit" class="btn btn-primary" id="newOrUpdate">
                                Create
                            </button>
                        </footer>
                    </form>                     
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->




@include('includes.scripts')

<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>



<script>
window.onload = function(){
  	$(".breadcrumb").append('<li>Settings Management</li><li><a href="{{URL::asset("/SettingsManagement/KioskManagement")}}">Kiosk Management</a></li>');
  	$("#SettingsManagementBlock").css('display', 'block');
  	$('#SettingsManagementBlock').addClass("active open");
  	$("#KioskManagement").addClass("active");
  	$("#dashboard").removeClass("active");
  	
  	$("#saveBtn").hide();
  	
  	

  	var errorClass = 'invalid';
    var errorElement = 'em';
    
    var $checkoutForm = $('#kiosk-form').validate({
        errorClass      : errorClass,
        errorElement    : errorElement,
        highlight: function(element) {
            $(element).parent().removeClass('state-success').addClass("state-error");
            $(element).removeClass('valid');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass("state-error").addClass('state-success');
            $(element).addClass('valid');
        },

    // Rules for form validation
        rules : {
            kioskID : {
                required : true
            },          
        },

        // Messages for form validation
        messages : {
            kioskID : {
                required : 'Please enter your preffered Kiosk ID'
            },         
        },

        // Do not change code below
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });


	$('#addKiosk').click(function(){
        $('#id').val("");
        $('#kioskID').val("");
    });

	$("#kioskID").blur(function(){
		if(this.value.match(/\s/g)){
		alert('Sorry, you are not allowed to enter any spaces');
		this.value=this.value.replace(/\s/g,'');
		}
	})

	var responsiveHelper_datatable_fixed_column = undefined;
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};


	// /* COLUMN FILTER  */
 //    var otable = $('#datatable_fixed_column').DataTable({
 //    	//"bFilter": false,
 //    	//"bInfo": false,
 //    	//"bLengthChange": false
 //    	//"bAutoWidth": false,
 //    	//"bPaginate": false,
 //    	//"bStateSave": true // saves sort state using localStorage
	// 	"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
	// 			"t"+
	// 			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
	// 	"autoWidth" : true,
	// 	"preDrawCallback" : function() {
	// 		// Initialize the responsive datatables helper once.
	// 		if (!responsiveHelper_datatable_fixed_column) {
	// 			responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
	// 		}
	// 	},
	// 	"rowCallback" : function(nRow) {
	// 		responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
	// 	},
	// 	"drawCallback" : function(oSettings) {
	// 		responsiveHelper_datatable_fixed_column.respond();
	// 	}

 //    });

 //    // custom toolbar
 //    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

 //    // Apply the filter
 //    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {

 //        otable
 //            .column( $(this).parent().index()+':visible' )
 //            .search( this.value )
 //            .draw();

 //    } );
 //    /* END COLUMN FILTER */

}


function GetKioskCategories(id)
{

	$("#title").text('of '+id);
	$("#saveBtn").show();
	$("#chosenKiosk").val(id);

    var postData = {
        'kioskID' : id,
    };

    //console.log(postData);
    $.ajax({
		type: "POST",
		url: '{{url("/SettingsManagement/KioskManagement/GetKioskCategories")}}',
		data: postData,
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	beforeSend: function (xhr) {
		var token = $('meta[name="csrf_token"]').attr('content');
		if (token) {
			return xhr.setRequestHeader('X-CSRF-TOKEN', token);
		}
	},
	success: function(data)
	{
		//alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
		var items='';
        $.each(data, function(i) {
            items += "<tr>";
            items += "<td>"+data[i].ParentCode+"</td>";
            items += "<td>"+data[i].ParentName+"</td>";
            items += "<td>"+data[i].ChildCode+"</td>";
            items += "<td>"+data[i].ChildName+"</td>";
            var breakfast = (data[i].Breakfast == 1) ? "checked" : " " ;
            var lunch = (data[i].Lunch == 1) ? "checked" : " " ;
            var dinner = (data[i].Dinner == 1) ? "checked" : " " ;
            items += "<td align='center'><input type='checkbox' "+breakfast+" onclick='updateStatus(this)' data-selected='Breakfast' data-parentCode='"+data[i].ParentCode+"' data-childCode='"+data[i].ChildCode+"' ></td>";
            items += "<td align='center'><input type='checkbox' "+lunch+" onclick='updateStatus(this)' data-selected='Lunch' data-parentCode='"+data[i].ParentCode+"' data-childCode='"+data[i].ChildCode+"'></td>";
            items += "<td align='center'><input type='checkbox' "+dinner+" onclick='updateStatus(this)' data-selected='Dinner' data-parentCode='"+data[i].ParentCode+"' data-childCode='"+data[i].ChildCode+"'></td>";
            
            items += "</tr>";
		})
        $('#datatable_fixed_column tbody').empty().append(items);
	}
        
    });

}

function updateStatus(obj){
	var $this = $(obj);
	@if(empty($data['edit']))
    	alert('You dont have access to change status of the Kiosk. Contact your administrator');
    	if($this.is(':checked'))
        {
             $this.prop('checked',false);
        }
        else $this.prop('checked','checked');
        return false;
    @endif

	//console.log(obj);
	
	//var selected =  (obj.getAttribute('data-selected') == 'breakfast') ? "1" : "0";

	var postData = {
        'parentCode' : obj.getAttribute('data-parentCode'),
        'childCode' : obj.getAttribute('data-childCode'),
        'selected'	: obj.getAttribute('data-selected'),
        'status'	: $this.is(':checked'),
        'kioskID'	: $("#chosenKiosk").val(),
	};
        

        $.ajax({
			type: "POST",
			url: '{{url("/SettingsManagement/KioskManagement/UpdateKioskStatus")}}',
			data: postData,
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		
		beforeSend: function (xhr) {
			var token = $('meta[name="csrf_token"]').attr('content');
			if (token) {
				return xhr.setRequestHeader('X-CSRF-TOKEN', token);
			}
		},
		
		success: function(data)
		{
			//alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
        }
		
		});
	
}


</script>
@endsection