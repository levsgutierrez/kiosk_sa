@extends('includes.template')
@section('content')
<head>
</head>

<!-- Page Header -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i>Parameter Settings</h1>
        </div>
    </div>

    <!-- Sessions -->
    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

    <!-- Main Content -->
    <div class="col-md-12 col-lg-12">
        <div id="tabs" style="float:left; width:84vw;">
  				<ul>
  					<li>
  						<a href="#tabs-a">Money Acceptor Settings</a>
  					</li>
  					<li>
  						<a href="#tabs-b">Receipt Format Settings</a>
  					</li>
  					<li>
  						<a href="#tabs-c">Receipt Type Settings</a>
  					</li>
  				</ul>
  				
                <div id="tabs-a">
                   <form id="acceptor-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('SettingsManagement/DeviceSettings/SaveMoneyAcceptor')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="row">
                            <div class="col-md-6">
                                <section class="col-md-12">                              
                                    <div></div>
                                    <label class="label col col-4">Machine Number</label>
                                    <div class="row">
                                        <section class="col col-8">
                                            <label class="inline">
                                                <section class="col">
                                                    <label class="input"> <i class="icon-prepend fa fa-wrench"></i>
                                                        <input type="text" name="machineNumber" placeholder="Device Number" value="{{(!empty($data['moneyAcceptor'][0])) ? $data['moneyAcceptor'][0] : null   }}">
                                                    </label>
                                                </section>
                                            </label>
                                        </section>
                                    </div>

                                    <label class="label col col-4">Paper Acceptor</label>
                                    <div class="row">
                                        <section class="col col-12">
                                            <label class="inline">
                                                <section class="col col-5">
                                                    <label class="input"> <i class="icon-prepend fa fa-wrench"></i>
                                                        <input type="text" name="paperPort" placeholder="Port" value="{{(!empty($data['moneyAcceptor'][1])) ? $data['moneyAcceptor'][1] : null   }}">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                        <input type="text" name="paperModel" placeholder="Model" value="{{(!empty($data['moneyAcceptor'][2])) ? $data['moneyAcceptor'][2] : null   }}">
                                                    </label>
                                                </section>
                                            </label>
                                        </section>
                                    </div>

                                    <label class="label col col-4">Coin Acceptor</label>
                                    <div class="row">
                                        <section class="col col-12">
                                            <label class="inline">
                                                <section class="col col-5">
                                                    <label class="input"> <i class="icon-prepend fa fa-wrench"></i>
                                                        <input type="text" name="coinPort" placeholder="Port" value="{{(!empty($data['moneyAcceptor'][3])) ? $data['moneyAcceptor'][3] : null   }}">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                        <input type="text" name="coinModel" placeholder="Model" value="{{(!empty($data['moneyAcceptor'][4])) ? $data['moneyAcceptor'][4] : null   }}">
                                                    </label>
                                                </section>
                                            </label>
                                        </section>
                                    </div>
                                </section>
                            </div>

                            <div class="col-md-5">
                                <section>                              
                                    <table id="table1" class="table table-striped table-bordered col-md-10" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="30%">???</th>
                                                <th width="30%" data-class="expand"><i class="text-muted hidden-md hidden-sm hidden-xs"></i>Amount</th>
                                                <th width="40%">Model</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                            <button type="submit" class="btn btn-primary" id=" ">Create</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete" onclick=" ">Delete</button>
                            </div> 
                        </div><!--row-->
                        <button type="submit" class="btn btn-success btn-lg pull-right">Save</button>
                    </form>
  				</div><!-- close tab 1 -->

  				<div id="tabs-b">
                    <form id="receipt-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('SettingsManagement/DeviceSettings/SaveSettings')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="row">
      					<div class="col-md-4">
                            <section class="col-md-12">                              
                                <div></div>
                                <div class="row">
                                    <section class="col col-12">
                                        <label class="inline">
                                            <section class="col col-10">
                                                <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                    <input type="text" name="value[]" value="{{$data['receiptHeader']}}" placeholder="Company Name">
                                                </label>
                                            </section>
                                            
                                        
                                            <section class="col col-10">
                                                <label class="input"> <i class="icon-prepend fa fa-barcode"></i>
                                                    <input type="text" name="value[]" value="{{$data['receiptFooter']}}" placeholder="Footer Remarks">
                                                </label>
                                            </section>
                                            
                                        </label>                                     

                                        <label class="checkbox">
                                            <input type="checkbox" id="memberBalance" name="value[]"  {{(!$data['memberBalance']) ? ' ' : 'checked' }}>
                                            <i></i>Display membership balance
                                        </label>

                                        <label class="checkbox">
                                            <input type="checkbox" id="queueNumber" name="value[]" {{(!$data['queueNumber']) ? ' ' : 'checked' }}>
                                            <i></i>Display queue number
                                        </label>
                                    </section> <!-- /input group -->
                                </div>
                            </section>
                        </div>
                    </div>
                    </form>
                     <button type="submit" class="btn btn-success btn-lg pull-right" onclick="saveReceipt()">Save</button>
  				</div>
                <div id="tabs-c">
                    <form id="receiptType-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('SettingsManagement/DeviceSettings/SaveSettings')}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="row">
  					<div class="col-md-8">
                        <section class="col col-12">
                        <div></div>
                            <label>Local Print Settings</label>

                            <label class="checkbox">
                                <input type="checkbox" id="print0" name="printReceipt[0]" value="Customer settlement small note" 
                                {{ in_array("print0", $data['printReceipt']) ? 'checked' : '' }}>
                                <i></i> Customer settlement small note
                            </label>
                           <label class="checkbox">
                                <input type="checkbox" id="print1" name="printReceipt[1]" value="Customer take small ticket rooms"   
                                {{ in_array("print1", $data['printReceipt']) ? 'checked' : '' }}>
                                <i></i> Customer take small ticket rooms
                            </label>
                            <label class="checkbox">
                                <input type="checkbox" id="print2" name="printReceipt[2]" value="Cook kitchen small ticket room"   
                                {{ in_array("print2", $data['printReceipt']) ? 'checked' : '' }}>
                                <i></i> Cook kitchen small ticket room
                            </label>
                            <label class="checkbox">
                                <input type="checkbox" id="print3" name="printReceipt[3]" value="Kitchen Screen Display"   
                                {{ in_array("print3", $data['printReceipt']) ? 'checked' : '' }}>
                                <i></i> Kitchen Screen Display
                            </label>
                            
                        </section>
                    </div>
                    </div>
                    </form>
                         <button type="submit" class="btn btn-success btn-lg pull-right" onclick="saveReceiptType()">Save</button>
  				</div>
  			</div>
    </div>
    

</div><!-- /content -->


@include('includes.scripts')
<!-- ============================= JAVSCRIPT ================================ -->
<script>
window.onload = function(){
    $(".breadcrumb").append('<li>Settings Management</li><li><a href="{{URL::asset("/SettingsManagement/ParameterSettings")}}">Parameter Settings</a></li>');
    $('#SettingsManagement').addClass("active open");
    $("#SettingsManagementBlock").css('display', 'block');
    $("#ParameterSettings").addClass("active");
    $("#dashboard").removeClass("active");

}

 $(document).ready(function() {

  /** Just Tabs*/
  $('#tabs').tabs();

  /* Spinners */
  $("#depositAmount").spinner({
    step : 1,
    numberFormat : "n"
  });

});

function MoneyAcceptor()
{
    $('#acceptor-form').attr('action',"{{url('SettingsManagement/DeviceSettings/SaveReceipt')}}");
    $('#acceptor-form').removeAttr('onsubmit');
    $('#acceptor-form').submit();
}

function saveReceipt()
{
    $('#receipt-form').attr('action',"{{url('SettingsManagement/DeviceSettings/SaveReceipt')}}");
    $('#receipt-form').removeAttr('onsubmit');
    $('#receipt-form').submit();
}

function saveReceiptType()
{
    $('#receiptType-form').attr('action',"{{url('SettingsManagement/DeviceSettings/saveReceiptType')}}");
    $('#receiptType-form').removeAttr('onsubmit');
    $('#receiptType-form').submit();
}
</script>
@endsection
