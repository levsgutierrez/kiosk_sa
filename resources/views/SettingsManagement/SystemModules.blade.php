@extends('includes.template')
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa fa-desktop fa-fw "></i> 
            UI Elements 
            <span>>
            Tree View
            </span>
        </h1>
    </div>
</div>



<!-- widget grid -->


    <!-- NEW WIDGET START -->
    <article class="col-sm-4 col-md-4 col-lg-4">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
            <!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
            <header>
                <span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
                <h2>Simple View </h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <div class="tree smart-form">
                        <ul>
                            <li>
                                <span><i class="fa fa-lg fa-folder-open"></i> Parent</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Administrators</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" name="checkbox-inline">
                                                        <i></i>Michael.Jackson</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" checked="checked" name="checkbox-inline">
                                                        <i></i>Sunny.Ahmed</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" checked="checked" name="checkbox-inline">
                                                        <i></i>Jackie.Chan</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-minus-circle"></i> Child</span>
                                        <ul>
                                            <li>
                                                <span><i class="icon-leaf"></i> Grand Child</span>
                                            </li>
                                            <li>
                                                <span><i class="icon-leaf"></i> Grand Child</span>
                                            </li>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i> Grand Child</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> Great Grand Child</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <span><i class="icon-leaf"></i> Great great Grand Child</span>
                                                            </li>
                                                            <li style="display:none">
                                                                <span><i class="icon-leaf"></i> Great great Grand Child</span>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li style="display:none">
                                                        <span><i class="icon-leaf"></i> Great Grand Child</span>
                                                    </li>
                                                    <li style="display:none">
                                                        <span><i class="icon-leaf"></i> Great Grand Child</span>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <span><i class="fa fa-lg fa-folder-open"></i> Parent2</span>
                                <ul>
                                    <li>
                                        <span><i class="icon-leaf"></i> Child</span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->

    </article>
    <!-- WIDGET END -->

      <article class="col-sm-2 col-md-2 col-lg-2">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
            <!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
            <header>
                <span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
                <h2>Simple View </h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <div class="tree smart-form">
                        <ul>
                            <li>
                                <span><i class="fa fa-lg fa-folder-open"></i> Parent</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Administrators</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" name="checkbox-inline">
                                                        <i></i>Michael.Jackson</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" checked="checked" name="checkbox-inline">
                                                        <i></i>Sunny.Ahmed</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" checked="checked" name="checkbox-inline">
                                                        <i></i>Jackie.Chan</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-minus-circle"></i> Child</span>
                                        <ul>
                                            <li>
                                                <span><i class="icon-leaf"></i> Grand Child</span>
                                            </li>
                                            <li>
                                                <span><i class="icon-leaf"></i> Grand Child</span>
                                            </li>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i> Grand Child</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> Great Grand Child</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <span><i class="icon-leaf"></i> Great great Grand Child</span>
                                                            </li>
                                                            <li style="display:none">
                                                                <span><i class="icon-leaf"></i> Great great Grand Child</span>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li style="display:none">
                                                        <span><i class="icon-leaf"></i> Great Grand Child</span>
                                                    </li>
                                                    <li style="display:none">
                                                        <span><i class="icon-leaf"></i> Great Grand Child</span>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <span><i class="fa fa-lg fa-folder-open"></i> Parent2</span>
                                <ul>
                                    <li>
                                        <span><i class="icon-leaf"></i> Child</span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->

    </article>
    <!-- WIDGET END -->



    <div class="row">   </div>
     <!-- end row -->


@include('includes.scripts')



<script>
window.onload = function(){
    $(".breadcrumb").append('<li>Settings Management</li><li><a href="{{URL::asset("/SettingsManagement/SystemModules")}}">System Modules</a></li>');
    $('#SettingsManagement').addClass("active open");
    $("#SettingsManagementBlock").css('display', 'block');
    $("#SystemModules").addClass("active");
    $("#dashboard").removeClass("active");


    $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
    $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(':visible')) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
        }
        e.stopPropagation();
    }); 
}
</script>




@endsection
