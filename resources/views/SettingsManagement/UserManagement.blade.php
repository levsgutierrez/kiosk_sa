@extends('includes.template')
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
				User Management
		</h1>
	</div>
</div>

    @if(session()->has('message'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('message') }}
        </p>
    </div>
    @endif

    @if(session()->has('deleted'))
    <div class="alert alert-block alert-warning">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
        <p>
            {{ session()->get('deleted') }}
        </p>
    </div>
    @endif

    @if($errors->any())
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
        <p>
            {{$errors->first()}}
        </p>
    </div>
    @endif

        <!-- NEW WIDGET START -->
        <article class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1"
                data-widget-editbutton="true"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false"
                >

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Users</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->


                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="table-responsive">

                            <table class="table table-hover" id="tableUsers">
                                <thead >
                                    <tr class="text-center">
                                        <th>USER ID</th>
                                        <th>USERNAME</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                @foreach($data['users'] as $users)
                                    <tr data-name="{{$users->name}}" >
                                        <td>{{$users->email}}</td>
                                        <td>{{$users->name}}</td>
                                        <td>
                                            @if(!empty($data['edit']))
                                            <button type="button" class="btn btn-sm btn-default" id="editUser"
                                            data-id="{{$users->id}}"
                                            data-userID="{{$users->email}}"
                                            data-company="{{$users->company}}"
                                            data-password="{{$users->password}}"
                                            data-fname="{{$users->name}}"
                                            onclick="edit(this)">Edit</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                                @if(!empty($data['add']))
                                <tfoot class="text-center">
                                    <tr>
                                        <td colspan="3" class="text-right"><button type="button" class="btn btn-primary" id="addUser" data-toggle="modal" href="#myModal">Add</button></td>
                                    </tr>
                                </tfoot>
                                @endif
                            </table>

                        </div>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>

        <article class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-2" data-widget-editbutton="true">
            <!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
            <header>
                <span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
                <h2>User Rights</h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    <input type="hidden" id="selected_name" name="selected_name">
                    <div class="tree smart-form">
                        <ul>
                            <li>
                                <span><i class="fa fa-lg fa-plus-circle"></i> Settings Management</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> User Management</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox">
                                                        <input type="checkbox" id="AccessUser" name="AccessUser" data-moduleCode="110" data-actionName="AccessUser" onchange="addModule(this)">
                                                        <i></i>User Management</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="CreateUser" name="CreateUser" data-moduleCode="110" data-actionName="CreateUser" onchange="addModule(this)">
                                                        <i></i>Create User</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditUser" name="EditUser" data-moduleCode="110" data-actionName="EditUser" onchange="addModule(this)" >
                                                        <i></i>Edit User</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteUser" name="DeleteUser" data-moduleCode="110" data-actionName="DeleteUser" onchange="addModule(this)" >
                                                        <i></i>Delete User</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Outlet Management</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessOutletKiosk" name="AccessOutletKiosk" data-moduleCode="120" data-actionName="AccessOutletKiosk" onchange="addModule(this)">
                                                        <i></i>Outlet Management</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddOutletKiosk" name="AddOutletKiosk" data-moduleCode="120" data-actionName="AddOutletKiosk" onchange="addModule(this)">
                                                        <i></i>Create Outlet</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditOutletKiosk" name="EditOutletKiosk" data-moduleCode="120" data-actionName="EditOutletKiosk" onchange="addModule(this)">
                                                        <i></i>Edit Outlet</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteOutletKiosk" name="DeleteOutletKiosk" data-moduleCode="120" data-actionName="DeleteOutletKiosk" onchange="addModule(this)">
                                                        <i></i>Delete Outlet</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li style=" ">
                                        <span> <label class="checkbox inline-block">
                                                <input type="checkbox" id="AccessDevice" name="AccessDevice" data-moduleCode="130" data-actionName="AccessDevice" onchange="addModule(this)">
                                                <i></i>Device Settings</label> </span>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Kiosk Management</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                         <input type="checkbox" id="AccessKioskManagement" name="AccessKioskManagement" data-moduleCode="140" data-actionName="AccessKioskManagement" onchange="addModule(this)">
                                                        <i></i>Kiosk Management</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddKiosk" name="AddKiosk" data-moduleCode="140" data-actionName="AddKiosk" onchange="addModule(this)">
                                                        <i></i>Create Kiosk</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditKiosk" name="EditKiosk" data-moduleCode="140" data-actionName="EditKiosk" onchange="addModule(this)">
                                                        <i></i>Edit Kiosk</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteKiosk" name="DeleteKiosk" data-moduleCode="140" data-actionName="DeleteKiosk" onchange="addModule(this)">
                                                        <i></i>Delete Kiosk</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                    <li style=" ">
                                        <span> <label class="checkbox inline-block">
                                                <input type="checkbox" id="AccessActivityLogs" name="AccessActivityLogs" data-moduleCode="150" data-actionName="AccessActivityLogs" onchange="addModule(this)">
                                                <i></i>Activity Logs</label> </span>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <span><i class="fa fa-lg fa-plus-circle"></i> Menu Management</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Menu Items</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMenu" name="AccessMenu" data-moduleCode="210" data-actionName="AccessMenu" onchange="addModule(this)">
                                                        <i></i>Menu Item</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddItem" name="AddItem" data-moduleCode="210" data-actionName="AddItem" onchange="addModule(this)">
                                                        <i></i>Add Item</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditItem" name="EditItem" data-moduleCode="210" data-actionName="EditItem" onchange="addModule(this)">
                                                        <i></i>Edit Item</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DuplicateItem" name="DuplicateItem" data-moduleCode="210" data-actionName="DuplicateItem" onchange="addModule(this)">
                                                        <i></i>Duplicate Item</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteItem" name="DeleteItem" data-moduleCode="210" data-actionName="DeleteItem" onchange="addModule(this)">
                                                        <i></i>Delete Item</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Menu Categories</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMenuCategories" name="AccessMenuCategories" data-moduleCode="220" data-actionName="AccessMenuCategories" onchange="addModule(this)">
                                                        <i></i>Menu Categories</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddCategories" name="AddCategories" data-moduleCode="220" data-actionName="AddCategories" onchange="addModule(this)">
                                                        <i></i>Add Categories</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditCategories" name="EditCategories" data-moduleCode="220" data-actionName="EditCategories" onchange="addModule(this)">
                                                        <i></i>Edit Categories</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteCategories" name="DeleteCategories" data-moduleCode="220" data-actionName="DeleteCategories" onchange="addModule(this)">
                                                        <i></i>Delete Categories</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddSubCategories" name="AddSubCategories" data-moduleCode="220" data-actionName="AddSubCategories" onchange="addModule(this)">
                                                        <i></i>Add Sub Categories</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditSubCategories" name="EditSubCategories" data-moduleCode="220" data-actionName="EditSubCategories" onchange="addModule(this)">
                                                        <i></i>Edit Sub Categories</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteSubCategories" name="DeleteSubCategories" data-moduleCode="220" data-actionName="DeleteSubCategories" onchange="addModule(this)">
                                                        <i></i>Delete Sub Categories</label> </span>
                                            </li>

                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Menu Item Display</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Menu Item Display</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddItems" name="AddItems" data-moduleCode="230" data-actionName="AddItems" onchange="addModule(this)">
                                                        <i></i>Add Items to Categories</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteItems" name="DeleteItems" data-moduleCode="230" data-actionName="DeleteItems" onchange="addModule(this)">
                                                        <i></i>Delete Items to Categories</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                            <li>
                                <span><i class="fa fa-lg fa-plus-circle"></i> Sales Management</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Receipt History</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMenu" name="AccessMenu" data-moduleCode="210" data-actionName="AccessMenu" onchange="addModule(this)">
                                                        <i></i>Sample 1</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddItem" name="AddItem" data-moduleCode="210" data-actionName="AddItem" onchange="addModule(this)">
                                                        <i></i>Sample 2</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditItem" name="EditItem" data-moduleCode="210" data-actionName="EditItem" onchange="addModule(this)">
                                                        <i></i>Sample 3</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Discount Settings</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMenuCategories" name="AccessMenuCategories" data-moduleCode="220" data-actionName="AccessMenuCategories" onchange="addModule(this)">
                                                        <i></i>Add New Discount</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddCategories" name="AddCategories" data-moduleCode="220" data-actionName="AddCategories" onchange="addModule(this)">
                                                        <i></i>Edit Discount Settings</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditCategories" name="EditCategories" data-moduleCode="220" data-actionName="EditCategories" onchange="addModule(this)">
                                                        <i></i>Delete Discount Settings </label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteCategories" name="DeleteCategories" data-moduleCode="220" data-actionName="DeleteCategories" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Content</label> </span>
                                            </li>
                                          </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Replenish Cash</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Cash Replenishment</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddItems" name="AddItems" data-moduleCode="230" data-actionName="AddItems" onchange="addModule(this)">
                                                        <i></i>Edit Cash Replenishment Transaction</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteItems" name="DeleteItems" data-moduleCode="230" data-actionName="DeleteItems" onchange="addModule(this)">
                                                        <i></i>Delete Cash Replenishment Transaction</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteItems" name="DeleteItems" data-moduleCode="230" data-actionName="DeleteItems" onchange="addModule(this)">
                                                        <i></i>Allow Export of Transactions Content</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Cash Out</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Cash Out</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddItems" name="AddItems" data-moduleCode="230" data-actionName="AddItems" onchange="addModule(this)">
                                                        <i></i>Edit Cash Out Transaction</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteItems" name="DeleteItems" data-moduleCode="230" data-actionName="DeleteItems" onchange="addModule(this)">
                                                        <i></i>Delete Cash Out Transaction</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteItems" name="DeleteItems" data-moduleCode="230" data-actionName="DeleteItems" onchange="addModule(this)">
                                                        <i></i>Allow Export of Transactions Content</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Kiosk History</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Sample 1</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddItems" name="AddItems" data-moduleCode="230" data-actionName="AddItems" onchange="addModule(this)">
                                                        <i></i>Sample 2</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteItems" name="DeleteItems" data-moduleCode="230" data-actionName="DeleteItems" onchange="addModule(this)">
                                                        <i></i>Sample 3</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Order History</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Allow Export</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                            <li>
                                <span><i class="fa fa-lg fa-plus-circle"></i> Membership Accounts</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Members Details</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMembers" name="AccessMembers" data-moduleCode="410" data-actionName="AccessMembers" onchange="addModule(this)">
                                                        <i></i>Members Details</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddMembers" name="AddMembers" data-moduleCode="410" data-actionName="AddMembers" onchange="addModule(this)">
                                                        <i></i>Add Member</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditMembers" name="EditMembers" data-moduleCode="410" data-actionName="EditMembers" onchange="addModule(this)">
                                                        <i></i>Edit Member Details</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteMembers" name="DeleteMembers" data-moduleCode="410" data-actionName="DeleteMembers" onchange="addModule(this)">
                                                        <i></i>Delete Member</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportMembers" name="ExportMembers" data-moduleCode="410" data-actionName="ExportMembers" onchange="addModule(this)">
                                                        <i></i>Allow Export</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Membership Types</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMembership" name="AccessMembership" data-moduleCode="420" data-actionName="AccessMembership" onchange="addModule(this)">
                                                        <i></i>Membership Types</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddMembership" name="AddMembership" data-moduleCode="420" data-actionName="AddMembership" onchange="addModule(this)">
                                                        <i></i>Add Membership Types</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="EditMembership" name="EditMembership" data-moduleCode="420" data-actionName="EditMembership" onchange="addModule(this)">
                                                        <i></i>Edit Membership Types</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteMembership" name="DeleteMembership" data-moduleCode="420" data-actionName="DeleteMembership" onchange="addModule(this)">
                                                        <i></i>Delete Membership Types</label> </span>
                                            </li>
                                          </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Topup Accounts</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessTopup" name="AccessTopup" data-moduleCode="430" data-actionName="AccessTopup" onchange="addModule(this)">
                                                        <i></i>Topup Accounts</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="TopupMembers" name="TopupMembers" data-moduleCode="430" data-actionName="TopupMembers" onchange="addModule(this)">
                                                        <i></i>Topup Members Accounts</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ReprintReceipt" name="ReprintReceipt" data-moduleCode="430" data-actionName="ReprintReceipt" onchange="addModule(this)">
                                                        <i></i>Reprint Receipt</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportTopup" name="ExportTopup" data-moduleCode="430" data-actionName="ExportTopup" onchange="addModule(this)">
                                                        <i></i>Allow Export</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Deposit Statistics</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                         <input type="checkbox" id="AccessDepositStatistics" name="AccessDepositStatistics" data-moduleCode="440" data-actionName="AccessDepositStatistics" onchange="addModule(this)">
                                                        <i></i>Deposit Statistics</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportDepositStatistics" name="ExportDepositStatistics" data-moduleCode="440" data-actionName="ExportDepositStatistics" onchange="addModule(this)">
                                                        <i></i>Allow Export</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Expenditure List</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                         <input type="checkbox" id="AccessExpenditure" name="AccessExpenditure" data-moduleCode="450" data-actionName="AccessExpenditure" onchange="addModule(this)">
                                                        <i></i>Expenditure List</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportExpenditure" name="ExportExpenditure" data-moduleCode="450" data-actionName="ExportExpenditure" onchange="addModule(this)">
                                                        <i></i>Allow Export</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Members Statistics</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                         <input type="checkbox" id="AccessMembersStatistics" name="AccessMembersStatistics" data-moduleCode="460" data-actionName="AccessMembersStatistics" onchange="addModule(this)">
                                                        <i></i> Members Statistics</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportMembersStatistics" name="ExportMembersStatistics" data-moduleCode="460" data-actionName="ExportMembersStatistics" onchange="addModule(this)">
                                                        <i></i> Allow Export</label> </span>
                                            </li>
                                          </ul>
                                    </li>

                                </ul>
                            </li>

                            <li>
                                <span><i class="fa fa-lg fa-plus-circle"></i> Inventory Management</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Inventory</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessInventory" name="AccessInventory" data-moduleCode="510" data-actionName="AccessInventory" onchange="addModule(this)">
                                                        <i></i>Inventory</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ChangeInventoryStatus" name="ChangeInventoryStatus" data-moduleCode="510" data-actionName="ChangeInventoryStatus" onchange="addModule(this)">
                                                        <i></i>Change Menu Item Status</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Incoming Inventory</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessIncomingInventory" name="AccessIncomingInventory" data-moduleCode="520" data-actionName="AccessIncomingInventory" onchange="addModule(this)">
                                                        <i></i>Incoming Inventory</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AddIncomingInventory" name="AddIncomingInventory" data-moduleCode="520" data-actionName="AddIncomingInventory" onchange="addModule(this)">
                                                        <i></i>Add Incoming Inventory</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="DeleteIncomingInventory" name="DeleteIncomingInventory" data-moduleCode="520" data-actionName="DeleteIncomingInventory" onchange="addModule(this)">
                                                        <i></i>Delete Incoming Inventory</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                         <input type="checkbox" id="InventoryReprintReceipt" name="InventoryReprintReceipt" data-moduleCode="520" data-actionName="InventoryReprintReceipt" onchange="addModule(this)">
                                                        <i></i>Reprint Receipt</label> </span>
                                            </li>
                                            <li style="display:none">
                                                  <span> <label class="checkbox inline-block">
                                                          <input type="checkbox" id="ExportInventory" name="ExportInventory" data-moduleCode="520" data-actionName="ExportInventory" onchange="addModule(this)">
                                                          <i></i>Allow Export</label> </span>
                                              </li>
                                          </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Outgoing Inventory</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessOutgoingInventory" name="AccessOutgoingInventory" data-moduleCode="530" data-actionName="AccessOutgoingInventory" onchange="addModule(this)">
                                                        <i></i>Outgoing Inventory</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                         <input type="checkbox" id="AddOutgoingInventory" name="AddOutgoingInventory" data-moduleCode="530" data-actionName="AddOutgoingInventory" onchange="addModule(this)">
                                                        <i></i>Add Outgoing Inventory</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ClearInventory" name="ClearInventory" data-moduleCode="530" data-actionName="ClearInventory" onchange="addModule(this)">
                                                        <i></i>Clear Inventory</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ReprintInventory" name="ReprintInventory" data-moduleCode="530" data-actionName="ReprintInventory" onchange="addModule(this)">
                                                        <i></i>Reprint Receipt</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportOutgoingInventory" name="ExportOutgoingInventory" data-moduleCode="530" data-actionName="ExportOutgoingInventory" onchange="addModule(this)">
                                                        <i></i>Allow Export</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Inventory Inquiry</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessInventoryInquiry" name="AccessInventoryInquiry" data-moduleCode="540" data-actionName="AccessInventoryInquiry" onchange="addModule(this)">
                                                        <i></i>Inventory Inquiry</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportInventoryInquiry" name="ExportInventoryInquiry" data-moduleCode="540" data-actionName="ExportInventoryInquiry" onchange="addModule(this)">
                                                        <i></i>Allow Export</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Inventory Ledger</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                       <input type="checkbox" id="AccessInventoryLedger" name="AccessInventoryLedger" data-moduleCode="550" data-actionName="AccessInventoryLedger" onchange="addModule(this)">
                                                        <i></i>Inventory Ledger</label> </span>
                                            </li>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="ExportInventoryLedger" name="ExportInventoryLedger" data-moduleCode="550" data-actionName="ExportInventoryLedger" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Contents</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li collapsed>
                                <span><i class="fa fa-lg fa-plus-circle"></i> Sales Report</span>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Daily Sales Report</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMenu" name="AccessMenu" data-moduleCode="210" data-actionName="AccessMenu" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Contents</label> </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Item Sales Statistics</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessMenuCategories" name="AccessMenuCategories" data-moduleCode="220" data-actionName="AccessMenuCategories" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Contents</label> </span>
                                            </li>
                                          </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Daily Sales Statistics</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Contents</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Topup Invoice Statistics</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Contents</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Order Invoice Statistics</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Contents</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Scheduling Statistics</span>
                                        <ul>
                                            <li style="display:none">
                                                <span> <label class="checkbox inline-block">
                                                        <input type="checkbox" id="AccessItems" name="AccessItems" data-moduleCode="230" data-actionName="AccessItems" onchange="addModule(this)">
                                                        <i></i>Allow Export of Table Contents</label> </span>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>


                        </ul>
                    </div>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->
            dsad
            {{print_r($data['company'])}}
        </div>
        <!-- end widget -->

        </article>


        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="modalTitle">
                            <!-- <img src="img/logo.png" width="150" alt="SmartAdmin"> -->
                            User Configuration
                        </h4>
                    </div>
                    <div class="modal-body no-padding">

                        <form id="user-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('SettingsManagement/UserManagement/AddUser ')}}">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <fieldset>
                                
                                @if(Auth::user()->name == 'Administrator')
                                <section id="companyName">
                                    <div class="row">
                                        <label class="label col col-4">Company Name</label> 
                                        <section class="col col-8">
                                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                                                <input type="text" id="company" name="company" placeholder="Company Name" autocomplete="off">
                                                <!-- Example single danger button -->
                                                <div class="btn-group pull-right">
                                                  <i class="fa fa-caret-square-o-down" type="button" class="btn btn-danger btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                                  <ul class="dropdown-menu">

                                                    @foreach($data['company'] as $company)
                                                    <li class="dropdown-item" onclick="$('#company').val('{{$company->company}}')" >{{$company->company}}</li>
                                                    @endforeach
                                                  </ul>
                                                </div>
                                            </label>
                                        </section>
                                    </div>
                                </section>
                                @endif

                                <section id="userid-section">
                                    <div class="row">
                                        <label class="label col col-4">Email ID</label>
                                        <section class="col col-8">
                                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                                                <input type="text" id="email" name="email" placeholder="Valid Email Address" autocomplete="off">
                                                <input type="hidden" id="id" name="id" value="">
                                            </label>
                                        </section>
                                    </div>
                                </section>

                                <section>
                                    <div class="row">
                                        <label class="label col col-4">Password</label>
                                        <section class="col col-8">
                                            <label class="input"> <i class="icon-prepend fa fa-lock"></i>
                                                <input type="password" id="password" name="password">
                                            </label>
                                        </section>
                                    </div>
                                </section>

                                <section>
                                    <div class="row">
                                        <label class="label col col-4">Name</label>
                                        <section class="col col-8">
                                            <label class="input">
                                                <input type="text" id="fname" name="fname">
                                            </label>
                                        </section>
                                    </div>
                                </section>
                            </fieldset>

                            <footer>
                                <button type="submit" class="btn btn-primary" id="newOrUpdate">
                                    Create
                                </button>
                                @if(!empty($data['delete']))
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete" onclick="deleteUser()">
                                    Delete
                                </button>
                                @endif
                            </footer>
                        </form>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

@include('includes.scripts')

<script src="{{URL::asset('js/plugin/jquery-form/jquery-form.min.js')}}"></script>

<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {

    $(".breadcrumb").append('<li>Settings Management</li><li><a href="{{URL("/SettingsManagement/UserManagement")}}">User Management</a></li>');
    $("#SettingsManagementBlock").css('display', 'block');
    $('#SettingsManagement').addClass("active open");
    $("#UserManagement").addClass("active");
    $("#dashboard").removeClass("active");

    $('#delete').hide();

    @if(empty($data['edit']))
        $('#wid-id-2').hide();
    @endif



    var errorClass = 'invalid';
    var errorElement = 'em';

    var $checkoutForm = $('#user-form').validate({
        errorClass      : errorClass,
        errorElement    : errorElement,
        highlight: function(element) {
            $(element).parent().removeClass('state-success').addClass("state-error");
            $(element).removeClass('valid');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass("state-error").addClass('state-success');
            $(element).addClass('valid');
        },

        // Rules for form validation
        rules : {
            email : {
                required : true,
                email : true
            },
            password : {
                required : true
            },
            fname : {
                required : true
            },

        },

        // Messages for form validation
        messages : {
            email : {
                required : 'Please enter your email address',
                email : 'Please enter a VALID email address'
            },
            password : {
                required : 'Please enter your password'
            },
            fname : {
                required : 'Please enter your Full Name'
            },

        },

        // Do not change code below
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });



    $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');

    $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
    var children = $(this).parent('li.parent_li').find(' > ul > li');
    if (children.is(':visible')) {
        children.hide('fast');
        $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
    } else {
        children.show('fast');
        $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
    }
    e.stopPropagation();
    });


})

$('#addUser').click(function(){
        $('#id').val("");
        $('#userID').val("");
        $('#password').val("");
        $('#fname').val("");
    });

function deleteUser(){
    if(confirm('Are you sure you want to delete the selected user?'))
    {
        $('#user-form').attr('action',"{{url('SettingsManagement/UserManagement/DeleteUser')}}");
        $('#user-form').removeAttr('onsubmit');
        $('#user-form').submit();
    }
};



function edit(obj)
{
    $('.modal-title').text('Edit '+obj.getAttribute('data-userID'));
    $('#newOrUpdate').text('Update').addClass('btn-warning');
    $('#delete').show();
    $('#userID').attr('readonly', true).css('background-color' , '#DEDEDE');
    //$('#userid-section').prop('readonly',true);
    @if(Auth::user()->name == 'Administrator')
    $('#company').val(obj.getAttribute('data-company'));
    @endif
    $('#id').val(obj.getAttribute('data-id'));
    $('#email').val(obj.getAttribute('data-userID')).prop('readonly',true);
    
    $('#userID').val(obj.getAttribute('data-userID'));
    $('#password').val(obj.getAttribute('data-password'));
    $('#fname').val(obj.getAttribute('data-fname'));
    $('#myModal').modal('show');
}


$('#tableUsers tbody tr').click(function() {
    $(this).addClass('bg-success').siblings().removeClass('bg-success');
    $("#selected_name").val(this.getAttribute('data-name'));


    $.ajax({
      type: "POST",
      url: '{{url("/SettingsManagement/UserRights/GetRights")}}',
      data: {
        'userID' : this.getAttribute('data-name'),
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function (xhr) {
      var token = $('meta[name="csrf_token"]').attr('content');
      if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      success: function(data)
      {
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
        $('input:checkbox').prop('checked',false);
        $.each(data, function(i) {
            $('input[name='+data[i].actionName+']').prop('checked',true);
        });
      }

    });

});

function addModule(obj){

    var user = $("#selected_name").val();
    if(!user)
    {
        alert('Please select User Name you want to set rights');
        if($('input[name='+obj.id+']').is(':checked'))
        {
             $('input[name='+obj.id+']').prop('checked',false);
        }
        else $('input[name='+obj.id+']').prop('checked','checked');
        return false;
    }

    var postData = {
            'userID' : $('#selected_name').val(),
            'actionName' : obj.getAttribute('data-actionName'),
            'moduleCode' : obj.getAttribute('data-moduleCode'),
        };

    if($('input[name='+obj.id+']').is(':checked'))
    {
        postData['active'] = '1';
    }
    else
    {
       postData['active'] = '0';
    }

    //console.log(postData);
    $.ajax({
      type: "POST",
      url: '{{url("/SettingsManagement/UserRights/SetRights")}}',
      data: postData,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function (xhr) {
      var token = $('meta[name="csrf_token"]').attr('content');
      if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      success: function(data)
      {
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
        }

    });

}

</script>
@endsection
