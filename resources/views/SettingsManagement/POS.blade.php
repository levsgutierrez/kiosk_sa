@extends('includes.template')
@section('content')

<head>
</head>

@include('includes.scripts')
<script>
window.onload = function(){
  $(".breadcrumb").append('<li>Settings Management</li><li><a href="{{URL::asset("/SettingsManagement/SystemModules")}}">POS</a></li>');
  $('#SettingsManagement').addClass("active open");
  $("#SettingsManagementBlock").css('display', 'block');
  $("#POS").addClass("active");
  $("#dashboard").removeClass("active");
}
</script>
@endsection