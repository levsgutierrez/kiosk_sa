@extends('includes.template')
@section('content')

<div id="content">
	<div class="row">
	    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
	        <span><h1 class="page-title txt-color-blueDark"><i class="fa fa-building fa-fw "></i> Outlet Management </h1></span>
	    </div> 
    </div>

 	<!-- Sessions -->
    @if(session()->has('message'))
	    <div class="alert alert-block alert-success">
	        <a class="close" data-dismiss="alert" href="#">×</a>
	        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
	        <p>
	        {{ session()->get('message') }}
	        </p>
	    </div>
    @endif

    @if(session()->has('deleted'))
	    <div class="alert alert-block alert-warning">
	        <a class="close" data-dismiss="alert" href="#">×</a>
	        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Success!</h4>
	        <p>
	          {{ session()->get('deleted') }}
	        </p>
	    </div>
    @endif

    @if($errors->any())
	    <div class="alert alert-block alert-danger">
	        <a class="close" data-dismiss="alert" href="#">×</a>
	        <h4 class="alert-heading"><i class="fa fa-exclamation"></i> Error! Please contact Admin</h4>
	        <p>
	          {{$errors->first()}}
	        </p>
	    </div>
    @endif
    <!-- END Sessions -->

    <section id="widget-grid" class="">
        <!-- Menu Item Buttons -->
    	<article class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" 
                data-widget-editbutton="false" 
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false">

            <header>
                <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                <h2>Outlet Management</h2>
                @if(!empty($data['add']))
                <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showAddModal();" >Add</button>
                @endif
            </header>

           <!-- widget div-->
               <div>
                 <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                       <!-- This area used as dropdown edit box -->
                    </div><!-- /end widget edit box -->
                    <!-- widget content -->
                    
                    <div class="widget-body no-padding">
                       <table id="outlet_table" class="table table-hover">
                            <thead>
                                <tr >
                                    <th><i class="text-muted"></i>Outlet Name</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['outlets'] as $outlets)
                                <tr  data-OutletName="{{$outlets->OutletName}}" data-OutletID="{{$outlets->id}}">
                                    <td>{{$outlets->OutletName}}</td>
                                    <td>{{$outlets->Location}}</td>
                                    <td>
                                        @if(!empty($data['edit']))
                                        <button type="button" class="btn btn-sm btn-default btn-info" id="edit" rel="tooltip" data-placement="top" title="Edit" 
                                            data-id="{{$outlets->id}}"
                                            data-OutletName="{{$outlets->OutletName}}"
                                            data-Location="{{$outlets->Location}}"
                                            onclick="edit(this)"><i class="fa fa-edit fa-fw"></i></button>
                                        @endif
                                        </td>
                                <tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /end widget content -->

                </div><!-- /end widget -->

            </div> <!-- /content -->
        </article>

        <article class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" 
                data-widget-editbutton="false" 
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false">

            <header>
                <span class="widget-icon"> <i class="fa fa-file-text-o"></i> </span>
                <h2>Kiosks List</h2>
            </header>

           <!-- widget div-->
               <div>
                 <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                       <!-- This area used as dropdown edit box -->
                    </div><!-- /end widget edit box -->
                    <!-- widget content -->
                    
                    <div class="widget-body no-padding">
                       <table id="dt_basic" class="table table-striped table-hover" width="100%">
                            <thead>
                                <tr >
            						<th><i class="text-muted"></i>Outlet Name</th>
            						<th>Action</th>
              					</tr>
              				</thead>
              				<tbody>
                                @foreach($data['kiosks'] as $kiosks)
                                <tr>
                                    <td width="60%">{{$kiosks->KioskID}}</td>
                                    <td width="40%"> 
                                            @if(!empty($data['add']))
                                            <button type="button" class="btn btn-sm btn-default"
                                            data-KioskID="{{$kiosks->KioskID}}"
                                            onclick="tolist(this)"><i class="fa fa-mail-forward"></i></button>
                                            @endif
                                    </td>
                                <tr>
            					@endforeach
              				</tbody>
              			</table>
              		</div><!-- /end widget content -->

      		    </div><!-- /end widget -->

            </div> <!-- /content -->
        </article>


        <article class="col-sm-4 col-md-4 col-lg-4">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" 
                data-widget-deletebutton="false"
                data-widget-editbutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-collapsed="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
                    <h2><i id="title"></i> Kiosks</h2>
                    @if(!empty($data['add']))
                    <button type="button" class="btn btn-primary btn-sm pull-right" onclick="SaveGroup();" style="margin-left: 6px" >Save</button>
                    @endif
                </header>
                <div>
                    <div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
                    <form id="groupOutlet-form" class="smart-form" novalidate="novalidate" method="POST" action=" ">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="widget-body">
                            <input type="hidden" id="outletID" name="outletID">
                                <table id="details_table" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="55%">Code</th>
                                            <th width="35%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                        </div><!-- /end widget content -->
                    </form>
                </div>
            </div>
        </article>
    </section>

	<!--======================= Modal Add Outlet ====================-->
	<div class="modal fade" id="AddOutletModal" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-sm">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
	                    &times;
	                </button>
	                <h4 class="modal-title" id="modalTitle">
	                    <!-- <img src="img/logo.png" width="150" alt="SmartAdmin"> -->
	                    Sub Category
	                </h4>
	            </div>
	            <div class="modal-body no-padding">

	                <form id="outlet-form" class="smart-form" novalidate="novalidate" method="POST" action="{{url('/SettingsManagement/OutletManagement/AddOutlet ')}}">
	                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
	                    <fieldset>
	                        <section>
	                            <div class="row">
	                                <label class="label col col-4">Outlet Name</label>
	                                <section class="col col-8">
	                                    <label class="input">
	                                        <input type="text" id="OutletName" name="OutletName" placeholder="Sub Category" autocomplete="off">
	                                        <input type="hidden" id="id" name="id">
	                                    </label>
	                                </section>
	                            </div>
		                        <div class="row">
		                            <label class="label col col-4">Location</label>
	                                <section class="col col-8">
	                                    <label class="input">
	                                        <input type="text" id="Location" name="Location" placeholder="Location" autocomplete="off">
	                                    </label>
	                                </section>
	                            </div>
	                        </section>
	                    </fieldset>

	                    <footer>
	                        <button type="submit" class="btn btn-primary" id="newOrUpdate"> Create </button>
                            @if(!empty($data['delete']))
	                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="outletdelete" onclick="deleteOutlet()"> Delete </button>
	                       @endif
                        </footer>
	                </form>
	            </div>

	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->



</div> <!-- END Contents -->


<script>
window.onload = function(){
    $(".breadcrumb").append('<li>Settings Management</li><li><a href="{{URL::asset("/SettingsManagement/OutletManagement")}}">Outlet Management</a></li>');
    $("#SettingsManagementBlock").css('display', 'block');
    $('#SettingsManagement').addClass("active open");
    $("#OutletManagement").addClass("active");
    $("#dashboard").removeClass("active");
    
    $('#outletdelete').hide();

    var errorClass = 'invalid';
    var errorElement = 'em';

    var $checkoutForm = $('#outlet-form').validate({
        errorClass      : errorClass,
        errorElement    : errorElement,
        highlight: function(element) {
            $(element).parent().removeClass('state-success').addClass("state-error");
            $(element).removeClass('valid');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass("state-error").addClass('state-success');
            $(element).addClass('valid');
        },

        // Rules for form validation
        rules: {
		    OutletName: {
			    required: true,
		    },
		    Location: {
		        required: true
		    },
		},

        // Messages for form validation
		messages: {
		    location: "Please specify location.",
		    OutletName: {
		        required: "We need an Outlet Name to Assign Dishes in the Menu.",
		        ItemName: "Your Outlet Name is required"
			   }
		},

        // Do not change code below
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });

$('#outlet_table tbody tr').click(function() {
    $(this).addClass('bg-success').siblings().removeClass('bg-success');
    $("#outletID").val(this.getAttribute('data-OutletID'));
    $("#title").text(this.getAttribute('data-OutletName')+'\'s');


    $('#details_table tbody').empty();

    var postData = {
    'outletID' : this.getAttribute('data-OutletID'),
    };

    //console.log(postData);
    $.ajax({
      type: "POST",
      url: '{{url("/SettingsManagement/OutletManagement/GetOutletGroup")}}',
      data: postData,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function (xhr) {
      var token = $('meta[name="csrf_token"]').attr('content');
      if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
        }
      },
      success: function(data)
      {
        var items='';
        //alert(JSON.stringify(data, null, 4)); //<- Check your value here!!!
        $.each(data, function(i) {
            items += "<tr id='item_"+data[i].KioskID+"'>";
            items += "<td><input type='hidden' name='KioskID[]' value='"+data[i].KioskID+"'>"+data[i].KioskID+"</td>";
            items += "<td><button type='button' class='btn btn-sm btn-default fa fa-times' onclick='(this).closest(`tr`).remove();'></button></td>";
            items += "</tr>";
         })

        $('#details_table tbody').empty().append(items).show();
        }
        
    });

});


}



function showAddModal()
{
    $('#OutletName').val('');
    $('#Location').val('');
    $('#outletdelete').hide();
    $('#AddOutletModal').modal('show');
}

function edit(obj)
{
    $('#newOrUpdate').text('Update').addClass('btn-warning');
    $('.modal-title').text('Edit '+obj.getAttribute('data-OutletName'));
    $('#outletdelete').show();

    $('#id').val(obj.getAttribute('data-id'));
    $('#OutletName').val(obj.getAttribute('data-OutletName')).attr('readonly', true).css('background-color' , '#DEDEDE');
    $('#Location').val(obj.getAttribute('data-Location'));
    $('#AddOutletModal').modal('show');
}

function deleteOutlet(){
    if(confirm('Are you sure you want to delete the selected Outlet?'))
    {
        $('#outlet-form').attr('action',"{{url('SettingsManagement/OutletManagement/DeleteOutlet')}}");
        $('#outlet-form').removeAttr('onsubmit');
        $('#outlet-form').submit();
    }
};

function tolist(obj)
{
    console.log(obj);
    if($('#item_'+obj.getAttribute('data-KioskID')).length == 0)
    {
        var items;
        items += "<tr id='item_"+obj.getAttribute('data-KioskID')+"'>";
        items += "<td><input type='hidden' name='KioskID[]' value='"+obj.getAttribute('data-KioskID')+"'>"+obj.getAttribute('data-KioskID')+"</td>";
        items += "<td><button id='fa-multiply' type='button' class='btn btn-sm btn-default fa fa-times' onclick='(this).closest(`tr`).remove();'></button></td>";
        items += "</tr>";
        $('#details_table').append(items);
    }
    else
        //alert('Hey! You put it there already! Stupid Lah!');
        $('#item_'+obj.getAttribute('data-KioskID')).fadeOut();
        $('#item_'+obj.getAttribute('data-KioskID')).fadeIn();
        $('#item_'+obj.getAttribute('data-KioskID')).fadeOut();
        $('#item_'+obj.getAttribute('data-KioskID')).fadeIn();
}

function SaveGroup()
{

    if(!$("#outletID").val())
    {
        alert('Please select Outlet you want to assign Kiosks');
        return false;
    }
    if(confirm('Are you sure you want to save the selected item/s?'))
    {
        $('#groupOutlet-form').attr('action',"{{url('SettingsManagement/OutletManagement/SaveOutletGroup')}}");
        $('#groupOutlet-form').removeAttr('onsubmit');
        $('#groupOutlet-form').submit();
    }
  
}

</script>

@include('includes.scripts')

@endsection
