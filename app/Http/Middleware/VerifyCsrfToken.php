<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/InventoryManagement/UpdateMenuItemStatus',
        '/TopupAccounts/GetTopupAccountsByDate',
        '/TopupAccounts/GetTopupAccountsByCardNumber',
        '/InventoryManagement/IncomingInventory/RetrieveMenuItemName',
        '/MembershipAccounts/DepositStatistics/GetDepositStatsByDate',
        '/MembershipAccounts/ExpenditureList/GetExpenditureByDate',
        '/InventoryManagement/IncomingInventory/GetInventoryByDate',
        '/InventoryManagement/OutgoingInventory/GetOutgoingInventoryByDate',
        '/SalesReports/DailySalesDailySalesReport/GetDailySalesByDate',
        '/SalesReports/ItemSalesStatistics/GetItemSalesByDate',
        '/SalesReports/SchedulingStatistics/GetSchedulingStatsByDate'


    ];
}
