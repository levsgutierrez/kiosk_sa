<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

use App\Models\InventoryStock;
use App\Models\OrderH;
use App\Models\Invoice;
use App\Models\Kitchen;

class SalesReportController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }

  //Daily Sales Reports Page
  public function DailySalesReport()
  {
    $data = array(
      'order_hs' => OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
                ->leftJoin('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
                ->where('order_hs.deleted','=','0')
                ->get(),

//     MY VERSION
//     $data = DB::table('order_hs')::on(Auth::user()->company)
//            ->select(DB::raw("
//             A.id, A.orderNo, A.orderDate, A.tableNo, A.tablePax, A.remarks, A.accountNumber, A.totalAmount, A.discount, A.amountReceived,
//             A.paymentType, A.otherPayments, A.amountChange, A.status, A.deleted, A.orderType, A.shopName, A.outletId, A.kioskNumber,
//             A.paymentNumber, A.invoiceAmount, mkjnb v,mnbn bm
//             (CASE WHEN (paymentType = '') THEN 'Cash' ELSE 'Nets' END) as PaymentTypes), B.operator AS InvoiceOperator, B.created_at AS InvoiceDate")
//             -> leftJoin('invoices', 'invoices.fileNo', '=', 'A.orderNo')
//             -> where('A.deleted', '=', '0')
//             ->get();
// );

  //ORIGINAL
  // SELECT        A.AutoId, A.OrderNo, A.ODate, A.PlaceNo, A.People, A.Remark, A.VIPNo, A.Amount, A.Discount, A.Mantissa, A.ActRecv, A.Payment, A.OtherPayType, ISNULL(A.OtherPayment, 0) AS OtherPayment, ISNULL(A.PayChange, 0)
  //                          AS PayChange, A.Status, A.OrderType, A.PDate, A.Addition1, ISNULL(A.Addition2, 0) AS Addition2, A.ShopName, A.OldOrderNo, A.MachineNo, A.CouponValue, A.PaymentNo, A.FileType, A.Oprt, A.InvoiceAmount, A.VIPDiscount,
  //                          CASE OtherPayType WHEN '' THEN N'现金' WHEN 'VIP' THEN N'会员卡' WHEN 'WX' THEN N'微信' WHEN 'ZFB' THEN N'支付宝' ELSE OtherPayType END AS PaymentType, B.Oprt AS InvoiceOprt,
  //                          B.CDate AS InvoiceCDate
  // FROM            dbo.T_OrderH AS A LEFT OUTER JOIN
  //                          dbo.T_Invoice AS B ON A.OrderNo = B.FileNo

      );
    return view('SalesReports/DailySalesReport', compact('data'));
  }

  public function GetDailySalesByDate(Request $request){
    $fromDate = $request->fromDate;
    $toDate   = $request->toDate;
    $data;

    if(!empty($fromDate) && !empty($fromDate))
    {
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
          ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
          ->where('order_hs.deleted','=','0')
          ->where("order_hs.created_at", ">=", $fromDate)
          ->where("order_hs.created_at", "<=", $toDate)
          ->get();
    }
    else if(!empty($fromDate) && empty($fromDate)){
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
          ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
          ->where('order_hs.deleted','=','0')
          ->where("order_hs.created_at", ">=", $fromDate)
          ->get();
    }
    else if(empty($fromDate) && !empty($fromDate)){
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
          ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
          ->where('order_hs.deleted','=','0')
          ->where("order_hs.created_at", "<=", $toDate)
          ->get();
    }
    else {
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
            ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
            ->get();
    }
    return $data;
  }
  //Item Sales Statistics Page
  public function ItemSalesStatistics()
  {    //     select MTypeNo,MTypeName,SUM(Qty) AS Qty,SUM(Amount) AS Amount,SUM(Discount) AS Discount,SUM(Ttl) AS Ttl,SUM(PackingAmt) AS PackingAmt from (
    // select B.MCode,C.MName,C.MGroupNo AS MTypeNo,ISNULL(C.MGroupName,'') MTypeName,B.Qty,B.Price*B.Qty AS Amount,B.Discount,B.Price*B.Qty - B.Discount AS Ttl,case when A.OrderType>0 then C.PackingPrice*B.Qty else 0 end AS PackingAmt
    // from T_OrderH A inner join T_OrderT B ON A.OrderNo = B.OrderNo inner join V_Menu C ON B.MCode = C.MCode Where A.Status<0 And cast(A.PDate AS Date) between '2016-01-21' And '2018-02-21') K
    // Group By MTypeNo,MTypeName

    // $menuSql = DB::raw("( menus.*, menu_categories.CategoryName AS CategoryGroupName, menu_categories.SeqNo AS CategorySeqNo
    //                       menus LEFT OUTER JOIN menu_categories ON menus.SubCategoryId = menu_categories.CategoryId
    //                       LEFT OUTER JOIN menu_categories AS D ON menus.CategoryId = menu_categories.CategoryId) as menuSql");
    //
    // $data = array(
    //   'order_hs' => OrderH::on(Auth::user()->company)->select('order_hs.*', 'menuSql.CategoryId', 'menuSql.CategoryGroupName')
    //             ->leftJoin('order_ts', 'order_ts.OrderNo', '=', 'order_hs.orderNo')
    //             ->leftJoin('menus', 'menus.SKU', '=', 'order_ts.SKU')
    //             ->leftJoin($menuSql, 'menuSql.menu_categories.')
    //             ->where('order_hs.deleted','=','0')
    //             ->where('order_hs.Status', '<', '0')
    //             ->groupby('menuSql.CategorySeqNo', 'menuSql.CategoryGroupName')
    //             ->get(),
    //           );
$data = array(
    'order_hs' => OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
              ->leftJoin('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
              ->where('order_hs.deleted','=','0')
              ->get(),
            );

    return view('SalesReports/ItemSalesStatistics');
  }

  public function GetItemSalesByDate(Request $request){
    $fromDate = $request->fromDate;
    $toDate   = $request->toDate;
    $checkedValues = $request->checkedValues;
    $data;

    if(!empty($fromDate) && !empty($fromDate))
    {
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
          ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
          ->where('order_hs.deleted','=','0')
          ->where("order_hs.created_at", ">=", $fromDate)
          ->where("order_hs.created_at", "<=", $toDate)
          ->get();
    }
    else if(!empty($fromDate) && empty($fromDate)){
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
          ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
          ->where('order_hs.deleted','=','0')
          ->where("order_hs.created_at", ">=", $fromDate)
          ->get();
    }
    else if(empty($fromDate) && !empty($fromDate)){
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
          ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
          ->where('order_hs.deleted','=','0')
          ->where("order_hs.created_at", "<=", $toDate)
          ->get();
    }
    else {
      $data = OrderH::on(Auth::user()->company)->select('order_hs.*', 'invoices.invoiceAmount as InvoiceAmount')
            ->join('invoices', 'invoices.fileNo', '=', 'order_hs.orderNo')
            ->get();
    }
    return $data;
  }
  //Daily Sales Statistics Page
  public function DailySalesStatistics()
  {
    // Select ISNULL(PaymentDesc,PayType) AS PayType,0-SUM(Payment) AS Payment From (
    //             select N'现金' AS PayType,Payment-ISNULL(PayChange,0) AS Payment from T_OrderH where Status<0 And Payment<>0 And cast(PDate AS Date) between '2016-02-16' And '2018-02-21'
    //             union All select OtherPayType,OtherPayment from T_OrderH where Status<0 And OtherPayment<>0  And cast(PDate AS Date) between '2016-02-16' And '2018-02-21') K left join V_PaymentType G ON K.PayType = G.PaymentType
    //             where Payment<0 Group By PayType,PaymentDesc

    return view('SalesReports/DailySalesStatistics');
  }
  //Invoice Statistics Page
  public function InvoiceStatistics()
  {
    $data = array(
      'invoices' => Invoice::on(Auth::user()->company)->select('invoices.*')
                ->where('invoices.deleted','=','0')
                ->get(),
              );
    // Select * from T_Invoice Where cast(CDate as date) between '2016-01-04' And '2018-02-21' And InvoiceType = N'充值' Order By AutoId
    return view('SalesReports/InvoiceStatistics', compact('data'));
  }

  public function GetInvoiceStatsByDate(Request $request){
    $fromDate = $request->fromDate;
    $toDate   = $request->toDate;
    $data;

    if(!empty($fromDate) && !empty($fromDate))
    {
      $data = Invoice::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", ">=", $fromDate)
          ->where("top_up_accounts.created_at", "<=", $toDate)
          ->get();
    }
    else if(!empty($fromDate) && empty($fromDate)){
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", ">=", $fromDate)
          ->get();
    }
    else if(empty($fromDate) && !empty($fromDate)){
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", "<=", $toDate)
          ->get();
    }
    else {
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->get();
    }
    return $data;
  }
  //Order Invoice Page
  public function OrderInvoiceStatistics()
  {
    return view('SalesReports/OrderInvoiceStatistics');
  }
  //Scheduling Statistics Page
  public function SchedulingStatistics()
  {
    $data = array(
      'kitchens' => Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName as ItemName')
                ->leftJoin('menus', 'menus.SKU', '=', 'kitchens.SKU')
                ->where('kitchens.deleted','=','0')
                ->get(),
              );
    return view('SalesReports/SchedulingStatistics', compact('data'));
  }

  public function GetSchedulingStatsByDate(Request $request){
    $fromDate = $request->fromDate;
    $toDate   = $request->toDate;
    $data;

    if(!empty($fromDate) && !empty($fromDate))
    {
      $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName as ItemName')
          ->leftJoin('menus', 'menus.SKU', '=', 'kitchens.SKU')
          ->where("kitchens.created_at", ">=", $fromDate)
          ->where("kitchens.created_at", "<=", $toDate)
          ->get();
    }
    else if(!empty($fromDate) && empty($fromDate)){
      $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName as ItemName')
          ->leftJoin('menus', 'menus.SKU', '=', 'kitchens.SKU')
          ->where("kitchens.created_at", ">=", $fromDate)
          ->get();
    }
    else if(empty($fromDate) && !empty($fromDate)){
      $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName as ItemName')
          ->leftJoin('menus', 'menus.SKU', '=', 'kitchens.SKU')
          ->where("kitchens.created_at", "<=", $toDate)
          ->get();
    }
    else {
      $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName as ItemName')
          ->leftJoin('menus', 'menus.SKU', '=', 'kitchens.SKU')
          ->get();
    }
    return $data;
  }
}
