<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;


use App\Http\Controllers\Controller;
use App\Models\User;

use Auth;

class MintController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest:admin', ['except' => ['logout']]);
	}

    public function index()
    {
        return view('dashboard');
    }

    public function showLoginForm()
    {
    	return view('auth.login_sa');
    }

    public function login(Request $request)
    {
        //return ($request->all());
        $this->validate($request,[
             'email' => 'bail|required',
             'password' => 'required|min:3',
        ]);


       	if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) 
        {
    	   return redirect()->intended(route('admin.dashboard'));
    	   //echo 'success';
        }
        else
    	return redirect()->back()->withInput($request->only('username'))->withErrors(['username'=>'Username or Password is incorrect']);
        //echo 'Error';
    }

    // public function register()
    // {
    //     $data   = new User();
    //     $data->email     = 'lev@mint.com.sg';
    //     $data->name      = 'Lev';
    //     $data->password  = Hash::make('password');
    //     $data->save();

    //    return view('auth.login_sa');
    // }

    public function logout()
    {
        Auth::guard('admin')->logout();
       // $request->session()->invalidate();
        return redirect(route('login'));
    }

}
