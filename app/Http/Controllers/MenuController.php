<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\sys_userRights;
use App\Models\Sys_FileNo;
use App\Models\Menu;
use App\Models\Image as ImageDB;
use App\Models\MenuSide;
use App\Models\MenuAddOns;
use App\Models\MenuCategory;
use App\Models\MenuCategorySet;
use App\Models\InventoryStock;

use Response;
use Input;
use Image;
use File;
use Carbon\Carbon;

use Auth;

class MenuController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function userRights($code,$action)
    {
        return sys_userRights::on(Auth::user()->company)->select('*')->where([
            ['userID', '=', Auth::user()->name]
            ,['moduleCode', '=', $code]
            ,['actionName', 'like', $action.'%']
            ,['active', '=', '1']
        ])->get()->pluck('active')->first();
    }

    public function getFileNo($code) //Generate FileNumber Depends on What Type of Code they give
    {

        if($code == 'MenuID')
        {
            //Get Current Number
            $current = Sys_FileNo::on(Auth::user()->company)->select('FileNo')->whereRaw('FileType = ?',(string)$code)->get()->pluck('FileNo')[0];

            //Inititate
            $FileNo = 000;
            $SKU = 'M'.$FileNo;

            //If File Number is Existing, increment
            do{
                $FileNo = str_pad(($FileNo + 1), 3, '0', STR_PAD_LEFT);
                $SKU = 'M'.$FileNo;
            }while($current >= $SKU);

            //Update Incremented Data
            $Sys_FileNo = Sys_FileNo::on(Auth::user()->company)->whereRaw('FileType = ?',(string)$code)->update(['FileNo'=>$SKU]);
            return $SKU;
        }
        elseif($code == 'SideID')
        {
            $current = Sys_FileNo::on(Auth::user()->company)->select('FileNo')->whereRaw('FileType = ?',(string)$code)->get()->pluck('FileNo')[0];
            //Inititate
            $FileNo = 000;
            $SideID = 'S'.$FileNo;

            //If File Number is Existing, increment
            do{
                $FileNo = str_pad(($FileNo + 1), 3, '0', STR_PAD_LEFT);
                $SideID = 'S'.$FileNo;
            }while($current >= $SideID);

            //Update Incremented Data
            $Sys_FileNo = Sys_FileNo::on(Auth::user()->company)->whereRaw('FileType = ?',(string)$code)->update(['FileNo'=>$SideID]);

            return $SideID;
        }
        elseif($code == 'CategoryID')
        {
            $current = Sys_FileNo::on(Auth::user()->company)->select('FileNo')->whereRaw('FileType = ?',(string)$code)->get()->pluck('FileNo')[0];
            //Inititate
            $FileNo = 000;
            $CategoryID = 'C'.$FileNo;

            //If File Number is Existing, increment
            do{
                $FileNo = str_pad(($FileNo + 1), 3, '0', STR_PAD_LEFT);
                $CategoryID = 'C'.$FileNo;
            }while($current >= $CategoryID);

            //Update Incremented Data
            $Sys_FileNo = Sys_FileNo::on(Auth::user()->company)->whereRaw('FileType = ?',(string)$code)->update(['FileNo'=>$CategoryID]);

            return $CategoryID;
        }
    }

    // Menu Items Page
    public function MenuItems()
    {
        if(empty($this->userRights('210','Access')))return view('noAccess');
        $data = array(
            'Menu'      => Menu::on(Auth::user()->company)->get()->whereIn('deleted',0),
            'Category'  =>  MenuCategory::on(Auth::user()->company)->select('CategoryID','CategoryName')->where('Flag','=','0')->get(),
            'AddOnsList' => MenuAddOns::on(Auth::user()->company)->select('AddOns','CategoryID','menu_categories.CategoryName as CategoryName')->groupby('AddOns','CategoryID','menu_categories.CategoryName')->join('menu_categories','menu_add_ons.CategoryID','=','menu_categories.id'),
            'add'       => $this->userRights('210','AddItem'),
            'edit'      => $this->userRights('210','EditItem'),
            'duplicate' => $this->userRights('210','DuplicateItem'),
            'delete'    => $this->userRights('210','DeleteItem'),
        );
        return view('MenuManagement/MenuItems',compact('data'));
        //dd($data);
    }


    public function AddMenuItem(Request $request)
    {
        //return $request->all();
        try
        {
            $SKU = $request->SKU;
            if(empty($SKU))
            {
                $SKU = $this->getFileNo('MenuID');
            }

            $ImgNo = $request->old_image;
            if ($request->hasFile('image')) {

                if($request->has('old_image'))
                {
                    $image_path = public_path().'/images/uploads/'.$request->old_image;
                    if(File::exists($image_path)) File::delete($image_path);
                        //unlink($image_path);
                }

                $image = $request->file('image');
                $fileName = time().'.'.$image->getClientOriginalExtension();

                $img = Image::make($image->getRealPath());
                $img->resize(205, 400, function ($constraint)
                {
                    $constraint->aspectRatio();
                });
                $img->stream();
                //Storage::disk('local')->put('public/'.$fileName, $img, 'public');
                $location = public_path('images\uploads\\' . $fileName);
                $img->save($location);

                $ImgNo = $fileName;
            }


            $data  = Menu::on(Auth::user()->company)->updateOrCreate([
                'SKU' => $request->SKU,
                ],[
                'SKU'            => $SKU,                    //<-- This one is generated on SKU generator
                'ItemName'       => $request->ItemName,
                'Desc'           => $request->Desc,
                'Unit'           => $request->Unit,
                'CollectPlace'   => $request->CollectPlace,
                'Price'          => $request->Price,
                'ImgNo'          => $ImgNo,                 //<-- This one is generated on saving images
                'CategoryID'     => $request->CategoryID,
                'SubCategoryID'  => $request->SubCategoryID,
                'PrinterName'    => $request->PrinterName,
                'KitchenDesc'    => $request->KitchenDesc,
                'SafetyStock'    => $request->SafetyStock,
                'Status'         => $request->Status,
                'OutletID'       => $request->OutletID,
                'CanTakeOut'     => $request->CanTakeOut,
                'PackPrice'      => $request->PackPrice,
                'Discount'     => $request->Discount,
                'Department'     => $request->Department,
                'BriefCode'      => $request->BriefCode,
                'CreatedBy'      => $request->CreatedBy,
                  ]);

                  DB::table('inventory_stocks')->insert(
                      [
                        'itemCode' => $SKU,
                        'itemName'  => $request->ItemName,
                        'isMenu' => 1,
                        'price' => $request->Price,
                        'quantity' => '0',
                        'limitQuantity' => '1000',
                        'remarks'       => null,
                        'status'        => 1,
                        'deleted'       => 0,
                      ]
                  );

              $data->save();
              return redirect()->back()->with('message', 'data saved!');
      }
      catch (\Illuminate\Database\QueryException $e){
          return redirect()->back()->withErrors($e->getMessage())->withInput($request->all());
      }
      catch (PDOException $e){
          return redirect()->back()->withErrors($e->getMessage())->withInput($request->all());
      }
    }

    public function DuplicateMenuItem(Request $request)
    {
        //return $request->all();
        try
        {
            $SKU = $request->SKU;
            if(empty($SKU))
            {
                $SKU = $this->getFileNo('MenuID');
            }

            $ImgNo = $request->old_image;
            if ($request->hasFile('image')) {

                if($request->has('old_image'))
                {
                    $image_path = public_path().'/images/uploads/'.$request->old_image;
                    unlink($image_path);
                }

                $image = $request->file('image');
                $fileName = time().'.'.$image->getClientOriginalExtension();

                $img = Image::make($image->getRealPath());
                $img->resize(205, 400, function ($constraint)
                {
                    $constraint->aspectRatio();
                });
                $img->stream();
                //Storage::disk('local')->put('public/'.$fileName, $img, 'public');
                $location = public_path('images\uploads\\' . $fileName);
                $img->save($location);

                $ImgNo = $fileName;
            }


            $data  = Menu::on(Auth::user()->company)->updateOrCreate([
                'SKU' => $request->SKU,
                ],[
                'SKU'            => $SKU,                    //<-- This one is generated on SKU generator
                'ItemName'       => $request->ItemName,
                'Desc'           => $request->Desc,
                'Unit'           => $request->Unit,
                'CollectPlace'   => $request->CollectPlace,
                'Price'          => $request->Price,
                'ImgNo'          => $ImgNo,                 //<-- This one is generated on saving images
                'CategoryID'     => $request->CategoryID,
                'SubCategoryID'  => $request->SubCategoryID,
                'PrinterName'    => $request->PrinterName,
                'KitchenDesc'    => $request->KitchenDesc,
                'SafetyStock'    => $request->SafetyStock,
                'Status'         => $request->Status,
                'OutletID'       => $request->OutletID,
                'CanTakeOut'     => $request->CanTakeOut,
                'PackPrice'      => $request->PackPrice,
                'Discount'       => $request->Discount,
                'Department'     => $request->Department,
                'BriefCode'      => $request->BriefCode,
                'CreatedBy'      => Auth::user()->name,
                  ]);

            if($data->save())
            {

                //Duplicate entry for new SKU
                $menuSide = MenuSide::on(Auth::user()->company)->where('SideID','=',$request->referenceSKU)->get();
                foreach($menuSide as $menu)
                {
                    $copymenu = MenuSide::on(Auth::user()->company)->find($menu->id);

                    $data2 = $copymenu->replicate();
                    $data2->SideID = $SKU;
                    $data2->CreatedBy = Auth::user()->name;
                    $data2->save();
                }

                $menuAddOns = MenuAddOns::on(Auth::user()->company)->where('SKU','=', $request->referenceSKU)->get();
                foreach($menuAddOns as $addOns)
                {
                    $copyaddons = MenuAddOns::on(Auth::user()->company)->find($addOns->id);

                    $data3      = $copyaddons->replicate();
                    $data3->SKU = $SKU;
                    $data3->CreatedBy = Auth::user()->name;
                    $data3->save();
                }
                return redirect()->back()->with('message', 'data saved!');
            }
            else echo 'Database Error Occur. Please contact Administrator';
        }
      catch (\Illuminate\Database\QueryException $e){
          return redirect()->back()->withErrors($e->getMessage())->withInput($request->all());
      }
      catch (PDOException $e){
          return redirect()->back()->withErrors($e->getMessage())->withInput($request->all());
      }
    }

    public function DeleteMenuItem($SKU)
    {   //return $SKU;
        try {
                $data = Menu::on(Auth::user()->company)->where('SKU','=',$SKU)
                    ->update(['deleted' => '1']);


                return redirect()->back()->with('deleted', $SKU.' successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function SideDish($id)
    {
        // return 'here';
        $data = array(
            'Menu'  => Menu::on(Auth::user()->company)->get()->whereIn('deleted',0),
            'SKU'   => $id,
            'ItemName'  => Menu::on(Auth::user()->company)->select('ItemName')->where('SKU','=',$id)->get()->pluck('ItemName')[0],
            'Sides' => MenuSide::on(Auth::user()->company)->select('SideID','SideName','SideMinQty','SideMaxQty','SerialCode')->groupby('SideID','SideName','SideMinQty','SideMaxQty','SerialCode')->where('SideID','=',$id)->where('Flag','=','0')->get(),
        );
        return view('MenuManagement/SideDish',compact('data'));
    }

    public function AddSideDish(Request $request)
    {
        //return($request->all());
        try
        {
                 MenuSide::on(Auth::user()->company)->where([
                ['SideID','=',$request->SideID],
                ['SideName','=',$request->SideName]
                ])->delete();


            for($x=0; $x<$request->rowCount; $x++)
            {

                $data  = MenuSide::on(Auth::user()->company)->updateOrCreate([
                'SideID'   => $request->SideID,
                'SideName' => $request->SideName,
                'SKU'       => $request->iSKU_[$x],
                ],[
                    'SideID'        => $request->SideID,
                    'SideName'      => $request->SideName,
                    'SideMinQty'    => $request->MinQty,
                    'SideMaxQty'    => $request->MaxQty,
                    'SerialCode'    => $request->SerialCode,
                    'SKU'           => $request->iSKU_[$x],
                    'ItemName'      => $request->itemName_[$x],
                    'SidePrice'     => $request->setPrice_[$x],
                    'MaxQty'        => $request->qty_[$x],
                    'SeqID'         => $x,
                ]);
                $data->save();
            }
            return redirect()->back()->with('message', 'data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
          return redirect()->back()->withErrors($e->getMessage())->withInput($request->all());
        }
        catch (PDOException $e){
          return redirect()->back()->withErrors($e->getMessage())->withInput($request->all());
        }
    }

    public function DetailsSideDish(Request $request)
    {
        //return $request->all();
        return MenuSide::on(Auth::user()->company)->select('*')->where([['SideName','=',$request->SideName],['SideID','=',$request->SideID]])->where('flag','=','0')->get();
    }

    public function DetailsSideDish_byName(Request $request)
    {
        //return $request->all();
        return MenuSide::on(Auth::user()->company)->select('*')->where('SideName','=',$request->SideName)->get();
    }

    public function DeleteSideDish(Request $request)
    {
        //return $request->all();
        try {
                $data = MenuSide::on(Auth::user()->company)->where([['SideName','=',$request->SideName],['SideID','=',$request->SideID]])
                    ->update(['flag' => '3']);
                //return redirect()->back()->with('deleted', $SKU.' successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function AddOns($id)
    {
          $data = array(
            'ItemName'  => Menu::on(Auth::user()->company)->select('ItemName')->where('SKU','=',$id)->get()->pluck('ItemName')[0],
            'SKU' => $id,
            'AddOnsListDetails' => MenuAddOns::on(Auth::user()->company)->where([['Flag','=',0],['SKU','=',$id]])->get(),
            'AddOnsList' => MenuAddOns::on(Auth::user()->company)->select('AddOns')->groupby('AddOns')
                ->where([['Flag','=',0],['SKU','=',$id]])->get(),
        );
        return view('MenuManagement/AddOns',compact('data'));
    }

    public function NewAddOns(Request $request)
    {
        //return($request->all());
        try
        {
            MenuAddOns::on(Auth::user()->company)->where('AddOns','=', $request->AddOns)->where('SKU','=', $request->SKU)->delete();

            for($x=0; $x<$request->rowno; $x++)
            {
                $query[] = array(
                'SKU'       => $request->SKU,
                'AddOns'    => $request->AddOns,
                'ItemName'  => $request->itemName[$x],
                'Price'     => $request->price[$x]
                );
            }

            if(MenuAddOns::on(Auth::user()->company)->insert($query))
            {
                return redirect()->back()->with('message', 'data saved!');
            }
            else echo 'error';
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function AddOnsItemList(Request $request)
    {
        //return $request->all();
        return MenuAddOns::on(Auth::user()->company)->select('id','ItemName','Price')->where('AddOns','=',$request->AddOns)->where('SKU','=',$request->SKU)->get();
    }

    public function DeleteAddOns(Request $request)
    {
        //return ($request->all());
        try {
                $data = MenuAddOns::on(Auth::user()->company)->where([['AddOns','=',$request->AddOns],['SKU','=',$request->SKU]])->update(['flag' => '3']);
                return redirect()->back()->with('deleted', ' successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    // Menu Categories Page
    public function MenuCategories()
    {
        if(empty($this->userRights('220','Access')))return view('noAccess');
        $data = array(
            'category' => MenuCategory::on(Auth::user()->company)->where('Flag','=','0')->orderBy('SeqNo','ASC')->get(),
            'subCategory' => MenuCategory::on(Auth::user()->company)->where('Flag','=','6')->get(),
            'add' => $this->userRights('220','AddCategories'),
            'edit' => $this->userRights('220','EditCategories'),
            'delete' => $this->userRights('220','DeleteCategories'),
            'addsub' => $this->userRights('220','AddSubCategories'),
            'editsub' => $this->userRights('220','EditSubCategories'),
            'deletesub' => $this->userRights('220','DeleteSubCategories'),
        );
        return view('MenuManagement/MenuCategories',compact('data'));
    }

    public function AddCategory(Request $request)
    {
        //echo 'bang!';
        //return ($request->all());
        try
        {

            $CategoryID = $request->CategoryID;
            if(empty($CategoryID))
            {
                $CategoryID = $this->getFileNo('CategoryID');
            }
            // echo $CategoryID;
            // die();
            $data  = MenuCategory::on(Auth::user()->company)->updateOrCreate([
                'CategoryID'   => $request->CategoryID,
                ],[
                'CategoryID' => $CategoryID,
                'CategoryName' => $request->categoryName,
                'SeqNo'        => $request->SeqNo
                ]);
                $data->save();

            return redirect()->back()->with('message', 'data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
     }

     public function DeleteCategory(Request $request)
     {
        //return ($request->all());
        try {
                $data = MenuCategory::on(Auth::user()->company)->where([['CategoryID','=',$request->CategoryID],['categoryName','=',$request->categoryName]])
                    ->update(['flag' => '3']);
                return redirect()->back()->with('deleted', ' successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function AddSubCategory(Request $request)
    {
        //return($request->all());
        try
        {
            $subCategoryID = $request->subCategoryID;
            if(empty($subCategoryID))
            {
                $subCategoryID = $this->getFileNo('CategoryID');
            }
            $data  = MenuCategory::updateOrCreate([
                'CategoryID'   => $request->subCategoryID,
                ],[
                'CategoryID'   => $subCategoryID,
                'CategoryName' => $request->subcategoryName,
                'SeqNo'        => '0',
                'Flag'        => '6',
                ]);

            $data->save();
            return redirect()->back()->with('message', 'data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
     }

    public function DeleteSubCategory(Request $request)
    {
        //return($request->all());
        try {
                $data = MenuCategory::where([['CategoryID','=',$request->subCategoryID],['categoryName','=',$request->subcategoryName]])
                    ->update(['flag' => '3']);
                return redirect()->back()->with('deleted', ' successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function SaveGroup(Request $request)
    {
        //return($request->all());
        try
        {
            MenuCategorySet::on(Auth::user()->company)->where('ParentCode','=',$request->groupID)->delete();

            foreach($request->sub_item as $sub_item)
            {
            $query[] = array(
              'ParentCode'  => $request->groupID,
              'ChildCode'   => $sub_item,
              'SeqNo'       => array_search($sub_item, $request->sub_item),
              'created_at'  => Carbon::now(),
              'updated_at'  => Carbon::now(),
              'CreatedBy'   =>Auth::user()->name,
              );
            }
            MenuCategorySet::on(Auth::user()->company)->insert($query);
            return redirect()->back()->with('message', 'data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function GetGroup(Request $request)
    {
        //return $request->all();
        return MenuCategorySet::on(Auth::user()->company)->
        select('menu_categories.CategoryID','menu_categories.CategoryName','menu_category_sets.SeqNo')
        ->join('menu_categories','menu_categories.CategoryID','=','menu_category_sets.ChildCode')
        ->where('menu_category_sets.ParentCode','=',$request->CategoryID)
        ->orderBy('menu_category_sets.SeqNo','ASC')
        ->get();
    }

    // Menu Item Display Page
    public function MenuItemDisplay()
    {
        if(empty($this->userRights('230','Access')))return view('noAccess');
        $data = array(
            'Menu'          => Menu::on(Auth::user()->company)->get()->whereIn('deleted',0),
            'category'      => MenuCategory::on(Auth::user()->company)->where('Flag','=','0')->orderBy('menu_categories.SeqNo','ASC')->get(),
            'subCategory'   => MenuCategorySet::on(Auth::user()->company)->leftjoin('menu_categories','menu_category_sets.ChildCode','=','menu_categories.CategoryID')->where('menu_categories.Flag','=','6')->orderBy('menu_category_sets.SeqNo','ASC')->get(),
            'add' => $this->userRights('230','AddItems'),
            'delete' => $this->userRights('230','DeleteItems'),

        );
        //dd($data['subCategory']);
        return view('MenuManagement/MenuItemDisplay',compact('data'));
    }

    public function SaveDisplay(Request $request)
    {
        //return($request->all());
        try
        {
            MenuCategorySet::on(Auth::user()->company)->where('ParentCode','=',$request->groupID)->delete();

            //If they save it without data inputted, it will go from here
            if(empty($request->SKU)){
                return redirect()->back()->with('message', 'data saved!');
            }

            foreach($request->SKU as $SKU)
            {
                $query[] = array(
                  'ParentCode'  => $request->groupID,
                  'ChildCode'   => $SKU,
                  'SeqNo'       => array_search($SKU, $request->SKU),
                  'created_at'  => Carbon::now(),
                  'updated_at'  => Carbon::now(),
                  'CreatedBy'   =>Auth::user()->name,
                  );
            }
            MenuCategorySet::on(Auth::user()->company)->insert($query);
            return redirect()->back()->with('message', 'data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function GetDisplayGroup(Request $request)
    {
        //return $request->all();
        return MenuCategorySet::on(Auth::user()->company)->
        select('menus.SKU','menus.ItemName')
        ->join('menus','menus.SKU','=','menu_category_sets.ChildCode')
        ->where('menu_category_sets.ParentCode','=',$request->CategoryID)
        ->orderBy('menu_category_sets.SeqNo','ASC')
        ->get();
    }

    // Menu Item Details Page
    public function MenuItemDetails()
    {
      return view('MenuManagement/MenuItemDetails');
    }

    // Menu Item Details Page
    public function ImportedDishes()
    {
      return view('MenuManagement/ImportedDishes');
    }
}
