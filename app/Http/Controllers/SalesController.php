<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use \Datetime;
use Illuminate\Http\Request;
use Auth;

use App\Models\InventoryStock;
use App\Models\OrderH;
use App\Models\Invoice;
use App\Models\Kitchen;
use App\Models\Discount;
use App\Models\Menu;
use App\Models\MenuCategory;
use App\Models\PaymentLog;
use App\Models\Sys_FileNo;

class SalesController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function getFileNo($code) //Generate FileNumber Depends on What Type of Code they give
  {
      if($code == 'paymentLogId')
      {
          //Get Current Number
          $current = Sys_FileNo::on(Auth::user()->company)->select('FileNo')->whereRaw('FileType = ?',(string)$code)->get()->pluck('FileNo')[0];

          //Inititate
          $FileNo = 0000;
          $paymentLogId = 'P'.$FileNo;

          //If File Number is Existing, increment
          do{
              $FileNo = str_pad(($FileNo + 1), 3, '0', STR_PAD_LEFT);
              $paymentLogId = 'P'.$FileNo;
          }while($current >= $paymentLogId);

          //Update Incremented Data
          $Sys_FileNo = Sys_FileNo::on(Auth::user()->company)->whereRaw('FileType = ?',(string)$code)->update(['FileNo'=>$paymentLogId]);
          return $paymentLogId;
      }
  }
  // Receipt History Page
  public function ReceiptHistory()
  {
    $data = array(
      'order_hs' => OrderH::on(Auth::user()->company)->select('order_hs.*')
                  ->where('order_hs.deleted','=','0')
                  ->get(),
      );
    return view('SalesManagement/ReceiptHistory', compact('data'));
  }

  // Discount Settings Page
  public function DiscountSettings()
  {

    $data = array(
      'discounts'=> Discount::on(Auth::user()->company)->select('discounts.*', 'menus.ItemName AS MenuItemName', 'menu_categories.CategoryName AS ItemCategoryName')
                  ->leftJoin('menus', 'menus.SKU', '=', 'discounts.SKU')
                  ->leftJoin('menu_categories', 'menu_categories.CategoryID', '=', 'discounts.categoryId')
                  ->where('discounts.deleted', '=', '0')
                  ->orderBy('id', 'asc')
                  ->get(),

      'menus' => Menu::on(Auth::user()->company)->select('menus.*')
              ->where('menus.deleted', '=', '0')
              ->get(),

      'menu_categories' => MenuCategory::on(Auth::user()->company)->select('menu_categories.*')
                        ->get(),
    );
    return view('SalesManagement/DiscountSettings', compact('data'));
  }

  public function RetrieveMenuItemName(Request $request)
  {
    $data =  Menu::on(Auth::user()->company)->select('menus.itemName', 'menus.SKU' )
          ->where('menus.itemName','=',$request->itemName)
          ->get();

    return $data;
  }

  public function AddDiscount(Request $request)
  {
    try {
      $data  = Discount::on(Auth::user()->company)->updateOrCreate([
                  'id' => $request->id,
                  'discountName' => $request->discountName,
              ],[
                  'id'                        => $request->id,
                  'discountName'              => $request->discountName,
                  'discountType'              => $request->discountType,
                  'minimumAmount'             => $request->minimumAmount,
                  'SKU'                       => $request->SKU,
                  'categoryId'                => $request->categoryId,
                  'discountAmount'            => $request->discountAmount,
                  'discountPercentage'        => $request->discountPercentage,
                  'beginDate'                 => new DateTime($request->beginDate),
                  'endDate'                   => new DateTime($request->endDate),
                  'beginTime'                 => $request->beginTime,
                  'endTime'                   => $request->endTime,
                  'week'                      => $request->week,
                  'priority'                  => $request->priority,
                  'status'                    => $request->status,
              ]);
            $data->save();

            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  // Replenish Cash Page
  public function ReplenishCash()
  {
    $data = array(
      'payment_logs' => PaymentLog::on(Auth::user()->company)->select('payment_logs.*')
                  ->where('payment_logs.paymentLogType','=','Replenish Cash')
                  ->where('payment_logs.deleted','=','0')
                  ->get(),
      );
    return view('SalesManagement/ReplenishCash', compact('data'));
  }

  public function AddReplenishCash(Request $request)
  {
      $today = Carbon::now();
      $paymentLogId = $this->getFileNo('paymentLogId');
    try {
      $data  = PaymentLog::on(Auth::user()->company)->updateOrCreate([
                  'id' => $request->id,
                  'paymentLogId' => $paymentLogId,
              ],[
                  'id'                        => $request->id,
                  'paymentLogId'              => $paymentLogId,
                  'paymentLogType'            => "Replenish Cash",
                  'koiskNumber'               => $request->koiskNumber,
                  'denominations'             => $request->denominations,
                  'quantity'                  => $request->quantity,
                  'totalAmount'               => $request->totalAmount,
                  'created_at'                => $today,
                  'operator'                  => Auth::user()->name,
              ]);
            $data->save();

            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  // Cash Out Page
  public function CashOut()
  {
    $data = array(
      'payment_logs' => PaymentLog::on(Auth::user()->company)->select('payment_logs.*')
                  ->where('payment_logs.paymentLogType','=','Cash Out')
                  ->where('payment_logs.deleted','=','0')
                  ->get(),
      );
    return view('SalesManagement/CashOut', compact('data'));
  }

  public function AddCashOut(Request $request)
  {
      $today = Carbon::now();
      $paymentLogId = $this->getFileNo('paymentLogId');
    try {
      $data  = PaymentLog::on(Auth::user()->company)->updateOrCreate([
                  'id' => $request->id,
                  'paymentLogId' => $paymentLogId,
              ],[
                  'id'                        => $request->id,
                  'paymentLogId'              => $paymentLogId,
                  'paymentLogType'            => "Cash Out",
                  'koiskNumber'               => $request->koiskNumber,
                  'denominations'             => $request->denominations,
                  'totalAmount'               => $request->totalAmount,
                  'created_at'                => $today,
                  'operator'                  => Auth::user()->name,
              ]);
            $data->save();

            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  // Menu Item Details Page
  public function KioskHistory()
  {
    $data = array(
      'payment_logs' => PaymentLog::on(Auth::user()->company)->select('payment_logs.*')
                  ->where('payment_logs.paymentLogType','=','Bill Settlements')
                  ->get(),
      );
    return view('SalesManagement/KioskHistory', compact('data'));
  }
  // Menu Item Details Page
  public function OrderHistory()
  {
    $data = array(
      'kitchens' => Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
                  ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
                  ->where('kitchens.deleted','=','0')
                  ->get(),
      );

    return view('SalesManagement/OrderHistory', compact('data'));
  }

  public function GetOrderHistoryByDate(Request $request){
      $fromDate = $request->fromDate;
      $toDate   = $request->toDate;
      $checkedValues = $request->checkedValues;
      $data;

      // if all not empty
      if(!empty($fromDate) && !empty($toDate) && !empty($checkedValues))
      {
        $data = Kitchen ::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
            ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
            ->where('kitchens.created_at', '>=', $fromDate)
            ->where('kitchens.created_at', '<=', $toDate)
            ->whereIn('kitchens.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only from date
      else if(!empty($fromDate) && empty($toDate)){
        $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
            ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
            ->where("kitchens.created_at", ">=", $fromDate)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only to date
      else if(empty($fromDate) && !empty($toDate)){
        $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
            ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
            ->where("kitchens.created_at", "<=", $toDate)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only checkValues
      else if(empty($fromDate) && empty($toDate) && !empty($checkedValues)){
        $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
            ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
            ->whereIn('kitchens.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
          }
      // only fromdate and checked values
      else if(!empty($fromDate) && empty($toDate) && !empty($checkedValues)){
        $data = membersTradeLog::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
            ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
            ->where("kitchens.created_at", ">=", $fromDate)
            ->whereIn('kitchens.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only todate and checked values
      else if(empty($fromDate) && !empty($toDate)){
        $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
            ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
            ->where("kitchens.created_at", "<=", $toDate)
            ->whereIn('kitchens.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      else {
        $data = Kitchen::on(Auth::user()->company)->select('kitchens.*', 'menus.itemName AS ItemName', 'menus.SKU AS SKU')
            ->join('menus', 'menus.SKU', '=', 'kitchens.SKU')
            ->orderByRaw('created_at ASC')
            ->get();
      }
      return $data;
    }


  // Utilities Functions

}
