<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use App\Models\MembersDetails;
use App\Models\MembershipTypes;
use App\Models\TopUpAccounts;
use App\Models\Deposit;
use App\Models\generalSettings;
use App\Models\Sys_FileNo;
use App\Models\membersTradeLog;
use App\Models\Invoice;
use App\Models\sys_userRights;

use Carbon\Carbon;
use Auth;

use Illuminate\Http\Request;

class MembershipController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function userRights($code,$action)
  {
      return sys_userRights::on(Auth::user()->company)->select('*')->where([
          ['userID', '=', Auth::user()->name]
          ,['moduleCode', '=', $code]
          ,['actionName', 'like', $action.'%']
          ,['active', '=', '1']
      ])->get()->pluck('active')->first();
  }

  public function getFileNo($code) //Generate FileNumber Depends on What Type of Code they give
  {
      if($code == 'topupId')
      {
          //Get Current Number
          $current = Sys_FileNo::on(Auth::user()->company)->select('FileNo')->whereRaw('FileType = ?',(string)$code)->get()->pluck('FileNo')[0];

          //Inititate
          $FileNo = 0000;
          $topupId = 'T'.$FileNo;

          //If File Number is Existing, increment
          do{
              $FileNo = str_pad(($FileNo + 1), 3, '0', STR_PAD_LEFT);
              $topupId = 'T'.$FileNo;
          }while($current >= $topupId);

          //Update Incremented Data
          $Sys_FileNo = Sys_FileNo::on(Auth::user()->company)->whereRaw('FileType = ?',(string)$code)->update(['FileNo'=>$topupId]);
          return $topupId;
      }
      elseif($code == 'depositId')
      {
          $current = Sys_FileNo::on(Auth::user()->company)->select('FileNo')->whereRaw('FileType = ?',(string)$code)->get()->pluck('FileNo')[0];
          //Inititate
          $FileNo = 0000;
          $depositId = 'D'.$FileNo;

          //If File Number is Existing, increment
          do{
              $FileNo = str_pad(($FileNo + 1), 3, '0', STR_PAD_LEFT);
              $depositId = 'D'.$FileNo;
          }while($current >= $depositId);

          //Update Incremented Data
          $Sys_FileNo = Sys_FileNo::on(Auth::user()->company)->whereRaw('FileType = ?',(string)$code)->update(['FileNo'=>$depositId]);
          return $depositId;
      }
  }

  // Members Details Page
  public function MembersDetails()
  {
    if(empty($this->userRights('410','Access')))return view('noAccess');
    $topupAccountSql = DB::raw("( SELECT accountCardNumber, MAX(initialBalance) as initialBalance
                                  FROM top_up_accounts
								                  GROUP BY accountCardNumber
                                  ) as topupAccounts");

    $data = array(

        'members_details' => MembershipTypes::on(Auth::user()->company)->select('members_details.*', 'membership_types.id as membershipTypesId', 'membership_types.membershipTypes as membershipTypesName', 'topupAccounts.initialBalance as initialBalance' )
                                  ->join('members_details', 'members_details.membershipTypes', '=', 'membership_types.id')
                                  ->leftJoin($topupAccountSql, 'topupAccounts.accountCardNumber', '=', 'members_details.accountCardNumber')
                                  ->where('members_details.deleted','=','0')
                                  ->get(),

        'membership_types' => MembershipTypes::on(Auth::user()->company)->select('*')
                          ->where('membership_types.deleted','=','0')
                          -> get(),

        'membership_defaultDepositAmount' => generalSettings::on(Auth::user()->company)->select("*")
                                             ->where('general_settings.settingsName', '=', 'defaultDepositAmount')
                                             -> get(),
        'add'       => $this->userRights('410','AddMembers'),
        'edit'      => $this->userRights('410','EditMembers'),
        'delete'    => $this->userRights('410','DeleteMembers'),
        'export'    => $this->userRights('410','ExportMembers'),

      );
    return view('MembershipAccounts/MembersDetails', compact('data'));
  }

  public function AddMembers(Request $request)
  {
    // return ($request->all());
    try {
            $today = Carbon::now();
            $depositId = $this->getFileNo('depositId');
            $status = 0;
            if($request->status != "" || $request->status != null)
              $status = 1;

            $data  = MembersDetails::on(Auth::user()->company)->updateOrCreate([
                    'accountNumber'    => $request->accountNumber,
                    'accountCardNumber' => $request->accountCardNumber,
                    'nric'              => $request->nric,
                ],[
                    'id'                      => $request->id,
                    'accountNumber'           => $request->accountNumber,
                    'accountCardNumber'       => $request->accountCardNumber,
                    'membershipTypes'         => $request->membershipTypes,
                    'membersName'             => $request->membersName,
                    'accountBalance'          => $request->accountBalance,
                    'status'                  => $status,
                    'nric'                    => $request->nric,
                    'contactNumber'           => $request->contactNumber,
                    'deposit'                 => $request->deposit,
                    'registeredDate'          => $today,
                    'membersPoints'           => 0,
                    'createdBy'               => Auth::user()->name,
                ]);

          $data->save();

          if(empty($request->id))
          {
              $deposit =  DB::table('deposits')->insert(
                          [
                            'depositType'   => 'deposit',
                            'fileNumber'    => $depositId,
                            'accountNumber' => $request->accountNumber,
                            'contactNumber' => $request->contactNumber,
                            'amount'        => $request->deposit,
                            'remarks'       => null,
                            'createdBy'     => Auth::user()->name,
                            'deleted'       => 0,
                            'created_at'    => $today,
                            'updated_at'    => null,
                          ]
                      );

                DB::table('members_trade_logs')->insert(
               [
                 'accountCardNumber'    => $request->accountCardNumber,
                 'actionType'           => 'Deposit',
                 'actionTypeId'         => $depositId,
                 'amount'               => $request->deposit,
                 'createdBy'            => Auth::user()->name,
                 'created_at'           => $today,
               ]
       );
          }
            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function DeleteMembers(Request $request)
  {
    // return $request->all();
      try {
            $data  = MembersDetails::find($request->id);
            $data->deleted = 1;
            $data->setConnection(Auth::user()->company);
            $data->save();
            return redirect()->back()->with('deleted', $request->membersName.' has been successfully deleted');
      }
      catch (\Illuminate\Database\QueryException $e){
          return redirect()->back()->withErrors($e->getMessage());
      }
      catch (PDOException $e){
          return redirect()->back()->withErrors($e->getMessage());
      }
  }

  public function AddDefaultDepositAmount(Request $request)
  {
    // return ($request->all());
    try {

        $today = Carbon::now();
        generalSettings::on(Auth::user()->company)->where('settingsName', '=', 'defaultDepositAmount')
                         ->update(['valueDouble' => $request->defaultDepositAmount,
                                   'updated_at'  => $today]);

        return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function GetTotalBalance()
  {
      $topupAccountSql = DB::raw("( SELECT accountCardNumber, MAX(initialBalance) as initialBalance
                                    FROM top_up_accounts
                                    GROUP BY accountCardNumber
                                    ) as topupAccounts");

      $data =  MembershipTypes::on(Auth::user()->company)->select('members_details.*', 'membership_types.id as membershipTypesId', 'membership_types.membershipTypes as membershipTypesName', 'topupAccounts.initialBalance as initialBalance' )
                              ->join('members_details', 'members_details.membershipTypes', '=', 'membership_types.id')
                              ->leftJoin($topupAccountSql, 'topupAccounts.accountCardNumber', '=', 'members_details.accountCardNumber')
                              ->where('members_details.deleted','=','0')
                              ->get();
      $result = null;
      foreach ($data as $p ) {
        $result = $result + $p->initialBalance;
      }
    return $result;
  }


  // Membership Types Page
  public function MembershipTypes()
  {
    if(empty($this->userRights('420','Access')))return view('noAccess');
    $data = array(
        'membership_types' => MembershipTypes::on(Auth::user()->company)->select('*')
                           ->where('membership_types.deleted','=','0')
                           ->get(),
        'add'       => $this->userRights('420','AddMembership'),
        'edit'      => $this->userRights('420','EditMembership'),
        'delete'    => $this->userRights('420','DeleteMembership'),
      );
    return view('MembershipAccounts/MembershipTypes', compact('data'));
  }

  public function AddMembershipType(Request $request)
  {
    try {
          $status = 0;
          if($request->status != "" || $request->status != null)
            $status = 1;

          $accountBalance = 0;
          if($request->accountBalance != "" || $request->accountBalance != null || $request->accountBalance != ".00")
            $accountBalance = 0.00;

          $data  = MembershipTypes::on(Auth::user()->company)->updateOrCreate([
                 'id' => $request->id,
              ],[
                  'id'                   => $request->id,
                  'membershipTypes'      => $request->membershipTypes,
                  'discountPercentage'   => $request->discountPercentage,
                  'status'               => $status,
              ]);
            $data->save();
            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function DeleteMembershipType(Request $request)
  {
    // return $request->all();
      try {
              $data  = MembershipTypes::find($request->id);
              $data->deleted = 1;
              $data = setConnection(Auth::user()->company);
              $data->save();
              return redirect()->back()->with('deleted', $request->membershipTypes.' has been successfully deleted');
      }
      catch (\Illuminate\Database\QueryException $e){
          return redirect()->back()->withErrors($e->getMessage());
      }
      catch (PDOException $e){
          return redirect()->back()->withErrors($e->getMessage());
      }
  }
  // Topup Accounts Page
  public function TopupAccounts()
  {
    $topup_accounts = TopUpAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName', 'invoices.invoiceAmount as invoiceAmountModal', 'invoices.operator as invoiceOperator', 'invoices.created_at as invoiceCreatedAt')
                      ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
                      ->leftJoin('invoices', 'invoices.fileNo', '=' ,'top_up_accounts.topupId')
                      ->where('top_up_accounts.deleted','=','0')
                      ->get();

    $unique = $topup_accounts->unique('id');

    $data = array(
      'topup_accounts' => TopUpAccounts::on(Auth::user()->company)->select('top_up_accounts.*')
                   ->where('top_up_accounts.deleted','=','0')
                   ->get(),

       'members_details' => $unique
      );

      return view('MembershipAccounts/TopupAccounts', compact('data'));
    }

    public function RetrieveTopupAccountsDetails(Request $request)
    {
      $data =  MembersDetails::on(Auth::user()->company)->select('*')
            // ->join('top_up_accounts', 'top_up_accounts.initialBalance', '=', 'members_details.accountBalance')
            ->where('members_details.accountCardNumber','=',$request->accountCardNumber)
            ->get();

      return $data;
    }

  public function TopUpMembersAccount(Request $request)
  {
    try {
            $today = Carbon::now();
            $topupId = $this->getFileNo('topupId');

            $members = MembersDetails::on(Auth::user()->company)
                  ->where('accountCardNumber', "=", $request->accountCardNumber)
                  ->first();

            $data  = TopUpAccounts::on(Auth::user()->company)->updateOrCreate([
                   'id' => $request->id,
                   'topupId' => $topupId
                ],[
                    'id'                    => $request->id,
                    'topupId'               => $topupId,
                    'accountCardNumber'     => $request->accountCardNumber,
                    // 'membersName'           => $request->membersName,
                    'initialBalance'        => $members->accountBalance,
                    'topupAmount'           => $request->topupAmount,
                    'giftAmount'            => $request->giftAmount,
                    'totalAmount'           => $request->totalAmount,
                    'invoiceAmount'         => $request->invoiceAmount,
                    'paymentType'           => $request->paymentType,
                    'topupDate'             => $today,
                    'createdBy'             => Auth::user()->name,
                ]);
            $data->save();

            DB::table('members_details')
            ->where('accountCardNumber', "=", $request->accountCardNumber)
            ->update([
                      'accountBalance' => $request->totalAmount,
                      'updated_at' => $today
                      ]);

            DB::table('members_trade_logs')->insert(
                [
                  'accountCardNumber'    => $request->accountCardNumber,
                  'actionType'       => 'Topup',
                  'actionTypeId'     => $topupId,
                  'amount'        => $request->topupAmount,
                  'createdBy'     => Auth::user()->name,
                  'created_at'    => $today,
                ]
            );

            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function GetTopupAccountsByDate(Request $request){
    $fromDate = $request->fromDate;
    $toDate   = $request->toDate;
    $data;

    if(!empty($fromDate) && !empty($fromDate))
    {
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", ">=", $fromDate)
          ->where("top_up_accounts.created_at", "<=", $toDate)
          ->get();
    }
    else if(!empty($fromDate) && empty($fromDate)){
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", ">=", $fromDate)
          ->get();
    }
    else if(empty($fromDate) && !empty($fromDate)){
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", "<=", $toDate)
          ->get();
    }
    else {
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->get();
    }
    return $data;
  }

  function GetTopupAccountsByCardNumber(Request $request) {
      $accountCardNumber = $request->accountCardNumber;

      $data = MembersDetails::select('members_details.*')
          ->where("members_details.accountCardNumber", "=", $accountCardNumber)
          ->first();

      return $data;
  }
  // Click on Invoice button and update the database with invoice date, invoice amount and invoice operator //
  // Update [dbo].[T_VIPRecharge] set InvoiceAmount = 10.00 Where RechargeNo = 'R000003'
  //          if exists (select FileNo from T_Invoice where FileNo='R000003')
  //              Update T_Invoice Set InvoiceAmount = 10.00, Oprt='admin', CDate=GETDATE() Where VIPNo = '10.00'
  //          else
  //          INSERT INTO T_Invoice(InvoiceType, FileNo, VIPNo, InvoiceAmount, Oprt, CDate)
  //          VALUES(N'充值', 'R000003', '123', 10.00, 'admin', GETDATE()) End

  public function GetTotalTopupAmount()
  {
      $topupAmountSql = DB::raw("(SELECT accountCardNumber, SUM(topupAmount) as topupAmount
                                    FROM top_up_accounts
                                    GROUP BY accountCardNumber
                                    ) as totalTopupAmount");

      $data =  MembersDetails::on(Auth::user()->company)->select('members_details.*','totalTopupAmount.topupAmount as topupAmount' )
                              // ->join('members_details', 'members_details.membershipTypes', '=', 'membership_types.id')
                              ->leftJoin($topupAmountSql, 'totalTopupAmount.accountCardNumber', '=', 'members_details.accountCardNumber')
                              ->where('members_details.deleted','=','0')
                              ->get();
      $result = null;
      foreach ($data as $p ) {
        $result = $result + $p->topupAmount;
      }
    return $result;
  }

  public function GetTotalGiftAmount()
  {
      $giftAmountSql = DB::raw("( SELECT accountCardNumber, SUM(giftAmount) as giftAmount
                                    FROM top_up_accounts
                                    GROUP BY accountCardNumber
                                    ) as totalGiftAmount");

      $data =  MembersDetails::on(Auth::user()->company)->select('members_details.*','totalGiftAmount.giftAmount as giftAmount' )
                              ->leftJoin($giftAmountSql, 'totalGiftAmount.accountCardNumber', '=', 'members_details.accountCardNumber')
                              ->where('members_details.deleted','=','0')
                              ->get();
      $result = null;
      foreach ($data as $p ) {
        $result = $result + $p->giftAmount;
      }
    return $result;
  }

  public function AddInvoiceAmount(Request $request)
  {
      $today = Carbon::now();
      DB::table('invoices')->insert(
                 [
                   'invoiceType'   => 'TopUp',
                   'fileNo'    => $request->topUpIdModal,
                   'accountNumber' => $request->accountCardNumberModal,
                   'invoiceAmount' => $request->invoiceAmountModal,
                   'operator'        => $request->operatorModal,
                   'deleted'       => 0,
                   'outletId'       => null,
                   'created_at'    => $today,
                   'updated_at'    => null,
                 ]
             );

             $topup_accounts = TopUpAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName', 'invoices.invoiceAmount as invoiceAmountModal', 'invoices.operator as invoiceOperator', 'invoices.created_at as invoiceCreatedAt')
                               ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
                               ->leftJoin('invoices', 'invoices.fileNo', '=' ,'top_up_accounts.topupId')
                               ->where('top_up_accounts.deleted','=','0')
                               ->get();

             $unique = $topup_accounts->unique('id');

             $data = array(
               'topup_accounts' => TopUpAccounts::on(Auth::user()->company)->select('top_up_accounts.*')
                            ->where('top_up_accounts.deleted','=','0')
                            ->get(),

                'members_details' => $unique
               );

       return redirect()->back()->with('message', 'data saved!');
  }

  // Topup Inquiry Page
  // public function TopupInquiry()
  // {
  //   $data = array(
  //     'members_details' => TopUpAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
  //                       ->leftJoin('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
  //                       ->where('top_up_accounts.deleted','=','0')
  //                       ->get(),
  //   );
  //
  //   return view('MembershipAccounts/TopupInquiry', compact('data'));
  // }
  //
  // public function GetTopupTransactionsByDate(Request $request){
  //   $fromDate = $request->fromDate;
  //   $toDate   = $request->toDate;
  //   $data;
  //
  //   if(!empty($fromDate) && !empty($toDate))
  //   {
  //     $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
  //         ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
  //         ->where("top_up_accounts.created_at", ">=", $fromDate)
  //         ->where("top_up_accounts.created_at", "<=", $toDate)
  //         ->get();
  //   }
  //   else if(!empty($fromDate) && empty($toDate)){
  //     $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
  //         ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
  //         ->where("top_up_accounts.created_at", ">=", $fromDate)
  //         ->get();
  //   }
  //   else if(empty($fromDate) && !empty($toDate)){
  //     $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
  //         ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
  //         ->where("top_up_accounts.created_at", "<=", $toDate)
  //         ->get();
  //   }
  //   else {
  //     $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
  //         ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
  //         ->get();
  //   }
  //   return $data;
  // }
  // Deposit Statistics Page
  public function DepositStatistics()
  {
    if(empty($this->userRights('440','Access')))return view('noAccess');
    $data = array(

         'members_details' => Deposit::on(Auth::user()->company)->select('members_details.membersName as MembersName', 'members_details.accountCardNumber as AccountCardNumber', 'deposits.*')
                           ->join('members_details', 'members_details.accountNumber', '=', 'deposits.accountNumber')
                           ->where('deposits.deleted','=','0')
                           ->get(),
          'export'       => $this->userRights('440','ExportDepositStatistics'),
      );
    return view('MembershipAccounts/DepositStatistics', compact('data'));
  }
  public function GetDepositStatsByDate(Request $request){
    $fromDate = $request->fromDate;
    $toDate   = $request->toDate;
    $data;

    if(!empty($fromDate) && !empty($fromDate))
    {
      $data = Deposit::on(Auth::user()->company)->select('members_details.*','deposits.*')
          ->join('members_details', 'members_details.accountNumber', '=', 'deposits.accountNumber')
          ->where("deposits.created_at", ">=", $fromDate)
          ->where("deposits.created_at", "<=", $toDate)
          ->get();
    }
    else if(!empty($fromDate) && empty($fromDate)){
      $data = Deposit::on(Auth::user()->company)->select('members_details.*','deposits.*')
          ->join('members_details', 'members_details.accountNumber', '=', 'deposits.accountNumber')
          ->where("deposits.created_at", ">=", $fromDate)
          ->get();
    }
    else if(empty($fromDate) && !empty($fromDate)){
      $data = Deposit::on(Auth::user()->company)->select('members_details.*','deposits.*')
          ->join('members_details', 'members_details.accountNumber', '=', 'deposits.accountNumber')
          ->where("deposits.created_at", "<=", $toDate)
          ->get();
    }
    else {
      $data = Deposit::on(Auth::user()->company)->select('members_details.*','deposits.*')
          ->join('members_details', 'members_details.accountNumber', '=', 'deposits.accountNumber')
          ->get();
    }
    return $data;
  }

  public function GetTotalDepositAmount()
  {
    $data =  DB::table('deposits')
           ->select(DB::raw('sum(amount) as depositAmount'))
           ->groupBy('accountNumber')
           ->get();

      $result = null;
      foreach ($data as $p ) {
        $result = $result + $p->depositAmount;
      }
    return $result;
  }

  // Expenditure List Page
  public function ExpenditureList()
  {
    if(empty($this->userRights('450','Access')))return view('noAccess');
    $data = array(
       'members_trade_logs' => membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
                            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
                             ->orderByRaw('created_at ASC')
                            ->get(),
        'export'       => $this->userRights('450','ExportExpenditure'),
    );
    return view('MembershipAccounts/ExpenditureList', compact('data'));
  }

  public function GetExpenditureByDate(Request $request){
      $fromDate = $request->fromDate;
      $toDate   = $request->toDate;
      $checkedValues = $request->checkedValues;
      $data;

      // if all not empty
      if(!empty($fromDate) && !empty($toDate) && !empty($checkedValues))
      {
        $data = membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
            ->where('members_trade_logs.created_at', '>=', $fromDate)
            ->where('members_trade_logs.created_at', '<=', $toDate)
            ->whereIn('members_trade_logs.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only from date
      else if(!empty($fromDate) && empty($toDate)){
        $data = membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
            ->where("members_trade_logs.created_at", ">=", $fromDate)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only to date
      else if(empty($fromDate) && !empty($toDate)){
        $data = membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
            ->where("members_trade_logs.created_at", "<=", $toDate)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only checkValues
      else if(empty($fromDate) && empty($toDate) && !empty($checkedValues)){
        $data = membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
            ->whereIn('members_trade_logs.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
          }
      // only fromdate and checked values
      else if(!empty($fromDate) && empty($toDate) && !empty($checkedValues)){
        $data = membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
            ->where("members_trade_logs.created_at", ">=", $fromDate)
            ->whereIn('members_trade_logs.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      // only todate and checked values
      else if(empty($fromDate) && !empty($toDate)){
        $data = membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
            ->where("members_trade_logs.created_at", "<=", $toDate)
            ->whereIn('members_trade_logs.actionType', $checkedValues)
            ->orderByRaw('created_at ASC')
            ->get();
      }
      else {
        $data = membersTradeLog::on(Auth::user()->company)->select('members_trade_logs.*', 'members_details.membersName As MembersName', 'members_details.accountNumber As AccountNumber', 'members_details.accountBalance As AccountBalance')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'members_trade_logs.accountCardNumber')
            ->orderByRaw('created_at ASC')
            ->get();
      }
      return $data;
    }
  // Members Statistics Page
  public function MembersStatistics(Request $request)
  {

    // $value = $request->actionType;
    // $MembersCount = DB::table('members_details')->count('membersName');
    // $AccountsOpened = DB::table('members_details')->count('accountNumber');
    // $tradeLogsSql = DB::raw("(SELECT members_trade_logs.actionType', 'members_trade_logs.created_at AS tradeDate', 'members_trade_logs.amount','(CASE WHEN members_trade_logs.actionType ='.$value.' THEN members_trade_logs.amount ELSE 0 END)
    //                           FROM members_trade_logs GROUP BY members_trade_logs.created_by')as tradeLog");

    // $data = array(
    //     'invoices' => Invoice::on(Auth::user()->company)->selectRaw('invoices.invoiceAmount', 'invoices.invoiceType', 'invoices.created_by','SUM(invoices.invoice.amount) AS invoiceAmount')
    //                                                     ->where('invoices.invoiceType', '=', 'TopUp')
    //                                                     ->join($tradeLogsSql, 'tradeLog.tradeDate', '=', 'invoices.created_at')
    //                                                     ->leftJoin($topupAccountSql, 'topupAccounts.accountCardNumber', '=', 'members_details.accountCardNumber')
    //                                                     ->groupby('invoices.created_at')

    // $value = $request->actionType
    $AccountOpened = collect(DB::select( DB::raw("SELECT CAST(created_at AS DATE) as createDate, COUNT(membersName) as accountOpenCount, COUNT(membersName) as membersCount FROM members_details GROUP BY CAST(created_at AS DATE)")));
    $TopupAmount = collect(DB::select(DB::raw("SELECT CAST(created_at AS DATE) as createDate, SUM(amount) as topupAmount FROM members_trade_logs WHERE actionType = 'Topup' GROUP BY CAST(created_at AS DATE)")));
    $DepositAmount = collect(DB::select(DB::raw("SELECT CAST(created_at AS DATE) as createDate, SUM(amount) as depositAmount FROM members_trade_logs WHERE actionType = 'Deposit' GROUP BY CAST(created_at AS DATE)")));
    $InvoiceAmount = collect(DB::select(DB::raw("SELECT CAST(created_at AS DATE) as createDate, SUM(invoiceAmount) as invoiceAmount FROM invoices GROUP BY CAST(created_at AS DATE)")));
    // $tradeLogsSql = DB::raw("(SELECT members_trade_logs.actionType', 'members_trade_logs.created_at AS tradeDate', 'members_trade_logs.amount','(CASE WHEN members_trade_logs.actionType ='.$value.' THEN members_trade_logs.amount ELSE 0 END)
    //                           FROM members_trade_logs GROUP BY members_trade_logs.created_by')as tradeLog");




    $statistic = $AccountOpened->merge([$TopupAmount])->merge([$DepositAmount])->merge([$InvoiceAmount])->groupBy('createDate')->all();
    //->merge($TopupAmount)->->merge($DepositAmount)->merge($InvoiceAmount)->groupBy('createDate');


    $data = array(
        'invoices' => $statistic

        //
        // 'invoices' => Invoice::on(Auth::user()->company)->select('invoices.invoiceAmount', 'invoices.invoiceType', 'invoices.created_by','SUM(invoices.invoice.amount) AS invoiceAmount')
        //                                                 ->where('invoices.invoiceType', '=', 'TopUp')
        //                                                 ->groupby('invoices.created_at')

    // );
    //return view('MembershipAccounts/MembersStatistics', compact('data'));

    );
    return $statistic;
    return view('MembershipAccounts/MembersStatistics', compact('data'));
  }
  public function GetMemberStatsByDate(Request $request){
      $fromDate = $request->fromDate;
      $toDate   = $request->toDate;
      $data;

      if(!empty($fromDate) && !empty($toDate))
      {
        $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
            ->where("top_up_accounts.created_at", ">=", $fromDate)
            ->where("top_up_accounts.created_at", "<=", $toDate)
            ->get();
      }
      else if(!empty($fromDate) && empty($toDate)){
        $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
            ->where("top_up_accounts.created_at", ">=", $fromDate)
            ->get();
      }
      else if(empty($fromDate) && !empty($toDate)){
        $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
            ->where("top_up_accounts.created_at", "<=", $toDate)
            ->get();
      }
      else {
        $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.*')
            ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
            ->get();
      }
      return $data;
    }
}
