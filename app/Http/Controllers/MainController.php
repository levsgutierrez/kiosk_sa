<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
	public function __construct()
    {
     $this->middleware('auth:admin');
    }
	
    public function dashboard()
    {
      return view('dashboard');
    }

    public function test()
    {

    	// 1. Check if this is palindrome or not
  		// $a = "madAM";
		// $b =  strrev(strtolower($a));

	 	// $string_reverse = str_split($b);
	    
	    // print_r($string_reverse);
		// $palin = '';
		// foreach($string_reverse as $value)
		// {
		//     $palin.= $value; 
		// }

		// if(strtolower($a) == $palin){
		// 		print "<br>Palindrome";
		// } else {
		// 		print "<br>Not Palindrome"; 
		// }
	
    	// 2. Reverse the Word
	   	// $word = 'Good Morning';
    	// echo strrev($word);

    	// 3. Remove specific Data on the string
    	// $data = 'Good Morning';
   		// $word = str_replace('o','*', $data);
    	// echo $word;

    	// 4. Get the first array occurence
		// $array = array(1, "hello", 1, "world", "hello");
		// print_r(array_search(1,array_count_values($array)));

    	// 5. get the occurence of the string
  		// $string = 'The quick brown fox jumps over the lazy dog';
  		// $array = str_split($string);
		// sort($array, SORT_NATURAL | SORT_FLAG_CASE);
		// print_r(array_count_values($array));

		// $data = "Two Ts and one F.";
		// foreach (count_chars($data, 1) as $i => $count) {
		//    echo "There were ".$count." instance(s) of \"" , chr($i) , "\" in the string.\n<br/>";
		// }

    	//Get numbers that are not present on both arrays
		// $a = [1,2,3,4,5];
		// $b = [2,3,1,0,5];

		// $c = array_merge($a,$b);

		// //print_r(array_count_values($c));
		// foreach(array_count_values($c) as $i => $val){
		//  	if($val == 1) echo $i.'<br/>';
		// }
		
		$setNumber = array(20, 34, 21, 87, 92, 123456789);
		// rsort($setNumber);
		// print_r($setNumber[1]);
		
		$largest = $setNumber[0];
		$smallest = $setNumber[0];
		//print_r($setNumber);
		foreach($setNumber as $num)
		{
			//echo $num.'<br/>';
			if($num > $largest)
			{
				$largest = $num;
			}
			else if($num < $smallest)
			{
				$smallest = $num;
			}
		}
		echo 'smallest: '.$smallest.'<br/>';
		echo 'largest: '.$largest.'<br/>';

    }
}
