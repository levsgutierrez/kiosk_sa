<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Kiosk;
use App\Models\Outlets;
use App\Models\sys_userRights;
use App\Models\sys_modules;
use App\Models\sys_modules_action;
use App\Models\kiosks_group;
use App\Models\sys_settings;

use App\mintCompany;

use Auth;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;

class SettingsController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function userRights($code,$action)
    {
        return sys_userRights::on(Auth::user()->company)->select('*')->where([
            ['userID', '=', Auth::user()->name]
            ,['moduleCode', '=', $code]
            ,['actionName', 'like', $action.'%']
            ,['active', '=', '1']
        ])->get()->pluck('active')->first();
    }

    // System Modules Page
    public function SystemModules()
    {
        return view('SettingsManagement/SystemModules');
    }
    
    //User Management Page
    public function UserManagement()
    {
        if(empty($this->userRights('110','Access')))return view('noAccess');
        $user = ((Auth::user()->name == 'Administrator') ? mintCompany::get()->where('deleted','=',0) : mintCompany::get()->where('deleted','=',0)->where('company','=',Auth::user()->company));
        $data = array(
            'users' => $user,
            'company' => mintCompany::select('company')->groupby('company')->get(),
            'add' => $this->userRights('110','CreateUser'),
            'edit' => $this->userRights('110','EditUser'),
            'delete' => $this->userRights('110','DeleteUser'),
        );


        return view('SettingsManagement/UserManagement',compact('data'));
    }

    public function AddUser(Request $request)
    {
        //return ($request->all());
        try {
                Auth::user()->name == 'Administrator' ? $company = $request->company : $company = Auth::user()->company;
               

                $data  = mintCompany::updateOrCreate([
                       'id' => $request->id,
                       'email' => $request->email
                    ],[
                        'password' => Hash::make($request->password),
                        'name' => $request->fname,
                        'email' => $request->email,
                        'company' => $company
                    ]);
                $data->save();
                return redirect()->back()->with('message', 'Awesome, data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function DeleteUser(Request $request)
    {
        try {
                $data  = User::find($request->id);
                $data->deleted = 1;
                $data->save();
                return redirect()->back()->with('deleted', $request->userID.'\'s account successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function SetRights(Request $request)
    {
        try {
            $data  = sys_userRights::on(Auth::user()->company)->updateOrCreate([
                   'userID' => $request->userID,
                   'actionName' => $request->actionName
                ],[
                    'userID' => $request->userID,
                    'moduleCode' => $request->moduleCode,
                    'actionName' => $request->actionName,
                    'active' => $request->active,
                ]);
            $data->save();
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function GetRights(Request $request)
    {
        //return $request->all();
        return sys_userRights::on(Auth::user()->company)->select('*')->where([['userID','=',$request->userID],['active','=','1']])->get();
    }

    public function OutletManagement(Request $request)
    {
        if(empty($this->userRights('120','Access')))return view('noAccess');
        $data = array(
            'outlets' => Outlets::on(Auth::user()->company)->get()->where('deleted','=',0),
            'kiosks'  => Kiosk::on(Auth::user()->company)->select('KioskID')->groupby('KioskID')->where('deleted','=',0)->get(),
            'add' => $this->userRights('120','AddOutletKiosk'),
            'edit' => $this->userRights('120','EditOutletKiosk'),
            'delete' => $this->userRights('120','DeleteOutletKiosk'),
        );
        return view('SettingsManagement/OutletManagement',compact('data'));
    }

    public function AddOutlet(Request $request)
    {
        try {
                $data  = Outlets::on(Auth::user()->company)->updateOrCreate([
                       'id' => $request->id,
                    ],[
                        'Location'  => $request->Location,
                        'OutletName' => $request->OutletName,
                        'CreatedBy' => Auth::user()->name,
                    ]);
                $data->save();
                return redirect()->back()->with('message', 'Awesome, data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function DeleteOutlet(Request $request)
    {
        try {
                $data  = Outlets::on(Auth::user()->company)->find($request->id);
                $data->deleted = 1;
                $data->save();
                return redirect()->back()->with('deleted', $request->OutletName.' successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function SaveOutletGroup(Request $request)
    {
        //return $request->all();

        try
        {
            kiosks_group::on(Auth::user()->company)->where('OutletID','=',$request->outletID)->delete();

        //     //If they save it without data inputted, it will go from here
            if(empty($request->outletID)){
                return redirect()->back()->with('message', 'data saved!');
            }

            foreach($request->KioskID as $KioskID)
            {
                $query[] = array(
                  'OutletID'  => $request->outletID,
                  'KioskID'   => $KioskID,
                  'created_at'  => Carbon::now(),
                  'updated_at'  => Carbon::now(),
                  'CreatedBy'   =>Auth::user()->name,
                  );
            }
            kiosks_group::on(Auth::user()->company)->insert($query);
            return redirect()->back()->with('message', 'data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function GetOutletGroup(Request $request)
    {
        //return $request->all();
        return kiosks_group::on(Auth::user()->company)
        ->select('KioskID')
        ->where('OutletID','=',$request->outletID)
        ->get();
    }


    // Parameter Settings Page
    public function DeviceSettings()
    {
        if(empty($this->userRights('130','Access')))return view('noAccess');
        $data = array(
            'moneyAcceptor'     => sys_settings::on(Auth::user()->company)->where('Settings','=','MoneyAcceptorSettings')->pluck('Value')->toArray(),
            'receiptHeader'     => sys_settings::on(Auth::user()->company)->where('Title','=','receiptHeader')->get()->pluck('Value')->first(),
            'receiptFooter'     => sys_settings::on(Auth::user()->company)->where('Title','=','receiptFooter')->get()->pluck('Value')->first(),
            'memberBalance'     => sys_settings::on(Auth::user()->company)->select('Value')->where('Title','=','showMemberBalance')->first(),
            'queueNumber'       => sys_settings::on(Auth::user()->company)->select('Value')->where('Title','=','showQueueNumber')->first(),
            'printReceipt'      => sys_settings::on(Auth::user()->company)->where('Settings','=','ReceiptType')->pluck('Title')->toArray(),
        );
        return view('SettingsManagement/DeviceSettings',compact('data'));
    }

    public function SaveMoneyAcceptor(Request $request)
    {
        try
        {
            sys_settings::on(Auth::user()->company)->where('Settings','=','MoneyAcceptorSettings')->delete();
            $Title = array('machineNumber','paperPort','paperModel','coinPort','coinModel');
            $Value = array($request->machineNumber, $request->paperPort, $request->paperModel, $request->coinPort, $request->coinModel);
            
            for($x=0; $x<=4; $x++)
            {
                $data  = sys_settings::on(Auth::user()->company)->updateOrCreate([
                           'Title' => $Title[$x],
                           'Value' => $Value[$x],
                        ],[
                            'Settings'  => 'MoneyAcceptorSettings',
                            'Title' => $Title[$x],
                            'Value' => $Value[$x],
                        ]);
                $data->save();
            }
            return redirect()->back()->with('message', 'data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function SaveReceipt(Request $request)
    {
        //return $request->all();
        try 
        {
            sys_settings::on(Auth::user()->company)->where('Settings','=','ReceiptSettings')->delete();
            $Title = array('receiptHeader','receiptFooter','showMemberBalance','showQueueNumber');
            $x = 0;
            foreach($request->value as $v)
            {
                $data  = sys_settings::on(Auth::user()->company)->updateOrCreate([
                       'Title' => $Title[$x],
                       'Value' => $v,

                    ],[
                        'Settings'  => 'ReceiptSettings',
                        'Title' => $Title[$x],
                        'Value' => $v,
                    ]);
                $data->save();
                $x++;
            }
            return redirect()->back()->with('message', 'Awesome, data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function saveReceiptType(Request $request)
    {
        //return $request->all();
        try 
        {
            sys_settings::on(Auth::user()->company)->where('Settings','=','ReceiptType')->delete();
            
           
            foreach($request->printReceipt as $p)
            {
                //echo array_search('Customer settlement small note', $request->printReceipt);
                $data  = sys_settings::on(Auth::user()->company)->updateOrCreate([
                       'Title' => 'print'.array_search($p, $request->printReceipt),
                       'Value' => $p,

                    ],[
                        'Settings'  => 'ReceiptType',
                        'Title' => 'print'.array_search($p, $request->printReceipt),
                        'Value' => $p,
                    ]);
                $data->save();
            }
            return redirect()->back()->with('message', 'Awesome, data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    // Menu Scheduling Page
    public function KioskManagement()
    {
        if(empty($this->userRights('140','Access')))return view('noAccess');
        $data = array(
            'kiosk' => Kiosk::on(Auth::user()->company)->select('KioskID')->groupby('KioskID')->where('deleted','=',0)->get(),
            'add' => $this->userRights('140','AddKiosk'),
            'edit' => $this->userRights('140','EditKiosk'),
            'delete' => $this->userRights('140','DeleteKiosk'),
        );
        return view('SettingsManagement/KioskManagement',compact('data'));
    }

    public function AddKiosk(Request $request)
    {
        try {
                $data  = Kiosk::on(Auth::user()->company)->updateOrCreate([
                       'KioskID' => $request->kioskID,
                    ],[
                        'KioskID' => $request->kioskID,
                    ]);
                $data->save();
                return redirect()->back()->with('message', 'Awesome, data saved!');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }


    public function DeleteKiosk($kioskID)
    {
        try {
                $data  = Kiosk::on(Auth::user()->company)->where('KioskID', $kioskID)
                ->update(['deleted' => 1]);
                return redirect()->back()->with('deleted', $kioskID.' successfuly deleted');
        }
        catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
        catch (PDOException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function GetKioskCategories(Request $request)
    {
            return DB::connection(Auth::user()->company)->select(DB::raw("SELECT 
            k.ParentCode, 
            k.ParentName, 
            k.ChildCode, 
            k.ChildName, 
            t.kioskID, 
            ISNULL(t.Breakfast, 0) AS Breakfast, 
            ISNULL(t.Lunch, 0) AS Lunch, 
            ISNULL(t.Dinner, 0) AS Dinner
            FROM
            (SELECT 
                a.ParentCode, 
                b.categoryName as ParentName, 
                a.ChildCode, 
                c.categoryName as ChildName 
            FROM menu_category_sets a
            INNER JOIN menu_categories b on a.ParentCode = b.CategoryID
            INNER JOIN menu_categories c on a.ChildCode = c.CategoryID) as k
            LEFT OUTER JOIN (SELECT * FROM kiosks WHERE kioskID = :kioskID ) AS t ON k.ParentCode = t.CategoryID AND k.ChildCode = t.CategorySetID
            "),
            array(
                'kioskID' => $request->kioskID,
            )
        );
    }

    public function UpdateKioskStatus(Request $request)
    {
        //return $request->all();
            $data  = Kiosk::on(Auth::user()->company)->updateOrCreate([
                       'KioskID'        => $request->kioskID,
                       'CategoryID'     => $request->parentCode,
                       'CategorySetID'  => $request->childCode,
                    ],[

                        'KioskID'       => $request->kioskID,
                        'CategoryID'    => $request->parentCode,
                        'CategorySetID' => $request->childCode,
                        $request->selected  => ($request->status == 'true') ? '1' : '0',
                    ]);
            $data->save();

    }

   
    // System Updates Page
    public function SystemUpdates()
    {
    return view('SettingsManagement/SystemUpdates');
    }
    // POS Page
    public function POS()
    {
    return view('SettingsManagement/POS');
    }
    // Activity Log Page
    public function ActivityLog()
    {
        if(empty($this->userRights('150','Access')))return view('noAccess');
        return view('SettingsManagement/ActivityLog');
    }
}
