<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use App\Models\Menu;
use App\Models\InventoryIO;
use App\Models\Sys_FileNo;
use App\Models\InventoryStock;
use App\Models\sys_userRights;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class InventoryController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }

  public function userRights($code,$action)
  {
      return sys_userRights::on(Auth::user()->company)->select('*')->where([
          ['userID', '=', Auth::user()->name]
          ,['moduleCode', '=', $code]
          ,['actionName', 'like', $action.'%']
          ,['active', '=', '1']
      ])->get()->pluck('active')->first();
  }

  public function getFileNo($code) //Generate FileNumber Depends on What Type of Code they give
  {
      if($code == 'inventoryId')
      {
          //Get Current Number
          $current = Sys_FileNo::on(Auth::user()->company)->select('FileNo')->whereRaw('FileType = ?',(string)$code)->get()->pluck('FileNo')[0];

          //Inititate
          $FileNo = 0000;
          $inventoryId = 'IO'.$FileNo;

          //If File Number is Existing, increment
          do{
              $FileNo = str_pad(($FileNo + 1), 3, '0', STR_PAD_LEFT);
              $inventoryId = 'IO'.$FileNo;
          }while($current >= $inventoryId);

          //Update Incremented Data
          $Sys_FileNo = Sys_FileNo::on(Auth::user()->company)->whereRaw('FileType = ?',(string)$code)->update(['FileNo'=>$inventoryId]);
          return $inventoryId;
      }
  }

  //Inventory Page
  public function Inventory()
  {
    if(empty($this->userRights('510','Access')))return view('noAccess');
    $data = array(

        'menus' => Menu::on(Auth::user()->company)->select('menus.*')
                ->where('menus.deleted','=','0')
                ->get(),
        'edit' => $this->userRights('510','ChangeInventoryStatus'),
              );

    return view('InventoryManagement/Inventory', compact('data'));
  }

  public function UpdateMenuItemStatus(Request $request)
  {
    // return ($request->all());
    try {
          $today = Carbon::now();
          Menu::on(Auth::user()->company)->where('itemName', '=', $request->itemName)
              ->update(['status' => $request->status,
                'updated_at'  => $today]);

        return "Menu items success updated";
    }
    catch (\Illuminate\Database\QueryException $e){
        return "Menu items failed updated";

    }
    catch (PDOException $e){
        return "Menu items failed updated";
    }
  }

  // Incoming Inventory Page
  public function IncomingInventory()
  {
    if(empty($this->userRights('520','Access')))return view('noAccess');
    $data = array(
        'menus' => Menu::on(Auth::user()->company)->select('menus.SKU', 'menus.ItemName')
                // ->join('inventory_i_os', 'inventory_i_os.itemCode', '=', 'menus.SKU')
                ->where('menus.deleted','=','0')
                ->get(),

        'inventory_i_os' => InventoryIO::on(Auth::user()->company)->select('inventory_i_os.*')
                         ->where('inventory_i_os.inventoryType', '=', '1')
                         ->where('inventory_i_os.deleted','=','0')
                         ->get(),
        'add'     => $this->userRights('520','AddIncomingInventory'),
        'delete'  => $this->userRights('520','DeleteIncomingInventory'),
        'reprint' => $this->userRights('520','InventoryReprintReceipt'),
        'export'  => $this->userRights('520','ExportInventory'),
        );

    return view('InventoryManagement/IncomingInventory', compact('data'));
  }

  public function AddIncomingInventory(Request $request)
  {
    try {
          $inventoryId = $this->getFileNo('inventoryId');
          $today = Carbon::now();
          // $inventoryType = 'Incoming Inventory';

          $data  = InventoryIO::on(Auth::user()->company)->updateOrCreate([
                 'id' => $request->id,
              ],[
                  'id'                   => $request->id,
                  'inventoryId'          => $inventoryId,
                  'inventoryType'        => 1,
                  'actionName'           => 'Incoming Inventory',
                  'itemCode'             => $request->itemCode,
                  'itemName'             => $request->itemName,
                  'totalQuantity'        => $request->totalQuantity,
                  'operator'             => Auth::user()->name,
                  'remarks'              => $request->remarks,
              ]);
            $data->save();

            $item_quantity = InventoryStock::on(Auth::user()->company)
                  ->where('itemCode', "=", $request->itemCode)
                  ->first();

            $total_quantity = $item_quantity->quantity + $request->totalQuantity;

            DB::table('inventory_stocks')
            ->where('itemCode', "=", $request->itemCode)
            ->update([
                      'quantity' => $total_quantity,
                      'updated_at' => $today
                      ]);

            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function DeleteItem($inventoryId)
  {
    try {
      $data = InventoryIO::on(Auth::user()->company)->where('inventoryId','=', $inventoryId)
                ->update(['deleted' => '1']);

            return redirect()->back()->with('deleted', $inventoryId.' successfuly deleted');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
}

  public function RetrieveMenuItemName(Request $request)
  {
    $data =  Menu::on(Auth::user()->company)->select('menus.itemName', 'menus.SKU' )
          // ->join('inventory_i_os', 'inventory_i_os.itemName', '=', 'menus.itemName')
          ->where('menus.SKU','=',$request->itemCode)
          ->get();

    return $data;
  }

  public function GetInventoryByDate(Request $request){
    $fromDate = $request->fromDate;
    $data;

    if(!empty($fromDate)){
      $data = InventoryIO::on(Auth::user()->company)->select('inventory_i_os.*')
          ->whereDate('inventory_i_os.created_at', '=', $fromDate)
          ->where('inventory_i_os.inventoryType', '=', '1')
          ->where('inventory_i_os.deleted', '=', '0')
          ->get();
    }
    else {
      $data = InventoryIO::on(Auth::user()->company)->select('inventory_i_os.*')
          ->where('inventory_i_os.inventoryType', '=', '1')
          ->where('inventory_i_os.deleted', '=', '0')
          ->get();
    }
    return $data;
  }

  // Outgoing Inventory Page
  public function OutgoingInventory()
  {
    if(empty($this->userRights('530','Access')))return view('noAccess');
    $data = array(
        'menus' => Menu::on(Auth::user()->company)->select('menus.SKU', 'menus.ItemName')
                // ->join('inventory_i_os', 'inventory_i_os.itemCode', '=', 'menus.SKU')
                ->where('menus.deleted','=','0')
                ->get(),

        'inventory_i_os' => InventoryIO::on(Auth::user()->company)->select('inventory_i_os.*')
                        ->where('inventory_i_os.inventoryType', '=', '0')
                        ->where('inventory_i_os.deleted','=','0')
                        ->get(),
        'add'     => $this->userRights('530','AddOutgoingInventory'),
        'clear'   => $this->userRights('530','ClearInventory'),
        'reprint' => $this->userRights('530','ReprintInventory'),
        'export'  => $this->userRights('530','ExportOutgoingInventory'),
              );
    return view('InventoryManagement/OutgoingInventory',compact('data'));
  }

  public function AddOutgoingInventory(Request $request)
  {
    try {
          $inventoryId = $this->getFileNo('inventoryId');
          $today = Carbon::now();
          // $inventoryType = 'Incoming Inventory';

          $data  = InventoryIO::on(Auth::user()->company)->updateOrCreate([
                 'id' => $request->id,
              ],[
                  'id'                   => $request->id,
                  'inventoryId'          => $inventoryId,
                  'inventoryType'        => 0,
                  'actionName'           => 'Outgoing Inventory',
                  'itemCode'             => $request->itemCode,
                  'itemName'             => $request->itemName,
                  'totalQuantity'        => $request->totalQuantity,
                  'operator'             => Auth::user()->name,
                  'remarks'              => $request->remarks,
              ]);
            $data->save();

            $item_quantity = InventoryStock::on(Auth::user()->company)
                  ->where('itemCode', "=", $request->itemCode)
                  ->first();

            $total_quantity = $item_quantity->quantity -  $request->totalQuantity;

            DB::table('inventory_stocks')
            ->where('itemCode', "=", $request->itemCode)
            ->update([
                      'quantity' => $total_quantity,
                      'updated_at' => $today
                      ]);

            return redirect()->back()->with('message', 'data saved!');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function GetOutgoingInventoryByDate(Request $request){
    $fromDate = $request->fromDate;
    $data;

    if(!empty($fromDate)){
      $data = InventoryIO::on(Auth::user()->company)->select('inventory_i_os.*')
          ->whereDate('inventory_i_os.created_at', '=', $fromDate)
          ->where('inventory_i_os.inventoryType', '=', '1')
          ->where('inventory_i_os.deleted', '=', '0')
          ->get();
    }
    else {
      $data = InventoryIO::on(Auth::user()->company)->select('inventory_i_os.*')
          ->where('inventory_i_os.inventoryType', '=', '1')
          ->where('inventory_i_os.deleted', '=', '0')
          ->get();
    }
    return $data;
  }

  public function DeleteOutgoingItem($inventoryId)
  {
    try {
      $data = InventoryIO::on(Auth::user()->company)->where('inventoryId','=', $inventoryId)
                ->update(['deleted' => '1']);

            return redirect()->back()->with('deleted', $inventoryId.' successfuly deleted');
    }
    catch (\Illuminate\Database\QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
    catch (PDOException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }
}

  // Inventory Inquiry Page
  public function InventoryInquiry()
  {
    if(empty($this->userRights('540','Access')))return view('noAccess');
    $data = array(
      'inventory_stocks' => InventoryStock::on(Auth::user()->company)->select('inventory_stocks.*')
                      ->where('inventory_stocks.deleted','=','0')
                      ->get(),
      'export'     => $this->userRights('540','ExportInventoryInquiry'),
      );
    return view('InventoryManagement/InventoryInquiry',compact('data'));
  }
  // Inventory Ledger Page
  public function InventoryLedger()
  {
    if(empty($this->userRights('550','Access')))return view('noAccess');
    $data = array(
      'inventory_i_os' => InventoryIO::on(Auth::user()->company)->select('inventory_i_os.*')
                        ->where('inventory_i_os.deleted','=','0')
                        ->get(),
      'export'     => $this->userRights('550','ExportInventoryLedger'),
              );

    return view('InventoryManagement/InventoryLedger', compact('data'));
  }

  public function GetLedgerByDate(Request $request){
    $fromDate = $request->fromDate;
    $toDate   = $request->toDate;
    $data;

    if(!empty($fromDate) && !empty($fromDate))
    {
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", ">=", $fromDate)
          ->where("top_up_accounts.created_at", "<=", $toDate)
          ->get();
    }
    else if(!empty($fromDate) && empty($fromDate)){
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", ">=", $fromDate)
          ->get();
    }
    else if(empty($fromDate) && !empty($fromDate)){
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->where("top_up_accounts.created_at", "<=", $toDate)
          ->get();
    }
    else {
      $data = TopupAccounts::on(Auth::user()->company)->select('top_up_accounts.*', 'members_details.membersName as membersDetailsName')
          ->join('members_details', 'members_details.accountCardNumber', '=', 'top_up_accounts.accountCardNumber')
          ->get();
    }
    return $data;
  }
}
