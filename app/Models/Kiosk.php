<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kiosk extends Model
{
    //
    protected $fillable = [
        'KioskID',
        'CategoryID',
        'CategorySetID',
        'Breakfast',
        'Lunch',
        'Dinner',
    ];
}
