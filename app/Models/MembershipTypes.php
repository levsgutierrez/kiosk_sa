<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipTypes extends Model
{
  protected $fillable = [
      'membershipTypes', 'discountPercentage',
  ];
}
