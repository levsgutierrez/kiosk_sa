<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuCategory extends Model
{
    protected $fillable = [
    	'CategoryID'
    	,'CategoryName'
      	,'SeqNo'
      	,'Flag'
      	,'Status'
      	,'CreatedBy'
    ];
}
