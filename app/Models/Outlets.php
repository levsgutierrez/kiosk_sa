<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outlets extends Model
{
    protected $fillable = [
        'OutletName',
        'Location',
        'CreatedBy',
        'deleted',
    ];
}
