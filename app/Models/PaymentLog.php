<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
  protected $fillable = [
    'paymentLogId','paymentLogType', 'denominations', 'quantity', 'totalAmount', 'operator', 'created_at'
  ];
}
