<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class membersTradeLog extends Model
{
  protected $fillable = [
      'accountNumber','actionType', 'actionTypeId', 'amount', 'createdBy'
  ];
}
