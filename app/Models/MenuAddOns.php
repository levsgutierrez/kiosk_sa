<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuAddOns extends Model
{
    protected $fillable = [
    	'AddOns'
      ,'CategoryID'
      ,'ItemName'
      ,'Price'
      ,'Flag'
      ,'CreatedBy'
    ];
}
