<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSide extends Model
{
    protected $fillable = [
     	'SideID'
      	,'SideName'
     	,'SideMinQty'
        ,'SideMaxQty'
      	,'SerialCode'
      	,'SKU'
        ,'ItemName'
      	,'SidePrice'
      	,'MaxQty'
      	,'SeqID'
      	,'Flag'
  ];
}
