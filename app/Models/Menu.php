<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
      protected $fillable = [
      'SKU', 
      'ItemName', 
      'Desc','Unit', 
      'Portion', 
      'Price',
      'ImgNo', 
      'CategoryID', 
      'SubCategoryID', 
      'MenuGroup',
      'PrinterName',
      'CollectPlace', 
      'Price',
      'KitchenDesc', 
      'SafetyStock', 
      'Status',
      'ShopName', 
      'CanTakeOut', 
      'PackPrice',
      'NoDiscount', 
      'Department', 
      'BriefCode',
      'CreatedBy'
  ];
}
