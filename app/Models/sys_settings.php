<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sys_settings extends Model
{
     protected $fillable = [
        'Settings',
        'Title',
        'Value',
    ];
}
