<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class generalSettings extends Model
{
    protected $fillable = ['settingsName', 'valueDouble', 'valueString', 'updated_at'];
}
