<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopUpAccounts extends Model
{
  protected $fillable = [
    'accountCardNumber','topupAmount', 'giftAmount', 'paymentType', 'totalAmount', 'invoiceAmount', 'initialBalance',
    'topupId', 'topupDate'
];
}
