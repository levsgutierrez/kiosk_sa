<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuCategorySet extends Model
{
    protected $fillable = [
    	'ParentCode'
      	,'ChildCode'
      	,'SeqNo'
      	,'CreatedBy'
    ];
}
