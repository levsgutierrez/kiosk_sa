<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
  protected $fillable = [
      'discountName','discountType', 'SKU', 'categoryId', 'minimumAmount', 'discountAmount', 'discountPercentage', 'beginDate', 'endDate','beginTime', 'endTime',
      'week'
  ];
}
