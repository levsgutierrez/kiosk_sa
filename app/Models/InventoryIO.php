<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryIO extends Model
{
  protected $fillable = [
      'itemCode','itemName', 'totalQuantity', 'remarks','inventoryId','inventoryType', 'price', 'actionName', 'shopName', 'outletId', 'operator'
  ];
}
