<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembersDetails extends Model
{
  protected $fillable = [
      'accountNumber','accountCardNumber', 'membershipTypes', 'membersName', 'nric',
      'contactNumber', 'registeredDate', 'balance', 'cancellationDate', 'deposit', 'status', 'membersPoints', 'createdBy'
  ];
}
