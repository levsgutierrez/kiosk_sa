<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sys_userRights extends Model
{
    protected $fillable = [
        'userID',
        'moduleCode',
        'actionName',
        'active',
    ];
}
