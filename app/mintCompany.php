<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class mintCompany extends Authenticatable
{
	use Notifiable;

    protected $connection = 'mint_companies';
    protected $guard = 'admin';
    protected $table = 'mint_companies';
    

    protected $fillable = [
        'name', 'email', 'password', 'company',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
